
FROM cs8-jdk1.8:latest

RUN mkdir -p /opt/opencmp /etc/opencmp
RUN ln -svf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
COPY target/opencmp-server-0.0.1-SNAPSHOT.jar /opt/opencmp/opencmp-server.jar
COPY configs/opencmp.conf /etc/opencmp/opencmp.conf

WORKDIR /opt/opencmp
EXPOSE 9099
ENV JAVA_OPTS="-Xmx1024m -Xms1024m -Xmn512m  -XshowSettings:vm"
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Dfile.encoding=UTF8 -Djava.security.egd=file:/dev/./urandom -jar opencmp-server.jar"]

