package com.massclouds.opencmp.dto;

public class CategoryDTO {
	
	private String href;
	
	private String id;
	
	private String description;
	
	private String name;
	
	private String fullName;
	
	private boolean read_only;
	
	private String parent_id;
	
	private Boolean businessTag;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isRead_only() {
		return read_only;
	}

	public void setRead_only(boolean read_only) {
		this.read_only = read_only;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public Boolean getBusinessTag() {
		return businessTag;
	}

	public void setBusinessTag(Boolean businessTag) {
		this.businessTag = businessTag;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
}
