package com.massclouds.opencmp.dto;

public class StorageDTO {
	
	private String id;
	
	private String name;
	
	private String store_type;
	
	private String total_space;
	
	private String free_space;
	
	private String created_on;
	
	private String updated_on;
	
	private String storage_domain_type;
	
	private String status;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStore_type() {
		return store_type;
	}
	public void setStore_type(String store_type) {
		this.store_type = store_type;
	}
	public String getTotal_space() {
		return total_space;
	}
	public void setTotal_space(String total_space) {
		this.total_space = total_space;
	}
	public String getFree_space() {
		return free_space;
	}
	public void setFree_space(String free_space) {
		this.free_space = free_space;
	}
	public String getCreated_on() {
		return created_on;
	}
	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}
	public String getUpdated_on() {
		return updated_on;
	}
	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}
	public String getStorage_domain_type() {
		return storage_domain_type;
	}
	public void setStorage_domain_type(String storage_domain_type) {
		this.storage_domain_type = storage_domain_type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
