package com.massclouds.opencmp.dto;

public class ServiceTemplateCreateProvisionDTO{
	
	private String fqname;
	
	private String dialog_id;

	public String getFqname() {
		return fqname;
	}

	public void setFqname(String fqname) {
		this.fqname = fqname;
	}

	public String getDialog_id() {
		return dialog_id;
	}

	public void setDialog_id(String dialog_id) {
		this.dialog_id = dialog_id;
	}
	
}

