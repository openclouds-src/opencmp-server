package com.massclouds.opencmp.dto;

import java.util.Map;

public class DashboardPlatformCountDTO {
	
	/**
	 * 虚拟化平台数量
	 */
	private int vmpCount;
	
	/**
	 * 云平台数量
	 */
	private int cloudCount;
	
	/**
	 * 容器平台数量
	 */
	private int containerCount;
	
	/**
	 * 物理平台数量
	 */
	private int physicalCount;
	
	/**
	 * 每个平台的虚机总数
	 */
	private Map<String, Integer> vmsCountPerPlatform;
	
	/**
	 * 每个平台的vcpu总数
	 */
	private Map<String, Integer> vcpuCountPerPlatform;
	
	/**
	 * 每个平台的内存总数
	 */
	private Map<String, Double> memoryCountPerPlatform;
	
	/**
	 * 每个平台的存储总数
	 */
	private Map<String, Double> storageCountPerPlatform;
	/**
	 * 统计公有云平台虚机总数
	 */
	private	int vmsCountOfPrivateClouds;
	/**
	 * 统计私有云虚机总数
	 */
	private int vmsCountOfPublicClouds;
	/**
	 * 统计公有云vcpu总数
	 */
	private int vcpuCountOfPrivateClouds;
	/**
	 * 统计私有云vcpu总数
	 */
	private int vcpuCountOfPublicClouds;
	/**
	 * 统计公有云内存总数
	 */
	private double memoryCountOfPrivateClouds;
	/**
	 * 统计私有云内存总数
	 */
	private double memoryCountOfPublicClouds;
	/**
	 * 统计公有云存储总数
	 */
	private double storageCountOfPrivateClouds;
	/**
	 * 统计私有云存储总数
	 */
	private double storageCountOfPublicClouds;

	public int getVmpCount() {
		return vmpCount;
	}

	public void setVmpCount(int vmpCount) {
		this.vmpCount = vmpCount;
	}

	public int getCloudCount() {
		return cloudCount;
	}

	public void setCloudCount(int cloudCount) {
		this.cloudCount = cloudCount;
	}

	public int getContainerCount() {
		return containerCount;
	}

	public void setContainerCount(int containerCount) {
		this.containerCount = containerCount;
	}

	public int getPhysicalCount() {
		return physicalCount;
	}

	public void setPhysicalCount(int physicalCount) {
		this.physicalCount = physicalCount;
	}

	public Map<String, Integer> getVmsCountPerPlatform() {
		return vmsCountPerPlatform;
	}

	public void setVmsCountPerPlatform(Map<String, Integer> vmsCountPerPlatform) {
		this.vmsCountPerPlatform = vmsCountPerPlatform;
	}

	public Map<String, Integer> getVcpuCountPerPlatform() {
		return vcpuCountPerPlatform;
	}

	public void setVcpuCountPerPlatform(Map<String, Integer> vcpuCountPerPlatform) {
		this.vcpuCountPerPlatform = vcpuCountPerPlatform;
	}

	public Map<String, Double> getMemoryCountPerPlatform() {
		return memoryCountPerPlatform;
	}

	public void setMemoryCountPerPlatform(Map<String, Double> memoryCountPerPlatform) {
		this.memoryCountPerPlatform = memoryCountPerPlatform;
	}

	public Map<String, Double> getStorageCountPerPlatform() {
		return storageCountPerPlatform;
	}

	public void setStorageCountPerPlatform(Map<String, Double> storageCountPerPlatform) {
		this.storageCountPerPlatform = storageCountPerPlatform;
	}

	public int getVmsCountOfPrivateClouds() {
		return vmsCountOfPrivateClouds;
	}

	public void setVmsCountOfPrivateClouds(int vmsCountOfPrivateClouds) {
		this.vmsCountOfPrivateClouds = vmsCountOfPrivateClouds;
	}

	public int getVmsCountOfPublicClouds() {
		return vmsCountOfPublicClouds;
	}

	public void setVmsCountOfPublicClouds(int vmsCountOfPublicClouds) {
		this.vmsCountOfPublicClouds = vmsCountOfPublicClouds;
	}

	public int getVcpuCountOfPrivateClouds() {
		return vcpuCountOfPrivateClouds;
	}

	public void setVcpuCountOfPrivateClouds(int vcpuCountOfPrivateClouds) {
		this.vcpuCountOfPrivateClouds = vcpuCountOfPrivateClouds;
	}

	public int getVcpuCountOfPublicClouds() {
		return vcpuCountOfPublicClouds;
	}

	public void setVcpuCountOfPublicClouds(int vcpuCountOfPublicClouds) {
		this.vcpuCountOfPublicClouds = vcpuCountOfPublicClouds;
	}

	public double getMemoryCountOfPrivateClouds() {
		return memoryCountOfPrivateClouds;
	}

	public void setMemoryCountOfPrivateClouds(double memoryCountOfPrivateClouds) {
		this.memoryCountOfPrivateClouds = memoryCountOfPrivateClouds;
	}

	public double getMemoryCountOfPublicClouds() {
		return memoryCountOfPublicClouds;
	}

	public void setMemoryCountOfPublicClouds(double memoryCountOfPublicClouds) {
		this.memoryCountOfPublicClouds = memoryCountOfPublicClouds;
	}

	public double getStorageCountOfPrivateClouds() {
		return storageCountOfPrivateClouds;
	}

	public void setStorageCountOfPrivateClouds(double storageCountOfPrivateClouds) {
		this.storageCountOfPrivateClouds = storageCountOfPrivateClouds;
	}

	public double getStorageCountOfPublicClouds() {
		return storageCountOfPublicClouds;
	}

	public void setStorageCountOfPublicClouds(double storageCountOfPublicClouds) {
		this.storageCountOfPublicClouds = storageCountOfPublicClouds;
	}

}
