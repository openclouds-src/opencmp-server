package com.massclouds.opencmp.dto;

import java.util.List;

public class VmDetailsDTO extends VmDTO{
	
	private ClusterDTO ems_cluster;
	
	private StorageDTO storage;
	
	private List<VlanDTO> lans;
	
	private List<TagDTO> tags;
	
	private String assigned_tag_name;
	
	public ClusterDTO getEms_cluster() {
		return ems_cluster;
	}

	public void setEms_cluster(ClusterDTO ems_cluster) {
		this.ems_cluster = ems_cluster;
	}

	public StorageDTO getStorage() {
		return storage;
	}

	public void setStorage(StorageDTO storage) {
		this.storage = storage;
	}

	public List<VlanDTO> getLans() {
		return lans;
	}

	public void setLans(List<VlanDTO> lans) {
		this.lans = lans;
	}

	public List<TagDTO> getTags() {
		return tags;
	}

	public void setTags(List<TagDTO> tags) {
		this.tags = tags;
	}

	public String getAssigned_tag_name() {
		return assigned_tag_name;
	}

	public void setAssigned_tag_name(String assigned_tag_name) {
		this.assigned_tag_name = assigned_tag_name;
	}
	
}
