package com.massclouds.opencmp.dto;

import java.util.List;
import java.util.Map;

public class ContainerManagerDetailsDTO {
	
	private String id;
	
	private List<Map<String, Object>> container_nodes;
	
	private List<Map<String, Object>> container_projects;
	
	private List<Map<String, Object>> container_routes;
	
	private List<Map<String, Object>> container_templates;
	
	private List<Map<String, Object>> containers;
	
	private List<Map<String, Object>> container_groups;
	
	private List<Map<String, Object>> container_services;
	
	private List<Map<String, Object>> container_images;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Map<String, Object>> getContainer_nodes() {
		return container_nodes;
	}

	public void setContainer_nodes(List<Map<String, Object>> container_nodes) {
		this.container_nodes = container_nodes;
	}

	public List<Map<String, Object>> getContainer_projects() {
		return container_projects;
	}

	public void setContainer_projects(List<Map<String, Object>> container_projects) {
		this.container_projects = container_projects;
	}

	public List<Map<String, Object>> getContainer_routes() {
		return container_routes;
	}

	public void setContainer_routes(List<Map<String, Object>> container_routes) {
		this.container_routes = container_routes;
	}

	public List<Map<String, Object>> getContainer_templates() {
		return container_templates;
	}

	public void setContainer_templates(List<Map<String, Object>> container_templates) {
		this.container_templates = container_templates;
	}

	public List<Map<String, Object>> getContainers() {
		return containers;
	}

	public void setContainers(List<Map<String, Object>> containers) {
		this.containers = containers;
	}

	public List<Map<String, Object>> getContainer_groups() {
		return container_groups;
	}

	public void setContainer_groups(List<Map<String, Object>> container_groups) {
		this.container_groups = container_groups;
	}

	public List<Map<String, Object>> getContainer_services() {
		return container_services;
	}

	public void setContainer_services(List<Map<String, Object>> container_services) {
		this.container_services = container_services;
	}

	public List<Map<String, Object>> getContainer_images() {
		return container_images;
	}

	public void setContainer_images(List<Map<String, Object>> container_images) {
		this.container_images = container_images;
	}

}
