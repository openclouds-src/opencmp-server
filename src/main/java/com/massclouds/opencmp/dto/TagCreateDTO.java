package com.massclouds.opencmp.dto;

public class TagCreateDTO {
	
	private String id;
	
    private String categoryHref;
	
	private String description;
	
	private String name;
	
	private Boolean businessTag;

	public String getCategoryHref() {
		return categoryHref;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCategoryHref(String categoryHref) {
		this.categoryHref = categoryHref;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getBusinessTag() {
		return businessTag;
	}

	public void setBusinessTag(Boolean businessTag) {
		this.businessTag = businessTag;
	}

}
