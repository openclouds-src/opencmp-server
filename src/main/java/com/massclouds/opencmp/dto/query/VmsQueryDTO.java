package com.massclouds.opencmp.dto.query;

import com.massclouds.opencmp.utils.resultdto.QueryDTO;

public class VmsQueryDTO extends QueryDTO{
	
	private static final long serialVersionUID = 1L;
	
	private String clusterName;
	
	private String providerId;
	
	private String providerType;
	
	private String tagId;
	
	private String tenantId;

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
}
