package com.massclouds.opencmp.dto.query;

import com.massclouds.opencmp.utils.resultdto.QueryDTO;

public class ServiceTemplateQueryDTO extends QueryDTO{
	
	private static final long serialVersionUID = 1L;
	
	private String catalogId;

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public ServiceTemplateQueryDTO(Integer page, Integer pageSize, String criteria) {
		super(page, pageSize, criteria);
	}
	
}
