package com.massclouds.opencmp.dto.query;

import com.massclouds.opencmp.utils.resultdto.QueryDTO;

public class ServerQueryDTO extends QueryDTO{
	
	private static final long serialVersionUID = 1L;
	
	private String createdDate;
	
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

}
