package com.massclouds.opencmp.dto.query;

import com.massclouds.opencmp.utils.resultdto.QueryDTO;

public class ReportsQueryDTO extends QueryDTO{
	
	private static final long serialVersionUID = 1L;
	
	private String templateId;
	
	private String createDate;

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

}
