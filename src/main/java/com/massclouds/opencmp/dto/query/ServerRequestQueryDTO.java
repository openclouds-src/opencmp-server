package com.massclouds.opencmp.dto.query;

import com.massclouds.opencmp.utils.resultdto.QueryDTO;

public class ServerRequestQueryDTO extends QueryDTO{
	
	private static final long serialVersionUID = 1L;
	
	private String approvalState;
	
	private String requestType;
	
	private String createdDate;
	
	private String requesterName;
	
	private String reason;

	public String getApprovalState() {
		return approvalState;
	}

	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
