package com.massclouds.opencmp.dto;

public class ServiceTemplateCreateParamsDTO {
	
	private String name;
	
	private String service_type;
	
	private String prov_type;
	
	private String display;
	
	private String description;
	
	private String service_template_catalog_id;
	
	private ServiceTemplateCreateConfigInfoDTO config_info;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getProv_type() {
		return prov_type;
	}

	public void setProv_type(String prov_type) {
		this.prov_type = prov_type;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getService_template_catalog_id() {
		return service_template_catalog_id;
	}

	public void setService_template_catalog_id(String service_template_catalog_id) {
		this.service_template_catalog_id = service_template_catalog_id;
	}

	public ServiceTemplateCreateConfigInfoDTO getConfig_info() {
		return config_info;
	}

	public void setConfig_info(ServiceTemplateCreateConfigInfoDTO config_info) {
		this.config_info = config_info;
	}
	
}
