package com.massclouds.opencmp.dto;

public class UserGroupCreateDTO {
	
	private String id;
	
	private String name;
	
	private String roleId;
	
	private String tenantId;
	
	private String tagName;
	
	public UserGroupCreateDTO() {
		super();
	}

	public UserGroupCreateDTO(String name, String roleId, String tenantId) {
		this.name = name;
		this.roleId = roleId;
		this.tenantId = tenantId;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

}
