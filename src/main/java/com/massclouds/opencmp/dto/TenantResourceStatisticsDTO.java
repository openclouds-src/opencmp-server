package com.massclouds.opencmp.dto;

public class TenantResourceStatisticsDTO {
	
	private double cpuTotal;
	private double cpuUsed;
	private double cpuAvailable;
	
	private double memoryTotal;
	private double memoryUsed;
	private double memoryAvailable;
	
	private double storageTotal;
	private double storageUsed;
	private double storageAvailable;
	
	private double vmTotal;
	private double vmUsed;
	private double vmAvailable;
	public double getCpuTotal() {
		return cpuTotal;
	}
	public void setCpuTotal(double cpuTotal) {
		this.cpuTotal = cpuTotal;
	}
	public double getCpuUsed() {
		return cpuUsed;
	}
	public void setCpuUsed(double cpuUsed) {
		this.cpuUsed = cpuUsed;
	}
	public double getCpuAvailable() {
		return cpuAvailable;
	}
	public void setCpuAvailable(double cpuAvailable) {
		this.cpuAvailable = cpuAvailable;
	}
	public double getMemoryTotal() {
		return memoryTotal;
	}
	public void setMemoryTotal(double memoryTotal) {
		this.memoryTotal = memoryTotal;
	}
	public double getMemoryUsed() {
		return memoryUsed;
	}
	public void setMemoryUsed(double memoryUsed) {
		this.memoryUsed = memoryUsed;
	}
	public double getMemoryAvailable() {
		return memoryAvailable;
	}
	public void setMemoryAvailable(double memoryAvailable) {
		this.memoryAvailable = memoryAvailable;
	}
	public double getStorageTotal() {
		return storageTotal;
	}
	public void setStorageTotal(double storageTotal) {
		this.storageTotal = storageTotal;
	}
	public double getStorageUsed() {
		return storageUsed;
	}
	public void setStorageUsed(double storageUsed) {
		this.storageUsed = storageUsed;
	}
	public double getStorageAvailable() {
		return storageAvailable;
	}
	public void setStorageAvailable(double storageAvailable) {
		this.storageAvailable = storageAvailable;
	}
	public double getVmTotal() {
		return vmTotal;
	}
	public void setVmTotal(double vmTotal) {
		this.vmTotal = vmTotal;
	}
	public double getVmUsed() {
		return vmUsed;
	}
	public void setVmUsed(double vmUsed) {
		this.vmUsed = vmUsed;
	}
	public double getVmAvailable() {
		return vmAvailable;
	}
	public void setVmAvailable(double vmAvailable) {
		this.vmAvailable = vmAvailable;
	}
	
}
