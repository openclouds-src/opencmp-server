package com.massclouds.opencmp.dto;

import java.util.Map;

public class ServiceOptionsDTO {
	
	private Map<String, Object> config_info;
	
	private Dialog dialog;

	public Map<String, Object> getConfig_info() {
		return config_info;
	}

	public void setConfig_info(Map<String, Object> config_info) {
		this.config_info = config_info;
	}

	public Dialog getDialog() {
		return dialog;
	}

	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}
	
	public static class Dialog {
		
		private String dialog_service_name;
		
		private String dialog_vm_name;
		
		private String dialog_reason;
		
		private String dialog_number_of_sockets;
		
		private String dialog_cores_per_socket;
		
		private String dialog_vm_memory;

		public String getDialog_service_name() {
			return dialog_service_name;
		}

		public void setDialog_service_name(String dialog_service_name) {
			this.dialog_service_name = dialog_service_name;
		}

		public String getDialog_vm_name() {
			return dialog_vm_name;
		}

		public void setDialog_vm_name(String dialog_vm_name) {
			this.dialog_vm_name = dialog_vm_name;
		}

		public String getDialog_reason() {
			return dialog_reason;
		}

		public void setDialog_reason(String dialog_reason) {
			this.dialog_reason = dialog_reason;
		}

		public String getDialog_number_of_sockets() {
			return dialog_number_of_sockets;
		}

		public void setDialog_number_of_sockets(String dialog_number_of_sockets) {
			this.dialog_number_of_sockets = dialog_number_of_sockets;
		}

		public String getDialog_cores_per_socket() {
			return dialog_cores_per_socket;
		}

		public void setDialog_cores_per_socket(String dialog_cores_per_socket) {
			this.dialog_cores_per_socket = dialog_cores_per_socket;
		}

		public String getDialog_vm_memory() {
			return dialog_vm_memory;
		}

		public void setDialog_vm_memory(String dialog_vm_memory) {
			this.dialog_vm_memory = dialog_vm_memory;
		}
		
	}

}
