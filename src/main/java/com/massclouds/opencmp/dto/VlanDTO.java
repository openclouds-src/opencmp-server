package com.massclouds.opencmp.dto;

public class VlanDTO {
	
	private String href;
	
	private String id;
	
	private String name;
	
	private String created_on;
	
	private String updated_on;
	
	private String uid_ems;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getUid_ems() {
		return uid_ems;
	}

	public void setUid_ems(String uid_ems) {
		this.uid_ems = uid_ems;
	}

}
