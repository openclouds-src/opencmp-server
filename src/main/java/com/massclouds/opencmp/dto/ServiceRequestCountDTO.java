package com.massclouds.opencmp.dto;

public class ServiceRequestCountDTO {
	
	private int allCount;
	private int pendingApproval;
	private int approved;
	private int denied;
    public int getAllCount() {
        return allCount;
    }
    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }
    public int getPendingApproval() {
        return pendingApproval;
    }
    public void setPendingApproval(int pendingApproval) {
        this.pendingApproval = pendingApproval;
    }
    public int getApproved() {
        return approved;
    }
    public void setApproved(int approved) {
        this.approved = approved;
    }
    public int getDenied() {
        return denied;
    }
    public void setDenied(int denied) {
        this.denied = denied;
    }
 
}
