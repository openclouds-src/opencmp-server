package com.massclouds.opencmp.dto;

import java.util.Map;

public class DashboardTerminalStatisticsByTypeDTO {
	
	private Map<String, Integer> terminalCountPerType;
	
	private int totalTerminal;

	public Map<String, Integer> getTerminalCountPerType() {
		return terminalCountPerType;
	}

	public void setTerminalCountPerType(Map<String, Integer> terminalCountPerType) {
		this.terminalCountPerType = terminalCountPerType;
	}

	public int getTotalTerminal() {
		return totalTerminal;
	}

	public void setTotalTerminal(int totalTerminal) {
		this.totalTerminal = totalTerminal;
	}

}
