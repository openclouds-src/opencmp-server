package com.massclouds.opencmp.dto;

public class InstanceActionDTO {
	
	private String action;
	
	private String id;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
