package com.massclouds.opencmp.dto;

import java.util.Map;

public class ReportDTO {
	
	public String href;
	
	public String id;
	
	public String name;
	
	public String miq_report_id;
	
	public String created_on;
	
	public String miq_group_id;
	
	public Map<String, String> miq_task;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMiq_report_id() {
		return miq_report_id;
	}

	public void setMiq_report_id(String miq_report_id) {
		this.miq_report_id = miq_report_id;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getMiq_group_id() {
		return miq_group_id;
	}

	public void setMiq_group_id(String miq_group_id) {
		this.miq_group_id = miq_group_id;
	}

	public Map<String, String> getMiq_task() {
		return miq_task;
	}

	public void setMiq_task(Map<String, String> miq_task) {
		this.miq_task = miq_task;
	}

}
