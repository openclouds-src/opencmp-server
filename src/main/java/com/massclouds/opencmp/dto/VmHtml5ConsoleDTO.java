package com.massclouds.opencmp.dto;

public class VmHtml5ConsoleDTO {
	
	public String url;
	
	public String secret;
	
	public String proto;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getProto() {
		return proto;
	}

	public void setProto(String proto) {
		this.proto = proto;
	}
	
}
