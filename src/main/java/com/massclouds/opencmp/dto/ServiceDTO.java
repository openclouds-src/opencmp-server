package com.massclouds.opencmp.dto;

public class ServiceDTO {
	
	private String id;
	
	private String name;
	
	private String description;
	
	private String guid;
	
	private String service_template_id;
	
	private String created_at;
	
	private String updated_at;
	
	private String lifecycle_state;
	
	private boolean retired;
	
	private String retires_on;
	
	private String retirement_state;
	
	private ServiceOptionsDTO options;
	
	private ServiceTemplateConfig service_template;
	
	@SuppressWarnings("unused")
	private static class ServiceTemplateConfig{
		private ServiceTemplateCreateConfigInfoDTO config_info;
		public ServiceTemplateCreateConfigInfoDTO getConfig_info() {
			return config_info;
		}
		public void setConfig_info(ServiceTemplateCreateConfigInfoDTO config_info) {
			this.config_info = config_info;
		}
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getService_template_id() {
		return service_template_id;
	}

	public void setService_template_id(String service_template_id) {
		this.service_template_id = service_template_id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getLifecycle_state() {
		return lifecycle_state;
	}

	public void setLifecycle_state(String lifecycle_state) {
		this.lifecycle_state = lifecycle_state;
	}

	public ServiceOptionsDTO getOptions() {
		return options;
	}

	public void setOptions(ServiceOptionsDTO options) {
		this.options = options;
	}

	public ServiceTemplateConfig getService_template() {
		return service_template;
	}

	public void setService_template(ServiceTemplateConfig service_template) {
		this.service_template = service_template;
	}

	public boolean isRetired() {
		return retired;
	}

	public void setRetired(boolean retired) {
		this.retired = retired;
	}

	public String getRetires_on() {
		return retires_on;
	}

	public void setRetires_on(String retires_on) {
		this.retires_on = retires_on;
	}

	public String getRetirement_state() {
		return retirement_state;
	}

	public void setRetirement_state(String retirement_state) {
		this.retirement_state = retirement_state;
	}

}
