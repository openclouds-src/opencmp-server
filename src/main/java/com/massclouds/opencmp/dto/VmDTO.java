package com.massclouds.opencmp.dto;

import java.util.List;
import java.util.Map;

public class VmDTO {
	
	public String href;
	
	public String id;
	
	public String name;
	
	public String vendor;
	
	public String description;
	
	public String created_on;
	
	public String updated_on;
	
	public String guid;
	
	public String uid_ems;
	
	public String service_type;
	
	public String tools_status;
	
	public String power_state;
	
	public String raw_power_state;
	
	public String connection_state;
	
	public String host_name;
	
	public String ems_cluster_name;
	
	public int ram_size;
	
	public int num_hard_disks;
	
	public int num_cpu;
	
	public int cpu_total_cores;
	
	public int cpu_cores_per_socket;
	
	public String used_storage;
	
	public String total_disks_size;
	
	public String memory_reserve;
	
	public String memory_limit;
	
	public List<String> ipaddresses;
	
	public String evm_owner_id;
	
	public String evm_owner_name;
	
	/**
	 * 虚机所属平台的信息对象
	 */
	public Map<String, Object> ext_management_system;
	
	private OSInfo operating_system;
	
	public static class OSInfo{
		private String product_name;
		public String getProduct_name() {
			return product_name;
		}
		public void setProduct_name(String product_name) {
			this.product_name = product_name;
		}
	}
	
	public String getUid_ems() {
		return uid_ems;
	}

	public void setUid_ems(String uid_ems) {
		this.uid_ems = uid_ems;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getTools_status() {
		return tools_status;
	}

	public void setTools_status(String tools_status) {
		this.tools_status = tools_status;
	}

	public String getPower_state() {
		return power_state;
	}

	public void setPower_state(String power_state) {
		this.power_state = power_state;
	}

	public String getConnection_state() {
		return connection_state;
	}

	public void setConnection_state(String connection_state) {
		this.connection_state = connection_state;
	}

	public String getMemory_reserve() {
		return memory_reserve;
	}

	public void setMemory_reserve(String memory_reserve) {
		this.memory_reserve = memory_reserve;
	}

	public String getMemory_limit() {
		return memory_limit;
	}

	public void setMemory_limit(String memory_limit) {
		this.memory_limit = memory_limit;
	}

	public Map<String, Object> getExt_management_system() {
		return ext_management_system;
	}

	public void setExt_management_system(Map<String, Object> ext_management_system) {
		this.ext_management_system = ext_management_system;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getHost_name() {
		return host_name;
	}

	public void setHost_name(String host_name) {
		this.host_name = host_name;
	}

	public String getEms_cluster_name() {
		return ems_cluster_name;
	}

	public void setEms_cluster_name(String ems_cluster_name) {
		this.ems_cluster_name = ems_cluster_name;
	}

	public int getRam_size() {
		return ram_size;
	}

	public void setRam_size(int ram_size) {
		this.ram_size = ram_size;
	}

	public int getNum_hard_disks() {
		return num_hard_disks;
	}

	public void setNum_hard_disks(int num_hard_disks) {
		this.num_hard_disks = num_hard_disks;
	}

	public int getNum_cpu() {
		return num_cpu;
	}

	public void setNum_cpu(int num_cpu) {
		this.num_cpu = num_cpu;
	}

	public int getCpu_total_cores() {
		return cpu_total_cores;
	}

	public void setCpu_total_cores(int cpu_total_cores) {
		this.cpu_total_cores = cpu_total_cores;
	}

	public int getCpu_cores_per_socket() {
		return cpu_cores_per_socket;
	}

	public void setCpu_cores_per_socket(int cpu_cores_per_socket) {
		this.cpu_cores_per_socket = cpu_cores_per_socket;
	}

	public String getUsed_storage() {
		return used_storage;
	}

	public void setUsed_storage(String used_storage) {
		this.used_storage = used_storage;
	}

	public String getTotal_disks_size() {
		return total_disks_size;
	}

	public void setTotal_disks_size(String total_disks_size) {
		this.total_disks_size = total_disks_size;
	}

	public List<String> getIpaddresses() {
		return ipaddresses;
	}

	public void setIpaddresses(List<String> ipaddresses) {
		this.ipaddresses = ipaddresses;
	}

	public String getRaw_power_state() {
		return raw_power_state;
	}

	public void setRaw_power_state(String raw_power_state) {
		this.raw_power_state = raw_power_state;
	}

	public OSInfo getOperating_system() {
		return operating_system;
	}

	public void setOperating_system(OSInfo operating_system) {
		this.operating_system = operating_system;
	}

	public String getEvm_owner_id() {
		return evm_owner_id;
	}

	public void setEvm_owner_id(String evm_owner_id) {
		this.evm_owner_id = evm_owner_id;
	}

	public String getEvm_owner_name() {
		return evm_owner_name;
	}

	public void setEvm_owner_name(String evm_owner_name) {
		this.evm_owner_name = evm_owner_name;
	}

}
