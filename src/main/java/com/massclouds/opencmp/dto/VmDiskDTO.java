package com.massclouds.opencmp.dto;

public class VmDiskDTO {
	private String id;
	private String device_name;
	private String filename;
	private String disk_type;
	private String size;
	private String size_on_disk;
	private String created_on;
	private String updated_on;
	private boolean bootable;
	
	private String hardware_id;
	private String mode;
	private String controller_type;
	private String storage_id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDevice_name() {
		return device_name;
	}
	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDisk_type() {
		return disk_type;
	}
	public void setDisk_type(String disk_type) {
		this.disk_type = disk_type;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getSize_on_disk() {
		return size_on_disk;
	}
	public void setSize_on_disk(String size_on_disk) {
		this.size_on_disk = size_on_disk;
	}
	public String getCreated_on() {
		return created_on;
	}
	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}
	public String getUpdated_on() {
		return updated_on;
	}
	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}
	public boolean isBootable() {
		return bootable;
	}
	public void setBootable(boolean bootable) {
		this.bootable = bootable;
	}
	public String getHardware_id() {
		return hardware_id;
	}
	public void setHardware_id(String hardware_id) {
		this.hardware_id = hardware_id;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getController_type() {
		return controller_type;
	}
	public void setController_type(String controller_type) {
		this.controller_type = controller_type;
	}
	public String getStorage_id() {
		return storage_id;
	}
	public void setStorage_id(String storage_id) {
		this.storage_id = storage_id;
	}
    
}
