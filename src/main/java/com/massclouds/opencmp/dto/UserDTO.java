package com.massclouds.opencmp.dto;

import java.util.List;

public class UserDTO {
	
	private String href;
	
	private String id;
	
	private String name;
	
	private String email;
	
	private String icon;
	
	private String created_on;
	
	private String updated_on;
	
	private String userid;
	
	private String lastlogon;
	
	private String lastlogoff;
	
	private String current_group_id;
	
	private String roleId;
	
	private String roleName;
	
	private List<UserGroupDTO> miq_groups;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getLastlogon() {
		return lastlogon;
	}

	public void setLastlogon(String lastlogon) {
		this.lastlogon = lastlogon;
	}

	public String getLastlogoff() {
		return lastlogoff;
	}

	public void setLastlogoff(String lastlogoff) {
		this.lastlogoff = lastlogoff;
	}

	public String getCurrent_group_id() {
		return current_group_id;
	}

	public void setCurrent_group_id(String current_group_id) {
		this.current_group_id = current_group_id;
	}

	public List<UserGroupDTO> getMiq_groups() {
		return miq_groups;
	}

	public void setMiq_groups(List<UserGroupDTO> miq_groups) {
		this.miq_groups = miq_groups;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}
