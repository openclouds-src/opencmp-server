package com.massclouds.opencmp.dto;

/***
 * 
 */
public class InstanceResourceOfCloudPlatformDTO {

	private int totalCpus;
	
	private double totalMemory; //MB
	
	private double totalDisk; //KB

	public int getTotalCpus() {
		return totalCpus;
	}

	public void setTotalCpus(int totalCpus) {
		this.totalCpus = totalCpus;
	}

	public double getTotalMemory() {
		return totalMemory;
	}

	public void setTotalMemory(double totalMemory) {
		this.totalMemory = totalMemory;
	}

	public double getTotalDisk() {
		return totalDisk;
	}

	public void setTotalDisk(double totalDisk) {
		this.totalDisk = totalDisk;
	}

}
