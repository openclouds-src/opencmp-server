package com.massclouds.opencmp.dto;

public class PictureDTO {
	
	private String id;
	
	private String md5;
	
	private String image_href;
	
	private String href;
	
	/*
	 * 要添加logo的资源的id
	 */
	private String resourceId;
	
	/*
	 * 要添加logo的资源的类型
	 */
	private String resourceType;
	
	/*
	 * logo 扩展名，png、jpg
	 */
	private String extension;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getImage_href() {
		return image_href;
	}

	public void setImage_href(String image_href) {
		this.image_href = image_href;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
}
