package com.massclouds.opencmp.dto;

import java.util.Map;

public class DashboardStatisticsByTypeDTO {
	
	private Map<String, Integer> vmCountPerOS;
	
	private Map<String, Integer> cpuArch;

	public Map<String, Integer> getVmCountPerOS() {
		return vmCountPerOS;
	}

	public void setVmCountPerOS(Map<String, Integer> vmCountPerOS) {
		this.vmCountPerOS = vmCountPerOS;
	}

	public Map<String, Integer> getCpuArch() {
		return cpuArch;
	}

	public void setCpuArch(Map<String, Integer> cpuArch) {
		this.cpuArch = cpuArch;
	}

}
