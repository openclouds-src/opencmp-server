package com.massclouds.opencmp.dto;

import java.util.List;

public class TenantsTreeDTO extends TenantsDTO{
	
	private List<TenantsTreeDTO> children;

	public List<TenantsTreeDTO> getChildren() {
		return children;
	}

	public void setChildren(List<TenantsTreeDTO> children) {
		this.children = children;
	}
	
}
