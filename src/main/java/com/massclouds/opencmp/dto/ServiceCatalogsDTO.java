package com.massclouds.opencmp.dto;

import java.util.List;

public class ServiceCatalogsDTO {
	
	private String href;
	
	private String id;
	
	private String name;
	
	private String description;
	
	private List<ServiceTemplatesDTO> service_templates;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ServiceTemplatesDTO> getService_templates() {
		return service_templates;
	}

	public void setService_templates(List<ServiceTemplatesDTO> service_templates) {
		this.service_templates = service_templates;
	}
	

}
