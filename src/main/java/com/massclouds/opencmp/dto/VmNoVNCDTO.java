package com.massclouds.opencmp.dto;

public class VmNoVNCDTO {
	
	public String href;
	
	public String id;
	
	public String name;
	
	/**
	 * VNC端口
	 */
	public String vnc_port;
	
	/**
	 * VNC  ticket
	 */
	public String vnc_ticket;
	
	/**
	 * VNC  代理ticket
	 */
	public String vnc_proxyticket;
	
	/**
	 * 虚机所属平台的ip
	 */
	public String provider_address;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getVnc_port() {
		return vnc_port;
	}

	public void setVnc_port(String vnc_port) {
		this.vnc_port = vnc_port;
	}

	public String getVnc_ticket() {
		return vnc_ticket;
	}

	public void setVnc_ticket(String vnc_ticket) {
		this.vnc_ticket = vnc_ticket;
	}

	public String getVnc_proxyticket() {
		return vnc_proxyticket;
	}

	public void setVnc_proxyticket(String vnc_proxyticket) {
		this.vnc_proxyticket = vnc_proxyticket;
	}

	public String getProvider_address() {
		return provider_address;
	}

	public void setProvider_address(String provider_address) {
		this.provider_address = provider_address;
	}

}
