package com.massclouds.opencmp.dto;

import java.util.List;
import java.util.Map;

public class TenantsDTO {
	
	public String href;
	
	public String id;
	
	public String name;
	
	public String description;
	
	public String ancestry;
	
	public String default_miq_group_id;
	
	public List<UserGroupDTO> miq_groups;
	
	public List<TagDTO> tags;
	
	private Map<Object, Object> combined_quotas;
	

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDefault_miq_group_id() {
		return default_miq_group_id;
	}

	public void setDefault_miq_group_id(String default_miq_group_id) {
		this.default_miq_group_id = default_miq_group_id;
	}

	public String getAncestry() {
		return ancestry;
	}

	public void setAncestry(String ancestry) {
		this.ancestry = ancestry;
	}

	public List<UserGroupDTO> getMiq_groups() {
		return miq_groups;
	}

	public void setMiq_groups(List<UserGroupDTO> miq_groups) {
		this.miq_groups = miq_groups;
	}

	public Map<Object, Object> getCombined_quotas() {
		return combined_quotas;
	}

	public void setCombined_quotas(Map<Object, Object> combined_quotas) {
		this.combined_quotas = combined_quotas;
	}

	public List<TagDTO> getTags() {
		return tags;
	}

	public void setTags(List<TagDTO> tags) {
		this.tags = tags;
	}
	
}
