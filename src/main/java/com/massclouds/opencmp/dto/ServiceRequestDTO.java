package com.massclouds.opencmp.dto;

public class ServiceRequestDTO {
	
	private String id;
	
	private String description;
	
	private String approval_state;
	
	private String type;
	
	private String created_on;
	
	private String updated_on;
	
	private String fulfilled_on;
	
	private String requester_id;
	
	private String requester_name;
	
	private String request_type;
	
	private String request_state;
	
	private String message;
	
	private String status;
	
	private String userid;
	
	private String source_id;
	
	private String source_type;
	
	private String reason;
	
	private String v_approved_by;
	
	private ServiceOptionsDTO options;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getApproval_state() {
		return approval_state;
	}

	public void setApproval_state(String approval_state) {
		this.approval_state = approval_state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getFulfilled_on() {
		return fulfilled_on;
	}

	public void setFulfilled_on(String fulfilled_on) {
		this.fulfilled_on = fulfilled_on;
	}

	public String getRequester_id() {
		return requester_id;
	}

	public void setRequester_id(String requester_id) {
		this.requester_id = requester_id;
	}

	public String getRequester_name() {
		return requester_name;
	}

	public void setRequester_name(String requester_name) {
		this.requester_name = requester_name;
	}

	public String getRequest_type() {
		return request_type;
	}

	public void setRequest_type(String request_type) {
		this.request_type = request_type;
	}

	public String getRequest_state() {
		return request_state;
	}

	public void setRequest_state(String request_state) {
		this.request_state = request_state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getSource_id() {
		return source_id;
	}

	public void setSource_id(String source_id) {
		this.source_id = source_id;
	}

	public String getSource_type() {
		return source_type;
	}

	public void setSource_type(String source_type) {
		this.source_type = source_type;
	}

	public ServiceOptionsDTO getOptions() {
		return options;
	}

	public void setOptions(ServiceOptionsDTO options) {
		this.options = options;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getV_approved_by() {
		return v_approved_by;
	}

	public void setV_approved_by(String v_approved_by) {
		this.v_approved_by = v_approved_by;
	}
	
}
