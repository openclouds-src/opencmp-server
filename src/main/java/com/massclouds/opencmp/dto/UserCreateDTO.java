package com.massclouds.opencmp.dto;

public class UserCreateDTO {
	
	private String id;
	
	private String name;
	
	private String loginName;
	
	private String password;
	
	private String groupId;
	
	public UserCreateDTO() {
		super();
	}
	
	public UserCreateDTO(String name, String loginName, String password, String groupId) {
		this.name = name;
		this.loginName = loginName;
		this.password = password;
		this.groupId = groupId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
}
