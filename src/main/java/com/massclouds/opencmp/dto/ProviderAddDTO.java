package com.massclouds.opencmp.dto;

public class ProviderAddDTO {
	
	/************公共参数****************/
	private String type;
	
	private String typeName;
	
	private String name;
	
    private String hostname;
	
	private String ipaddress;
	
	private int port;
	
	/************ovirt平台参数****************/
	
	private String certificateAuthority;
	
	private String userName;
	
	private String userPassword;
	
	private String zoneId;
	
	private String zoneHref;
	
	private String historyDBPassword;
	
	private Boolean isVMP;
	
	private String syncUserName;
	
	private String syncPassword;
	
	/************OpenStack平台参数****************/
	
	private String providerRegion;
	
	private String uidEms;
	
	/************VMWare平台参数****************/
	private int vncPortStart;
	
	private int vncPortEnd;
	
	private String consoleUserName;
	
	private String consoleUserPassword;
	
	/************K8S平台参数****************/
	private String authKey;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getCertificateAuthority() {
		return certificateAuthority;
	}

	public void setCertificateAuthority(String certificateAuthority) {
		this.certificateAuthority = certificateAuthority;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneHref() {
		return zoneHref;
	}

	public void setZoneHref(String zoneHref) {
		this.zoneHref = zoneHref;
	}

	public String getHistoryDBPassword() {
		return historyDBPassword;
	}

	public void setHistoryDBPassword(String historyDBPassword) {
		this.historyDBPassword = historyDBPassword;
	}

	public String getProviderRegion() {
		return providerRegion;
	}

	public void setProviderRegion(String providerRegion) {
		this.providerRegion = providerRegion;
	}

	public String getUidEms() {
		return uidEms;
	}

	public void setUidEms(String uidEms) {
		this.uidEms = uidEms;
	}

	public int getVncPortStart() {
		return vncPortStart;
	}

	public void setVncPortStart(int vncPortStart) {
		this.vncPortStart = vncPortStart;
	}

	public int getVncPortEnd() {
		return vncPortEnd;
	}

	public void setVncPortEnd(int vncPortEnd) {
		this.vncPortEnd = vncPortEnd;
	}

	public String getConsoleUserName() {
		return consoleUserName;
	}

	public void setConsoleUserName(String consoleUserName) {
		this.consoleUserName = consoleUserName;
	}

	public String getConsoleUserPassword() {
		return consoleUserPassword;
	}

	public void setConsoleUserPassword(String consoleUserPassword) {
		this.consoleUserPassword = consoleUserPassword;
	}

	public boolean isVMP() {
		return isVMP;
	}

	public void setVMP(boolean isVMP) {
		this.isVMP = isVMP;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public Boolean getIsVMP() {
		return isVMP;
	}

	public void setIsVMP(Boolean isVMP) {
		this.isVMP = isVMP;
	}

	public String getSyncUserName() {
		return syncUserName;
	}

	public void setSyncUserName(String syncUserName) {
		this.syncUserName = syncUserName;
	}

	public String getSyncPassword() {
		return syncPassword;
	}

	public void setSyncPassword(String syncPassword) {
		this.syncPassword = syncPassword;
	}
	
}
