package com.massclouds.opencmp.dto;

public class DashboardStatisticsOnlineDTO {
	
	private int vmCountOnline;
	
	private int userCountOnline;

	public int getVmCountOnline() {
		return vmCountOnline;
	}

	public void setVmCountOnline(int vmCountOnline) {
		this.vmCountOnline = vmCountOnline;
	}

	public int getUserCountOnline() {
		return userCountOnline;
	}

	public void setUserCountOnline(int userCountOnline) {
		this.userCountOnline = userCountOnline;
	}

}
