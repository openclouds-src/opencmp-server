package com.massclouds.opencmp.dto;

public class ComponentStatusDTO {
	
	private String componentName;
	
	private String componentAddress;
	
	private String status;
	
	private String description;

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getComponentAddress() {
		return componentAddress;
	}

	public void setComponentAddress(String componentAddress) {
		this.componentAddress = componentAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
