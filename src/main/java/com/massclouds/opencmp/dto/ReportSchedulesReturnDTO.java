package com.massclouds.opencmp.dto;

public class ReportSchedulesReturnDTO extends ReportSchedulesDTO{
	
	private String reportTemplateId;
	
	private String reportTemplateName;
	
	private String schedulecycle;
	
	private String startTime;

	public String getReportTemplateId() {
		return reportTemplateId;
	}

	public void setReportTemplateId(String reportTemplateId) {
		this.reportTemplateId = reportTemplateId;
	}

	public String getReportTemplateName() {
		return reportTemplateName;
	}

	public void setReportTemplateName(String reportTemplateName) {
		this.reportTemplateName = reportTemplateName;
	}

	public String getSchedulecycle() {
		return schedulecycle;
	}

	public void setSchedulecycle(String schedulecycle) {
		this.schedulecycle = schedulecycle;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
}
