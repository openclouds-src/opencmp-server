package com.massclouds.opencmp.dto;

public class TagDTO {
	
	private String href;
	
	private String id;
	
	private String name;
	
	private String fullName;
	
	private Boolean businessTag;
	
	private CategoryDTO category;
	
	private Classification classification;
	
	@SuppressWarnings("unused")
	private static class Classification{
		private String description;
		
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CategoryDTO getCategory() {
		return category;
	}

	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Boolean getBusinessTag() {
		return businessTag;
	}

	public void setBusinessTag(Boolean businessTag) {
		this.businessTag = businessTag;
	}

}


