package com.massclouds.opencmp.dto;

public class VmSpiceDTO {
	
	public String href;
	
	public String id;
	
	public String name;
	
	/**
	 * 虚机所属平台的ip
	 */
	public String provider_address;
	
	/**
	 * SPICE  证书
	 */
	public String certificate;
	
	/**
	 * SPICE  安全端口
	 */
	public String spice_secure_port;
	
	/**
	 * SPICE  端口
	 */
	public String spice_port;
	
	/**
	 * SPICE  ticket
	 */
	public String ticket;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getSpice_secure_port() {
		return spice_secure_port;
	}

	public void setSpice_secure_port(String spice_secure_port) {
		this.spice_secure_port = spice_secure_port;
	}

	public String getSpice_port() {
		return spice_port;
	}

	public void setSpice_port(String spice_port) {
		this.spice_port = spice_port;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getProvider_address() {
		return provider_address;
	}

	public void setProvider_address(String provider_address) {
		this.provider_address = provider_address;
	}

}
