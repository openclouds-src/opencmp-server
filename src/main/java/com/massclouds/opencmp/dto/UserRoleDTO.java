package com.massclouds.opencmp.dto;

import java.util.List;

public class UserRoleDTO {
	
	private String href;
	
	private String id;
	
	private String name;
	
	private String read_only;
	
	private String created_at;
	
	private String updated_at;
	
	private List<UserGroupDTO> miq_groups;
	
	private List<UserGroupRoleFeaturesDTO> features;
	
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRead_only() {
		return read_only;
	}

	public void setRead_only(String read_only) {
		this.read_only = read_only;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public List<UserGroupRoleFeaturesDTO> getFeatures() {
		return features;
	}

	public void setFeatures(List<UserGroupRoleFeaturesDTO> features) {
		this.features = features;
	}

	public List<UserGroupDTO> getMiq_groups() {
		return miq_groups;
	}

	public void setMiq_groups(List<UserGroupDTO> miq_groups) {
		this.miq_groups = miq_groups;
	}

}
