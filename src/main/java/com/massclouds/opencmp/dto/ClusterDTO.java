package com.massclouds.opencmp.dto;

public class ClusterDTO {
	
	private String id;
	
	private String name;
	
	private String ems_id;
	
	private String created_on;
	
	private String updated_on;
	
	private String uid_ems;
	
	private int total_hosts;
	
	private int total_vms;
	
	private int total_miq_templates;

	private String cpu_arch;
	
	private String cpu_type;
	
	private ProviderDTO ext_management_system;

	public String getCpu_arch() {
		return cpu_arch;
	}

	public void setCpu_arch(String cpu_arch) {
		this.cpu_arch = cpu_arch;
	}

	public String getCpu_type() {
		return cpu_type;
	}

	public void setCpu_type(String cpu_type) {
		this.cpu_type = cpu_type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEms_id() {
		return ems_id;
	}

	public void setEms_id(String ems_id) {
		this.ems_id = ems_id;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getUid_ems() {
		return uid_ems;
	}

	public void setUid_ems(String uid_ems) {
		this.uid_ems = uid_ems;
	}

	public int getTotal_hosts() {
		return total_hosts;
	}

	public void setTotal_hosts(int total_hosts) {
		this.total_hosts = total_hosts;
	}

	public int getTotal_vms() {
		return total_vms;
	}

	public void setTotal_vms(int total_vms) {
		this.total_vms = total_vms;
	}

	public int getTotal_miq_templates() {
		return total_miq_templates;
	}

	public void setTotal_miq_templates(int total_miq_templates) {
		this.total_miq_templates = total_miq_templates;
	}

	public ProviderDTO getExt_management_system() {
		return ext_management_system;
	}

	public void setExt_management_system(ProviderDTO ext_management_system) {
		this.ext_management_system = ext_management_system;
	}
	
	

}
