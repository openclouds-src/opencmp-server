package com.massclouds.opencmp.dto;

public class ServiceCountDTO {
	
	private int allCount;
	
	private int activeCount; //活跃的
	
	private int retiredCount;//已退订的
	
	private int waitRetire;//将过期的
	
    public int getAllCount() {
        return allCount;
    }
    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }
	public int getActiveCount() {
		return activeCount;
	}
	public void setActiveCount(int activeCount) {
		this.activeCount = activeCount;
	}
	public int getRetiredCount() {
		return retiredCount;
	}
	public void setRetiredCount(int retiredCount) {
		this.retiredCount = retiredCount;
	}
	public int getWaitRetire() {
		return waitRetire;
	}
	public void setWaitRetire(int waitRetire) {
		this.waitRetire = waitRetire;
	}
    
}
