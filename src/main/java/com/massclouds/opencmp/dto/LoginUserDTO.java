package com.massclouds.opencmp.dto;

public class LoginUserDTO {
	
	private String name;
	
	private String password;
	
	private String auth_token;
	
	private String expires_on;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuth_token() {
		return auth_token;
	}

	public void setAuth_token(String auth_token) {
		this.auth_token = auth_token;
	}

	public String getExpires_on() {
		return expires_on;
	}

	public void setExpires_on(String expires_on) {
		this.expires_on = expires_on;
	}

}
