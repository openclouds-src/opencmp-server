package com.massclouds.opencmp.dto;

public class ReportScheduleCreateDTO {
	
	private String reportId;
	
	private String name;
	
	private String description;
	
	private String startDate;
	
	private String intervalUnit;
	
	private String intervalValue;

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getIntervalUnit() {
		return intervalUnit;
	}

	public void setIntervalUnit(String intervalUnit) {
		this.intervalUnit = intervalUnit;
	}

	public String getIntervalValue() {
		return intervalValue;
	}

	public void setIntervalValue(String intervalValue) {
		this.intervalValue = intervalValue;
	}
	
}
