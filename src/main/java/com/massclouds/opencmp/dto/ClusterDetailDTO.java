package com.massclouds.opencmp.dto;

import java.util.List;

public class ClusterDetailDTO {
	
	private String id;
	
	private String name;
	
	private String ems_id;
	
	private String created_on;
	
	private String updated_on;
	
	private String uid_ems;
	
	private int total_hosts;
	
	private int total_vms;
	
	private int total_miq_templates;
	
	private ProviderDTO ext_management_system;
	
	private List<HostDTO> hosts;
	
	private List<StorageDTO> storages;
	
	private List<VlanDTO> lans;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEms_id() {
		return ems_id;
	}

	public void setEms_id(String ems_id) {
		this.ems_id = ems_id;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getUid_ems() {
		return uid_ems;
	}

	public void setUid_ems(String uid_ems) {
		this.uid_ems = uid_ems;
	}

	public int getTotal_hosts() {
		return total_hosts;
	}

	public void setTotal_hosts(int total_hosts) {
		this.total_hosts = total_hosts;
	}

	public int getTotal_vms() {
		return total_vms;
	}

	public void setTotal_vms(int total_vms) {
		this.total_vms = total_vms;
	}

	public int getTotal_miq_templates() {
		return total_miq_templates;
	}

	public void setTotal_miq_templates(int total_miq_templates) {
		this.total_miq_templates = total_miq_templates;
	}

	public ProviderDTO getExt_management_system() {
		return ext_management_system;
	}

	public void setExt_management_system(ProviderDTO ext_management_system) {
		this.ext_management_system = ext_management_system;
	}

	public List<HostDTO> getHosts() {
		return hosts;
	}

	public void setHosts(List<HostDTO> hosts) {
		this.hosts = hosts;
	}

	public List<StorageDTO> getStorages() {
		return storages;
	}

	public void setStorages(List<StorageDTO> storages) {
		this.storages = storages;
	}

	public List<VlanDTO> getLans() {
		return lans;
	}

	public void setLans(List<VlanDTO> lans) {
		this.lans = lans;
	}
}
