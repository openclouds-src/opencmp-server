package com.massclouds.opencmp.dto;

import java.util.List;

public class ProviderDetailsDTO extends ProviderDTO{
	
	private List<HostDTO> hosts;
	
	private List<StorageDTO> storages;
	
	private List<VlanDTO> lans;
	
	private int port;
	  
	/**平台认证信息  start**/
	private List<ProviderEndpoint> endpoints;
	
	private List<ProviderAuthentication> authentications;
	/**平台认证信息  end**/
	
	/**VMware平台的端口 start**/
	private int host_default_vnc_port_start;
	
	private int host_default_vnc_port_end;
	
	private String provider_region;
	/**VMware平台的端口 end**/
	
	private String keystone_v3_domain;

	public List<HostDTO> getHosts() {
		return hosts;
	}

	public void setHosts(List<HostDTO> hosts) {
		this.hosts = hosts;
	}

	public List<StorageDTO> getStorages() {
		return storages;
	}

	public void setStorages(List<StorageDTO> storages) {
		this.storages = storages;
	}

	public List<VlanDTO> getLans() {
		return lans;
	}

	public void setLans(List<VlanDTO> lans) {
		this.lans = lans;
	}
	
	public List<ProviderEndpoint> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<ProviderEndpoint> endpoints) {
		this.endpoints = endpoints;
	}

	public List<ProviderAuthentication> getAuthentications() {
		return authentications;
	}

	public void setAuthentications(List<ProviderAuthentication> authentications) {
		this.authentications = authentications;
	}

	public int getHost_default_vnc_port_start() {
		return host_default_vnc_port_start;
	}

	public void setHost_default_vnc_port_start(int host_default_vnc_port_start) {
		this.host_default_vnc_port_start = host_default_vnc_port_start;
	}

	public int getHost_default_vnc_port_end() {
		return host_default_vnc_port_end;
	}

	public void setHost_default_vnc_port_end(int host_default_vnc_port_end) {
		this.host_default_vnc_port_end = host_default_vnc_port_end;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getProvider_region() {
		return provider_region;
	}

	public void setProvider_region(String provider_region) {
		this.provider_region = provider_region;
	}

	public String getKeystone_v3_domain() {
		return keystone_v3_domain;
	}

	public void setKeystone_v3_domain(String keystone_v3_domain) {
		this.keystone_v3_domain = keystone_v3_domain;
	}


	public static class ProviderAuthentication {
		private String href;
		private String id;
		private String name;
		private String authtype;
		private String userid;
		private String status;
		private String status_details;
		private String type;
		public String getHref() {
			return href;
		}
		public void setHref(String href) {
			this.href = href;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAuthtype() {
			return authtype;
		}
		public void setAuthtype(String authtype) {
			this.authtype = authtype;
		}
		public String getUserid() {
			return userid;
		}
		public void setUserid(String userid) {
			this.userid = userid;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getStatus_details() {
			return status_details;
		}
		public void setStatus_details(String status_details) {
			this.status_details = status_details;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
	}
	
	public static class ProviderEndpoint {
		private String id;
		private String ipaddress;
		private String hostname;
		private String port;
		private String certificate_authority;
		private String security_protocol;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getIpaddress() {
			return ipaddress;
		}
		public void setIpaddress(String ipaddress) {
			this.ipaddress = ipaddress;
		}
		public String getHostname() {
			return hostname;
		}
		public void setHostname(String hostname) {
			this.hostname = hostname;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
		public String getCertificate_authority() {
			return certificate_authority;
		}
		public void setCertificate_authority(String certificate_authority) {
			this.certificate_authority = certificate_authority;
		}
		public String getSecurity_protocol() {
			return security_protocol;
		}
		public void setSecurity_protocol(String security_protocol) {
			this.security_protocol = security_protocol;
		}
		
	}
	
	
}
