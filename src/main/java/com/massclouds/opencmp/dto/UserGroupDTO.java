package com.massclouds.opencmp.dto;

import java.util.List;

public class UserGroupDTO {
	
	private String href;
	
	private String id;
	
	private String description;
	
	private String alias_name;
	
	private String group_type;
	
	private String created_on;
	
	private String updated_on;
	
	private String tenant_id;
	
	private String detailed_description;
	
	private List<UserDTO> users;
	
	private  UserRoleDTO miq_user_role;
	
	private TenantsDTO tenant;
	
	private UserGroupEntitlementDTO entitlement;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGroup_type() {
		return group_type;
	}

	public void setGroup_type(String group_type) {
		this.group_type = group_type;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getDetailed_description() {
		return detailed_description;
	}

	public void setDetailed_description(String detailed_description) {
		this.detailed_description = detailed_description;
	}

	public UserRoleDTO getMiq_user_role() {
		return miq_user_role;
	}

	public void setMiq_user_role(UserRoleDTO miq_user_role) {
		this.miq_user_role = miq_user_role;
	}

	public TenantsDTO getTenant() {
		return tenant;
	}

	public void setTenant(TenantsDTO tenant) {
		this.tenant = tenant;
	}

	public List<UserDTO> getUsers() {
		return users;
	}

	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}

	public UserGroupEntitlementDTO getEntitlement() {
		return entitlement;
	}

	public void setEntitlement(UserGroupEntitlementDTO entitlement) {
		this.entitlement = entitlement;
	}

	public String getAlias_name() {
		return alias_name;
	}

	public void setAlias_name(String alias_name) {
		this.alias_name = alias_name;
	}
	
}
