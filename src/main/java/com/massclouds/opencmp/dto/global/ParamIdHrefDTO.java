package com.massclouds.opencmp.dto.global;

public class ParamIdHrefDTO {
	
	private String id;
	
	private String href;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public ParamIdHrefDTO(String id, String href) {
		super();
		this.id = id;
		this.href = href;
	}
}
