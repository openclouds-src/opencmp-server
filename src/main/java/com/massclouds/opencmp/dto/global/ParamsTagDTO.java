package com.massclouds.opencmp.dto.global;

public class ParamsTagDTO {
	private String category;
	
	private String name;
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ParamsTagDTO(String category, String name) {
		super();
		this.category = category;
		this.name = name;
	}
}
