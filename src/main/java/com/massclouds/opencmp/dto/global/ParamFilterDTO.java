package com.massclouds.opencmp.dto.global;

import java.util.List;

public class ParamFilterDTO {
	
	private List<Object> managed;

	public List<Object> getManaged() {
		return managed;
	}

	public void setManaged(List<Object> managed) {
		this.managed = managed;
	}

	public ParamFilterDTO(List<Object> managed) {
		super();
		this.managed = managed;
	}
	
}
