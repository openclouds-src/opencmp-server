package com.massclouds.opencmp.dto.global;

public class ParamIdDTO {
	
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ParamIdDTO() {
	}
	
	public ParamIdDTO(String id) {
		super();
		this.id = id;
	}

}
