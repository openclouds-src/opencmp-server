package com.massclouds.opencmp.dto;

import java.util.List;

public class ServiceTemplateCreateConfigInfoDTO {
		
	private String miq_request_dialog_name;
	
	private boolean service_template_request;
	
	private boolean initial_pass;
	
	private List<Object> src_vm_id;
	
	private List<Object> placement_auto;
	
	private List<Object> number_of_vms;
	
	private List<Object> vm_auto_start;
	
	private List<Object> linked_clone;
	
	//private boolean stateless;
	
	private List<Object> vlan;
	
	private String vm_name;
	
	private List<Object> src_ems_id;
	
	private List<Object> placement_cluster_name;
	
	private List<Object> placement_host_name;
	
	private List<Object> placement_ds_name;
	
	private List<Object> provision_type;
	
	private List<Object> number_of_sockets;
	
	private List<Object> cores_per_socket;
	
	private List<Object> vm_memory;
	
	private List<Object> disk_format;
	
	private int memory_reserve;
	
	private int memory_limit;
	
	private List<Object> schedule_type;
	
//	private int memory_reserve;
	
//	private int memory_limit;
	
//	private String network_adapters;
	
	private String owner_email;
	
	private String zone_id;
	
	private ServiceTemplateCreateProvisionDTO provision;

	public List<Object> getLinked_clone() {
		return linked_clone;
	}

	public void setLinked_clone(List<Object> linked_clone) {
		this.linked_clone = linked_clone;
	}

	public String getMiq_request_dialog_name() {
		return miq_request_dialog_name;
	}

	public void setMiq_request_dialog_name(String miq_request_dialog_name) {
		this.miq_request_dialog_name = miq_request_dialog_name;
	}

	public boolean isService_template_request() {
		return service_template_request;
	}

	public void setService_template_request(boolean service_template_request) {
		this.service_template_request = service_template_request;
	}

//	public boolean isStateless() {
//		return stateless;
//	}
//
//	public void setStateless(boolean stateless) {
//		this.stateless = stateless;
//	}

	public boolean isInitial_pass() {
		return initial_pass;
	}

	public void setInitial_pass(boolean initial_pass) {
		this.initial_pass = initial_pass;
	}

	public List<Object> getNumber_of_vms() {
		return number_of_vms;
	}

	public void setNumber_of_vms(List<Object> number_of_vms) {
		this.number_of_vms = number_of_vms;
	}


	public List<Object> getVlan() {
		return vlan;
	}

	public void setVlan(List<Object> vlan) {
		this.vlan = vlan;
	}

	public String getVm_name() {
		return vm_name;
	}

	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}


	public List<Object> getProvision_type() {
		return provision_type;
	}

	public void setProvision_type(List<Object> provision_type) {
		this.provision_type = provision_type;
	}


//	public int getMemory_reserve() {
//		return memory_reserve;
//	}
//
//	public void setMemory_reserve(int memory_reserve) {
//		this.memory_reserve = memory_reserve;
//	}
//
//	public int getMemory_limit() {
//		return memory_limit;
//	}
//
//	public void setMemory_limit(int memory_limit) {
//		this.memory_limit = memory_limit;
//	}

//	public String getNetwork_adapters() {
//		return network_adapters;
//	}
//
//	public void setNetwork_adapters(String network_adapters) {
//		this.network_adapters = network_adapters;
//	}

	public List<Object> getNumber_of_sockets() {
		return number_of_sockets;
	}

	public void setNumber_of_sockets(List<Object> number_of_sockets) {
		this.number_of_sockets = number_of_sockets;
	}

	public List<Object> getCores_per_socket() {
		return cores_per_socket;
	}

	public void setCores_per_socket(List<Object> cores_per_socket) {
		this.cores_per_socket = cores_per_socket;
	}

	public List<Object> getVm_memory() {
		return vm_memory;
	}

	public void setVm_memory(List<Object> vm_memory) {
		this.vm_memory = vm_memory;
	}

	public List<Object> getPlacement_auto() {
		return placement_auto;
	}

	public void setPlacement_auto(List<Object> placement_auto) {
		this.placement_auto = placement_auto;
	}

	public List<Object> getVm_auto_start() {
		return vm_auto_start;
	}

	public void setVm_auto_start(List<Object> vm_auto_start) {
		this.vm_auto_start = vm_auto_start;
	}

	public List<Object> getSrc_vm_id() {
		return src_vm_id;
	}

	public void setSrc_vm_id(List<Object> src_vm_id) {
		this.src_vm_id = src_vm_id;
	}

	public List<Object> getSrc_ems_id() {
		return src_ems_id;
	}

	public void setSrc_ems_id(List<Object> src_ems_id) {
		this.src_ems_id = src_ems_id;
	}

	public String getOwner_email() {
		return owner_email;
	}

	public void setOwner_email(String owner_email) {
		this.owner_email = owner_email;
	}

	public String getZone_id() {
		return zone_id;
	}

	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

	public ServiceTemplateCreateProvisionDTO getProvision() {
		return provision;
	}

	public void setProvision(ServiceTemplateCreateProvisionDTO provision) {
		this.provision = provision;
	}
	
	public List<Object> getPlacement_cluster_name() {
		return placement_cluster_name;
	}

	public void setPlacement_cluster_name(List<Object> placement_cluster_name) {
		this.placement_cluster_name = placement_cluster_name;
	}

	public List<Object> getPlacement_host_name() {
		return placement_host_name;
	}

	public void setPlacement_host_name(List<Object> placement_host_name) {
		this.placement_host_name = placement_host_name;
	}

	public List<Object> getPlacement_ds_name() {
		return placement_ds_name;
	}

	public void setPlacement_ds_name(List<Object> placement_ds_name) {
		this.placement_ds_name = placement_ds_name;
	}

	public List<Object> getSchedule_type() {
		return schedule_type;
	}

	public void setSchedule_type(List<Object> schedule_type) {
		this.schedule_type = schedule_type;
	}

	public int getMemory_reserve() {
		return memory_reserve;
	}

	public void setMemory_reserve(int memory_reserve) {
		this.memory_reserve = memory_reserve;
	}

	public int getMemory_limit() {
		return memory_limit;
	}

	public void setMemory_limit(int memory_limit) {
		this.memory_limit = memory_limit;
	}

	public List<Object> getDisk_format() {
		return disk_format;
	}

	public void setDisk_format(List<Object> disk_format) {
		this.disk_format = disk_format;
	}

}
