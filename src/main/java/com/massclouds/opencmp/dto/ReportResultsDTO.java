package com.massclouds.opencmp.dto;

import java.util.List;
import java.util.Map;

public class ReportResultsDTO extends ReportDTO{
	
	private ReportTemplateDTO report;
	
	private List<Map<Object, Object>> result_set;

	public ReportTemplateDTO getReport() {
		return report;
	}

	public void setReport(ReportTemplateDTO report) {
		this.report = report;
	}

	public List<Map<Object, Object>> getResult_set() {
		return result_set;
	}

	public void setResult_set(List<Map<Object, Object>> result_set) {
		this.result_set = result_set;
	}
	
}
