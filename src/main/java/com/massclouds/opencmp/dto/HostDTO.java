package com.massclouds.opencmp.dto;

import java.util.Map;

public class HostDTO {
	
	private String id;
	
	private String name;
	
	private String hostname;
	
	private String ipaddress;
	
	private String created_on;
	
	private String updated_on;
	
	private String guid;
	
	private String power_state;
	
	private String connection_state;
	
	private String vmm_vendor;
	
	private String vmm_version;
	
	private String vmm_product;
	
	private String vmm_buildnumber;
	
	private int v_total_vms;
	
	private int v_on_total_vms;
	
	private int v_total_miq_templates;
	
	private int num_cpu;
	
	private int total_vcpus;
	
	private int ram_size;
	
	private Map<String, Object> ext_management_system;
	
	private ClusterDTO ems_cluster;
	
	private String ems_id;
	
	private String zoneName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getPower_state() {
		return power_state;
	}

	public void setPower_state(String power_state) {
		this.power_state = power_state;
	}

	public String getConnection_state() {
		return connection_state;
	}

	public void setConnection_state(String connection_state) {
		this.connection_state = connection_state;
	}

	public String getVmm_vendor() {
		return vmm_vendor;
	}

	public void setVmm_vendor(String vmm_vendor) {
		this.vmm_vendor = vmm_vendor;
	}

	public String getVmm_version() {
		return vmm_version;
	}

	public void setVmm_version(String vmm_version) {
		this.vmm_version = vmm_version;
	}

	public String getVmm_product() {
		return vmm_product;
	}

	public void setVmm_product(String vmm_product) {
		this.vmm_product = vmm_product;
	}

	public String getVmm_buildnumber() {
		return vmm_buildnumber;
	}

	public void setVmm_buildnumber(String vmm_buildnumber) {
		this.vmm_buildnumber = vmm_buildnumber;
	}

	public Map<String, Object> getExt_management_system() {
		return ext_management_system;
	}

	public void setExt_management_system(Map<String, Object> ext_management_system) {
		this.ext_management_system = ext_management_system;
	}

	public ClusterDTO getEms_cluster() {
		return ems_cluster;
	}

	public void setEms_cluster(ClusterDTO ems_cluster) {
		this.ems_cluster = ems_cluster;
	}

	public int getV_total_vms() {
		return v_total_vms;
	}

	public void setV_total_vms(int v_total_vms) {
		this.v_total_vms = v_total_vms;
	}

	public int getV_on_total_vms() {
		return v_on_total_vms;
	}

	public void setV_on_total_vms(int v_on_total_vms) {
		this.v_on_total_vms = v_on_total_vms;
	}

	public int getV_total_miq_templates() {
		return v_total_miq_templates;
	}

	public void setV_total_miq_templates(int v_total_miq_templates) {
		this.v_total_miq_templates = v_total_miq_templates;
	}

	public int getNum_cpu() {
		return num_cpu;
	}

	public void setNum_cpu(int num_cpu) {
		this.num_cpu = num_cpu;
	}

	public int getTotal_vcpus() {
		return total_vcpus;
	}

	public void setTotal_vcpus(int total_vcpus) {
		this.total_vcpus = total_vcpus;
	}

	public int getRam_size() {
		return ram_size;
	}

	public void setRam_size(int ram_size) {
		this.ram_size = ram_size;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getEms_id() {
		return ems_id;
	}

	public void setEms_id(String ems_id) {
		this.ems_id = ems_id;
	}
	
}
