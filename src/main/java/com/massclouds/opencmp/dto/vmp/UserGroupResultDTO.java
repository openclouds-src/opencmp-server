package com.massclouds.opencmp.dto.vmp;

import java.util.List;

public class UserGroupResultDTO {
	
	private String title;
	
	private String id;
	
	private String depth;
	
	private List<UserGroupDTO> children;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDepth() {
		return depth;
	}

	public void setDepth(String depth) {
		this.depth = depth;
	}

	public List<UserGroupDTO> getChildren() {
		return children;
	}

	public void setChildren(List<UserGroupDTO> children) {
		this.children = children;
	}
	
}
