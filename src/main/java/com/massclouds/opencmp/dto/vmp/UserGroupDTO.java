package com.massclouds.opencmp.dto.vmp;

import java.util.List;

public class UserGroupDTO {
	
	private String id;
	
	private String title;
	
	private List<UserGroupDTO> children;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<UserGroupDTO> getChildren() {
		return children;
	}

	public void setChildren(List<UserGroupDTO> children) {
		this.children = children;
	}
	
}
