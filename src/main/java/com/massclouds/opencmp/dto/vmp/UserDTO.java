package com.massclouds.opencmp.dto.vmp;

import java.util.List;

public class UserDTO {
	
	private String id;
	
	private String name;
	
	private String username;
	
	private String password;
	
	private String usergroups_id;
	
	private String user_group_name;
	
	private List<String> vms;
	
	private List<String> vmsname;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsergroups_id() {
		return usergroups_id;
	}

	public void setUsergroups_id(String usergroups_id) {
		this.usergroups_id = usergroups_id;
	}

	public String getUser_group_name() {
		return user_group_name;
	}

	public void setUser_group_name(String user_group_name) {
		this.user_group_name = user_group_name;
	}

	public List<String> getVms() {
		return vms;
	}

	public void setVms(List<String> vms) {
		this.vms = vms;
	}

	public List<String> getVmsname() {
		return vmsname;
	}

	public void setVmsname(List<String> vmsname) {
		this.vmsname = vmsname;
	}

}
