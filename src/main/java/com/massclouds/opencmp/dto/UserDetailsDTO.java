package com.massclouds.opencmp.dto;

public class UserDetailsDTO extends UserDTO {
	
	private UserRoleDTO role;

	public UserRoleDTO getRole() {
		return role;
	}

	public void setRole(UserRoleDTO role) {
		this.role = role;
	}


}
