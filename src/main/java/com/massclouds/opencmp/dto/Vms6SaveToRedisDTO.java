package com.massclouds.opencmp.dto;

import com.massclouds.opencmp.dto.VmDTO.OSInfo;

public class Vms6SaveToRedisDTO {
	
    public String id;
	
	public String name;
	
	public String description;
	
	public String created_on;
	
	private OSInfo operating_system;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public OSInfo getOperating_system() {
		return operating_system;
	}

	public void setOperating_system(OSInfo operating_system) {
		this.operating_system = operating_system;
	}
	
}
