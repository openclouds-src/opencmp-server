package com.massclouds.opencmp.dto;

public class DashboardEntityCountDTO {
	
	/**
	 * 域数量
	 */
	private int zoneCount;
	
	/**
	 * 物理主机数量
	 */
	private int hostCount;
	
	/**
	 * 集群数量
	 */
	private int clusterCount;
	
	/**
	 * 云主机数量
	 */
	private int vmServerCount;
	
	/**
	 * 云桌面数量
	 */
	private int vmDesktopCount;
	
	/**
	 * 虚机数量
	 */
	private int vmCount;
	
	/**
	 * 容器数量
	 */
	private int containerCount;
	
	/**
	 * 租户数量
	 */
	private int tenantCount;
	
	/**
	 * 用户数量
	 */
	private int userCount;
	
	/**
	 * 终端数量
	 */
	private int terminalCount;
	
	/**
	 * 所有模板数量
	 */
	private int templateAllCount;
	
	/**
	 * 虚机的模板数量
	 */
	private int templateVmCount;
	
	/**
	 * 容器的模板数量
	 */
	private int templateContainterCount;
	
	/**
	 * 服务目录数量
	 */
	private int serviceCatalogCount;

	public int getZoneCount() {
		return zoneCount;
	}

	public void setZoneCount(int zoneCount) {
		this.zoneCount = zoneCount;
	}

	public int getHostCount() {
		return hostCount;
	}

	public void setHostCount(int hostCount) {
		this.hostCount = hostCount;
	}

	public int getVmCount() {
		return vmCount;
	}

	public void setVmCount(int vmCount) {
		this.vmCount = vmCount;
	}

	public int getClusterCount() {
		return clusterCount;
	}

	public void setClusterCount(int clusterCount) {
		this.clusterCount = clusterCount;
	}

	public int getContainerCount() {
		return containerCount;
	}

	public void setContainerCount(int containerCount) {
		this.containerCount = containerCount;
	}

	public int getTenantCount() {
		return tenantCount;
	}

	public void setTenantCount(int tenantCount) {
		this.tenantCount = tenantCount;
	}

	public int getUserCount() {
		return userCount;
	}

	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}

	public int getTerminalCount() {
		return terminalCount;
	}

	public void setTerminalCount(int terminalCount) {
		this.terminalCount = terminalCount;
	}

	public int getTemplateAllCount() {
		return templateAllCount;
	}

	public void setTemplateAllCount(int templateAllCount) {
		this.templateAllCount = templateAllCount;
	}

	public int getTemplateVmCount() {
		return templateVmCount;
	}

	public void setTemplateVmCount(int templateVmCount) {
		this.templateVmCount = templateVmCount;
	}

	public int getTemplateContainterCount() {
		return templateContainterCount;
	}

	public void setTemplateContainterCount(int templateContainterCount) {
		this.templateContainterCount = templateContainterCount;
	}

	public int getServiceCatalogCount() {
		return serviceCatalogCount;
	}

	public void setServiceCatalogCount(int serviceCatalogCount) {
		this.serviceCatalogCount = serviceCatalogCount;
	}

	public int getVmServerCount() {
		return vmServerCount;
	}

	public void setVmServerCount(int vmServerCount) {
		this.vmServerCount = vmServerCount;
	}

	public int getVmDesktopCount() {
		return vmDesktopCount;
	}

	public void setVmDesktopCount(int vmDesktopCount) {
		this.vmDesktopCount = vmDesktopCount;
	}
	
}
