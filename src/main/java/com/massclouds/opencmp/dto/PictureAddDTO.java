package com.massclouds.opencmp.dto;

public class PictureAddDTO {
	
	/*
	 * 要添加logo的资源的id
	 */
	private String resourceId;
	
	/*
	 * 要添加logo的资源的类型
	 */
	private String resourceType;
	
	/*
	 * logo 扩展名，png、jpg
	 */
	private String extension;
	
	/*
	 * logo base64值
	 */
	private String content;

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	

}
