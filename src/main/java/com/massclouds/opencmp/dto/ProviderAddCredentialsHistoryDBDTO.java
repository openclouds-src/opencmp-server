package com.massclouds.opencmp.dto;

public class ProviderAddCredentialsHistoryDBDTO {
	
	private String userid;
	
	private String password;
	
	private String auth_type;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuth_type() {
		return auth_type;
	}

	public void setAuth_type(String auth_type) {
		this.auth_type = auth_type;
	}

	public ProviderAddCredentialsHistoryDBDTO(String userid, String password, String auth_type) {
		super();
		this.userid = userid;
		this.password = password;
		this.auth_type = auth_type;
	}

}
