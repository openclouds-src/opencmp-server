package com.massclouds.opencmp.dto;

public class DashboardEntityCountForSelfDTO {
	
	/**
	 * 云主机数量
	 */
	private int vmServerCount;
	
	/**
	 * 云桌面数量
	 */
	private int vmDesktopCount;
	
	/**
	 * 我的资源总数
	 */
	private int vmCount;
	
	/**
	 * 服务目录项数量
	 */
	private int serviceTemplatesCount;
	
	public int getVmServerCount() {
		return vmServerCount;
	}

	public void setVmServerCount(int vmServerCount) {
		this.vmServerCount = vmServerCount;
	}

	public int getVmDesktopCount() {
		return vmDesktopCount;
	}

	public void setVmDesktopCount(int vmDesktopCount) {
		this.vmDesktopCount = vmDesktopCount;
	}

	public int getVmCount() {
		return vmCount;
	}

	public void setVmCount(int vmCount) {
		this.vmCount = vmCount;
	}

	public int getServiceTemplatesCount() {
		return serviceTemplatesCount;
	}

	public void setServiceTemplatesCount(int serviceTemplatesCount) {
		this.serviceTemplatesCount = serviceTemplatesCount;
	}

}
