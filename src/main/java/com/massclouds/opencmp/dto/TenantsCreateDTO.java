package com.massclouds.opencmp.dto;

public class TenantsCreateDTO {
	
	public String id;
	
	public String ancestry;
	
	public String name;
	
	public String description;
	
	/**
	 * 标签类名称
	 */
	private String categoryName;
	
	/**
	 * 标签名称
	 */
	
	private String tagName;

	public TenantsCreateDTO(String name, String description, String ancestry) {
		this.ancestry = ancestry;
		this.name = name;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAncestry() {
		return ancestry;
	}

	public void setAncestry(String ancestry) {
		this.ancestry = ancestry;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
}
