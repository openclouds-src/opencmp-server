package com.massclouds.opencmp.dto;

public class ManageIQTokenDTO {
	
	private String auth_token;
	
	private int token_ttl;
	
	private String expires_on;
	
	private String requester_type;

	public String getAuth_token() {
		return auth_token;
	}

	public void setAuth_token(String auth_token) {
		this.auth_token = auth_token;
	}

	public int getToken_ttl() {
		return token_ttl;
	}

	public void setToken_ttl(int token_ttl) {
		this.token_ttl = token_ttl;
	}

	public String getExpires_on() {
		return expires_on;
	}

	public void setExpires_on(String expires_on) {
		this.expires_on = expires_on;
	}

	public String getRequester_type() {
		return requester_type;
	}

	public void setRequester_type(String requester_type) {
		this.requester_type = requester_type;
	}
	

}
