package com.massclouds.opencmp.dto.zabbix;

public class EventAcknowledgeDTO {
	/**
	 * 事件id
	 */
	private String eventId;
	/**
	 * 事件更新操作.可用值：1 - 关闭问题；2 - 确认事件；4 - 添加消息；8 - 更改严重性；16 - 取消确认事件。
	 */
	private int action;
	/**
	 * 消息的文本
	 */
	private String message;
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public int getAction() {
		return action;
	}
	public void setAction(int action) {
		this.action = action;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
