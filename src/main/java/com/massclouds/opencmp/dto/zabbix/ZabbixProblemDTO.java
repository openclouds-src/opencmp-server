package com.massclouds.opencmp.dto.zabbix;

import java.util.List;
import java.util.Map;

public class ZabbixProblemDTO {
	/**
	 * 问题事件的 ID
	 */
	private String eventid;
	/**
	 * 相关对象的ID
	 */
	private String objectid;
	/**
	 * 	问题当前严重性。可用值：
	 *  0 - 未分类；
	 *	1 - 信息；
	 *	2 - 警告；
	 *	3 - 平均；
	 *	4 - 高；
	 *	5 - 灾难。
	 */
	private String severity;
	/**
	 * 创建问题事件的时间
	 */
	private String clock;
	/**
	 * 已解决的问题名称
	 */
	private String name;
	/**
	 * 问题的确认状态。可用值：0 - 未确认；1 - 已确认;
	 */
	private String acknowledged;
	/**
	 * 创建恢复事件的时间。
	 */
	private String r_clock;
	/**
	 * 带有扩展宏的操作数据
	 */
	private String opdata;
	/**
	 * 问题是否被抑制。可用值：0 - 问题处于正常状态；1 - 问题被抑制;
	 */
	private String suppressed;
	
	/**
	 * 返回 主机 属性
	 */
	private List<Map<String, Object>> hosts;
	
	public String getEventid() {
		return eventid;
	}
	public void setEventid(String eventid) {
		this.eventid = eventid;
	}
	public String getObjectid() {
		return objectid;
	}
	public void setObjectid(String objectid) {
		this.objectid = objectid;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getClock() {
		return clock;
	}
	public void setClock(String clock) {
		this.clock = clock;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAcknowledged() {
		return acknowledged;
	}
	public void setAcknowledged(String acknowledged) {
		this.acknowledged = acknowledged;
	}
	public String getR_clock() {
		return r_clock;
	}
	public void setR_clock(String r_clock) {
		this.r_clock = r_clock;
	}
	public String getOpdata() {
		return opdata;
	}
	public void setOpdata(String opdata) {
		this.opdata = opdata;
	}
	public String getSuppressed() {
		return suppressed;
	}
	public void setSuppressed(String suppressed) {
		this.suppressed = suppressed;
	}
	public List<Map<String, Object>> getHosts() {
		return hosts;
	}
	public void setHosts(List<Map<String, Object>> hosts) {
		this.hosts = hosts;
	}
	
}
