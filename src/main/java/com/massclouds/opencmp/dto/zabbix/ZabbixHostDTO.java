package com.massclouds.opencmp.dto.zabbix;

import java.util.List;
import java.util.Map;

public class ZabbixHostDTO {
	/**
	 * 主机的ID
	 */
	private String hostid;
	/**
	 * 主机的正式名称
	 */
	private String host;
	/**
	 * 可见的主机名,默认host属性值
	 */
	private String name;
	/**
	 * 	主机的状态和功能。可能的值是:0 - (默认) 开启监控的主机;1 - 没有开启监控的主机。
	 */
	private String status;
	/**
	 * 主机的描述信息
	 */
	private String description;
	
	/**
	 * 主机的模板信息
	 */
	private List<Map<String, Object>> parentTemplates;
	
	/**
	 * 主机的接口对象信息
	 */
	private List<Map<String, Object>> interfaces;
	
	public String getHostid() {
		return hostid;
	}
	public void setHostid(String hostid) {
		this.hostid = hostid;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Map<String, Object>> getParentTemplates() {
		return parentTemplates;
	}
	public void setParentTemplates(List<Map<String, Object>> parentTemplates) {
		this.parentTemplates = parentTemplates;
	}
	public List<Map<String, Object>> getInterfaces() {
		return interfaces;
	}
	public void setInterfaces(List<Map<String, Object>> interfaces) {
		this.interfaces = interfaces;
	}

}
