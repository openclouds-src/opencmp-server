package com.massclouds.opencmp.dto.zabbix;

import java.util.List;

public class ZabbixTemplateDTO {
	
	/**
	 * 模版的ID
	 */
	private String templateid;
	/**
	 * 模板的技术名称
	 */
	private String host;
	/**
	 * 模板的可见名称。默认值 host 属性值。
	 */
	private String name;
	/**
	 * 	模板的状态
	 */
	private String status;
	/**
	 * 模板的描述
	 */
	private String description;
	
	/**
	 * 模板的触发器信息
	 */
	private List<ZabbixTriggerDTO> triggers;
	
	/**
	 * 模板的主机对象信息
	 */
	private List<ZabbixHostDTO> hosts;

	public String getTemplateid() {
		return templateid;
	}

	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ZabbixTriggerDTO> getTriggers() {
		return triggers;
	}

	public void setTriggers(List<ZabbixTriggerDTO> triggers) {
		this.triggers = triggers;
	}

	public List<ZabbixHostDTO> getHosts() {
		return hosts;
	}

	public void setHosts(List<ZabbixHostDTO> hosts) {
		this.hosts = hosts;
	}
	
}
