package com.massclouds.opencmp.dto.zabbix;

public class ZabbixHostGroupDTO {
	/**
	 * 主机组的ID
	 */
	private String groupid;
	/**
	 * 主机组的名称
	 */
	private String name;
	
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
