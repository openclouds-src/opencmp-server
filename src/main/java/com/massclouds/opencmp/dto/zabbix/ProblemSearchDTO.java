package com.massclouds.opencmp.dto.zabbix;

import com.massclouds.opencmp.utils.resultdto.QueryDTO;

public class ProblemSearchDTO extends QueryDTO{

	private static final long serialVersionUID = 1L;

	private String severities;
	
	private String acknowledged;
	
	private String hostId;

	public String getSeverities() {
		return severities;
	}

	public void setSeverities(String severities) {
		this.severities = severities;
	}

	public String getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(String acknowledged) {
		this.acknowledged = acknowledged;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

}
