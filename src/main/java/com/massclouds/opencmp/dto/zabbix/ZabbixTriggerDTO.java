package com.massclouds.opencmp.dto.zabbix;

public class ZabbixTriggerDTO {
	
	/**
	 * 触发器的ID
	 */
	private String triggerid;
	/**
	 * 父触发器模板ID
	 */
	private String templateid;
	/**
	 * 	简化的触发器表达式
	 */
	private String expression;
	/**
	 * 	触发器是否处于启用状态或禁用状态.有效的值为:0 - (默认) 已启用;1 - 已禁用.
	 */
	private String status;
	/**
	 * 触发器的名称.
	 */
	private String description;
	/**
	 * 触发器是否处于正常或故障状态.有效的值为:0 - (默认) 正常状态;1 - 故障状态.
	 */
	private String value;
	/**
	 * 触发器的严重性级别.有效的值为:
	 * 0 - (默认) 未分类;
	 * 1 - 信息;
	 * 2 - 警告;
	 * 3 - 一般严重;
	 * 4 - 严重;
	 * 5 - 灾难.
	 */
	private String priority;
	/**
	 * 触发器的附加说明
	 */
	private String comments;
	/**
	 * 当前的数据.
	 */
	private String opdata;
	/**
	 * 事件内容，比较全面的内容.
	 */
	private String event_name;
	/**
	 * 事件内容，中文翻译
	 */
	private String event_name_zh;
	
	public String getTriggerid() {
		return triggerid;
	}
	public void setTriggerid(String triggerid) {
		this.triggerid = triggerid;
	}
	public String getTemplateid() {
		return templateid;
	}
	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getOpdata() {
		return opdata;
	}
	public void setOpdata(String opdata) {
		this.opdata = opdata;
	}
	public String getEvent_name() {
		return event_name;
	}
	public void setEvent_name(String event_name) {
		this.event_name = event_name;
	}
	public String getEvent_name_zh() {
		return event_name_zh;
	}
	public void setEvent_name_zh(String event_name_zh) {
		this.event_name_zh = event_name_zh;
	}
	
}
