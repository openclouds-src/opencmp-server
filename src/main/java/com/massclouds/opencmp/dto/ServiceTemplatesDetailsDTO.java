package com.massclouds.opencmp.dto;

import java.util.List;

public class ServiceTemplatesDetailsDTO extends  ServiceTemplatesDTO{
	/*
	*所有者，即所属用户id
	*/
	private String evm_owner_id;
    /*
	*所属用户组id
	*/
	private String miq_group_id;

	/*
	*所属租户id
	*/
	private String tenant_id;

	private List<TagDTO> tags;

	private TenantsDTO tenant;
	
	private ServiceTemplateCreateConfigInfoDTO config_info;
	
	public String getEvm_owner_id() {
		return evm_owner_id;
	}

	public void setEvm_owner_id(String evm_owner_id) {
		this.evm_owner_id = evm_owner_id;
	}

	public String getMiq_group_id() {
		return miq_group_id;
	}

	public void setMiq_group_id(String miq_group_id) {
		this.miq_group_id = miq_group_id;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public List<TagDTO> getTags() {
		return tags;
	}

	public void setTags(List<TagDTO> tags) {
		this.tags = tags;
	}

	public ServiceTemplateCreateConfigInfoDTO getConfig_info() {
		return config_info;
	}

	public void setConfig_info(ServiceTemplateCreateConfigInfoDTO config_info) {
		this.config_info = config_info;
	}

	public TenantsDTO getTenant() {
		return tenant;
	}

	public void setTenant(TenantsDTO tenant) {
		this.tenant = tenant;
	}

}
