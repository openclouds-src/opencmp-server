package com.massclouds.opencmp.dto;

import java.util.List;

public class VmHostOnRunDTO {
	
	private String id;
	
	private String name;
	
	private List<String> ipaddresses;
	
	private String ipaddress;
	
	private String type;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getIpaddresses() {
		return ipaddresses;
	}
	public void setIpaddresses(List<String> ipaddresses) {
		this.ipaddresses = ipaddresses;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

}
