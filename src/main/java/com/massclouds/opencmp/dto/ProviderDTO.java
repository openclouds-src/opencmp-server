package com.massclouds.opencmp.dto;

public class ProviderDTO {
	
	public String id;
	
	public String href;
	
	public String name;
	
	public String type;
	
	public String typeName;
	
	public Boolean isVMP;
	
	public String guid;
	
	public String created_on;
	
	public int total_hosts;
	
	public int total_vms;
	
	public String hostname;
	
	public String ipaddress;
	
	public String total_vcpus;
	
	public String total_memory;//单位GB
	
	public String total_miq_templates;
	
	public String total_storages_size;
	
	public ZoneDTO zone;
	
	public String zone_id;
	
	public  String authentication_status;
	
	/************容器平台的统计数据*****************/
	
	public int containerNodeCount;
	
	public int containerProjectCount;
	
	public int containerPodsCount;
	
	public int containerCount;
	
	public int containerServiceCount;
	
	public int containerTemplateCount;
	
	public int containerImagesCount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public int getTotal_hosts() {
		return total_hosts;
	}

	public void setTotal_hosts(int total_hosts) {
		this.total_hosts = total_hosts;
	}

	public int getTotal_vms() {
		return total_vms;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public void setTotal_vms(int total_vms) {
		this.total_vms = total_vms;
	}

	public ZoneDTO getZone() {
		return zone;
	}

	public void setZone(ZoneDTO zone) {
		this.zone = zone;
	}

	public String getTotal_vcpus() {
		return total_vcpus;
	}

	public void setTotal_vcpus(String total_vcpus) {
		this.total_vcpus = total_vcpus;
	}

	public String getTotal_memory() {
		return total_memory;
	}

	public void setTotal_memory(String total_memory) {
		this.total_memory = total_memory;
	}

	public String getTotal_miq_templates() {
		return total_miq_templates;
	}

	public void setTotal_miq_templates(String total_miq_templates) {
		this.total_miq_templates = total_miq_templates;
	}

	public String getTotal_storages_size() {
		return total_storages_size;
	}

	public void setTotal_storages_size(String total_storages_size) {
		this.total_storages_size = total_storages_size;
	}

	public String getAuthentication_status() {
		return authentication_status;
	}

	public void setAuthentication_status(String authentication_status) {
		this.authentication_status = authentication_status;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Boolean getIsVMP() {
		return isVMP;
	}

	public void setIsVMP(Boolean isVMP) {
		this.isVMP = isVMP;
	}

	public int getContainerNodeCount() {
		return containerNodeCount;
	}

	public void setContainerNodeCount(int containerNodeCount) {
		this.containerNodeCount = containerNodeCount;
	}

	public int getContainerProjectCount() {
		return containerProjectCount;
	}

	public void setContainerProjectCount(int containerProjectCount) {
		this.containerProjectCount = containerProjectCount;
	}

	public int getContainerPodsCount() {
		return containerPodsCount;
	}

	public void setContainerPodsCount(int containerPodsCount) {
		this.containerPodsCount = containerPodsCount;
	}

	public int getContainerCount() {
		return containerCount;
	}

	public void setContainerCount(int containerCount) {
		this.containerCount = containerCount;
	}

	public int getContainerServiceCount() {
		return containerServiceCount;
	}

	public void setContainerServiceCount(int containerServiceCount) {
		this.containerServiceCount = containerServiceCount;
	}

	public int getContainerTemplateCount() {
		return containerTemplateCount;
	}

	public void setContainerTemplateCount(int containerTemplateCount) {
		this.containerTemplateCount = containerTemplateCount;
	}

	public int getContainerImagesCount() {
		return containerImagesCount;
	}

	public void setContainerImagesCount(int containerImagesCount) {
		this.containerImagesCount = containerImagesCount;
	}

	public String getZone_id() {
		return zone_id;
	}

	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

}
