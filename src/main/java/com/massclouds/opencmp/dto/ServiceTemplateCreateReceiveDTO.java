package com.massclouds.opencmp.dto;

public class ServiceTemplateCreateReceiveDTO {
	
	/**
	 * 目录项id
	 */
	private String id;
	
	/**
	 * 目录项名称
	 */
	private String name;
	
	/**
	 * 目录项类型，或供应商类型
	 */
	private String provType;
	
	/**
	 * 描述
	 */
	private String description;
	
	/**
	 * 对话框id
	 */
	private String dialogId;
	
	/**
	 * 目录id
	 */
	private String catalogId;
	
	/**
	 * 虚拟机模板id
	 */
	private String vmTemplateId;
	
	/**
	 * 虚拟机模板名称
	 */
	private String vmTemplateName;
	
	/**
	 * 虚拟机数量
	 */
	private int numberOfVms;
	
	/**
	 * 网络id
	 */
	private String vmVlanId;
	
	/**
	 * 网络名称
	 */
	private String vmVlanName;
	
	/**
	 * 自动配置，即根据模板信息自动选择集群、主机、存储域
	 */
	private boolean placementAuto;
	
	/**
	 * 集群id
	 */
	private String clusterId;
	
	/**
	 * 集群名称
	 */
	private String clusterName;
	
	/**
	 * 主机id
	 */
	private String hostId;
	
	/**
	 * 主机名称
	 */
	private String hostName;
	
	/**
	 * 存储id
	 */
	private String storageId;
	
	/**
	 * 存储名称
	 */
	private String storageName;
	
	/**
	 * 供应商id
	 */
	private String providerId;
	
	/**
	 * 供应商名称
	 */
	private String providerName;
	
	/**
	 * 虚拟机模板的颗数
	 */
	private int numberOfSockets;
	
	/**
	 * 虚拟机模板的核数
	 */
	private int coresPerSocket;
	
	/**
	 * 虚拟机模板内存
	 */
	private int vmMemory;
	
	/**
	 * 所有者id
	 */
	private String ownerId;
	
	/**
	 * 区域id
	 */
	private String  zoneId;
	
	/**
	 * 所有权组id
	 */
	private String groupId;
	
	/**
	 * 标签类名称
	 */
	private String categoryName;
	
	/**
	 * 标签名称
	 */
	private String tagName;
	
	/**
	 * logo图片的base64值
	 */
	private String logoBase64;
	
	/**
	 * logo图片的后缀，png或jpg
	 */
	private String logoExtension;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvType() {
		return provType;
	}

	public void setProvType(String provType) {
		this.provType = provType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public int getNumberOfVms() {
		return numberOfVms;
	}

	public void setNumberOfVms(int numberOfVms) {
		this.numberOfVms = numberOfVms;
	}

	public String getVmVlanId() {
		return vmVlanId;
	}

	public void setVmVlanId(String vmVlanId) {
		this.vmVlanId = vmVlanId;
	}

	public String getVmVlanName() {
		return vmVlanName;
	}

	public void setVmVlanName(String vmVlanName) {
		this.vmVlanName = vmVlanName;
	}


	public int getNumberOfSockets() {
		return numberOfSockets;
	}

	public void setNumberOfSockets(int numberOfSockets) {
		this.numberOfSockets = numberOfSockets;
	}

	public int getCoresPerSocket() {
		return coresPerSocket;
	}

	public void setCoresPerSocket(int coresPerSocket) {
		this.coresPerSocket = coresPerSocket;
	}

	public int getVmMemory() {
		return vmMemory;
	}

	public void setVmMemory(int vmMemory) {
		this.vmMemory = vmMemory;
	}

	public String getVmTemplateName() {
		return vmTemplateName;
	}

	public void setVmTemplateName(String vmTemplateName) {
		this.vmTemplateName = vmTemplateName;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getVmTemplateId() {
		return vmTemplateId;
	}

	public void setVmTemplateId(String vmTemplateId) {
		this.vmTemplateId = vmTemplateId;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getLogoBase64() {
		return logoBase64;
	}

	public void setLogoBase64(String logoBase64) {
		this.logoBase64 = logoBase64;
	}

	public String getLogoExtension() {
		return logoExtension;
	}

	public void setLogoExtension(String logoExtension) {
		this.logoExtension = logoExtension;
	}

	public boolean isPlacementAuto() {
		return placementAuto;
	}

	public void setPlacementAuto(boolean placementAuto) {
		this.placementAuto = placementAuto;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getDialogId() {
		return dialogId;
	}

	public void setDialogId(String dialogId) {
		this.dialogId = dialogId;
	}

}
