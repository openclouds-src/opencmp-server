package com.massclouds.opencmp.dto;

import java.util.Map;

public class ReportSchedulesDTO {
	
	public String href;
	
	public String id;
	
	public String name;
	
	public String description;
	
	public boolean enabled;
	
	public String last_run_on;
	
	public String created_on;
	
	public String updated_at;
	
	public Map<Object, Object> filter;
	
	public Map<Object, Object> run_at;  

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<Object, Object> getFilter() {
		return filter;
	}

	public void setFilter(Map<Object, Object> filter) {
		this.filter = filter;
	}

	public Map<Object, Object> getRun_at() {
		return run_at;
	}

	public void setRun_at(Map<Object, Object> run_at) {
		this.run_at = run_at;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getLast_run_on() {
		return last_run_on;
	}

	public void setLast_run_on(String last_run_on) {
		this.last_run_on = last_run_on;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	
}
