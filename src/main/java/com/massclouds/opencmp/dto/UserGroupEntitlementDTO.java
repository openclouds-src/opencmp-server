package com.massclouds.opencmp.dto;

import java.util.List;

public class UserGroupEntitlementDTO {
	
	private Filter filters;
	
	public Filter getFilters() {
		return filters;
	}

	public void setFilters(Filter filters) {
		this.filters = filters;
	}

	@SuppressWarnings("unused")
	private static class Filter {
		private List<Object> managed;

		public List<Object> getManaged() {
			return managed;
		}

		public void setManaged(List<Object> managed) {
			this.managed = managed;
		}

	}

}
