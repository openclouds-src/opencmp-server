package com.massclouds.opencmp.dto;

public class ProviderRedisDTO {
	
	private String id;
	
	private String type;
	
	private String typeName;
	
	private String name;
	
    private String hostname;
	
	private String ipaddress;
	
	private int port;
	
	private String userName;
	
	private String userPassword;
	
	private String historyDBPassword;
	
	private Boolean isVMP;
	
	private String syncUserName;
	
	private String syncPassword;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getHistoryDBPassword() {
		return historyDBPassword;
	}

	public void setHistoryDBPassword(String historyDBPassword) {
		this.historyDBPassword = historyDBPassword;
	}

	public Boolean getIsVMP() {
		return isVMP;
	}

	public void setIsVMP(Boolean isVMP) {
		this.isVMP = isVMP;
	}

	public String getSyncUserName() {
		return syncUserName;
	}

	public void setSyncUserName(String syncUserName) {
		this.syncUserName = syncUserName;
	}

	public String getSyncPassword() {
		return syncPassword;
	}

	public void setSyncPassword(String syncPassword) {
		this.syncPassword = syncPassword;
	}
	
}
