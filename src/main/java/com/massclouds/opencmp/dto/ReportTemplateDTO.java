package com.massclouds.opencmp.dto;

import java.util.List;

public class ReportTemplateDTO {
	
	private String href;
	
	private String id;
	
	private String name;
	
	private String title;
	
	private String created_on;
	
	private List<String> cols;
	
	private List<String> headers;
	
	private List<String> sortby;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public List<String> getCols() {
		return cols;
	}

	public void setCols(List<String> cols) {
		this.cols = cols;
	}

	public List<String> getHeaders() {
		return headers;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	public List<String> getSortby() {
		return sortby;
	}

	public void setSortby(List<String> sortby) {
		this.sortby = sortby;
	}

}
