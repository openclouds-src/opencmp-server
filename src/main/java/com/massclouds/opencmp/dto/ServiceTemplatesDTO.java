package com.massclouds.opencmp.dto;

public class ServiceTemplatesDTO {
	
	public String href;
	
	public String id;
	
	public String name;
	
	public String description;
	
	public String guid;
	
	public String created_at;
	
	public String updated_at;
	
	public String service_type;
	
	public String prov_type;
	
	public String service_template_catalog_id;
	
	public String price;
	
	public PictureDTO picture;

	public ZoneDTO zone;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getProv_type() {
		return prov_type;
	}

	public void setProv_type(String prov_type) {
		this.prov_type = prov_type;
	}

	public String getService_template_catalog_id() {
		return service_template_catalog_id;
	}

	public void setService_template_catalog_id(String service_template_catalog_id) {
		this.service_template_catalog_id = service_template_catalog_id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public PictureDTO getPicture() {
		return picture;
	}

	public void setPicture(PictureDTO picture) {
		this.picture = picture;
	}

	public ZoneDTO getZone() {
		return zone;
	}

	public void setZone(ZoneDTO zone) {
		this.zone = zone;
	}
	
}
