package com.massclouds.opencmp.dto.ansible;

public class AnsibleJobDTO {
	private String id;
	
	private String name;
	
	private String description;
	
	private String job_type;
	
	private String launch_type;
	
	private String status;
	
	private String started;
	
	private String finished;
	
	private String job_template;
	
	private String inventory;
	
	private String inventoryName;
	
	private String project;
	
	private String playbook;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getJob_type() {
		return job_type;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public String getLaunch_type() {
		return launch_type;
	}

	public void setLaunch_type(String launch_type) {
		this.launch_type = launch_type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStarted() {
		return started;
	}

	public void setStarted(String started) {
		this.started = started;
	}

	public String getFinished() {
		return finished;
	}

	public void setFinished(String finished) {
		this.finished = finished;
	}

	public String getJob_template() {
		return job_template;
	}

	public void setJob_template(String job_template) {
		this.job_template = job_template;
	}

	public String getInventory() {
		return inventory;
	}

	public void setInventory(String inventory) {
		this.inventory = inventory;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getPlaybook() {
		return playbook;
	}

	public void setPlaybook(String playbook) {
		this.playbook = playbook;
	}

	public String getInventoryName() {
		return inventoryName;
	}

	public void setInventoryName(String inventoryName) {
		this.inventoryName = inventoryName;
	}
	
}
