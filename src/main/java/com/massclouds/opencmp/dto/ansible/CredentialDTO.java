package com.massclouds.opencmp.dto.ansible;

import java.util.Map;

public class CredentialDTO {
	
	private String id;
	
	private String name;
	
	private String description;
	
	private String credential_type;
	
	private String username;
	
	private String password;
	
	private Map<String, String> inputs;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCredential_type() {
		return credential_type;
	}

	public void setCredential_type(String credential_type) {
		this.credential_type = credential_type;
	}

	public Map<String, String> getInputs() {
		return inputs;
	}

	public void setInputs(Map<String, String> inputs) {
		this.inputs = inputs;
	}
	
}
