package com.massclouds.opencmp.dto.ansible;

public class JobTemplateDTO {
	
	private String id;
	
	private String name;
	
	private String description;
	
	private String created;
	
	private String job_type;
	
	private int inventory;
	
	private int project;
	
	private String playbook;
	
	private String organization;
	
	private String status;
	
	private String last_job_run;
	
	private String last_job_failed;
	
    private String inventoryName;
	
	private String projectName;
	
	private String userName;
	
	private String password;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getJob_type() {
		return job_type;
	}
	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}
	public int getInventory() {
		return inventory;
	}
	public void setInventory(int inventory) {
		this.inventory = inventory;
	}
	public int getProject() {
		return project;
	}
	public void setProject(int project) {
		this.project = project;
	}
	public String getPlaybook() {
		return playbook;
	}
	public void setPlaybook(String playbook) {
		this.playbook = playbook;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLast_job_run() {
		return last_job_run;
	}
	public void setLast_job_run(String last_job_run) {
		this.last_job_run = last_job_run;
	}
	public String getLast_job_failed() {
		return last_job_failed;
	}
	public void setLast_job_failed(String last_job_failed) {
		this.last_job_failed = last_job_failed;
	}
	public String getInventoryName() {
		return inventoryName;
	}
	public void setInventoryName(String inventoryName) {
		this.inventoryName = inventoryName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
