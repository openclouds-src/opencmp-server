package com.massclouds.opencmp.dto.ansible;

public class ProjectDTO {
	
	private String id;
	
	private String name;
	
	private String description;
	
	private String local_path;
	
	private String scm_type;
	
	private String last_job_run;
	
	private Boolean last_job_failed;
	
	private String status;
	
	private Boolean allow_override;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocal_path() {
		return local_path;
	}

	public void setLocal_path(String local_path) {
		this.local_path = local_path;
	}

	public String getScm_type() {
		return scm_type;
	}

	public void setScm_type(String scm_type) {
		this.scm_type = scm_type;
	}

	public String getLast_job_run() {
		return last_job_run;
	}

	public void setLast_job_run(String last_job_run) {
		this.last_job_run = last_job_run;
	}

	public Boolean getLast_job_failed() {
		return last_job_failed;
	}

	public void setLast_job_failed(Boolean last_job_failed) {
		this.last_job_failed = last_job_failed;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getAllow_override() {
		return allow_override;
	}

	public void setAllow_override(Boolean allow_override) {
		this.allow_override = allow_override;
	}

}
