package com.massclouds.opencmp.dto.ansible;

import java.util.List;

public class InventoryDTO {
	
	private String id;
	
	private String name;
	
	private String description;
	
	private String created;
	
	private int total_hosts;
	
	private int total_groups;
	
	private Boolean has_active_failures;
	
	private List<HostDTO> hosts;
	
	private String variables;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getTotal_hosts() {
		return total_hosts;
	}

	public void setTotal_hosts(int total_hosts) {
		this.total_hosts = total_hosts;
	}

	public int getTotal_groups() {
		return total_groups;
	}

	public void setTotal_groups(int total_groups) {
		this.total_groups = total_groups;
	}

	public Boolean getHas_active_failures() {
		return has_active_failures;
	}

	public void setHas_active_failures(Boolean has_active_failures) {
		this.has_active_failures = has_active_failures;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public List<HostDTO> getHosts() {
		return hosts;
	}

	public void setHosts(List<HostDTO> hosts) {
		this.hosts = hosts;
	}

	public String getVariables() {
		return variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

}
