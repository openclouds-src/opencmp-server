package com.massclouds.opencmp.dto;

public class VMPTerminalsDTO {
	
	private String id;
	
	private String ip;
	
	private String mac;
	
	private String terminal_version;
	
	private String terminal_type;
	
	private String cpu_type;
	
	private String system_type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getTerminal_version() {
		return terminal_version;
	}

	public void setTerminal_version(String terminal_version) {
		this.terminal_version = terminal_version;
	}

	public String getTerminal_type() {
		return terminal_type;
	}

	public void setTerminal_type(String terminal_type) {
		this.terminal_type = terminal_type;
	}

	public String getCpu_type() {
		return cpu_type;
	}

	public void setCpu_type(String cpu_type) {
		this.cpu_type = cpu_type;
	}

	public String getSystem_type() {
		return system_type;
	}

	public void setSystem_type(String system_type) {
		this.system_type = system_type;
	}
	
}
