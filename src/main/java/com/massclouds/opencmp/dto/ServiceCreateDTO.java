package com.massclouds.opencmp.dto;

public class ServiceCreateDTO {
	
	private String service_catalogs_id;
	
	private String service_name;
	
	private String vm_name;
	
	private String reason;
	
	private String number_of_sockets;
	
	private String cores_per_socket;
	
	private String vm_memory;
	
	private int memory_limit;
	
	private int memory_reserve;
	
	private String href;
	
	public String getService_catalogs_id() {
		return service_catalogs_id;
	}

	public void setService_catalogs_id(String service_catalogs_id) {
		this.service_catalogs_id = service_catalogs_id;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public String getVm_name() {
		return vm_name;
	}

	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getNumber_of_sockets() {
		return number_of_sockets;
	}

	public void setNumber_of_sockets(String number_of_sockets) {
		this.number_of_sockets = number_of_sockets;
	}

	public String getCores_per_socket() {
		return cores_per_socket;
	}

	public void setCores_per_socket(String cores_per_socket) {
		this.cores_per_socket = cores_per_socket;
	}

	public String getVm_memory() {
		return vm_memory;
	}

	public void setVm_memory(String vm_memory) {
		this.vm_memory = vm_memory;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public int getMemory_limit() {
		return memory_limit;
	}

	public void setMemory_limit(int memory_limit) {
		this.memory_limit = memory_limit;
	}

	public int getMemory_reserve() {
		return memory_reserve;
	}

	public void setMemory_reserve(int memory_reserve) {
		this.memory_reserve = memory_reserve;
	}

}
