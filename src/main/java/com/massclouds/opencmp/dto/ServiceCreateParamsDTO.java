package com.massclouds.opencmp.dto;

public class ServiceCreateParamsDTO {
	
	private String serviceCatalogsId;
	
	private String serviceName;
	
	private String vmName;
	
	private String reason;
	
	private String numberOfSockets;
	
	private String coresPerSocket;
	
	private String vmMemory;
	
	private String href;

	public String getServiceCatalogsId() {
		return serviceCatalogsId;
	}

	public void setServiceCatalogsId(String serviceCatalogsId) {
		this.serviceCatalogsId = serviceCatalogsId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getVmName() {
		return vmName;
	}

	public void setVmName(String vmName) {
		this.vmName = vmName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getNumberOfSockets() {
		return numberOfSockets;
	}

	public void setNumberOfSockets(String numberOfSockets) {
		this.numberOfSockets = numberOfSockets;
	}

	public String getCoresPerSocket() {
		return coresPerSocket;
	}

	public void setCoresPerSocket(String coresPerSocket) {
		this.coresPerSocket = coresPerSocket;
	}

	public String getVmMemory() {
		return vmMemory;
	}

	public void setVmMemory(String vmMemory) {
		this.vmMemory = vmMemory;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

}
