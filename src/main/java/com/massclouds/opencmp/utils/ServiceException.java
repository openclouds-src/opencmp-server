/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.utils;

/**
 * @Description: 业务异常
 *
 * @date: 2018年11月8日 下午5:28:04
 * @author li_ming 
 */
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 4256579518374770492L;

	/**
	 * Creates a new instance of ServiceException. 
	 * 
	 */
	public ServiceException() {
		super();
	}

	/**
	 * Creates a new instance of ServiceException. 
	 * 
	 * @param message
	 * @param t
	 */
	public ServiceException(String message, Throwable t) {
		super(message, t);
	}

	/**
	 * Creates a new instance of ServiceException. 
	 * 
	 * @param message
	 */
	public ServiceException(String message) {
		super(message);
	}
}
