package com.massclouds.opencmp.utils;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
*
* 设置跨域请求
*/
@Configuration
public class CorsConfig implements WebMvcConfigurer {

	//配置允许跨域
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
        		.allowCredentials(true)
        		.allowedHeaders("*")
//        		.allowedOrigins("*")
        		.allowedOriginPatterns("*")
        		.allowedMethods("*");
	}

	//添加拦截器，token不能为空
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new OpenCMPInterceptor())
        .addPathPatterns("/**")       //拦截项目中的哪些请求
//        .excludePathPatterns("/login", "/dashboard//*");  //对项目中的哪些请求不拦截，Dashboard模块的接口不拦截
		.excludePathPatterns("/login");  //对项目中的哪些请求不拦截，Dashboard模块的接口不拦截
		
		//自动化、监控告警模块需要验证token
		registry.addInterceptor(new VaildTokenInterceptor())
        .addPathPatterns("/ansible/**", "/zabbix/**", "/dashboard//*");      //拦截项目中的哪些请求
        
	}

}
