package com.massclouds.opencmp.utils;

import java.util.HashMap;
import java.util.Map;

public class TriggerZHContantsUtils {
	
	/**
	 * zabbix中模板的触发器的内容的中文翻译
	 */
	public static Map<String, String> trigger_zh = new HashMap<>();
	
	static {

		trigger_zh.put("22992", "Zabbix代理不可用（持续了3分钟）");
		trigger_zh.put("22991", "高内存利用率（超过90%，持续了5分钟）");
		trigger_zh.put("22990", "内存不足（{ITEM.VALUE2}<20M）");
		trigger_zh.put("22989", "/etc/passwd 被修改");
		trigger_zh.put("22988", "主机{HOST.NAME}已重启（正常运行时间< 10m）");
		trigger_zh.put("22987", "交换分区使用率过高（可用小于20%）");
		trigger_zh.put("22986", "操作系统描述已更改");
		trigger_zh.put("22985", "系统时间不同步（与Zabbix服务器>60s）");
		trigger_zh.put("22984", "系统名称已更改（新名称：{ITEM.VALUE}）");
		trigger_zh.put("22983", "高CPU利用率(超过90%，持续了5分钟）");
		trigger_zh.put("22982", "平均负载过高（每个CPU负载超过1.5。持续了5分钟）");
		trigger_zh.put("22981", "接近进程限制(超过80%使用)");
		trigger_zh.put("22980", "配置的最大进程数太低（<1024）");
		trigger_zh.put("22979", "配置的打开文件描述符的最大数目太低（<256）");
		
		trigger_zh.put("22754", "Zabbix代理不可用（持续了3分钟）");
		trigger_zh.put("22753", "高内存利用率（超过90%，持续了5分钟）");
		trigger_zh.put("22752", "主机已重启（正常运行时间< 10m）");
		trigger_zh.put("22751", "交换分区使用率过高（可用小于20%）");
		trigger_zh.put("22750", "系统时间不同步（与Zabbix服务器>60s）");
		trigger_zh.put("22749", "系统名称已更改（新名称：{ITEM.VALUE}）");
		trigger_zh.put("22748", "高CPU利用率（超过90%，持续了5分钟）");
		trigger_zh.put("22747", "CPU队列长度过高（超过3，持续了5分钟）");
		trigger_zh.put("22746", "CPU特权时间过高（超过30%，持续了5分钟）");
		trigger_zh.put("22745", "CPU中断时间过高（超过50%，持续了5分钟）");
		trigger_zh.put("22744", "内存页/秒过高（超过了1000，持续了5分钟）");
		trigger_zh.put("22743", "空闲的系统页表条目太少了(少于5000，持续了5分钟）");

	}

}
