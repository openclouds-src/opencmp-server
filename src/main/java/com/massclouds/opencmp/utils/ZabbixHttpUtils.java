package com.massclouds.opencmp.utils;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.massclouds.opencmp.common.redis.RedisUtil;


/**
 * @Description: openCMP调用Zabbix接口的工具类
 * 
 * @date: 2022年6月23日 下午07:26:24
 * @author 
 */

@Component
public class ZabbixHttpUtils {
	
    @Autowired
    private RestTemplate restTemplate;

    private static ZabbixHttpUtils zabbixHttpUtils;
    
    @Autowired
    private RedisUtil redisUtil;

    @PostConstruct
    public void init(){
    	zabbixHttpUtils = this;
    	zabbixHttpUtils.restTemplate = this.restTemplate;
    	zabbixHttpUtils.redisUtil = this.redisUtil;
    }
   
   public static <T> ResponseEntity<T> httpRequest(String apiMethod, Object params, ParameterizedTypeReference<T> responseType) throws Exception{
     	//设置Http的Header
     	HttpHeaders header = new HttpHeaders();
     	header.set(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
     	header.set(HttpHeaders.ACCEPT, "application/json");
     	
     	String accessUrl = (String) zabbixHttpUtils.redisUtil.get("zabbix.ip");
     	String token = (String) zabbixHttpUtils.redisUtil.get("zabbix.token");
     	
     	HashMap<String, Object> apiParams = new HashMap<String, Object>();
     	apiParams.put("jsonrpc", "2.0");
     	apiParams.put("id", "1");
     	
     	apiParams.put("method", apiMethod);
     	apiParams.put("params", params);
     	
     	if (apiMethod.equals("user.login")) {//登录时，token为null
     		apiParams.put("auth", null);
     	} else {
     		apiParams.put("auth", token);
     	}
     	//所有的Zabbix的接口都是post
     	ResponseEntity<T> result = zabbixHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.POST, new HttpEntity<>(apiParams, header), responseType);
     	return result;
         
   }
   
}
