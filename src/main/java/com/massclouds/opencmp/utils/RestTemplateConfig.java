package com.massclouds.opencmp.utils;

import java.nio.charset.StandardCharsets;

import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
	
@Bean
public RestTemplate httpsRestTemplate(HttpComponentsClientHttpRequestFactory httpsFactory) {
    RestTemplate restTemplate = new RestTemplate(httpsFactory);
    restTemplate.setErrorHandler(
        new ResponseErrorHandler() {
          @Override
          public boolean hasError(ClientHttpResponse clientHttpResponse) {
            return false;
          }
 
          @Override
          public void handleError(ClientHttpResponse clientHttpResponse) {
            // 默认处理非200的返回，会抛异常
          }
        });
    
//    restTemplate.getMessageConverters().set(1,new StringHttpMessageConverter(StandardCharsets.UTF_8));
    
//    List<HttpMessageConverter<?>> httpMessageConverters = restTemplate.getMessageConverters();
//    httpMessageConverters.stream().forEach(httpMessageConverter -> {
//        if(httpMessageConverter instanceof StringHttpMessageConverter){
//            StringHttpMessageConverter messageConverter = (StringHttpMessageConverter) httpMessageConverter;
//            //设置编码为UTF-8
//            messageConverter.setDefaultCharset(Charset.forName("UTF-8"));
//        }
//    });
    
    StringHttpMessageConverter t = new StringHttpMessageConverter(); //设置为false就可以修改header中的accept-charset属性 
    t.setWriteAcceptCharset(false); 
    t.setDefaultCharset(StandardCharsets.UTF_8); 
    restTemplate.getMessageConverters().add(0,t); 
    
    return restTemplate;
}
 
  @Bean(name = "httpsFactory")
  public HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory()
      throws Exception {
    CloseableHttpClient httpClient = HttpClientUtils.acceptsUntrustedCertsHttpClient();
    HttpComponentsClientHttpRequestFactory httpsFactory =
        new HttpComponentsClientHttpRequestFactory(httpClient);
    httpsFactory.setReadTimeout(40000);
    httpsFactory.setConnectTimeout(40000);
    return httpsFactory;
  }

}
