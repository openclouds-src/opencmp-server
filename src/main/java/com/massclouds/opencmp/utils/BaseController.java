package com.massclouds.opencmp.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {
	
	@Autowired
	public HttpServletRequest request;
	
	/*
	 * 获取request的header中的token值
	 */
	public String getToken() {
		
		return request.getHeader("X-Auth-Token");
		
	}

}
