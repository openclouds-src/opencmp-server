package com.massclouds.opencmp.utils;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.common.redis.RedisUtil;
import com.massclouds.opencmp.dto.DashboardEntityCountDTO;
import com.massclouds.opencmp.dto.DashboardPlatformCountDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsOnlineDTO;
import com.massclouds.opencmp.dto.DashboardTerminalStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.ManageIQTokenDTO;
import com.massclouds.opencmp.dto.Vms6SaveToRedisDTO;
import com.massclouds.opencmp.dto.ZoneDTO;
import com.massclouds.opencmp.service.DashboardService;
import com.massclouds.opencmp.service.ProvidersService;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;


/**
 * @Description: springboot服务启动后初始化ManageIQ的连接，把相应的token放入redis
 * 
 * @date: 2022年3月7日 下午14:26:24
 * @author hou_jjing
 */

@Component
@EnableScheduling   // 开启定时任务的配置
@EnableAsync
public class InitManageIQConnect implements ApplicationRunner{
	
    private static Logger logger = LoggerFactory.getLogger(InitManageIQConnect.class);
    
    @Autowired
	private DashboardService dashboardService;
    
    @Autowired
	private ProvidersService providersService;
    
    @Autowired
    private RedisUtil redisUtil;

	@Override
	public void run(ApplicationArguments args){
		try {
			readConfigValue();
			initManageIQToken();
			initZabbixToken();
			initDashboardInfo();
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("openCMP初始化数据失败！");
		}
		
	}
	
	// 更新ManageIQ平台的token，服务启动后延迟50分钟执行第一次定时任务,每次任务结束后50 分钟再次启动
    @Scheduled(initialDelay = 1000*60*50, fixedDelay = 1000*60*50)
    public void dedesktopCloudTokenDelay(){
    	try {
    		initManageIQToken();
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("更新ManageIQ平台的token失败！");
		}
    	
    }
    
    //大屏调用接口时，token在半夜长时间不用时会过期，所以添加定时任务每隔15分钟调用一次token，以保证token长时间不用扔在规定的有效期内
    @Scheduled(initialDelay = 1000*60*60, fixedDelay = 1000*60*15)
    public void testToken(){
    	try {
    		testTokenW();
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("更新ManageIQ平台的token失败！testTokenW()");
		}
    	
    }
    
    //定时更新zabbix的token，每隔50分钟更新一次
    @Scheduled(initialDelay = 1000*60*60, fixedDelay = 1000*60*50)
    public void zabbixTokenDelay() {
    	try {
    		initZabbixToken();
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("定时更新zabbix的token失败！");
		}
    }
    
    //定时更新Dashboard页面的数据，每隔5分钟更新一次
    @Scheduled(initialDelay = 1000*60*5, fixedDelay = 1000*60*5)
    public void dashboardInfoDelay() {
    	try {
    		initDashboardInfo();
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("定时更新Dashboard页面的数据失败！");
		}
    }
    
    //定时刷新已接管的所有平台，每隔30分钟更新一次
    @Scheduled(initialDelay = 1000*60*30, fixedDelay = 1000*60*30)
    public void refreshAllProvidersDelay() {
    	logger.info("定时更新已接管的所有平台      开始！");
    	ResultDTO result = providersService.refreshAll(redisUtil.get("manageIQ.token").toString());
    	if(200 != result.getCode()) {
    		logger.error("定时更新已接管的所有平台失败！");
    	}
    	logger.info("定时更新已接管的所有平台      结束！");
    }
    
    private void testTokenW () {
    	String  attributes = "";
		String url = "/api/zones/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=description!=Maintenance Zone";
		
		try {
			ResponseEntity<MiqListReturnDTO<ZoneDTO>> zoneRes = ManageIQHttpUtils.httpGetRequest(url, 
					new ParameterizedTypeReference<MiqListReturnDTO<ZoneDTO>>(){}, redisUtil.get("manageIQ.token").toString());
//			logger.info("into testTokenW token = " + redisUtil.get("manageIQ.token").toString());
//			logger.info("into testTokenW status = " + zoneRes.getStatusCodeValue());
		}catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    //初始化配置文件中的值：读取配置文件中的值，放入redis
    private void readConfigValue() throws Exception  {
        logger.info("****************读取配置文件值   start**********************");
    	
//    	 List<String> lines = Files.readAllLines(Paths.get("D:\\OpenCMP\\OpenCMP_config.conf"), StandardCharsets.UTF_8);
    	 List<String> lines = Files.readAllLines(Paths.get("/etc/opencmp/opencmp.conf"), StandardCharsets.UTF_8);
    	 for(String line : lines){
     		if (line != null) {
     			logger.info(line);
     			String[] items = line.split("=");
     		    redisUtil.set(items[0].trim(), items[1].trim());
     		}
     	  }
    	  logger.info("****************读取配置文件值   end**********************");
    }
    
    //服务启动后初始化所有被接管平台的token，大屏调接口时使用token
    private void initManageIQToken() throws Exception {
    	logger.info("****************更新ManageIQ的TOKEN   start**********************");
    	
    	  String manageIQUrl = redisUtil.get("manageIQ.ip").toString();
   	      String userName = redisUtil.get("manageIQ.userName").toString();
   	      String password = redisUtil.get("manageIQ.password").toString();
    	  String loginUrl = manageIQUrl + "/api/auth?requester_type=ui";
    	  
    	  logger.info("manageIQ loginUrl==" + loginUrl);
    	  logger.info("manageIQ userName==" + userName);
    	  logger.info("manageIQ password==" + password);
    	  
    	  HashMap<String, Object> parms = new HashMap<String, Object>();
    	  parms.put("manageIQ.userName", userName);
    	  parms.put("manageIQ.password", password);
    	  
    	  ResponseEntity<ManageIQTokenDTO> tokenRes = ManageIQHttpUtils.httpRequest(loginUrl, "GET", parms, new ParameterizedTypeReference<ManageIQTokenDTO>(){}, "");
    	  
    	  if (tokenRes.getStatusCodeValue() == 200) {
    		  redisUtil.set("manageIQ.token", tokenRes.getBody().getAuth_token());
    	  }
    	  logger.info("tokenRes.getStatusCodeValue()==" + tokenRes.getStatusCodeValue());
    	  logger.info("tokenRes.getBody().getAuth_token()==" + tokenRes.getBody().getAuth_token());
    	  logger.info("tokenRes.getBody().getExpires_on()==" + tokenRes.getBody().getExpires_on());
    	  
		logger.info("****************更新ManageIQ的TOKEN   end**********************");
    }
    
//    @Async
//    private void testConnectionManageIQ() {
//    	
//    }
    
    //服务启动后初始化Zabbix的token
    private void initZabbixToken() throws Exception {
    	logger.info("****************更新Zabbix的TOKEN   start**********************");
    	
    	  String loginUrl = redisUtil.get("zabbix.ip").toString();
   	      String userName = redisUtil.get("zabbix.userName").toString();
   	      String password = redisUtil.get("zabbix.password").toString();
    	  
    	  logger.info("Zabbix loginUrl==" + loginUrl);
    	  logger.info("Zabbix userName==" + userName);
    	  logger.info("Zabbix password==" + password);
    	  
    	  HashMap<String, Object> parms = new HashMap<String, Object>();
    	  parms.put("user", userName);
    	  parms.put("password", password);
    	  String apiMethod = "user.login";
    	  ResponseEntity<Map<String, Object>> tokenRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, new ParameterizedTypeReference<Map<String, Object>>(){});
    	  
    	  if (tokenRes.getStatusCodeValue() == 200) {
    		  redisUtil.set("zabbix.token", tokenRes.getBody().get("result"));
    	  }
    	  logger.info("tokenRes.getStatusCodeValue()==" + tokenRes.getStatusCodeValue());
    	  logger.info("tokenRes.getBody().getAuth_token()==" + tokenRes.getBody().get("result"));
    	  
		logger.info("****************更新Zabbix的TOKEN   end**********************");
    }
    
    //初始化Dashboard的接口，并定时更新数据
    private void initDashboardInfo() throws Exception {
    	logger.info("****************更新Dashboard数据   start**********************");
    	
    	String token = redisUtil.get("manageIQ.token").toString();
    	ObjectMapper objectMapper = new ObjectMapper();
    	DataResultDTO<DashboardPlatformCountDTO> platformCount = dashboardService.platformCount(token);
    	if (200 == platformCount.getCode()) {
    		String platformCountJsonString = objectMapper.writeValueAsString(platformCount.getResult());
			redisUtil.set("platformCount", platformCountJsonString);
    	}
    			
    	DataResultDTO<DashboardEntityCountDTO> entityCount = dashboardService.entityCount(token);
    	if (200 == entityCount.getCode()) {
    		String entityCountJsonString = objectMapper.writeValueAsString(entityCount.getResult());
			redisUtil.set("entityCount", entityCountJsonString);
    	}
    	
    	DataResultDTO<DashboardStatisticsByTypeDTO> statisticsByType = dashboardService.statisticsByType(token);
    	if (200 == statisticsByType.getCode()) {
    		String statisticsByTypeJsonString = objectMapper.writeValueAsString(statisticsByType.getResult());
			redisUtil.set("statisticsByType", statisticsByTypeJsonString);
    	}
    	
    	DataResultDTO<DashboardStatisticsOnlineDTO> statisticsOnline = dashboardService.statisticsOnline(token);
    	if (200 == statisticsOnline.getCode()) {
    		String statisticsOnlineJsonString = objectMapper.writeValueAsString(statisticsOnline.getResult());
			redisUtil.set("statisticsOnline", statisticsOnlineJsonString);
    	}
    	
    	DataResultDTO<List<Vms6SaveToRedisDTO>> list6VmsServer = dashboardService.list6Vms("server", token);
    	if (200 == list6VmsServer.getCode()) {
    		String vms6OfServerString = objectMapper.writeValueAsString(list6VmsServer.getResult());
    		redisUtil.set("vms6OfServer", vms6OfServerString);
    	}
    	
    	DataResultDTO<List<Vms6SaveToRedisDTO>> list6VmsDesktop = dashboardService.list6Vms("desktop", token);
    	if (200 == list6VmsDesktop.getCode()) {
    		String vms6OfDesktopString = objectMapper.writeValueAsString(list6VmsDesktop.getResult());
    		redisUtil.set("vms6OfDesktop", vms6OfDesktopString);
    	}
    	
    	DataResultDTO<DashboardTerminalStatisticsByTypeDTO> terminalType = dashboardService.statisticsTerminalsByType(token);
		if (200 == terminalType.getCode()) {
    		String terminalTypeString = objectMapper.writeValueAsString(terminalType.getResult());
    		redisUtil.set("terminalStatisticsByType", terminalTypeString);
    	}
    	
    	logger.info("****************更新Dashboard数据   end**********************");
    }
   
	  
}
