package com.massclouds.opencmp.utils;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.massclouds.opencmp.dto.ReportResultsDTO;

public class PDFUtils {
public void createPDF(ReportResultsDTO reportResultsDTO, HttpServletResponse response) {
		
		Document document = new Document(PageSize.A4);
		
		PdfPCell cell = null;
		try {
			//加载字体文件
            BaseFont bf =  BaseFont.createFont("/font/simfang.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            Font font = new Font(bf);
            
            PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
			
			//添加pdf的标题
			String title = reportResultsDTO.getName() + "  " + DateFormatUtils.utcToBeijing(reportResultsDTO.getCreated_on());
            Font titleFont = new Font(bf, 16F, 1);
			Paragraph  paragraph = new Paragraph(title, titleFont);
            paragraph.setAlignment(Paragraph.ALIGN_CENTER);//居中
//            paragraph.setSpacingBefore(10f);//上间距
            paragraph.setSpacingAfter(10f);//下间距
	        document.add(paragraph);
			
	        //获取报表数据的列数、列名、相应字段值；
			List<String> cellHeader = reportResultsDTO.getReport().getHeaders();
			List<String> cellKey = reportResultsDTO.getReport().getCols();
			int cellCount = cellHeader.size();
			List<Map<Object, Object>> resultInfo = reportResultsDTO.getResult_set();
		    
		    //添加表格及其数据
	    	PdfPTable table = new PdfPTable(cellCount);//创建表
	    	table = setTableHeader(table, reportResultsDTO.getReport().getHeaders(), font);//设置table的header
	    	for (Map<Object, Object> item : resultInfo) {
		    	for (String key : cellKey) {
		    		String value = item.get(key) == null ? null : item.get(key).toString();
		    		cell = new PdfPCell(new Phrase(value));
		    		table.addCell(cell);
		    	}
	    	}
	    	document.add(table);
            //document.add(Chunk.NEWLINE);
	    	document.add(table);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			document.close();
		}
		
	}
    //每页指定了行数，如果每列数据太长自动换行后，每页的数据会乱，先屏蔽
	/*public void createPDF(ReportResultsDTO reportResultsDTO, HttpServletResponse response) {
		
		Document document = new Document(PageSize.A4);
		
		PdfPCell cell = null;
		try {
			//加载字体文件
            BaseFont bf =  BaseFont.createFont("/font/simfang.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            Font font = new Font(bf);
            
            PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
			
			//添加pdf的标题
			String title = reportResultsDTO.getName() + "  " + DateFormatUtils.utcToBeijing(reportResultsDTO.getCreated_on());
            Font titleFont = new Font(bf, 16F, 1);
			Paragraph  paragraph = new Paragraph(title, titleFont);
            paragraph.setAlignment(Paragraph.ALIGN_CENTER);//居中
//          paragraph.setSpacingBefore(10f);//上间距
            paragraph.setSpacingAfter(10f);//下间距
	        document.add(paragraph);
			
	        //获取报表数据的列数、列名、相应字段值；pdf中第一页因为有标题占位，所以第一页放16层数据，第二页开始每页放18条数据
			List<String> cellHeader = reportResultsDTO.getReport().getHeaders();
			List<String> cellKey = reportResultsDTO.getReport().getCols();
			int cellCount = cellHeader.size();
			int firstPageSize = 16;//第一页放16条数据
			int otherPageSize = 18;//从第二页开始每页放18条数据
			int allCount = reportResultsDTO.getResult_set().size(); //数据总数
			List<Map<Object, Object>> firstPageInfo = null;
			List<Map<Object, Object>> nextPagesInfo = null;
		    if (allCount >= firstPageSize) {
		    	firstPageInfo = reportResultsDTO.getResult_set().subList(0, firstPageSize);
		    	nextPagesInfo = reportResultsDTO.getResult_set().subList(firstPageSize, allCount);
		    } else {
		    	firstPageInfo = reportResultsDTO.getResult_set();
		    }
		    
		    //添加第一页的表格及其数据
	    	PdfPTable firstPageTable = new PdfPTable(cellCount);//创建表
	    	firstPageTable = setTableHeader(firstPageTable, reportResultsDTO.getReport().getHeaders(), font);//设置table的header
	    	for (Map<Object, Object> item : firstPageInfo) {
		    	for (String key : cellKey) {
		    		String value = item.get(key) == null ? null : item.get(key).toString();
		    		cell = new PdfPCell(new Phrase(value));
		    		firstPageTable.addCell(cell);
		    	}
	    	}
	    	document.add(firstPageTable);
            //document.add(Chunk.NEWLINE);
	    	
	    	//添加后面页面的表格及其数据
            document.newPage();
	    	PdfPTable table = new PdfPTable(cellCount); //在下一页中添加表格
	    	int n = 0; //计数，每页18条数据
    		table = setTableHeader(table, reportResultsDTO.getReport().getHeaders(), font);//设置table的header
	    	for(int i = 0; i < nextPagesInfo.size(); i++) {
		    	Map<Object, Object> map = nextPagesInfo.get(i);
		    	//添加数据
		    	for (String key : cellKey) {
		    		String value = map.get(key) == null ? null : map.get(key).toString();
		    		cell = new PdfPCell(new Phrase(value));
			    	table.addCell(cell);
		    	}
		    	n++;
		    	if (n % otherPageSize == 0) {
		    		document.add(table);
		    		document.newPage();
		    		table = new PdfPTable(cellCount); //在下一页中添加表格
		    		table = setTableHeader(table, reportResultsDTO.getReport().getHeaders(), font);//设置table的header
		    	}
		    	
		    }
	    	document.add(table);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			document.close();
		}
		
	}*/
	
	public PdfPTable setTableHeader(PdfPTable table, List<String> tableHeaders, Font font) {
		 table.setWidthPercentage(100);
		 table.getDefaultCell().setPadding(3);
		 table.getDefaultCell().setBorderWidth(3);
		 table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		 int cellHeight = 30;
	     for (String headerTitle : tableHeaders) {
	    	PdfPCell cell = new PdfPCell(new Phrase(headerTitle, font));
//	    	cell.setBackgroundColor(new BaseColor(0xC0,0xC0,0xC0));
	        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);//标题头背景灰色
	    	cell.setFixedHeight(cellHeight);
	    	table.addCell(cell);
	    }
	     return table;
	}
}
