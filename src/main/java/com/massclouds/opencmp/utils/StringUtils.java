package com.massclouds.opencmp.utils;

import java.text.DecimalFormat;

public class StringUtils {
	
	public static boolean isEmpty(String str) {
		
		if (str == null || "".equals(str)) {
			return true;
		} else {
			return false;
		}
	}
	
	// 转换文件大小
	public static String formetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "K";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "G";
        }
        return fileSizeString;
    }

}
