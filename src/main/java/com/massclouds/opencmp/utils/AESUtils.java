/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * @Description: 对称加密工具类
 *
 * @date: 2018年11月8日 下午4:15:56
 * @author li_ming 
 */
public class AESUtils {
	/**
	 * @Description: 密钥
	 */
	private static final String KEY = "aes-mass2018year";

	/**
	 * @Description: 获取密钥 
	 *
	 * @param key 密钥
	 * @return 初始化后的密钥 
	 * @throws NoSuchAlgorithmException 
	 */
	public static byte[] initKey(String key) throws NoSuchAlgorithmException {
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
		secureRandom.setSeed(key.getBytes());
		kgen.init(128, secureRandom);
		SecretKey secretKey = kgen.generateKey();
		byte[] enCodeFormat = secretKey.getEncoded();
		return enCodeFormat;
	}

	/**
	 * @Description: AES加密 
	 *
	 * @param data 需要加密的数据
	 * @return 加密后的数据
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
	public static String encrypt(String data) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		StringBuffer sb = new StringBuffer();
		byte[] buf = encrypt(data.getBytes(), initKey(KEY));
		for (int i = 0; i < buf.length; i++) {
			String hex = Integer.toHexString(buf[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}

	/**
	 * @Description: AES加密 
	 *
	 * @param data 需要加密的数据
	 * @param key 加密使用的密钥 
	 * @return 加密后获取的字节数组 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static byte[] encrypt(byte[] data, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		SecretKey secretKey = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		return cipher.doFinal(data);
	}

	/**
	 * @Description: AES解密 
	 *
	 * @param data 要解密的数据
	 * @return 解密后的数据
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 */
	public static String decrypt(String data) throws NoSuchAlgorithmException, InvalidKeyException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		if (data.length() < 1) {
			return null;
		}
		byte[] result = new byte[data.length() / 2];
		for (int i = 0; i < data.length() / 2; i++) {
			int high = Integer.parseInt(data.substring(i * 2, i * 2 + 1), 16);
			int low = Integer.parseInt(data.substring(i * 2 + 1, i * 2 + 2), 16);
			result[i] = (byte) (high * 16 + low);
		}
		return new String(decrypt(result, initKey(KEY)));
	}

	/**
	 * @Description: AES解密
	 *
	 * @param data 要解密的数据
	 * @param key 解密所使用的密钥
	 * @return 解密后的字节数组
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static byte[] decrypt(byte[] data, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		SecretKey secretKey = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		return cipher.doFinal(data);
	}

	public static void main(String[] args) throws Exception {
		System.out.println(decrypt("3F327391B4A6FEF6EB16E362E4B811EF75F4CC62EB9B235B50148A95621E8FB1"));
	}
}
