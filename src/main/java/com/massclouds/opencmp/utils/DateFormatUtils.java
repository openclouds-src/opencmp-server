package com.massclouds.opencmp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFormatUtils {
	
	/**
	 * 把UTC时间 格式化成 北京时间
	 * 过滤时间字段中的时区，把日志格式化成：yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static String utcToBeijing(String dateStr) throws ParseException {
		try {
			
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	        Date date = sdf.parse(dateStr);
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR)+8);
	        
	        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        String date2 = simpleDateFormat.format(calendar.getTime());
	        return date2;
		}catch(Exception e) {
			e.printStackTrace();
			return dateStr;
		}
	}
	
	public static String getCurrentDate() {
		//使用Calendar 获取当前日期和时间
		Calendar calendar = Calendar.getInstance(); 
		//转换格式  使用format将这个日期转换成我们需要的格式
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");  
		return formatter.format(calendar.getTime());
	}
	
	public static void main(String[] args) {
		
		String d = "2022-06-07T10:56:31Z";
		try {
			 Date date = null;
	        // ISO 8601 slightly modified */"yyyy-MM-dd'T'HH:mm:ss'Z'",
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	        // 2019-05-21T08:44:00Z 对应的时间格式 yyyy-MM-dd'T'HH:mm:ss'Z'
	        date = sdf.parse(d);
	        System.out.println("UTC时间："+date);
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        calendar.set(Calendar.HOUR,calendar.get(Calendar.HOUR)+8);
	        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        String date2 = simpleDateFormat.format(calendar.getTime());
	        System.out.println("北京时间："+date2);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
