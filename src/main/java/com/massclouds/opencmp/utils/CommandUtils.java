package com.massclouds.opencmp.utils;

import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandUtils {
	
	 private static Logger logger = LoggerFactory.getLogger(CommandUtils.class);
	 
	 public static Object exec(String cmd) { 
		 String[] cmdA = { "/bin/sh", "-c", cmd };  
		    try {  
		        Process process = Runtime.getRuntime().exec(cmdA);  
		        InputStreamReader ir = new InputStreamReader(process.getInputStream());  
		        LineNumberReader input = new LineNumberReader(ir);  
		        String line;  
		        while ((line = input.readLine()) != null)  
		        	logger.info(line);  
		    } catch (java.io.IOException e) {  
		       e.printStackTrace();
		    }  
        /* try {  
             
             Process process = Runtime.getRuntime().exec(cmdA);  
             LineNumberReader br = new LineNumberReader(new InputStreamReader(process.getInputStream()));  
             StringBuffer sb = new StringBuffer();  
             String line;  
             while ((line = br.readLine()) != null) {  
            	 logger.info(line); 
                 sb.append(line).append("\n");  
             }  
             return sb.toString();  
         } catch (Exception e) {  
             e.printStackTrace();  
         }*/  
         return null;  
     }  
}
