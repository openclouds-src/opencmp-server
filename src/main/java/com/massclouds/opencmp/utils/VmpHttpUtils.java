package com.massclouds.opencmp.utils;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.massclouds.opencmp.common.redis.RedisUtil;

/**
 * @Description: 统一门户调用VMP接口的工具类
 * 
 * @date: 2021年6月26日 下午07:26:24
 * @author hou_jjing
 */

@Component
public class VmpHttpUtils {
	
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RedisUtil redisUtil;
    
    private static VmpHttpUtils vmpHttpUtils;

    @PostConstruct
    public void init(){
    	vmpHttpUtils = this;
    	vmpHttpUtils.restTemplate = this.restTemplate;
    	vmpHttpUtils.redisUtil = this.redisUtil;
    }

    public static <T> ResponseEntity<T> httpRequest(String url, String httpMethod, HashMap<String, Object> params,ParameterizedTypeReference<T> responseType, String token) throws Exception{
      	//设置Http的Header
      	HttpHeaders header = null;
      	String accessUrl = null;
      	if (url.endsWith("/sso/oauth/token")) {
      		
      		header = new HttpHeaders();
      		accessUrl = url;
      		
      	} else {
      		
  	        header = new HttpHeaders();
  	        header.set(HttpHeaders.CONTENT_TYPE, "application/json");
  	        header.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
  	        accessUrl = url;
      	}
      	
      	ResponseEntity<T> result = null;
      	
      	if ("GET".equals(httpMethod.toUpperCase())) {
      		
      		result = vmpHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.GET, new HttpEntity<>(header), responseType);
      		
      	} else if ("POST".equals(httpMethod.toUpperCase())) {
      		
      		result = vmpHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.POST, new HttpEntity<>(params, header), responseType);
      		
      	} else if ("PATCH".equals(httpMethod.toUpperCase())) {
      		
      		result = vmpHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.PATCH, new HttpEntity<>(params, header), responseType);
      		
      	} else if ("DELETE".equals(httpMethod.toUpperCase())) {
      		
      		result = vmpHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.DELETE, new HttpEntity<>(header), responseType);
      		
      	}
      	
        
      	return result;
          
  }
}
