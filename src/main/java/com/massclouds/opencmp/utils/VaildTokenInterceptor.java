package com.massclouds.opencmp.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.massclouds.opencmp.dto.UserRoleDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;

@Component
public class VaildTokenInterceptor implements HandlerInterceptor {
	
	//配置拦截器，token是否过期，自动化、监控告警模块使用
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String token = request.getHeader("X-Auth-Token");
		if (token.endsWith("_LargeScreen")) {//如果是大屏调接口，则不需要验证token
			return true;
		}
		String url = "/api/roles?expand=resources&attributes=id,name" ;
		try {
			ResponseEntity<MiqListReturnDTO<UserRoleDTO>> rolesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserRoleDTO>>(){}, token);
			if (401 == rolesRes.getStatusCodeValue()) {
				response.setStatus(401);
				return false;
 		    }
		}catch(Exception e) {
			e.printStackTrace();
			response.setStatus(401);
			return false;
		}
	
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}

}
