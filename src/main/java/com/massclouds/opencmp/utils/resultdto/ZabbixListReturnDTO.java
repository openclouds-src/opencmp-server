package com.massclouds.opencmp.utils.resultdto;

import java.util.List;
import java.util.Map;

public class ZabbixListReturnDTO<T> {
	
	/*
	 * 版本
	 */
	private String jsonrpc;
	
	/*
	 * 随意的一个值
	 */
	private String id;
	
	/*
	 * 资源信息
	 */
	private List<T> result;
	
	/*
	 * 错误信息
	 */
	private Map<String, Object> error;

	public Map<String, Object> getError() {
		return error;
	}

	public void setError(Map<String, Object> error) {
		this.error = error;
	}

	public String getJsonrpc() {
		return jsonrpc;
	}

	public void setJsonrpc(String jsonrpc) {
		this.jsonrpc = jsonrpc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

}
