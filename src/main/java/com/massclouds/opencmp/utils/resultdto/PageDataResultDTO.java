/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.utils.resultdto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 分页查询结果
 *
 * @date: 2022年04月13日 下午5:41:33
 * @author  
 */
@ApiModel(value = "Rest返回结果")
public class PageDataResultDTO<T> extends ResultDTO {

	private static final long serialVersionUID = -2422264579090088999L;
	 
	private Result result;
	
	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	/**
	 * Creates a new instance of PageDataResultDTO. 
	 * 
	 */
	public PageDataResultDTO() {}

	/**
	 * Creates a new instance of PageDataResultDTO. 
	 * 
	 * @param list
	 * @param totalCount
	 */
	public PageDataResultDTO(T list, Long totalCount) {
		Result result = new Result(list, totalCount);
		this.result = result;
	}

	/**
	 * @Description: 操作成功时赋值
	 *
	 * @param list 查询结果
	 * @param totalCount 总条数
	 */
	public void setSuccess(T list, Long totalCount) {
		Result result = new Result(list, totalCount);
		this.result = result;
		super.setSuccess();
	}


	
	public  class Result{
		/**
		 * @Description: 查询结果List
		 */
		@ApiModelProperty(value = "查询结果List")
		private T items;
		
		/**
		 * @Description: 总条数
		 */
		@ApiModelProperty(value = "总条数")
		private Long total;
		
		public Result(T items, Long total) {
			super();
			this.items = items;
			this.total = total;
		}
		public T getItems() {
			return items;
		}
		public void setItems(T items) {
			this.items = items;
		}
		public Long getTotal() {
			return total;
		}
		public void setTotal(Long total) {
			this.total = total;
		}
		
	}
}
