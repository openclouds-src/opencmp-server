/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.utils.resultdto;

import java.io.Serializable;

import com.massclouds.opencmp.common.utils.JsonUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: Rest返回对象
 *
 * @date: 2022年04月13日 下午5:31:37
 * @author  
 */
@ApiModel(value = "Rest返回结果")
public class ResultDTO implements Serializable {

	private static final long serialVersionUID = -3708933827834030581L;
	/**
	 * @Description: 状态 
	 */
	@ApiModelProperty(value = "状态")
	private int code;
	
	/**
	 * @Description: 提示信息 
	 */
	@ApiModelProperty(value = "提示信息 ")
	private String message;
	
	/**
	 * @Description: 返回结果类型，success、error 
	 */
	@ApiModelProperty(value = "返回结果类型 ")
	private String type;
	
	private String id;
	
	/**
	 * @Description: 操作成功时赋值
	 * 
	 */
	public void setSuccess() {
		this.setCode(StatusEnum.SUCCESS.getCode());
		this.setType(StatusEnum.SUCCESS.getMessage());
		this.setMessage("OK");
	}
	
	/**
	 * @Description: 操作成功时赋值
	 * 
	 */
	public void setSuccess(String message) {
		this.setCode(StatusEnum.SUCCESS.getCode());
		this.setType(StatusEnum.SUCCESS.getMessage());
		this.setMessage(message);
	}

	/**
	 * @Description: 操作失败时赋值
	 * 
	 */
	public void setError() {
		this.setCode(StatusEnum.ERROR.getCode());
		this.setType(StatusEnum.ERROR.getMessage());
	}

	/**
	 * @Description: 操作失败时赋值
	 *
	 * @param message 错误提示
	 */
	public void setError(String message) {
		this.setCode(StatusEnum.ERROR.getCode());
		this.setType(StatusEnum.ERROR.getMessage());
		this.setMessage(message);
	}

	/**
	 * @Description: 操作失败时赋值
	 *
	 * @param status 错误码
	 * @param message 错误提示
	 */
	public void setError(int status, String message) {
		this.setCode(status);
		this.setMessage(message);
		this.setType(StatusEnum.ERROR.getMessage());
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/* 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + code;
		return result;
	}

	/* 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultDTO other = (ResultDTO) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (code != other.code)
			return false;
		return true;
	}

	/* 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return JsonUtils.obj2json(this);
	}

	/**
	 * @Description:  响应状态
	 *
	 * @date: 2018年11月8日 下午5:35:28
	 * @author  
	 */
	public enum StatusEnum {
		SUCCESS(200, "success"),
		ERROR(500, "error"),
		UNAUTHORIZED(401, "未认证!"),
		NOT_FOUND(404, "接口不存在");

		private int code;
		private String message;

		private StatusEnum(int code, String message) {
			this.code = code;
			this.message = message;
		}

		/**
		 * @return the code
		 */
		public int getCode() {
			return code;
		}

		/**
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}
	}
}
