/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.utils.resultdto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: Rest返回对象
 *
 * @date: 2022年04月13日 下午5:36:08
 * @author  
 */
@ApiModel(value = "Rest返回结果")
public class DataResultDTO<T> extends ResultDTO {

	private static final long serialVersionUID = 5926713521711976947L;

	/**
	 * @Description: 响应数据对象 
	 */
	@ApiModelProperty(value = "响应数据对象")
	private T result;

	/**
	 * Creates a new instance of DataResultDTO. 
	 * 
	 */
	public DataResultDTO() {}

	/**
	 * Creates a new instance of DataResultDTO. 
	 * 
	 * @param data
	 */
	public DataResultDTO(T data) {
		this.result = data;
		super.setSuccess();
	}

	/**
	 * @Description: 操作成功时赋值
	 *
	 * @param data 响应数据
	 */
	public void setSuccess(T data) {
		this.setResult(data);
		super.setSuccess();
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

}
