package com.massclouds.opencmp.utils.resultdto;

import java.util.List;

public class AnsibleListReturnDTO<T> {

	/*
	 * 资源总数
	 */
	private Integer count;
	
	/*
	 * 下一页url
	 */
	private String next;
	
	/*
	 * 上一页url
	 */
	private String previous;
	
	/*
	 * 资源信息
	 */
	private List<T> results;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPrevious() {
		return previous;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}
}
