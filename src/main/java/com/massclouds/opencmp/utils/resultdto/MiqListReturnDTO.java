package com.massclouds.opencmp.utils.resultdto;

import java.util.List;

public class MiqListReturnDTO<T> {

	/*
	 * 资源名称
	 */
	private String name;
	
	/*
	 * 资源总数
	 */
	private Integer count;
	
	/*
	 * 每页展示的数量
	 */
	private Integer subcount;
	
	/*
	 * 查询结果总数，有查询条件时可用该字段
	 */
	private Integer subquery_count;
	
	/*
	 * 返回资源信息的页码数
	 */
	private Integer pages;
	
	private List<T> resources;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getSubcount() {
		return subcount;
	}

	public void setSubcount(Integer subcount) {
		this.subcount = subcount;
	}

	public Integer getPages() {
		return pages;
	}

	public void setPages(Integer pages) {
		this.pages = pages;
	}

	public List<T> getResources() {
		return resources;
	}

	public void setResources(List<T> resources) {
		this.resources = resources;
	}

	public Integer getSubquery_count() {
		return subquery_count;
	}

	public void setSubquery_count(Integer subquery_count) {
		this.subquery_count = subquery_count;
	}

}
