/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.utils.resultdto;

import java.io.Serializable;
import java.util.List;

import com.massclouds.opencmp.common.utils.JsonUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "查询条件")
public class QueryDTO implements Serializable {

	private static final long serialVersionUID = 5429552360299311545L;
	
	private String id;
	
	private List<String> ids;
	
	/**
	 * @Description: 当前页数 
	 */
	@ApiModelProperty(value = "当前页数 ")
	private Integer page = 0;
	/**
	 * @Description: 每页条数
	 */
	@ApiModelProperty(value = "每页条数")
	private Integer pageSize = 10;
	/**
	 * @Description: 通用查询条件
	 */
	@ApiModelProperty(value = "通用查询条件")
	private String criteria;
	
	@ApiModelProperty(value = "名称")
	private String name;
	
	@ApiModelProperty(value = "描述")
	private String description;
	
	@ApiModelProperty(value = "排序列名")
	private String sortBy;
	
	@ApiModelProperty(value = "排序规则asc or desc")
	private String sortOrder;

	/**
	 * Creates a new instance of QueryDTO. 
	 * 
	 */
	public QueryDTO() {
		super();
	}

	/**
	 * Creates a new instance of QueryDTO. 
	 * 
	 * @param page
	 * @param size
	 * @param criteria
	 */
	public QueryDTO(Integer page, Integer pageSize, String criteria) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.criteria = criteria;
	}
	
	public QueryDTO(Integer page, Integer pageSize, String criteria, String name) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.criteria = criteria;
		this.name = name;
	}


	public QueryDTO(Integer page, Integer pageSize, String criteria, String name, String description) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.criteria = criteria;
		this.name = name;
		this.description = description;
	}

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the page
	 */
	public Integer getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the criteria
	 */
	public String getCriteria() {
		return criteria;
	}

	/**
	 * @param criteria the criteria to set
	 */
	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((criteria == null) ? 0 : criteria.hashCode());
		result = prime * result + ((pageSize == null) ? 0 : pageSize.hashCode());
		result = prime * result + ((page == null) ? 0 : page.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryDTO other = (QueryDTO) obj;
		if (criteria == null) {
			if (other.criteria != null)
				return false;
		} else if (!criteria.equals(other.criteria))
			return false;
		if (pageSize == null) {
			if (other.pageSize != null)
				return false;
		} else if (!pageSize.equals(other.pageSize))
			return false;
		if (page == null) {
			if (other.page != null)
				return false;
		} else if (!page.equals(other.page))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return JsonUtils.obj2json(this);
	}
}
