package com.massclouds.opencmp.utils;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.massclouds.opencmp.common.redis.RedisUtil;


/**
 * @Description: openCMP调用ansible接口的工具类
 * 
 * @date: 2022年6月14日 下午07:26:24
 * @author hou_jjing
 */

@Component
public class AnsibleHttpUtils {
	
    @Autowired
    private RestTemplate restTemplate;

    private static AnsibleHttpUtils ansibleHttpUtils;
    
    @Autowired
    private RedisUtil redisUtil;

    @PostConstruct
    public void init(){
    	ansibleHttpUtils = this;
    	ansibleHttpUtils.restTemplate = this.restTemplate;
    	ansibleHttpUtils.redisUtil = this.redisUtil;
    }
   
   public static <T> ResponseEntity<T> httpRequest(String url, String httpMethod, HashMap<String, Object> params, ParameterizedTypeReference<T> responseType) throws Exception{
     	//设置Http的Header
     	HttpHeaders header = new HttpHeaders();
     	header.set(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
     	header.set(HttpHeaders.ACCEPT, "application/json");
     	
     	String accessUrl = (String) ansibleHttpUtils.redisUtil.get("ansible.ip");
     	
     	if (url.equals("check")) {
     	//检测第三方组件状态时，传递url值是check
     	} else {
     	   String authStr = ansibleHttpUtils.redisUtil.get("ansible.userName").toString() + ":" + ansibleHttpUtils.redisUtil.get("ansible.password").toString();
     	   header.set(HttpHeaders.AUTHORIZATION, "Basic " + new String(Base64.encodeBase64(authStr.getBytes())));
 	       accessUrl = accessUrl + url;
     	}
     	
     	ResponseEntity<T> result = null;
     	
     	if ("GET".equals(httpMethod.toUpperCase())) {
     		
     		result = ansibleHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.GET, new HttpEntity<>(header), responseType);
     		
     	} else if ("POST".equals(httpMethod.toUpperCase())) {
     		
     		result = ansibleHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.POST, new HttpEntity<>(params, header), responseType);
     		
     	} else if ("PATCH".equals(httpMethod.toUpperCase())) {
     		
     		result = ansibleHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.PATCH, new HttpEntity<>(params, header), responseType);
     		
     	}else if ("PUT".equals(httpMethod.toUpperCase())) {
     		
     		result = ansibleHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.PUT, new HttpEntity<>(params, header), responseType);
     		
     	} else if ("DELETE".equals(httpMethod.toUpperCase())) {
     		
     		result = ansibleHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.DELETE, new HttpEntity<>(header), responseType);
     		
     	}
       
     	return result;
         
   }
   
}
