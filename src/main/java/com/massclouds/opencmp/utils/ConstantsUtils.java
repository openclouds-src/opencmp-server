package com.massclouds.opencmp.utils;

import java.util.HashMap;
import java.util.Map;

public class ConstantsUtils {
	
	/****************不同平台的List格式********************/
	/**
	 * 供应商的类型化  所有
	 */
	public static String[] providerType = {"ManageIQ::Providers::Redhat::InfraManager", 
										   "ManageIQ::Providers::Vmware::InfraManager", 
//										   "ManageIQ::Providers::Openstack::InfraManager", 
										   "ManageIQ::Providers::Microsoft::InfraManager",
										   
										   "ManageIQ::Providers::Amazon::CloudManager",
										   "ManageIQ::Providers::Azure::CloudManager",
										   "ManageIQ::Providers::Openstack::CloudManager",
//										   "ManageIQ::Providers::Vmware::CloudManager",
										   
										   "ManageIQ::Providers::Kubernetes::ContainerManager",
										   "ManageIQ::Providers::Openshift::ContainerManager",
										   
										   "ManageIQ::Providers::Lenovo::PhysicalInfraManager",
										   };
	
	/**
	 * 供应商的类型化   虚拟化平台 ,也叫私有云
	 */
	public static String[] providerTypeInfraManager = {"ManageIQ::Providers::Redhat::InfraManager", 
										               "ManageIQ::Providers::Vmware::InfraManager", 
//										               "ManageIQ::Providers::Openstack::InfraManager", 
										               "ManageIQ::Providers::Microsoft::InfraManager",
													  };
	/**
	 * 供应商的类型化   云平台 ，也叫公有云
	 */
	public static String[] providerTypeCloudManager = {"ManageIQ::Providers::Amazon::CloudManager",
										   			   "ManageIQ::Providers::Azure::CloudManager",
										   			   "ManageIQ::Providers::Openstack::CloudManager",
//										   			   "ManageIQ::Providers::Vmware::CloudManager",
										   			  };
	/**
	 * 供应商的类型化  容器平台
	 */
	public static String[] providerTypeContainerManager = {"ManageIQ::Providers::Kubernetes::ContainerManager",
										                   "ManageIQ::Providers::Openshift::ContainerManager",
										                  };
	/**
	 * 供应商的类型化  物理平台
	 */
	public static String[] providerTypePhysicalInfraManager = { "ManageIQ::Providers::Lenovo::PhysicalInfraManager"};
	
	
	/****************不同平台的Map格式********************/
	/**
	 * 供应商的类型化   虚拟化平台 ,也叫私有云
	 */
	public static Map<String, String> providerTypeInfraManagerMap = new HashMap<>();
	/**
	 * 供应商的类型化   云平台 ，也叫公有云
	 */
	public static Map<String, String> providerTypeCloudManagerMap = new HashMap<>();
	/**
	 * 供应商的类型化  容器平台
	 */
	public static Map<String, String> providerTypeContainerManagerMap = new HashMap<>();
	/**
	 * 供应商的类型化  物理平台
	 */
	public static Map<String, String> providerTypePhysicalInfraManagerMap = new HashMap<>();
	
	static {
		providerTypeInfraManagerMap.put("oVirt", "ManageIQ::Providers::Redhat::InfraManager");
		providerTypeInfraManagerMap.put("Red Hat Virtualization", "ManageIQ::Providers::Redhat::InfraManager");
		providerTypeInfraManagerMap.put("MCOS cServer", "ManageIQ::Providers::Redhat::InfraManager");
		providerTypeInfraManagerMap.put("VMware vCenter", "ManageIQ::Providers::Vmware::InfraManager");
		providerTypeInfraManagerMap.put("Microsoft System Center VMM", "ManageIQ::Providers::Microsoft::InfraManager");
//		providerTypeInfraManagerMap.put("OpenStack Platform Director", "ManageIQ::Providers::Openstack::InfraManager");
		
		
		providerTypeCloudManagerMap.put("Amazon EC2", "ManageIQ::Providers::Amazon::CloudManager");
		providerTypeCloudManagerMap.put("OpenStack", "ManageIQ::Providers::Openstack::CloudManager");
//		providerTypeCloudManagerMap.put("Azure/Azure Stack", "ManageIQ::Providers::Azure::CloudManager");
//		providerTypeCloudManagerMap.put("VMware vCloud", "ManageIQ::Providers::Vmware::CloudManager");
		
		providerTypeContainerManagerMap.put("Kubernetes", "ManageIQ::Providers::Kubernetes::ContainerManager");
		providerTypeContainerManagerMap.put("Openshift", "ManageIQ::Providers::Openshift::ContainerManager");
		
		providerTypePhysicalInfraManagerMap.put("Lenovo XClarity", "ManageIQ::Providers::Lenovo::PhysicalInfraManager");
	}
	
	
	
	/**
	 * 支持的用户角色
	 */
	public static String[] userRoles = {"EvmRole-super_administrator", "EvmRole-user_limited_self_service", "EvmRole-tenant_administrator"};
	
	/**
	 * token验证失败信息
	 */
	public static String tokenMessage = "登录超时，请重新登录。";
	
	/**
	 * 标签类、标签添加的前缀
	 */
	public static String tagPrefix = "opencmp_";
	
	/**
	 * 业务分类的 标签类、标签添加的前缀
	 */
	public static String businessTagPrefix = "opencmpbusiness_";
	
}
