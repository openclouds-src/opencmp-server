package com.massclouds.opencmp.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import io.swagger.models.HttpMethod;

@Component
public class OpenCMPInterceptor implements HandlerInterceptor {
	//配置拦截器，token不能为空
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String token = request.getHeader("X-Auth-Token");
		if (!HttpMethod.OPTIONS.toString().equals(request.getMethod()) && StringUtils.isEmpty(token)) {  //token 不能为空
			response.setStatus(401);
			return false;
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}

}
