package com.massclouds.opencmp.utils;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.massclouds.opencmp.common.redis.RedisUtil;


/**
 * @Description: openCMP调用manageIQ接口的工具类
 * 
 * @date: 2022年3月16日 下午07:26:24
 * @author hou_jjing
 */

@Component
public class ManageIQHttpUtils {
	
    @Autowired
    private RestTemplate restTemplate;

    private static ManageIQHttpUtils manageiqHttpUtils;
    
    @Autowired
    private RedisUtil redisUtil;

    @PostConstruct
    public void init(){
    	manageiqHttpUtils = this;
    	manageiqHttpUtils.restTemplate = this.restTemplate;
    	manageiqHttpUtils.redisUtil = this.redisUtil;
    }

    
   public static <T> ResponseEntity<T> httpGetRequest(String url, ParameterizedTypeReference<T> responseType, String token) throws Exception{
        
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
        headers.set(HttpHeaders.ACCEPT, "application/json");
       // headers.set("X-Auth-Token", (String) manageiqHttpUtils.redisUtil.get("manageIQ.token"));
        headers.set("X-Auth-Token", token);
        
        String accessUrl = (String) manageiqHttpUtils.redisUtil.get("manageIQ.ip") + url;
        
        ResponseEntity<T> result = manageiqHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.GET, new HttpEntity<>(headers), responseType);
        return result;
        
    }
   
   
   public static <T> ResponseEntity<T> httpRequest(String url, String httpMethod, HashMap<String, Object> params, ParameterizedTypeReference<T> responseType, String token) throws Exception{
     	//设置Http的Header
     	HttpHeaders header = new HttpHeaders();
     	header.set(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
     	header.set(HttpHeaders.ACCEPT, "application/json");
     	
     	String accessUrl = null;
     	
     	if (url.contains("/api/auth?requester_type=ui")) {
     		
     		String authStr = params.get("manageIQ.userName") + ":" + params.get("manageIQ.password");
     		header.set(HttpHeaders.AUTHORIZATION, "Basic " + new String(Base64.encodeBase64(authStr.getBytes())));
     		accessUrl = url;
     		
     	} else {
     		
 	       //header.set("X-Auth-Token",  (String) manageiqHttpUtils.redisUtil.get("manageIQ.token"));
 	       header.set("X-Auth-Token", token);
 	       accessUrl = (String) manageiqHttpUtils.redisUtil.get("manageIQ.ip") + url;
 	        
     	}
     	
     	ResponseEntity<T> result = null;
     	
     	if ("GET".equals(httpMethod.toUpperCase())) {
     		
     		result = manageiqHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.GET, new HttpEntity<>(header), responseType);
     		
     	} else if ("POST".equals(httpMethod.toUpperCase())) {
     		
     		result = manageiqHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.POST, new HttpEntity<>(params, header), responseType);
     		
     	} else if ("PATCH".equals(httpMethod.toUpperCase())) {
     		
     		result = manageiqHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.PATCH, new HttpEntity<>(params, header), responseType);
     		
     	} else if ("DELETE".equals(httpMethod.toUpperCase())) {
     		
     		result = manageiqHttpUtils.restTemplate.exchange(accessUrl, HttpMethod.DELETE, new HttpEntity<>(header), responseType);
     		
     	}
       
     	return result;
         
   }
   
}
