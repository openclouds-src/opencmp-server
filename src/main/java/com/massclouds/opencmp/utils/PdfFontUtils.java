package com.massclouds.opencmp.utils;

import java.io.IOException;
import java.net.URL;
import org.springframework.core.io.ClassPathResource;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;

public class PdfFontUtils {
         
    /**
     * 文档排版
     */
    public static Paragraph getFont(int type, String text) throws IOException{
    	BaseFont songti = null;
    	BaseFont heiti = null;
    	BaseFont fangsong = null;
    	URL url = new ClassPathResource("META-INF/resources/static/font/STZHONGS.TTF").getURL();
    	String song = url.getPath();
    	url = new ClassPathResource("META-INF/resources/static/font/SIMHEI.TTF").getURL();
    	String hei = url.getPath();
    	url = new ClassPathResource("META-INF/resources/static/font/STFANGSO.TTF").getURL();
    	String fang = url.getPath();
        try {
            /**
             * 设置字体
             * 
             * windows路径字体
             * FONT_TYPE=C:/Windows/fonts/simsun.ttc
             * linux路径字体 宋体 (如果没有这个字体文件，就将windows的字体传上去)
             * FONT_TYPE=/usr/share/fonts/win/simsun.ttc
             */
        	songti = BaseFont.createFont(song, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        	heiti = BaseFont.createFont(hei, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        	fangsong = BaseFont.createFont(fang, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Font font1 = new Font(songti);
        Font font2 = new Font(heiti);
        Font font3 = new Font(fangsong);
        if(1 == type){//1-标题
            font1.setSize(22f);
        } else if(2 == type){//2-标题一
        	font2.setSize(16f);
        } else if(3 == type){//3-标题二
        	font3.setSize(16f);
        } else if(4 == type){//4-标题三
        	font3.setSize(16f);
        } else if(5 == type){//5-正文
        	font3.setSize(10.5f);
        } else if(6 == type){//6-左对齐
        	font3.setSize(10.5f);
        } else {
        	font3.setSize(10.5f);//默认大小
        }
        //注： 字体必须和 文字一起new
        Paragraph paragraph = null;
        if(1 == type){
        	paragraph = new Paragraph(text, font1);
            paragraph.setAlignment(Paragraph.ALIGN_CENTER);//居中
            paragraph.setSpacingBefore(40f);//上间距
            paragraph.setSpacingAfter(30f);//下间距
        } else if(2 == type){//2-标题一
        	paragraph = new Paragraph(text, font2);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED); //默认
            paragraph.setFirstLineIndent(40);
            paragraph.setSpacingBefore(20f);//上间距
            paragraph.setSpacingAfter(30f);//下间距
        } else if(3 == type){
        	paragraph = new Paragraph(text, font3);
        	 paragraph.setFirstLineIndent(40);
            paragraph.setSpacingBefore(20f);//上间距
            paragraph.setSpacingAfter(1f);//下间距
        } else if(4 == type){//4-标题三
        	paragraph = new Paragraph(text, font3);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph.setFirstLineIndent(40);
            paragraph.setSpacingBefore(2f);//上间距
            paragraph.setSpacingAfter(2f);//下间距
            paragraph.setLeading(40f);//设置行间距
        } else if(5 == type){
        	paragraph = new Paragraph(text, font3);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED); 
            paragraph.setFirstLineIndent(40);//首行缩进
            paragraph.setSpacingBefore(1f);//上间距
            paragraph.setSpacingAfter(1f);//下间距
        } else if(6 == type){//左对齐
        	paragraph = new Paragraph(text, font3);
            paragraph.setAlignment(Element.ALIGN_LEFT); 
            paragraph.setSpacingBefore(1f);//上间距
            paragraph.setSpacingAfter(1f);//下间距
        }else if(7 == type){
            paragraph = new Paragraph(text, font3);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingBefore(1f);//上间距
            paragraph.setSpacingAfter(1f);//下间距
        }
        return paragraph;
    }
}

