package com.massclouds.opencmp.controller.ansible;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ansible.InventoryDTO;
import com.massclouds.opencmp.service.ansible.InventoryService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: ansible 清单 管理的控制层代码
 * 
 * @date: 2022年6月15日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "ansible 清单管理接口")
public class InventoryController  extends BaseController{
	
	@Autowired
	private InventoryService inventoryService;
	
	/**
	 * @Description: 分页查询 清单
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/ansible/inventory/listOfPage")
	@ApiOperation(value = "分页查询 清单", notes = "分页查询 清单")
	public PageDataResultDTO<List<InventoryDTO>> inventoryListOfPage(QueryDTO query) {
		return inventoryService.inventoryListOfPage(query);
	}
	
	/**
	 * @Description: 获取 清单 列表
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/ansible/inventory/listAll")
	@ApiOperation(value = "获取 清单 列表", notes = "获取 清单 列表")
	public DataResultDTO<List<InventoryDTO>> inventoryListAll() {
		return inventoryService.inventoryListAll();
	}
	
	/**
	 * @Description: 创建/编辑清单
	 * 
	 * @param InventoryDTO 清单信息
	 * @return 操作结果
	 */
	@PostMapping("/ansible/inventory/createUpdate")
	@ApiOperation(value = "创建/编辑清单", notes = "创建/编辑清单")
	public ResultDTO createUpdate(@RequestBody InventoryDTO inventoryDTO) {
		return inventoryService.createUpdateInventory(inventoryDTO);
	}
	
	/**
	 * @Description: 删除清单
	 * 
	 * @param id  清单id
	 * @return 操作结果
	 */
	@DeleteMapping("/ansible/inventory/{id}")
	@ApiOperation(value = "删除清单", notes = "删除清单")
	public ResultDTO deleteInventory(@PathVariable String id) {
		return inventoryService.deleteInventory(id);
	}
}
