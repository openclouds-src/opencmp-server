package com.massclouds.opencmp.controller.ansible;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ansible.CredentialDTO;
import com.massclouds.opencmp.service.ansible.CredentialService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: ansible 凭证 管理的控制层代码
 * 
 * @date: 2022年6月16日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "ansible 凭证管理接口")
public class CredentialController  extends BaseController{
	
	@Autowired
	private CredentialService credentialService;
	
	/**
	 * @Description: 分页查询 凭证
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/ansible/credential/listOfPage")
	@ApiOperation(value = "分页查询 凭证", notes = "分页查询 凭证")
	public PageDataResultDTO<List<CredentialDTO>> procredentialListOfPage(QueryDTO query) {
		return credentialService.credentialListOfPage(query);
	}
	
	/**
	 * @Description: 创建/编辑凭证
	 * 
	 * @param InventoryDTO 凭证信息
	 * @return 操作结果
	 */
	@PostMapping("/ansible/credential/createUpdate")
	@ApiOperation(value = "创建/编辑凭证", notes = "创建/编辑凭证")
	public ResultDTO createUpdatecredential(@RequestBody CredentialDTO credentialDTO) {
		return credentialService.createUpdateCredential(credentialDTO);
	}
	
	/**
	 * @Description: 删除凭证
	 * 
	 * @param id  凭证id
	 * @return 操作结果
	 */
	@DeleteMapping("/ansible/credential/{id}")
	@ApiOperation(value = "删除凭证", notes = "删除凭证")
	public ResultDTO deleteCredential(@PathVariable String id) {
		return credentialService.deleteCredential(id);
	}
}
