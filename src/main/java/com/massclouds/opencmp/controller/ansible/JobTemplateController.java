package com.massclouds.opencmp.controller.ansible;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ansible.JobTemplateDTO;
import com.massclouds.opencmp.service.ansible.JobTemplateService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: ansible 任务模板 管理的控制层代码
 * 
 * @date: 2022年6月17日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "ansible 任务模板管理接口")
public class JobTemplateController  extends BaseController{
	
	@Autowired
	private JobTemplateService jobTemplateService;
	
	/**
	 * @Description: 分页查询 任务模板
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/ansible/jobtemplate/listOfPage")
	@ApiOperation(value = "分页查询 任务模板", notes = "分页查询 任务模板")
	public PageDataResultDTO<List<JobTemplateDTO>> jobTemplateListOfPage(QueryDTO query) {
		return jobTemplateService.jobTemplateListOfPage(query);
	}
	
	/**
	 * @Description: 创建/编辑任务模板
	 * 
	 * @param InventoryDTO 任务模板信息
	 * @return 操作结果
	 */
	@PostMapping("/ansible/jobtemplate/createUpdate")
	@ApiOperation(value = "创建/编辑任务模板", notes = "创建/编辑任务模板")
	public ResultDTO createUpdateJobTemplate(@RequestBody JobTemplateDTO jobTemplateDTO) {
		return jobTemplateService.createUpdateJobTemplate(jobTemplateDTO);
	}
	
	/**
	 * @Description: 执行任务模板
	 * 
	 * @param QueryDTO 任务模板id
	 * @return 操作结果
	 */
	@PostMapping("/ansible/jobtemplate/launch")
	@ApiOperation(value = "执行任务模板", notes = "执行任务模板")
	public ResultDTO launchJobTemplate(@RequestBody QueryDTO query) {
		return jobTemplateService.launchJobTemplate(query.getId());
	}
	
	/**
	 * @Description: 删除任务模板
	 * 
	 * @param id  任务模板id
	 * @return 操作结果
	 */
	@DeleteMapping("/ansible/jobtemplate/{id}")
	@ApiOperation(value = "删除任务模板", notes = "删除任务模板")
	public ResultDTO deleteJobTemplate(@PathVariable String id) {
		return jobTemplateService.deleteJobTemplate(id);
	}
}
