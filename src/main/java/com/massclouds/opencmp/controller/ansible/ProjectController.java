package com.massclouds.opencmp.controller.ansible;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ansible.ProjectDTO;
import com.massclouds.opencmp.service.ansible.ProjectService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: ansible 项目 管理的控制层代码
 * 
 * @date: 2022年6月16日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "ansible 项目管理接口")
public class ProjectController  extends BaseController{
	
	@Autowired
	private ProjectService projectService;
	
	/**
	 * @Description: 分页查询 项目
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/ansible/project/listOfPage")
	@ApiOperation(value = "分页查询 项目", notes = "分页查询 项目")
	public PageDataResultDTO<List<ProjectDTO>> projectListOfPage(QueryDTO query) {
		return projectService.projectListOfPage(query);
	}
	
	/**
	 * @Description: 创建/编辑项目
	 * 
	 * @param InventoryDTO 项目信息
	 * @return 操作结果
	 */
	@PostMapping("/ansible/project/createUpdate")
	@ApiOperation(value = "创建/编辑项目", notes = "创建/编辑项目")
	public ResultDTO createUpdateProject(@RequestBody ProjectDTO projectDTO) {
		return projectService.createUpdateProject(projectDTO);
	}
	
	/**
	 * @Description: 删除项目
	 * 
	 * @param id  项目id
	 * @return 操作结果
	 */
	@DeleteMapping("/ansible/project/{id}")
	@ApiOperation(value = "删除项目", notes = "删除项目")
	public ResultDTO deleteProject(@PathVariable String id) {
		return projectService.deleteProject(id);
	}
	
	/**
	 * @Description: 获取项目的playbook
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/ansible/project/playbooks")
	@ApiOperation(value = "获取项目的playbook", notes = "获取项目的playbook")
	public DataResultDTO<List<String>> listPlaybooks() {
		return projectService.getPlaybooksOfDefault();
	}
}
