package com.massclouds.opencmp.controller.ansible;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ansible.AnsibleJobDTO;
import com.massclouds.opencmp.service.ansible.AnsibleJobService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: ansible 任务 管理的控制层代码
 * 
 * @date: 2022年6月20日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "ansible 任务管理接口")
public class AnsibleJobController  extends BaseController{
	
	@Autowired
	private AnsibleJobService ansibleJobService;
	
	/**
	 * @Description: 分页查询 任务
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/ansible/job/listOfPage")
	@ApiOperation(value = "分页查询 任务", notes = "分页查询 任务")
	public PageDataResultDTO<List<AnsibleJobDTO>> jobListOfPage(QueryDTO query) {
		return ansibleJobService.jobListOfPage(query);
	}
	
	/**
	 * @Description: 取消任务
	 * 
	 * @param QueryDTO 任务id
	 * @return 操作结果
	 */
	@PostMapping("/ansible/job/cancel")
	@ApiOperation(value = "执行任务", notes = "执行任务")
	public ResultDTO cancelJob(@RequestBody QueryDTO query) {
		return ansibleJobService.cancelJob(query.getId());
	}
	
	/**
	 * @Description: 再次执行任务
	 * 
	 * @param QueryDTO 任务id
	 * @return 操作结果
	 */
	@PostMapping("/ansible/job/relaunch")
	@ApiOperation(value = "再次执行任务", notes = "再次执行任务")
	public ResultDTO relaunchlJob(@RequestBody QueryDTO query) {
		return ansibleJobService.relaunchJob(query.getId());
	}
	
	/**
	 * @Description: 删除任务
	 * 
	 * @param id  任务id
	 * @return 操作结果
	 */
	@DeleteMapping("/ansible/job/{id}")
	@ApiOperation(value = "删除任务", notes = "删除任务")
	public ResultDTO deleteJob(@PathVariable String id) {
		return ansibleJobService.deleteJob(id);
	}
	
}
