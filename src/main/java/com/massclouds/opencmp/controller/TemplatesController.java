/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.VmDetailsDTO;
import com.massclouds.opencmp.service.TemplatesService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 模板管理的控制层代码
 * 
 * @date: 2022年3月1日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "模板管理接口")
public class TemplatesController extends BaseController{
	
	@Autowired
	private TemplatesService templatesService;
	
	/**
	 * @Description: 分页查询模板
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/templates/listOfPage")
	@ApiOperation(value = "分页查询模板", notes = "根据分页条件查询模板")
	public PageDataResultDTO<List<VmDetailsDTO>> listByPage(QueryDTO query) {
		
		return templatesService.templatesListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取所有模板列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/templates/listAll")
	@ApiOperation(value = "获取所有模板列表", notes = "获取所有模板列表")
	public DataResultDTO<List<VmDetailsDTO>> listAll() {
		
		return templatesService.templatesList(getToken());
		
	}
	
	/**
	 * @Description: 删除模板
	 * 
	 * @param id  模板id
	 * @return 操作结果
	 */
	@DeleteMapping("/templates/{id}")
	@ApiOperation(value = "删除模板", notes = "删除模板")
	public ResultDTO delete(@PathVariable String id) {
		
		return templatesService.deleteTemplate(id, getToken());
		
	}


}