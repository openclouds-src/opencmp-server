/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ZoneDTO;
import com.massclouds.opencmp.service.ZonesService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 区 管理的控制层代码
 * 
 * @date: 2022年4月18日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "区管理接口")
public class ZonesController extends BaseController{
	
	@Autowired
	private ZonesService zonesService;
	
	/**
	 * @Description: 分页查询区
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/zones/listOfPage")
	@ApiOperation(value = "分页查询区", notes = "根据分页条件查询区")
	public PageDataResultDTO<List<ZoneDTO>> zonesListOfPage(QueryDTO query) {
		
		return zonesService.zonesListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取所有区列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/zones/listAll")
	@ApiOperation(value = "获取所有区列表", notes = "获取所有区列表")
	public DataResultDTO<List<ZoneDTO>> listAll() {
		
		return zonesService.zonesList(getToken());
		
	}
	
	/**
	 * @Description: 添加区
	 * 
	 * @param ZoneDTO 区信息
	 * @return 操作结果
	 */
	@PostMapping("/zones/create")
	@ApiOperation(value = "添加区", notes = "添加区")
	public ResultDTO createZone(@RequestBody ZoneDTO zoneDTO) {
		
		return zonesService.createZone(zoneDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑 区
	 * 
	 * @param ZoneDTO 区信息
	 * @return 操作结果
	 */
	@PostMapping("/zones/update")
	@ApiOperation(value = "编辑区", notes = "编辑区")
	public ResultDTO updateZone(@RequestBody ZoneDTO zoneDTO) {
		
		return zonesService.updateZone(zoneDTO, getToken());
		
	}
	
	
	/**
	 * @Description: 删除区
	 * 
	 * @param id  区id
	 * @return 操作结果
	 */
	@DeleteMapping("/zones/{id}")
	@ApiOperation(value = "删除区", notes = "删除区")
	public ResultDTO delete(@PathVariable String id) {
		
		return zonesService.deleteZone(id, getToken());
		
	}


}