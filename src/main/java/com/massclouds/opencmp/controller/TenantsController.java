/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.TenantResourceStatisticsDTO;
import com.massclouds.opencmp.dto.TenantsCreateDTO;
import com.massclouds.opencmp.dto.TenantsDTO;
import com.massclouds.opencmp.dto.TenantsTreeDTO;
import com.massclouds.opencmp.service.TenantsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 租户 管理的控制层代码
 * 
 * @date: 2022年4月7日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "租户管理接口")
public class TenantsController extends BaseController{
	
	@Autowired
	private TenantsService tenantsService;
	
	/**
	 * @Description: 分页查询租户
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/tenants/listOfPage")
	@ApiOperation(value = "分页查询用户组", notes = "根据分页条件查询用户组")
	public PageDataResultDTO<List<TenantsDTO>> tenantsListOfPage(QueryDTO query) {
		
		return tenantsService.tenantsListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取所有租户
	 * 
	 * @param
	 * @return 操作结果
	 */
	@GetMapping("/tenants/list")
	@ApiOperation(value = "获取所有租户", notes = "获取所有租户")
	public DataResultDTO<List<TenantsDTO>> tenantsList() {
		
		return tenantsService.tenantsList(getToken());
		
	}
	
	/**
	 * @Description: 获取所有租户树
	 * 
	 * @param
	 * @return 操作结果
	 */
	@GetMapping("/tenants/listTree")
	@ApiOperation(value = "获取所有租户树", notes = "获取所有租户树")
	public DataResultDTO<List<TenantsTreeDTO>> tenantsListTree() {
		
		return tenantsService.tenantsListTree(getToken());
		
	}
	

	/**
	 * @Description: 添加租户
	 * 
	 * @param TenantsDTO 租户信息
	 * @return 操作结果
	 */
	@PostMapping("/tenants/create")
	@ApiOperation(value = "添加租户", notes = "添加租户")
	public ResultDTO createTenant(@RequestBody TenantsCreateDTO tenantsCreateDTO) {
		
		return tenantsService.createOrUpdateTenant(tenantsCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑租户
	 * 
	 * @param TenantsDTO 租户信息
	 * @return 操作结果
	 */
	@PostMapping("/tenants/update")
	@ApiOperation(value = "编辑租户", notes = "编辑租户")
	public ResultDTO updateTenant(@RequestBody TenantsCreateDTO tenantsCreateDTO) {
		
		return tenantsService.createOrUpdateTenant(tenantsCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 删除租户
	 * 
	 * @param id  租户id
	 * @return 操作结果
	 */
	@DeleteMapping("/tenants/{id}")
	@ApiOperation(value = "删除用户组", notes = "删除用户组")
	public ResultDTO deleteTenant(@PathVariable String id) {
		
		return tenantsService.deleteTenant(id, getToken());
		
	}
	
	/**
	 * @Description: 批量删除租户
	 * 
	 * @param List<String> 用户id
	 * @return 操作结果
	 */
	@PostMapping("/tenants/delete")
	@ApiOperation(value = "批量删除租户", notes = "批量删除租户")
	public ResultDTO deleteTenants(@RequestBody List<String> ids) {
		
		return tenantsService.deleteTenants(ids, getToken());
		
	}
	
	/**
	 * @Description: 获取租户的资源分配统计
	 * 
	 * @param id  租户id
	 * @return 操作结果
	 */
	@GetMapping("/tenants/{id}/statistics")
	@ApiOperation(value = "获取租户的资源分配统计", notes = "获取租户的资源分配统计")
	public DataResultDTO<TenantResourceStatisticsDTO> resourceStatisticsTenant(@PathVariable String id) {
		
		return tenantsService.resourceStatisticsTenant(id, getToken());
		
	}
}