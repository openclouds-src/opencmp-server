/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ServiceTemplateCreateReceiveDTO;
import com.massclouds.opencmp.dto.ServiceTemplatesDTO;
import com.massclouds.opencmp.dto.ServiceTemplatesDetailsDTO;
import com.massclouds.opencmp.dto.query.ServiceTemplateQueryDTO;
import com.massclouds.opencmp.service.ServiceTemplatesService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 服务目录项 管理的控制层代码
 * 
 * @date: 2022年4月1日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "服务目录项管理接口")
public class ServiceTemplatesController extends BaseController{
	
	@Autowired
	private ServiceTemplatesService serviceTemplatesService;
	
	/**
	 * @Description: 分页查询服务模板，也叫目录项
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/serviceTemplates/list")
	@ApiOperation(value = "分页查询服务模板", notes = "根据分页条件查询服务模板")
	public PageDataResultDTO<List<ServiceTemplatesDTO>> serviceTemplatesList(ServiceTemplateQueryDTO query) {
		
		return serviceTemplatesService.serviceTemplatesList(query, getToken());
		
	}
	
	/**
	 * @Description: 获取服务模板详情
	 * 
	 * @param id 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/serviceTemplates/{id}")
	@ApiOperation(value = "获取服务模板详情", notes = "获取服务模板详情")
	public DataResultDTO<ServiceTemplatesDetailsDTO> getServiceTemplatesDetails(@PathVariable String id) {
		
		return serviceTemplatesService.getServiceTemplatesDetails(id, getToken());
		
	}
	
	/**
	 * @Description: 添加服务模板，也叫目录项
	 * 
	 * @param ServiceTemplateCreateReceiveDTO 服务目录项信息
	 * @return 操作结果
	 */
	@PostMapping("/serviceTemplates/create")
	@ApiOperation(value = "添加服务目录项", notes = "添加服务目录项")
	public ResultDTO createServiceTemplate(@RequestBody ServiceTemplateCreateReceiveDTO serviceTemplateCreateReceiveDTO) {
		
		return serviceTemplatesService.createServiceTemplate(serviceTemplateCreateReceiveDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑服务模板，也叫目录项
	 * 
	 * @param ServiceTemplateCreateReceiveDTO 服务目录项信息
	 * @return 操作结果
	 */
	@PostMapping("/serviceTemplates/update")
	@ApiOperation(value = "添加服务目录项", notes = "添加服务目录项")
	public ResultDTO updateServiceTemplate(@RequestBody ServiceTemplateCreateReceiveDTO serviceTemplateCreateReceiveDTO) {
		
		return serviceTemplatesService.updateServiceTemplate(serviceTemplateCreateReceiveDTO, getToken());
		
	}
	
	/**
	 * @Description: 删除服务模板
	 * 
	 * @param id  服务模板id
	 * @return 操作结果
	 */
	@DeleteMapping("/serviceTemplates/{id}")
	@ApiOperation(value = "删除服务模板", notes = "删除服务模板")
	public ResultDTO deleteServiceTemplate(@PathVariable String id) {
		
		return serviceTemplatesService.deleteServiceTemplate(id, getToken());
		
	}
	
	/**
	 * @Description: 批量删除服务目录项
	 * 
	 * @param List<String> 服务目录项id
	 * @return 操作结果
	 */
	@PostMapping("/serviceTemplates/delete")
	@ApiOperation(value = "批量删除服务目录项", notes = "批量删除服务目录项")
	public ResultDTO deleteServiceTemplates(@RequestBody List<String> ids) {
		
		return serviceTemplatesService.deleteServiceTemplates(ids, getToken());
		
	}
	
}