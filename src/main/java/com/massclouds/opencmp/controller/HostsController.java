/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.HostDTO;
import com.massclouds.opencmp.dto.VmHostOnRunDTO;
import com.massclouds.opencmp.service.HostsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 主机管理的控制层代码
 * 
 * @date: 2022年3月1日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "主机管理接口")
public class HostsController extends BaseController{
	
	@Autowired
	private HostsService hostsService;
	
	/**
	 * @Description: 分页查询供应商
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/hosts/list")
	@ApiOperation(value = "分页查询主机", notes = "根据分页条件查询主机")
	public PageDataResultDTO<List<HostDTO>> list(QueryDTO query) {
		
		return hostsService.hostsList(query, getToken());
		
	}
	
	/**
	 * @Description: 获取运行的且ip不为空的主机
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/hosts/listOnRun")
	@ApiOperation(value = "获取运行的且ip不为空的主机", notes = "获取运行的且ip不为空的主机")
	public DataResultDTO<List<VmHostOnRunDTO>> listOnRun() {
		
		return hostsService.listOnRun(getToken());
		
	}

}