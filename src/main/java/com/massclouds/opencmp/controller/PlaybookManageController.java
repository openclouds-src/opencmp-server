/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.massclouds.opencmp.dto.AnsiblePlaybookDTO;
import com.massclouds.opencmp.service.PlaybookManageService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 自动化（ansible） 脚本管理的控制层代码
 * 
 * @date: 2022年8月24日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "脚本管理接口")
public class PlaybookManageController extends BaseController{
	
	@Value("${ansible.playbook.path}")
    private String playbookPath;
	
	@Autowired
	private PlaybookManageService playbookManageService;
	
	/**
	 * @Description: 上传脚本
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@PostMapping("/playbook/upload")
	@ApiOperation(value = "上传脚本", notes = "上传脚本")
	public ResultDTO uploadPlaybook(MultipartFile file) {
		ResultDTO result = new ResultDTO();
		
	    String fileName = file.getOriginalFilename();//获取文件名
        if (!file.isEmpty()) {
            try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(new File(playbookPath + File.separator + fileName)))) {
                out.write(file.getBytes());
                out.flush();
                
                result.setSuccess("文件上传成功");
                return result;
            } catch (Exception e) {
                result.setError("上传文件失败");
                return result;
            } 

        } else {
            result.setError("上传文件失败，文件为空");
            return result;
        }
		
	}
	
	/**
	 * @Description: 分页获取脚本文件
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/playbook/list")
	@ApiOperation(value = "分页获取脚本文件", notes = "根据分页条件查询脚本文件")
	public PageDataResultDTO<List<AnsiblePlaybookDTO>> list(QueryDTO query) {
		
		return playbookManageService.playbookList(query, getToken());
		
	}
	
	/**
	 * @Description: 批量删除脚本文件
	 * 
	 * @param List<String> 脚本名称
	 * @return 操作结果
	 */
	@PostMapping("/playbook/delete")
	@ApiOperation(value = "批量删除脚本文件", notes = "批量删除脚本文件")
	public ResultDTO deletePlaybooks(@RequestBody List<String> playbookNames) {
		
		return playbookManageService.deletePlaybooks(playbookNames, getToken());
		
	}
	
	/**
	 * @Description: 获取脚本内容
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/playbook/content")
	@ApiOperation(value = "获取脚本内容", notes = "获取脚本内容")
	public DataResultDTO<AnsiblePlaybookDTO> getPlaybookContent(QueryDTO query) {
		
		return playbookManageService.getPlaybookContent(query.getName(), getToken());
		
	}
	
	/**
	 * @Description: 修改脚本文件
	 * 
	 * @param AnsiblePlaybookDTO 脚本
	 * @return 操作结果
	 */
	@PostMapping("/playbook/update")
	@ApiOperation(value = "修改脚本文件", notes = "修改脚本文件")
	public ResultDTO updatePlaybook(@RequestBody AnsiblePlaybookDTO playbook) {
		
		return playbookManageService.updatePlaybook(playbook, getToken());
		
	}
}