/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ComponentStatusDTO;
import com.massclouds.opencmp.service.CheckComponentStatusService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 检测第三方组件的服务状态 管理的控制层代码
 * 
 * @date: 2022年6月14日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "组件检测模块接口")
public class CheckComponentStatusController extends BaseController{
	
	@Autowired
	private CheckComponentStatusService checkComponentStatusService;
	
	/**
	 * @Description: 获取第三方组件及其监控状态
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/componentstatus/list")
	@ApiOperation(value = "获取第三方组件及其监控状态", notes = "获取第三方组件及其监控状态")
	public PageDataResultDTO<List<ComponentStatusDTO>> list(QueryDTO query) {
		
		return checkComponentStatusService.componentList(query, getToken());
		
	}
	

}