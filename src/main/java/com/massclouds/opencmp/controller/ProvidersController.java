/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ProviderAddDTO;
import com.massclouds.opencmp.dto.ProviderDTO;
import com.massclouds.opencmp.dto.ProviderDetailsDTO;
import com.massclouds.opencmp.service.ProvidersService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 供应商管理的控制层代码
 * 
 * @date: 2022年3月1日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "供应商管理接口")
public class ProvidersController extends BaseController{
	
	@Autowired
	private ProvidersService providersService;
	
	/**
	 * @Description: 获取所有供应商类型
	 * @return 操作结果
	 */
	@GetMapping("/providers/type")
	@ApiOperation(value = "获取所有供应商类型", notes = "获取所有供应商类型")
	public DataResultDTO<Map<String, Object>> providerTypeAll() {
		
		return providersService.providerTypeAll(getToken());
		
	}
	
	/**
	 * @Description: 分页查询供应商
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/providers/list")
	@ApiOperation(value = "分页查询供应商", notes = "根据分页条件查询供应商")
	public PageDataResultDTO<List<ProviderDTO>> list(QueryDTO query) {
		
		return providersService.providersList(query, getToken());
		
	}
	
	/**
	 * @Description: 分类获取所有供应商
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/providers/listAll")
	@ApiOperation(value = "分类获取所有供应商", notes = "分类获取所有供应商")
	public DataResultDTO<Map<String, List<ProviderDTO>>> listAll() {
		
		return providersService.listAll(getToken());
		
	}
	
	/**
	 * @Description: 获取供应商详情
	 * 
	 * @param id 供应商id
	 * @return 操作结果
	 */
	@GetMapping("/providers/{id}")
	@ApiOperation(value = "获取供应商详情", notes = "获取供应商详情")
	public DataResultDTO<ProviderDetailsDTO> getProviderDetails(@PathVariable String id) {
		
		return providersService.getProviderDetails(id, getToken());
		
	}
	
	
	/**
	 * @Description: 添加供应商
	 * 
	 * @param ProviderAddDTO 供应商信息
	 * @return 操作结果
	 */
	@PostMapping("/providers/add")
	@ApiOperation(value = "添加供应商", notes = "添加供应商")
	public ResultDTO insert(@RequestBody ProviderAddDTO providerAddDTO) {
		
		return providersService.addProvider(providerAddDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑供应商
	 * 
	 * @param ProviderAddDTO 编辑供应商信息
	 * @return 操作结果
	 */
	@PostMapping("/providers/{id}")
	@ApiOperation(value = "编辑供应商", notes = "编辑供应商")
	public ResultDTO update(@PathVariable String id, @RequestBody ProviderAddDTO providerAddDTO) {
		
		return providersService.updateProvider(id, providerAddDTO, getToken());
		
	}
	
	/**
	 * @Description: 重新检测单个供应商认证状态
	 * 
	 * @param id 供应商id
	 * @return 操作结果
	 */
	@PostMapping("/providers/recheck/{id}")
	@ApiOperation(value = "重新检测单个供应商认证状态", notes = "重新检测单个供应商认证状态")
	public ResultDTO recheck(@PathVariable String id) {
		
		return providersService.recheckProvider(id, getToken());
		
	}
	
	/**
	 * @Description: 刷新单个供应商
	 * 
	 * @param id 刷新供应商id
	 * @return 操作结果
	 */
	@PostMapping("/providers/refresh/{id}")
	@ApiOperation(value = "刷新供应商", notes = "刷新供应商")
	public ResultDTO refresh(@PathVariable String id) {
		
		return providersService.refreshProvider(id, getToken());
		
	}
	
	/**
	 * @Description: 刷新所有供应商
	 * 
	 * @return 操作结果
	 */
	@PostMapping("/providers/refresh")
	@ApiOperation(value = "刷新所有供应商", notes = "刷新所有供应商")
	public ResultDTO refreshAll() {
		
		return providersService.refreshAll(getToken());
		
	}
	
	/**
	 * @Description: 删除供应商
	 * 
	 * @param id  供应商id
	 * @return 操作结果
	 */
	@DeleteMapping("/providers/{id}")
	@ApiOperation(value = "删除供应商", notes = "删除供应商")
	public ResultDTO delete(@PathVariable String id) {
		
		return providersService.deleteProvider(id, getToken());
		
	}
	
	/**
	 * @Description: 同步MCOS cServer供应商的用户组、用户
	 * 
	 * @param id 供应商id
	 * @return 操作结果
	 */
	@PostMapping("/providers/{id}/syncUserGroup")
	@ApiOperation(value = "同步MCOS cServer供应商的用户组、用户", notes = "同步MCOS cServer供应商的用户组、用户")
	public ResultDTO syncMCOScServerUserGroup(@PathVariable String id) {
		
		return providersService.syncMCOScServerUserGroup(id, getToken());
		
	}

}