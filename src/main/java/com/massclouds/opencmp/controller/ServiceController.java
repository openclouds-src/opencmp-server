/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ServiceCountDTO;
import com.massclouds.opencmp.dto.ServiceCreateDTO;
import com.massclouds.opencmp.dto.ServiceDTO;
import com.massclouds.opencmp.dto.ServiceRequestApprovalDTO;
import com.massclouds.opencmp.dto.ServiceRequestCountDTO;
import com.massclouds.opencmp.dto.ServiceRequestDTO;
import com.massclouds.opencmp.dto.query.ServerQueryDTO;
import com.massclouds.opencmp.dto.query.ServerRequestQueryDTO;
import com.massclouds.opencmp.service.ServiceService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 服务、请求管理的控制层代码
 * 
 * @date: 2022年4月1日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "服务、请求管理接口")
public class ServiceController extends BaseController{
	
	@Autowired
	private ServiceService serviceService;
	
	/**
	 * @Description: 获取服务列表
	 * 
	 * @param  query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/services/list")
	@ApiOperation(value = "分页查询服务", notes = "根据分页条件查询服务")
	public PageDataResultDTO<List<ServiceDTO>> servicesList(ServerQueryDTO query) {
		
		return serviceService.servicesList(query, getToken());
		
	}
	
	/**
	 * @Description: 添加服务
	 * 
	 * @param ServiceCreateDTO 服务目录
	 * @return 操作结果
	 */
	@PostMapping("/services/create")
	@ApiOperation(value = "添加服务", notes = "添加服务")
	public ResultDTO createService(@RequestBody ServiceCreateDTO serviceCreateDTO) {
		
		return serviceService.createService(serviceCreateDTO, getToken());
	}
	
	/**
	 * @Description: 归还服务
	 * 
	 * @return 操作结果
	 */
	@PostMapping("/services/{id}/retire")
	@ApiOperation(value = "归还服务", notes = "归还服务")
	public ResultDTO retireService(@PathVariable String id) {
		
		return serviceService.retireService(id, getToken());
	}
	
	/**
	 * @Description: 删除服务
	 * 
	 * @param id  服务id
	 * @return 操作结果
	 */
	@DeleteMapping("/services/{id}")
	@ApiOperation(value = "删除服务", notes = "删除服务")
	public ResultDTO deleteService(@PathVariable String id) {
		
		return serviceService.deleteService(id, getToken());
		
	}
	

	/**
	 * @Description: 批量删除服务
	 * 
	 * @param List<String> 服务id
	 * @return 操作结果
	 */
	@PostMapping("/services/delete")
	@ApiOperation(value = "批量删除服务", notes = "批量删除服务")
	public ResultDTO deleteServices(@RequestBody List<String> ids) {
		
		return serviceService.deleteServices(ids, getToken());
		
	}
	
	/**
	 * @Description: 获取不同状态服务的数量统计
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/services/serviceCount")
	@ApiOperation(value = "获取不同状态服务的数量统计", notes = "获取不同状态服务的数量统计")
	public DataResultDTO<ServiceCountDTO> servicesCount() {
		
		return serviceService.servicesCount(getToken());
		
	}
	
	/**
	 * @Description: 获取服务请求
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/serviceRequest/list")
	@ApiOperation(value = "分页查询服务请求", notes = "根据分页条件查询服务请求")
	public PageDataResultDTO<List<ServiceRequestDTO>> serviceRequestList(ServerRequestQueryDTO query) {
		
		return serviceService.serviceRequestList(query, getToken());
		
	}

	/**
	 * @Description: 获取不同状态服务请求的数量统计
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/serviceRequest/requestCount")
	@ApiOperation(value = "获取不同状态服务请求的数量统计", notes = "获取不同状态服务请求的数量统计")
	public DataResultDTO<ServiceRequestCountDTO> serviceRequestCount() {
		
		return serviceService.serviceRequestCount(getToken());
		
	}
	
	/**
	 * @Description: 服务请求审批
	 * 
	 * @param ServiceRequestApprovalDTO 服务请求审批信息
	 * @return 操作结果
	 */
	@PostMapping("/serviceRequest/Approval")
	@ApiOperation(value = "服务请求审批", notes = "服务请求审批")
	public ResultDTO serviceRequestApproval(@RequestBody ServiceRequestApprovalDTO serviceRequestApprovalDTO) {
		
		return serviceService.serviceRequestApproval(serviceRequestApprovalDTO, getToken());
	}
	
}