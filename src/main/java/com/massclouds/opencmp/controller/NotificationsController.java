/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.NotificationCountDTO;
import com.massclouds.opencmp.dto.NotificationDTO;
import com.massclouds.opencmp.dto.query.NotificationsQueryDTO;
import com.massclouds.opencmp.service.NotificationsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 事件通知 管理的控制层代码
 * 
 * @date: 2022年5月25日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "事件通知管理接口")
public class NotificationsController extends BaseController{
	
	@Autowired
	private NotificationsService notificationsService;
	
	/**
	 * @Description: 分页查询事件通知
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/notifications/listOfPage")
	@ApiOperation(value = "分页查询事件通知", notes = "根据分页条件查询事件通知")
	public PageDataResultDTO<List<NotificationDTO>> notificationsListOfPage(NotificationsQueryDTO query) {
		
		return notificationsService.notificationsListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取不同状态的事件通知的数量
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/notifications/count")
	@ApiOperation(value = "分页查询事件通知", notes = "根据分页条件查询事件通知")
	public DataResultDTO<NotificationCountDTO> notificationsCount(NotificationsQueryDTO query) {
		
		return notificationsService.notificationsCount(getToken());
		
	}
	
	/**
	 * @Description: 单个/批量已读标签类
	 * 
	 * @param List<String> 标签id
	 * @return 操作结果
	 */
	@PostMapping("/notifications/read")
	@ApiOperation(value = "单个/批量已读事件通知", notes = "单个/批量已读事件通知")
	public ResultDTO readNotifications(@RequestBody List<String> ids) {
		
		return notificationsService.readNotifications(ids, getToken());
		
	}
	
	/**
	 * @Description: 单个/批量删除标签类
	 * 
	 * @param List<String> 标签id
	 * @return 操作结果
	 */
	@PostMapping("/notifications/delete")
	@ApiOperation(value = "单个/批量删除事件通知", notes = "单个/批量删除事件通知")
	public ResultDTO deleteNotifications(@RequestBody List<String> ids) {
		
		return notificationsService.deleteNotifications(ids, getToken());
		
	}
	
}