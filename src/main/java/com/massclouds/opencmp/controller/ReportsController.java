/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ReportDTO;
import com.massclouds.opencmp.dto.ReportResultsDTO;
import com.massclouds.opencmp.dto.ReportScheduleCreateDTO;
import com.massclouds.opencmp.dto.ReportSchedulesReturnDTO;
import com.massclouds.opencmp.dto.ReportTemplateDTO;
import com.massclouds.opencmp.dto.query.ReportsQueryDTO;
import com.massclouds.opencmp.service.ReportsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 报表 管理的控制层代码
 * 
 * @date: 2022年5月26日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "报表管理接口")
public class ReportsController extends BaseController{
	
	@Autowired
	private ReportsService reportsService;
	
	/**
	 * @Description: 分页查询报表模板
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/reportTemplates/listOfPage")
	@ApiOperation(value = "分页查询报表模板", notes = "根据分页条件查询报表模板")
	public PageDataResultDTO<List<ReportTemplateDTO>> reportTemplatesListOfPage(QueryDTO query) {
		
		return reportsService.reportTemplatesListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取报表模板列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/reportTemplates/list")
	@ApiOperation(value = "获取报表模板列表", notes = "获取报表模板列表")
	public DataResultDTO<List<ReportTemplateDTO>> reportTemplatesList() {
		
		return reportsService.reportTemplatesList(getToken());
		
	}
	
	/**
	 * @Description: 删除报表模板
	 * 
	 * @param 报表模板id
	 * @return 操作结果
	 */
	@DeleteMapping("/reportTemplates/{id}")
	@ApiOperation(value = "删除报表模板", notes = "删除报表模板")
	public ResultDTO deleteReportTemplate(@PathVariable String id) {
		
		return reportsService.deleteReportTemplate(id, getToken());
		
	}
	
	/**
	 * @Description: 报表模板生成报表
	 * 
	 * @param 报表模板id
	 * @return 操作结果
	 */
	@PostMapping("/reportTemplates/{id}/createReport")
	@ApiOperation(value = "报表模板生成报表", notes = "报表模板生成报表")
	public ResultDTO createReportFromTemplate(@PathVariable String id) {
		
		return reportsService.createReportFromTemplate(id, getToken());
		
	}
	
	/**
	 * @Description: 分页查询已生成的报表
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/reports/listOfPage")
	@ApiOperation(value = "分页查询已生成的报表", notes = "分页查询已生成的报表")
	public PageDataResultDTO<List<ReportDTO>> reportsListOfPage(ReportsQueryDTO query) {
		
		return reportsService.reportsListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取某报表的内容
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/reports/{reportId}/results/{resultId}")
	@ApiOperation(value = "分页查询已生成的报表", notes = "分页查询已生成的报表")
	public DataResultDTO<ReportResultsDTO> reportResult(@PathVariable String reportId, @PathVariable String resultId) {
		
		return reportsService.reportResult(reportId, resultId, getToken());
		
	}
	
	/**
	 * @Description: 下载报表
	 * 
	 * @param reportId 报表模板id，resultId 报表id
	 * @return 操作结果
	 */
	@GetMapping("/reports/{reportId}/results/{resultId}/download")
	@ApiOperation(value = "下载挂牌交易资产交易合同", notes = "下载挂牌交易资产交易合同")
	public ResultDTO downloadReport(@PathVariable String reportId, @PathVariable String resultId, HttpServletResponse response) {
		ResultDTO result = new ResultDTO();
		reportsService.downloadReport(reportId, resultId, response, getToken());
		result.setSuccess();
		return null;
	}
	
	/**
	 * @Description: 删除报表
	 * 
	 * @param 报表id
	 * @return 操作结果
	 */
	@DeleteMapping("/reports/{id}")
	@ApiOperation(value = "删除报表", notes = "删除报表")
	public ResultDTO deleteReport(@PathVariable String id) {
		return reportsService.deleteReport(id, getToken());
	}
	
	
	/**
	 * @Description: 添加调度任务
	 * 
	 * @param ReportScheduleCreateDTO 调度任务
	 * @return 操作结果
	 */
	@PostMapping("/schedules/create")
	@ApiOperation(value = "添加调度任务", notes = "添加调度任务")
	public ResultDTO createSchedules(@RequestBody ReportScheduleCreateDTO reportScheduleCreateDTO) {
		return reportsService.createSchedules(reportScheduleCreateDTO, getToken());
	}
	
	/**
	 * @Description: 分页查询调度任务
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/schedules/listOfPage")
	@ApiOperation(value = "分页查询调度任务", notes = "分页查询调度任务")
	public PageDataResultDTO<List<ReportSchedulesReturnDTO>> schedulesListOfPage(QueryDTO query) {
		return reportsService.schedulesListOfPage(query, getToken());
	}
	
	/**
	 * @Description: 删除调度任务
	 * 
	 * @param 调度任务id
	 * @return 操作结果
	 */
	@DeleteMapping("/schedules/{id}")
	@ApiOperation(value = "删除调度任务", notes = "删除调度任务")
	public ResultDTO deleteSchedules(@PathVariable String id) {
		return reportsService.deleteSchedules(id, getToken());
	}
	
	/**
	 * @Description: 编辑调度任务
	 * 
	 * @param 调度任务id
	 * @return 操作结果
	 */
	@PostMapping("/schedules/{id}")
	@ApiOperation(value = "编辑调度任务", notes = "编辑调度任务")
	public ResultDTO editSchedules(@PathVariable String id, @RequestBody QueryDTO query) {
		return reportsService.editSchedules(id, query, getToken());
	}
	
	/**
	 * @Description: 调度任务的开启run、关闭stop，
	 * 
	 * @param ReportScheduleCreateDTO 调度任务
	 * @return 操作结果
	 */
	@PostMapping("/schedules/action")
	@ApiOperation(value = "调度任务的开启run、关闭stop", notes = "调度任务的开启run、关闭stop")
	public ResultDTO schedulesAction(@RequestBody QueryDTO query) {
		return reportsService.schedulesAction(query, getToken());
	}
}