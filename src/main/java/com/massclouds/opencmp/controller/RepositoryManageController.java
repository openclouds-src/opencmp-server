/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.massclouds.opencmp.dto.AnsibleRepositoryDTO;
import com.massclouds.opencmp.service.RepositoryManageService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: 软件仓库管理的控制层代码
 * 
 * @date: 2022年8月25日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "软件仓库管理接口")
public class RepositoryManageController extends BaseController{
	
	@Value("${ansible.repository.path}")
    private String repositoryPath;
	
	@Autowired
	private RepositoryManageService repositoryManageService;
	
	/**
	 * @Description: 上传软件
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@PostMapping("/repository/upload")
	@ApiOperation(value = "上传软件", notes = "上传软件")
	public ResultDTO uploadPlaybook(MultipartFile file) {
		ResultDTO result = new ResultDTO();
		
	    String fileName = file.getOriginalFilename();//获取文件名
        if (!file.isEmpty()) {
            try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(new File(repositoryPath + File.separator + fileName)))) {
                out.write(file.getBytes());
                out.flush();
                
                result.setSuccess("文件上传成功");
                return result;
            } catch (Exception e) {
                result.setError("上传文件失败");
                return result;
            } 

        } else {
            result.setError("上传文件失败，文件为空");
            return result;
        }
		
	}
	
	/**
	 * @Description: 分页获取软件仓库
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/repository/list")
	@ApiOperation(value = "分页获取软件仓库", notes = "根据分页条件查询软件仓库")
	public PageDataResultDTO<List<AnsibleRepositoryDTO>> list(QueryDTO query) {
		
		return repositoryManageService.repositoryList(query, getToken());
		
	}
	
	/**
	 * @Description: 批量删除软件仓库
	 * 
	 * @param List<String> 软件名称
	 * @return 操作结果
	 */
	@PostMapping("/repository/delete")
	@ApiOperation(value = "批量删除软件仓库", notes = "批量删除软件仓库")
	public ResultDTO deletePlaybooks(@RequestBody List<String> names) {
		
		return repositoryManageService.deleteRepository(names, getToken());
		
	}
	
}