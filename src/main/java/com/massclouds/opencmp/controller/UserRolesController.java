/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.UserRoleDTO;
import com.massclouds.opencmp.service.UserRolesService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 用户角色 管理的控制层代码
 * 
 * @date: 2022年4月6日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "用户角色管理接口")
public class UserRolesController extends BaseController{
	
	@Autowired
	private UserRolesService userRolesService;
	
	/**
	 * @Description: 分页查询用户组角色
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/userGroupRoles/listOfPage")
	@ApiOperation(value = "分页查询用户组角色", notes = "根据分页条件查询用户组角色")
	public PageDataResultDTO<List<UserRoleDTO>> userGroupRolesListOfPage(QueryDTO query) {
		
		return userRolesService.userGroupRolesListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取用户组角色列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/userGroupRoles/list")
	@ApiOperation(value = "用户组角色列表", notes = "查询用户组角色列表")
	public DataResultDTO<List<UserRoleDTO>> userGroupRolesList() {
		
		return userRolesService.userGroupRolesList(getToken());
		
	}
	
}