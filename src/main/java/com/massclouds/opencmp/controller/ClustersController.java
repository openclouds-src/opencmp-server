/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ClusterDTO;
import com.massclouds.opencmp.dto.ClusterDetailDTO;
import com.massclouds.opencmp.service.ClustersService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 集群管理的控制层代码
 * 
 * @date: 2022年3月16日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "集群管理接口")
public class ClustersController extends BaseController{
	
	@Autowired
	private ClustersService clustersService;
	
	/**
	 * @Description: 分页查询集群
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/clusters/listOfPage")
	@ApiOperation(value = "分页查询集群", notes = "根据分页条件查询集群")
	public PageDataResultDTO<List<ClusterDTO>> listOfPage(QueryDTO query) {
		
		return clustersService.clustersListOfPage(query.getPage(), query.getPageSize(), getToken());
		
	}
	
	/**
	 * @Description: 获取集群列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/clusters/list")
	@ApiOperation(value = "分页查询集群", notes = "根据分页条件查询集群")
	public DataResultDTO<List<ClusterDTO>> list() {
		
		return clustersService.clustersList(getToken());
		
	}
	
	/**
	 * @Description: 获取集群详情
	 * 
	 * @param 集群id 
	 * @return 操作结果
	 */
	@GetMapping("/clusters/getDetailById")
	@ApiOperation(value = "获取集群详情", notes = "获取集群详情")
	public DataResultDTO<ClusterDetailDTO> getDetailById(QueryDTO query) {
		
		return clustersService.getDetailById(query.getId(), getToken());
		
	}
	
}