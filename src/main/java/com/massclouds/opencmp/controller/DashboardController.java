/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.common.redis.RedisUtil;
import com.massclouds.opencmp.dto.DashboardEntityCountDTO;
import com.massclouds.opencmp.dto.DashboardEntityCountForSelfDTO;
import com.massclouds.opencmp.dto.DashboardPlatformCountDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsOnlineDTO;
import com.massclouds.opencmp.dto.DashboardTerminalStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.Vms6SaveToRedisDTO;
import com.massclouds.opencmp.service.DashboardService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: Dashboard管理的控制层代码
 * 
 * @date: 2022年4月25日 上午10:13:24
 * @author 
 */
@RestController
@Api(value = "Dashboard管理接口")
public class DashboardController extends BaseController{
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
    private RedisUtil redisUtil;
	
	/**
	 * @Description: 获取接管的各平台的数量
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/dashboard/platformCount")
	@ApiOperation(value = "获取接管的各平台的数量", notes = "获取接管的各平台的数量")
	public DataResultDTO<DashboardPlatformCountDTO> platformCount() {
		String token = getToken() != null ? getToken() : redisUtil.get("manageIQ.token").toString(); //大屏调用该接口时，token从redis里取
		return dashboardService.platformCountFromRedis(token);
	}
	
	/**
	 * @Description: 获取各个实例的数量
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/dashboard/entityCount")
	@ApiOperation(value = "获取各个实例的数量", notes = "获取各个实例的数量")
	public DataResultDTO<DashboardEntityCountDTO> entityCount() {
		String token = getToken() != null ? getToken() : redisUtil.get("manageIQ.token").toString();//大屏调用该接口时，token从redis里取
		return dashboardService.entityCountFromRedis(token);
	}
	
	/**
	 * @Description: 获取各个实例的数量，自服务用户模块调用,该接口不能缓存
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/dashboard/entityCountForSelf")
	@ApiOperation(value = "获取各个实例的数量，自服务用户模块调用", notes = "获取各个实例的数量，自服务用户模块调用")
	public DataResultDTO<DashboardEntityCountForSelfDTO> entityCountForSelf() {
		return dashboardService.entityCountForSelf(getToken());
	}

	/**
	 * @Description: 根据不同类型的统计数据
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/dashboard/statisticsByType")
	@ApiOperation(value = "根据不同类型的统计数据", notes = "根据不同类型的统计数据")
	public DataResultDTO<DashboardStatisticsByTypeDTO> statisticsByType() {
		String token = getToken() != null ? getToken() : redisUtil.get("manageIQ.token").toString();//大屏调用该接口时，token从redis里取
		return dashboardService.statisticsByTypeFromRedis(token);
	}
	
	/**
	 * @Description: 统计在线数，包括虚机在线数、用户在线数
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/dashboard/statisticsOnline")
	@ApiOperation(value = "统计在线数，包括虚机在线数、用户在线数", notes = "统计在线数，包括虚机在线数、用户在线数")
	public DataResultDTO<DashboardStatisticsOnlineDTO> statisticsOnline() {
		String token = getToken() != null ? getToken() : redisUtil.get("manageIQ.token").toString();//大屏调用该接口时，token从redis里取
		return dashboardService.statisticsOnlineFromRedis(token);
	}
	
	/**
	 * @Description: 统计在线数，包括虚机在线数、用户在线数
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/dashboard/vms6/{type}")
	@ApiOperation(value = "统计在线数，包括虚机在线数、用户在线数", notes = "统计在线数，包括虚机在线数、用户在线数")
	public DataResultDTO<List<Vms6SaveToRedisDTO>> list6VmsFromRedis(@PathVariable String type) {
		String token = getToken() != null ? getToken() : redisUtil.get("manageIQ.token").toString();//大屏调用该接口时，token从redis里取
		return dashboardService.list6VmsFromRedis(type, token);
	}
	
	/**
	 * @Description: 获取终端统计信息，统计终端总数、不同类型终端的数量。
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/dashboard/terminalStatisticsByType")
	@ApiOperation(value = "获取终端统计信息", notes = "获取终端统计信息")
	public DataResultDTO<DashboardTerminalStatisticsByTypeDTO> terminalStatisticsByType() {
		String token = getToken() != null ? getToken() : redisUtil.get("manageIQ.token").toString();;//大屏调用该接口时，token从redis里取
		return dashboardService.statisticsTerminalsByTypeFromRedis(token);
	}
	
}