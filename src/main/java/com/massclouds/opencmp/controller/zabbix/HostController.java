package com.massclouds.opencmp.controller.zabbix;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.zabbix.ZabbixHostCreateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixHostDTO;
import com.massclouds.opencmp.service.zabbix.HostService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: zabbix 主机 管理的控制层代码
 * 
 * @date: 2022年6月23日 上午10:13:24
 * @author 
 */
@RestController
@Api(value = "zabbix 主机管理接口")
public class HostController  extends BaseController{
	
	@Autowired
	private HostService hostService;
	
	/**
	 * @Description: 分页查询 主机
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/zabbix/host/listOfPage")
	@ApiOperation(value = "分页查询 主机", notes = "分页查询 主机")
	public PageDataResultDTO<List<ZabbixHostDTO>> hostsListOfPage(QueryDTO query) {
		return hostService.hostsListOfPage(query);
	}
	
	/**
	 * @Description: 创建主机
	 * 
	 * @param ZabbixHostCreateDTO  主机信息
	 * @return 操作结果
	 */
	@PostMapping("/zabbix/host/create")
	@ApiOperation(value = "创建主机", notes = "创建主机")
	public ResultDTO createHost(@RequestBody ZabbixHostCreateDTO zabbixHostCreateDTO) {
		return hostService.createHost(zabbixHostCreateDTO);
	}
	
	/**
	 * @Description: 启用/禁用主机
	 * 
	 * @param ZabbixHostCreateDTO  主机信息
	 * @return 操作结果
	 */
	@PostMapping("/zabbix/host/updateStatus")
	@ApiOperation(value = "启用/禁用主机", notes = "启用/禁用主机")
	public ResultDTO updateStatusHost(@RequestBody ZabbixHostCreateDTO zabbixHostCreateDTO) {
		return hostService.updateStatusHost(zabbixHostCreateDTO);
	}
	
	/**
	 * @Description: 删除主机
	 * 
	 * @param id  主机id
	 * @return 操作结果
	 */
	@PostMapping("/zabbix/host/delete")
	@ApiOperation(value = "删除主机", notes = "删除主机")
	public ResultDTO deleteHost(@RequestBody List<String> ids) {
		return hostService.deleteHost(ids);
	}
}
