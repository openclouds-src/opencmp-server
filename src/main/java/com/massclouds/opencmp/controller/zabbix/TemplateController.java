package com.massclouds.opencmp.controller.zabbix;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.zabbix.ZabbixHostCreateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixTemplateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixTriggerDTO;
import com.massclouds.opencmp.service.zabbix.TemplateService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: zabbix 模板 管理的控制层代码
 * 
 * @date: 2022年6月23日 上午10:13:24
 * @author 
 */
@RestController
@Api(value = "zabbix 模板管理接口")
public class TemplateController  extends BaseController{
	
	@Autowired
	private TemplateService templateService;
	
	/**
	 * @Description: 分页查询 模板
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/zabbix/template/listOfPage")
	@ApiOperation(value = "分页查询 模板", notes = "分页查询 模板")
	public PageDataResultDTO<List<ZabbixTemplateDTO>> templatesListOfPage(QueryDTO query) {
		return templateService.templatesListOfPage(query);
	}
	
	/**
	 * @Description: 获取模板列表
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/zabbix/template/list")
	@ApiOperation(value = "获取模板列表", notes = "获取模板列表")
	public DataResultDTO<List<ZabbixTemplateDTO>> templatesList() {
		return templateService.templatesList();
	}
	
	/**
	 * @Description: 分页查询 模板的触发器
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/zabbix/templateTrigger/listOfPage")
	@ApiOperation(value = "分页查询 模板的触发器", notes = "分页查询 模板的触发器")
	public PageDataResultDTO<List<ZabbixTriggerDTO>> templateTriggerListOfPage(QueryDTO query) {
		return templateService.templateTriggerListOfPage(query);
	}
	
	/**
	 * @Description: 启用/禁用触发器
	 * 
	 * @param ZabbixHostCreateDTO  触发器信息
	 * @return 操作结果
	 */
	@PostMapping("/zabbix/templateTrigger/updateStatus")
	@ApiOperation(value = "启用/禁用触发器", notes = "启用/禁用触发器")
	public ResultDTO updateStatusTrigger(@RequestBody ZabbixHostCreateDTO zabbixHostCreateDTO) {
		return templateService.updateStatusTrigger(zabbixHostCreateDTO);
	}
	
}
