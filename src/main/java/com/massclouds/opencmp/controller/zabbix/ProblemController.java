package com.massclouds.opencmp.controller.zabbix;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.zabbix.EventAcknowledgeDTO;
import com.massclouds.opencmp.dto.zabbix.ProblemSearchDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixProblemDTO;
import com.massclouds.opencmp.service.zabbix.ProblemService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Description: zabbix 告警事件 管理的控制层代码
 * 
 * @date: 2022年6月23日 上午10:13:24
 * @author 
 */
@RestController
@Api(value = "zabbix 告警事件管理接口")
public class ProblemController  extends BaseController{
	
	@Autowired
	private ProblemService problemService;
	
	/**
	 * @Description: 分页查询 告警事件
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/zabbix/problem/listOfPage")
	@ApiOperation(value = "分页查询 告警事件", notes = "分页查询 告警事件")
	public PageDataResultDTO<List<ZabbixProblemDTO>> problemListOfPage(ProblemSearchDTO query) {
		return problemService.problemListOfPage(query);
	}
	
	/**
	 * @Description: 确认事件
	 * 
	 * @param EventAcknowledgeDTO  确认信息
	 * @return 操作结果
	 */
	@PostMapping("/zabbix/problem/updateAcknowledge")
	@ApiOperation(value = "确认事件", notes = "确认事件")
	public ResultDTO updateAcknowledge(@RequestBody EventAcknowledgeDTO eventAcknowledgeDTO) {
		return problemService.updateAcknowledge(eventAcknowledgeDTO);
	}
	
}
