/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.InstanceActionDTO;
import com.massclouds.opencmp.dto.VmDetailsDTO;
import com.massclouds.opencmp.dto.VmDiskDTO;
import com.massclouds.opencmp.dto.VmHostOnRunDTO;
import com.massclouds.opencmp.dto.VmHtml5ConsoleDTO;
import com.massclouds.opencmp.dto.VmNoVNCDTO;
import com.massclouds.opencmp.dto.VmSetConfigDTO;
import com.massclouds.opencmp.dto.VmSetVmOwnershipDTO;
import com.massclouds.opencmp.dto.VmSpiceDTO;
import com.massclouds.opencmp.dto.VmTagDTO;
import com.massclouds.opencmp.dto.query.VmsQueryDTO;
import com.massclouds.opencmp.service.VmsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 虚拟机管理的控制层代码
 * 
 * @date: 2022年3月1日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "虚拟机管理接口")
public class VmsController extends BaseController{
	
	@Autowired
	private VmsService vmsService;
	
	/**
	 * @Description: 分页查询虚拟机
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/vms/list")
	@ApiOperation(value = "分页查询虚拟机", notes = "根据分页条件查询虚拟机")
	public PageDataResultDTO<List<VmDetailsDTO>> list(VmsQueryDTO query) {
		
		return vmsService.vmsList(query, getToken());
		
	}
	
	/**
	 * @Description: 虚拟机的电源操作
	 * 
	 * @param action 电源操作: start--开机、stop--断电、suspend--暂停、reboot--重启、shutdown--关机、reset--重置、refresh--刷新
	 * @return 操作结果
	 */
	@PostMapping("/vms/action")
	@ApiOperation(value = "电源操作", notes = "电源操作")
	public ResultDTO action(@RequestBody InstanceActionDTO instanceActionDTO) {
		
		return vmsService.vmAction(instanceActionDTO, getToken());
		
	}
	
	/**
	 * @Description: 获取虚拟机的noVNC信息
	 * 
	 * @param id
	 * @return 操作结果
	 */
	@GetMapping("/vms/{id}/vnc")
	@ApiOperation(value = "获取虚拟机的noVNC信息", notes = "获取虚拟机的noVNC信息")
	public DataResultDTO<VmNoVNCDTO> getVncInfo(@PathVariable String id) {
		
		return vmsService.getVncInfo(id, getToken());
		
	}
	
	/**
	 * @Description: 获取虚拟机的SPICE信息
	 * 
	 * @param id
	 * @return 操作结果
	 */
	@GetMapping("/vms/{id}/spice")
	@ApiOperation(value = "获取虚拟机的SPICE信息", notes = "获取虚拟机的SPICE信息")
	public DataResultDTO<VmSpiceDTO> getSpiceInfo(@PathVariable String id) {
		
		return vmsService.getSpiceInfo(id, getToken());
		
	}
	
	/**
	 * @Description: 获取虚拟机的Html5 console 信息
	 * 
	 * @param id
	 * @return 操作结果
	 */
	@GetMapping("/vms/{id}/html5")
	@ApiOperation(value = "获取虚拟机的Html5 console 信息", notes = "获取虚拟机的Html5 console 信息")
	public DataResultDTO<VmHtml5ConsoleDTO> getHtml5ControlInfo(@PathVariable String id) {
		
		return vmsService.getHtml5ControlInfo(id, getToken());
		
	}
	
	/**
	 * @Description: 获取不同平台的虚拟机的 console 信息
	 * 
	 * @param id
	 * @return 操作结果
	 */
	@GetMapping("/vms/{id}/console")
	@ApiOperation(value = "获取不同平台的虚拟机的 console 信息", notes = "获取不同平台的虚拟机的 console 信息")
	public DataResultDTO<Map<String, Object>> getControlInfo(@PathVariable String id,  String vendor) {
		
		return vmsService.getControlInfo(id, vendor, getToken());
		
	}
	
	/**
	 * @Description: 删除虚拟机
	 * 
	 * @param id  虚拟机id
	 * @return 操作结果
	 */
	@DeleteMapping("/vms/{id}")
	@ApiOperation(value = "删除虚拟机", notes = "删除虚拟机")
	public ResultDTO delete(@PathVariable String id) {
		
		return vmsService.deleteVm(id, getToken());
		
	}

	/**
	 * @Description: 获取运行的且ip不为空的虚机
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/vms/listOnRun")
	@ApiOperation(value = "获取运行的且ip不为空的虚机", notes = "获取运行的且ip不为空的虚机")
	public DataResultDTO<Map<String, List<VmHostOnRunDTO>>> listOnRun() {
		
		return vmsService.listOnRun(getToken());
		
	}
	
	/**
	 * @Description: 设置虚拟机的所有者
	 * @param VmSetVmOwnershipDTO
	 * @return 操作结果
	 */
	@PostMapping("/vms/ownership")
	@ApiOperation(value = "设置虚拟机的所有者", notes = "设置虚拟机的所有者")
	public ResultDTO setVmOwnership(@RequestBody VmSetVmOwnershipDTO vmSetVmOwnershipDTO) {
		
		return vmsService.setVmOwnership(vmSetVmOwnershipDTO.getId(), vmSetVmOwnershipDTO.getGroupName(), vmSetVmOwnershipDTO.getUserName(), getToken());
		
	}
	
	/**
	 * @Description: 设置虚拟机的标签
	 * @param VmTagDTO
	 * @return 操作结果
	 */
	@PostMapping("/vms/tag")
	@ApiOperation(value = "设置虚拟机的标签", notes = "设置虚拟机的标签")
	public ResultDTO setVmTag(@RequestBody VmTagDTO vmTagDTO) {
		
		return vmsService.setVmTag(vmTagDTO.getVmId(), vmTagDTO.getCategoryName(), vmTagDTO.getTagName(), getToken());
		
	}
	
	/**
	 * @Description: 编辑虚机的描述
	 * @param VmTagDTO
	 * @return 操作结果
	 */
	@PostMapping("/vms/{id}")
	@ApiOperation(value = "设置虚拟机的标签", notes = "设置虚拟机的标签")
	public ResultDTO updateVm(@PathVariable String id, @RequestBody VmsQueryDTO query) {
		
		return vmsService.updateVm(id, query, getToken());
		
	}
	
	/**
	 * @Description: 获取虚机的磁盘
	 * @return 操作结果
	 */
	@GetMapping("/vms/{id}/disks")
	@ApiOperation(value = "获取虚机的磁盘", notes = "获取虚机的磁盘")
	public DataResultDTO<List<VmDiskDTO>> getVmDisks(@PathVariable String id) {
		
		return vmsService.getVmDisks(id, getToken());
		
	}
	
	/**
	 * @Description: 虚机添加磁盘
	 * @param VmSetConfigDTO
	 * @return 操作结果
	 */
	@PostMapping("/vms/{id}/adddisk")
	@ApiOperation(value = "虚机添加磁盘", notes = "虚机添加磁盘")
	public ResultDTO addVmDisk(@PathVariable String id, @RequestBody VmSetConfigDTO vmSetConfigDTO) {
		
		return vmsService.addVmDisk(id, vmSetConfigDTO, getToken());
		
	}
	
	/**
	 * @Description: 虚机删除磁盘
	 * @param VmSetConfigDTO
	 * @return 操作结果
	 */
	@PostMapping("/vms/{id}/removedisk")
	@ApiOperation(value = "虚机删除磁盘", notes = "虚机删除磁盘")
	public ResultDTO removeVmDisk(@PathVariable String id, @RequestBody VmSetConfigDTO vmSetConfigDTO) {
		
		return vmsService.removeVmDisk(id, vmSetConfigDTO, getToken());
		
	}
	
	/**
	 * @Description: 虚机更改磁盘大小
	 * @param VmSetConfigDTO
	 * @return 操作结果
	 */
	@PostMapping("/vms/{id}/resizedisk")
	@ApiOperation(value = "虚机更改磁盘大小", notes = "虚机更改磁盘大小")
	public ResultDTO resizeVmDisk(@PathVariable String id, @RequestBody VmSetConfigDTO vmSetConfigDTO) {
		
		return vmsService.resizeVmDisk(id, vmSetConfigDTO, getToken());
		
	}
	
	/**
	 * @Description: 更改虚机的内存、CPU
	 * @param VmSetConfigDTO
	 * @return 操作结果
	 */
	@PostMapping("/vms/{id}/resetMemory")
	@ApiOperation(value = "更改虚机的内存、CPU", notes = "更改虚机的内存、CPU")
	public ResultDTO updateVmMemoryCPU(@PathVariable String id, @RequestBody VmSetConfigDTO vmSetConfigDTO) {
		
		return vmsService.updateVmMemoryCPU(id, vmSetConfigDTO, getToken());
		
	}
}