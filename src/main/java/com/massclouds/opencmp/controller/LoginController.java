/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.LoginUserDTO;
import com.massclouds.opencmp.service.LoginService;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: token管理的控制层代码
 * 
 * @date: 2022年4月24日 上午10:13:24
 */
@RestController
@Api(value = "token管理接口")
public class LoginController{
	
	@Autowired
	private LoginService loginService;
	
	/**
	 * @Description: 登录
	 * 
	 * @param  LoginUserDTO 用户登录信息
	 * @return 操作结果
	 */
	@PostMapping("/login")
	@ApiOperation(value = "登录", notes = "登录")
	public DataResultDTO<LoginUserDTO> login(@RequestBody LoginUserDTO loginUserDTO) {
		
		return loginService.login(loginUserDTO);
		
	}

}