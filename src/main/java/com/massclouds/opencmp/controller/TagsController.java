/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.TagCreateDTO;
import com.massclouds.opencmp.dto.TagDTO;
import com.massclouds.opencmp.dto.query.TagsQueryDTO;
import com.massclouds.opencmp.service.TagsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 标签 管理的控制层代码
 * 
 * @date: 2022年4月8日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "标签管理接口")
public class TagsController extends BaseController{
	
	@Autowired
	private TagsService tagsService;
	
	/**
	 * @Description: 分页查询标签
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/tags/listOfPage")
	@ApiOperation(value = "分页查询标签", notes = "根据分页条件查询标签")
	public PageDataResultDTO<List<TagDTO>> tagsListOfPage(TagsQueryDTO query) {
		
		return tagsService.tagsListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取标签列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/tags/list")
	@ApiOperation(value = "获取标签列表", notes = "获取标签列表")
	public DataResultDTO<List<TagDTO>> tagsList() {
		
		return tagsService.tagsList(getToken());
		
	}
	
	/**
	 * @Description: 添加标签
	 * 
	 * @param TagCreateDTO 标签信息
	 * @return 操作结果
	 */
	@PostMapping("/tags/create")
	@ApiOperation(value = "添加标签", notes = "添加标签")
	public ResultDTO createTag(@RequestBody TagCreateDTO tagCreateDTO) {
		
		return tagsService.createUpdateTag(tagCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑标签
	 * 
	 * @param TagCreateDTO 标签信息
	 * @return 操作结果
	 */
	@PostMapping("/tags/update")
	@ApiOperation(value = "编辑标签", notes = "编辑标签")
	public ResultDTO updateTag(@RequestBody TagCreateDTO tagCreateDTO) {
		
		return tagsService.createUpdateTag(tagCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 删除标签
	 * 
	 * @param id  标签id
	 * @return 操作结果
	 */
	@DeleteMapping("/tags/{id}")
	@ApiOperation(value = "删除标签", notes = "删除标签")
	public ResultDTO deleteTag(@PathVariable String id) {
		
		return tagsService.deleteTag(id, getToken());
		
	}
	
	/**
	 * @Description: 批量删除标签类
	 * 
	 * @param List<String> 标签id
	 * @return 操作结果
	 */
	@PostMapping("/tags/delete")
	@ApiOperation(value = "批量删除标签", notes = "批量删除标签")
	public ResultDTO deleteTags(@RequestBody List<String> ids) {
		
		return tagsService.deleteTags(ids, getToken());
		
	}
	
}