/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.CategoryDTO;
import com.massclouds.opencmp.service.CategoriesService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 标签类 管理的控制层代码
 * 
 * @date: 2022年4月8日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "标签类管理接口")
public class CategoriesController extends BaseController{
	
	@Autowired
	private CategoriesService categoriesService;
	
	/**
	 * @Description: 分页查询标签类
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/categories/listOfPage")
	@ApiOperation(value = "分页查询标签类", notes = "根据分页条件查询标签类")
	public PageDataResultDTO<List<CategoryDTO>> categoriesListOfPage(QueryDTO query) {
		
		return categoriesService.categoriesListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取标签类列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/categories/list")
	@ApiOperation(value = "获取标签类列表", notes = "获取标签类列表")
	public DataResultDTO<List<CategoryDTO>> categoriesList() {
		
		return categoriesService.categoriesList(getToken());
		
	}
	
	/**
	 * @Description: 添加标签类
	 * 
	 * @param UserCreateDTO 标签类信息
	 * @return 操作结果
	 */
	@PostMapping("/categories/create")
	@ApiOperation(value = "添加标签类", notes = "添加标签类")
	public ResultDTO createCategories(@RequestBody CategoryDTO categoryDTO) {
		
		return categoriesService.createOrUpdateCategories(categoryDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑标签类
	 * 
	 * @param UserCreateDTO 标签类信息
	 * @return 操作结果
	 */
	@PostMapping("/categories/update")
	@ApiOperation(value = "编辑标签类", notes = "编辑标签类")
	public ResultDTO updateCategories(@RequestBody CategoryDTO categoryDTO) {
		
		return categoriesService.createOrUpdateCategories(categoryDTO, getToken());
		
	}
	
	/**
	 * @Description: 删除标签类
	 * 
	 * @param id  标签类id
	 * @return 操作结果
	 */
	@DeleteMapping("/categories/{id}")
	@ApiOperation(value = "删除标签类", notes = "删除标签类")
	public ResultDTO deleteCategory(@PathVariable String id) {
		
		return categoriesService.deleteCategory(id, getToken());
		
	}
	
	/**
	 * @Description: 批量删除标签类户
	 * 
	 * @param List<String> 标签类id
	 * @return 操作结果
	 */
	@PostMapping("/categories/delete")
	@ApiOperation(value = "批量删除标签类", notes = "批量删除标签类")
	public ResultDTO deleteCategorys(@RequestBody List<String> ids) {
		
		return categoriesService.deleteCategorys(ids, getToken());
		
	}
	
}