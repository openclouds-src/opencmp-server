/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.ServiceCatalogsDTO;
import com.massclouds.opencmp.dto.ServiceCatalogsListDTO;
import com.massclouds.opencmp.dto.ServiceDialogsDTO;
import com.massclouds.opencmp.service.ServiceCatalogsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 服务目录 的控制层代码
 * 
 * @date: 2022年4月1日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "服务目录管理接口")
public class ServiceCatalogsController extends BaseController{
	
	@Autowired
	private ServiceCatalogsService serviceCatalogsService;
	
	/**
	 * @Description: 分页查询服务目录
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/serviceCatalogs/listOfPage")
	@ApiOperation(value = "分页查询服务目录", notes = "根据分页条件查询服务目录")
	public PageDataResultDTO<List<ServiceCatalogsDTO>> serviceCatalogsListOfPage(QueryDTO query) {
		
		return serviceCatalogsService.serviceCatalogsListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取所有服务目录
	 * 
	 * @return 操作结果
	 */
	@GetMapping("/serviceCatalogs/list")
	@ApiOperation(value = "获取所有服务目录", notes = "获取所有服务目录")
	public DataResultDTO<List<ServiceCatalogsListDTO>> serviceCatalogsList() {
		
		return serviceCatalogsService.serviceCatalogsList(getToken());
		
	}
	
	/**
	 * @Description: 添加服务目录
	 * 
	 * @param ServiceCatalogsDTO 服务目录信息
	 * @return 操作结果
	 */
	@PostMapping("/serviceCatalogs/create")
	@ApiOperation(value = "添加服务目录", notes = "添加服务目录")
	public ResultDTO createServiceCatalog(@RequestBody ServiceCatalogsDTO serviceCatalogsDTO) {
		
		return serviceCatalogsService.createServiceCatalog(serviceCatalogsDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑服务目录
	 * 
	 * @param ServiceCatalogsDTO 编辑目录信息
	 * @return 操作结果
	 */
	@PostMapping("/serviceCatalogs/update")
	@ApiOperation(value = "编辑服务目录", notes = "编辑服务目录")
	public ResultDTO updateServiceCatalog(@RequestBody ServiceCatalogsDTO serviceCatalogsDTO) {
		
		return serviceCatalogsService.updateServiceCatalog(serviceCatalogsDTO, getToken());
		
	}
	
	/**
	 * @Description: 删除服务目录
	 * 
	 * @param id  服务目录id
	 * @return 操作结果
	 */
	@DeleteMapping("/serviceCatalogs/{id}")
	@ApiOperation(value = "删除服务目录", notes = "删除服务目录")
	public ResultDTO deleteServiceCatalog(@PathVariable String id) {
		
		return serviceCatalogsService.deleteServiceCatalog(id, getToken());
		
	}
	

	/**
	 * @Description: 批量删除服务目录
	 * 
	 * @param List<String> 服务目录id
	 * @return 操作结果
	 */
	@PostMapping("/serviceCatalogs/delete")
	@ApiOperation(value = "批量删除服务目录", notes = "批量删除服务目录")
	public ResultDTO deleteServiceCatalogs(@RequestBody List<String> ids) {
		
		return serviceCatalogsService.deleteServiceCatalogs(ids, getToken());
		
	}
	
	
	/**
	 * @Description: 获取对话框列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/serviceDialogs/list")
	@ApiOperation(value = "分页查询服务模板", notes = "根据分页条件查询服务模板")
	public DataResultDTO<List<ServiceDialogsDTO>> serviceDialogsList() {
		
		return serviceCatalogsService.serviceDialogsList(getToken());
		
	}
	
}