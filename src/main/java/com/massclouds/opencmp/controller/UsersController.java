/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.UserCreateDTO;
import com.massclouds.opencmp.dto.UserDTO;
import com.massclouds.opencmp.dto.UserDetailsDTO;
import com.massclouds.opencmp.dto.query.UsersQueryDTO;
import com.massclouds.opencmp.service.UsersService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 用户管理的控制层代码
 * 
 * @date: 2022年4月6日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "用户管理接口")
public class UsersController extends BaseController{
	
	@Autowired
	private UsersService usersService;
	
	/**
	 * @Description: 分页查询用户
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/users/list")
	@ApiOperation(value = "分页查询用户", notes = "根据分页条件查询用户")
	public PageDataResultDTO<List<UserDTO>> usersList(UsersQueryDTO query) {
		
		return usersService.usersList(query, getToken());
		
	}

	/**
	 * @Description: 根据登录名称查询用户详情
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/users/details")
	@ApiOperation(value = "根据登录名称查询用户详情", notes = "根据登录名称查询用户详情")
	public DataResultDTO<UserDetailsDTO> usersDetailsByLoginName(@RequestParam String loginName) {
		
		return usersService.usersDetailsByLoginName(loginName, getToken());
		
	}
	
	/**
	 * @Description: 添加用户
	 * 
	 * @param UserCreateDTO 租户信息
	 * @return 操作结果
	 */
	@PostMapping("/users/create")
	@ApiOperation(value = "添加用户", notes = "添加用户")
	public ResultDTO createUser(@RequestBody UserCreateDTO userCreateDTO) {
		
		return usersService.createUser(userCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑 用户
	 * 
	 * @param UserCreateDTO 用户信息
	 * @return 操作结果
	 */
	@PostMapping("/users/update")
	@ApiOperation(value = "编辑用户", notes = "编辑用户")
	public ResultDTO updateUser(@RequestBody UserCreateDTO userCreateDTO) {
		
		return usersService.updateUser(userCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑 用户 密码
	 * 
	 * @param UserCreateDTO 用户信息
	 * @return 操作结果
	 */
	@PostMapping("/users/updatePassword")
	@ApiOperation(value = "编辑用户密码", notes = "编辑用户密码")
	public ResultDTO updateUserPassword(@RequestBody UserCreateDTO userCreateDTO) {
		
		return usersService.updateUserPassword(userCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 删除用户
	 * 
	 * @param id  用户id
	 * @return 操作结果
	 */
	@DeleteMapping("/users/{id}")
	@ApiOperation(value = "删除用户", notes = "删除用户")
	public ResultDTO deleteUser(@PathVariable String id) {
		
		return usersService.deleteUser(id, getToken());
		
	}
	
	/**
	 * @Description: 批量删除用户
	 * 
	 * @param List<String> 用户id
	 * @return 操作结果
	 */
	@PostMapping("/users/delete")
	@ApiOperation(value = "批量删除用户", notes = "批量删除用户")
	public ResultDTO deleteUsers(@RequestBody List<String> userIds) {
		
		return usersService.deleteUsers(userIds, getToken());
		
	}
	
}