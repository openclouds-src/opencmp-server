/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.massclouds.opencmp.dto.UserGroupCreateDTO;
import com.massclouds.opencmp.dto.UserGroupDTO;
import com.massclouds.opencmp.service.UserGroupsService;
import com.massclouds.opencmp.utils.BaseController;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @Description: 用户组 管理的控制层代码
 * 
 * @date: 2022年4月6日 上午10:13:24
 * @author hou_jjing
 */
@RestController
@Api(value = "用户组管理接口")
public class UserGroupsController extends BaseController{
	
	@Autowired
	private UserGroupsService userGroupsService;
	
	/**
	 * @Description: 分页查询用户组
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/userGroups/listOfPage")
	@ApiOperation(value = "分页查询用户组", notes = "根据分页条件查询用户组")
	public PageDataResultDTO<List<UserGroupDTO>> groupsListOfPage(QueryDTO query) {
		
		return userGroupsService.groupsListOfPage(query, getToken());
		
	}
	
	/**
	 * @Description: 获取用户组列表
	 * 
	 * @param 
	 * @return 操作结果
	 */
	@GetMapping("/userGroups/list")
	@ApiOperation(value = "获取用户组列表", notes = "获取用户组列表")
	public DataResultDTO<List<UserGroupDTO>> groupsList() {
		
		return userGroupsService.groupsList(getToken());
		
	}
	
	/**
	 * @Description: 根据id查询用户组
	 * 
	 * @param query 查询条件
	 * @return 操作结果
	 */
	@GetMapping("/userGroups/{id}")
	@ApiOperation(value = "分页查询用户组", notes = "根据分页条件查询用户组")
	public DataResultDTO<UserGroupDTO> getById(@PathVariable String id) {
		
		return userGroupsService.getById(id, getToken());
		
	}

	/**
	 * @Description: 添加用户组
	 * 
	 * @param UserGroupCreateDTO 用户组录信息
	 * @return 操作结果
	 */
	@PostMapping("/userGroups/create")
	@ApiOperation(value = "添加用户组", notes = "添加用户组")
	public ResultDTO createUserGroup(@RequestBody UserGroupCreateDTO userGroupCreateDTO) {
		
		return userGroupsService.createOrUpdateUserGroup(userGroupCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 编辑 用户组
	 * 
	 * @param UserGroupCreateDTO 用户组信息
	 * @return 操作结果
	 */
	@PostMapping("/userGroups/update")
	@ApiOperation(value = "编辑用户组", notes = "编辑用户组")
	public ResultDTO updateUserGroup(@RequestBody UserGroupCreateDTO userGroupCreateDTO) {
		
		return userGroupsService.createOrUpdateUserGroup(userGroupCreateDTO, getToken());
		
	}
	
	/**
	 * @Description: 删除用户组
	 * 
	 * @param id  用户组id
	 * @return 操作结果
	 */
	@DeleteMapping("/userGroups/{id}")
	@ApiOperation(value = "删除用户组", notes = "删除用户组")
	public ResultDTO deleteUserGroup(@PathVariable String id) {
		
		return userGroupsService.deleteUserGroup(id, getToken());
		
	}
	
	/**
	 * @Description: 批量删除用户组
	 * 
	 * @param List<String> 用户id
	 * @return 操作结果
	 */
	@PostMapping("/userGroups/delete")
	@ApiOperation(value = "批量删除用户组", notes = "批量删除用户组")
	public ResultDTO deleteUserGroups(@RequestBody List<String> userIds) {
		
		return userGroupsService.deleteUserGroups(userIds, getToken());
		
	}
	
}