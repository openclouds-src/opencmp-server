/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.UserCreateDTO;
import com.massclouds.opencmp.dto.UserDTO;
import com.massclouds.opencmp.dto.UserDetailsDTO;
import com.massclouds.opencmp.dto.query.UsersQueryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月6日 上午10:13:24
 * @author hou_jjing
 */
public interface UsersService {
	
	PageDataResultDTO<List<UserDTO>> usersList(UsersQueryDTO query, String token);

	DataResultDTO<UserDetailsDTO> usersDetailsByLoginName(String loginName, String token);
	
	ResultDTO createUser(UserCreateDTO userCreateDTO, String token);
	
	ResultDTO createMultipleUser(List<UserCreateDTO> userCreateDTOList, String token);
	
	ResultDTO updateUser(UserCreateDTO userCreateDTO, String token);
	
	ResultDTO updateUserPassword(UserCreateDTO userCreateDTO, String token);
	
	ResultDTO deleteUser(String id, String token);
	
	ResultDTO deleteUsers(List<String> userIds, String token);
	
	List<UserDTO> usersAllList(String token);
	
}