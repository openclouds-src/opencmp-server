/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.AnsiblePlaybookDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 自动化（ansible） 脚本管理的业务层接口
 * 
 * @date: 2022年8月24日 上午10:13:24
 * @author hou_jjing
 */
public interface PlaybookManageService {
	
	PageDataResultDTO<List<AnsiblePlaybookDTO>> playbookList(QueryDTO query, String token);
	
	ResultDTO deletePlaybooks(List<String> playbookNames, String token);
	
	DataResultDTO<AnsiblePlaybookDTO> getPlaybookContent(String name, String token);
	
	ResultDTO updatePlaybook(AnsiblePlaybookDTO playbook, String token);
}