/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.massclouds.opencmp.dto.ReportDTO;
import com.massclouds.opencmp.dto.ReportResultsDTO;
import com.massclouds.opencmp.dto.ReportScheduleCreateDTO;
import com.massclouds.opencmp.dto.ReportSchedulesReturnDTO;
import com.massclouds.opencmp.dto.ReportTemplateDTO;
import com.massclouds.opencmp.dto.query.ReportsQueryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年5月26日 上午10:13:24
 * @author hou_jjing
 */
public interface ReportsService {
	
	PageDataResultDTO<List<ReportTemplateDTO>> reportTemplatesListOfPage(QueryDTO query, String token);
	
	DataResultDTO<List<ReportTemplateDTO>> reportTemplatesList(String token);
	
	ResultDTO createReportFromTemplate(String templateId, String token);
	
	ResultDTO deleteReportTemplate(String id, String token);
	
	
	PageDataResultDTO<List<ReportDTO>> reportsListOfPage(ReportsQueryDTO query, String token);
	
	DataResultDTO<ReportResultsDTO> reportResult(String reportId, String resultId, String token);
	
	void downloadReport(String reportId, String resultId, HttpServletResponse response, String token);
	
	ResultDTO deleteReport(String id, String token);
	
	
	ResultDTO createSchedules(ReportScheduleCreateDTO reportScheduleCreateDTO, String token);
	
	PageDataResultDTO<List<ReportSchedulesReturnDTO>> schedulesListOfPage(QueryDTO query, String token);
	
	ResultDTO deleteSchedules(String id, String token);
	
	ResultDTO editSchedules(String id, QueryDTO query,String token);
	
	ResultDTO schedulesAction(QueryDTO query, String token);
}