/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.DashboardEntityCountDTO;
import com.massclouds.opencmp.dto.DashboardEntityCountForSelfDTO;
import com.massclouds.opencmp.dto.DashboardPlatformCountDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsOnlineDTO;
import com.massclouds.opencmp.dto.DashboardTerminalStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.Vms6SaveToRedisDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月25日 上午10:13:24
 * @author 
 */
public interface DashboardService {
	
	DataResultDTO<DashboardPlatformCountDTO> platformCount(String token);
	
	DataResultDTO<DashboardEntityCountDTO> entityCount(String token);
	
	DataResultDTO<DashboardStatisticsByTypeDTO> statisticsByType(String token);
	
	DataResultDTO<DashboardStatisticsOnlineDTO> statisticsOnline(String token);
	
	DataResultDTO<DashboardEntityCountForSelfDTO> entityCountForSelf(String token);
	
	DataResultDTO<List<Vms6SaveToRedisDTO>> list6Vms(String vmType,String token);
	
	DataResultDTO<DashboardTerminalStatisticsByTypeDTO> statisticsTerminalsByType(String token);
	
	
    DataResultDTO<DashboardPlatformCountDTO> platformCountFromRedis(String token);
	
	DataResultDTO<DashboardEntityCountDTO> entityCountFromRedis(String token);
	
	DataResultDTO<DashboardStatisticsByTypeDTO> statisticsByTypeFromRedis(String token);
	
	DataResultDTO<DashboardStatisticsOnlineDTO> statisticsOnlineFromRedis(String token);
	
	DataResultDTO<List<Vms6SaveToRedisDTO>> list6VmsFromRedis(String vmType,String token);
	
	DataResultDTO<DashboardTerminalStatisticsByTypeDTO> statisticsTerminalsByTypeFromRedis(String token);
	
}