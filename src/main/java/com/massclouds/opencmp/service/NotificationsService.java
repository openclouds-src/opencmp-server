/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.NotificationCountDTO;
import com.massclouds.opencmp.dto.NotificationDTO;
import com.massclouds.opencmp.dto.query.NotificationsQueryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年5月25日 上午10:13:24
 * @author hou_jjing
 */
public interface NotificationsService {
	
	PageDataResultDTO<List<NotificationDTO>> notificationsListOfPage(NotificationsQueryDTO query, String token);
	
	DataResultDTO<NotificationCountDTO> notificationsCount(String token);
	
	ResultDTO readNotifications(List<String> ids, String token);
	
	ResultDTO deleteNotifications(List<String> ids, String token);
	
}