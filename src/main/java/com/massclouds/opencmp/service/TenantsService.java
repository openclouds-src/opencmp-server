/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.TenantResourceStatisticsDTO;
import com.massclouds.opencmp.dto.TenantsCreateDTO;
import com.massclouds.opencmp.dto.TenantsDTO;
import com.massclouds.opencmp.dto.TenantsTreeDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月7日 上午10:13:24
 * @author hou_jjing
 */
public interface TenantsService {
	
	PageDataResultDTO<List<TenantsDTO>> tenantsListOfPage(QueryDTO query, String token);
	
	DataResultDTO<List<TenantsDTO>> tenantsList(String token);
	
	DataResultDTO<List<TenantsTreeDTO>> tenantsListTree(String token);
	
	ResultDTO createOrUpdateTenant(TenantsCreateDTO tenantsCreateDTO, String token);
	
	ResultDTO createTenant(TenantsCreateDTO tenantsCreateDTO, String token);
	
	ResultDTO deleteTenant(String id, String token);
	
	ResultDTO deleteTenants(List<String> ids, String token);
	
	DataResultDTO<TenantResourceStatisticsDTO> resourceStatisticsTenant(String id, String token);
	
	DataResultDTO<TenantsDTO> isExistByName(String name, String token);
	
}