/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.ServiceCountDTO;
import com.massclouds.opencmp.dto.ServiceCreateDTO;
import com.massclouds.opencmp.dto.ServiceDTO;
import com.massclouds.opencmp.dto.ServiceRequestApprovalDTO;
import com.massclouds.opencmp.dto.ServiceRequestCountDTO;
import com.massclouds.opencmp.dto.ServiceRequestDTO;
import com.massclouds.opencmp.dto.query.ServerQueryDTO;
import com.massclouds.opencmp.dto.query.ServerRequestQueryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月1日 上午10:13:24
 * @author hou_jjing
 */
public interface ServiceService {
	
	
	PageDataResultDTO<List<ServiceDTO>> servicesList(ServerQueryDTO query, String token);
	
	ResultDTO createService(ServiceCreateDTO serviceCreateDTO, String token);
	
	ResultDTO retireService(String id, String token);
	
	ResultDTO deleteService(String id, String token);
	
	ResultDTO deleteServices(List<String> ids, String token);
	
	DataResultDTO<ServiceCountDTO> servicesCount(String token);
	
	
	
	PageDataResultDTO<List<ServiceRequestDTO>> serviceRequestList(ServerRequestQueryDTO query, String token);
	
	DataResultDTO<ServiceRequestCountDTO> serviceRequestCount(String token);
	
	ResultDTO serviceRequestApproval(ServiceRequestApprovalDTO serviceRequestApprovalDTO, String token);
	
	
}