package com.massclouds.opencmp.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.AnsiblePlaybookDTO;
import com.massclouds.opencmp.service.PlaybookManageService;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("PlaybookManageService")
public class PlaybookManageServiceImpl  implements PlaybookManageService{

	@Value("${ansible.playbook.path}")
    private String playbookPath;
	
	@Override
	public PageDataResultDTO<List<AnsiblePlaybookDTO>> playbookList(QueryDTO query, String token) {
		PageDataResultDTO<List<AnsiblePlaybookDTO>> result = new PageDataResultDTO<>();
		List<AnsiblePlaybookDTO> playbookList = new ArrayList<AnsiblePlaybookDTO>();
		
		File[] files = new File(playbookPath).listFiles();
		for (int i = 0; i < files.length; i++) {
			AnsiblePlaybookDTO playbook = new AnsiblePlaybookDTO();
			playbook.setName(files[i].getName());
			playbook.setSize(StringUtils.formetFileSize(files[i].length()));
			
			playbookList.add(playbook);
		}
		
		int page = query.getPage();
		int size = query.getPageSize();
		int count = playbookList.size();
		int pageBegin = page == 0 ? 0 : page * size;
		int pageEnd = pageBegin + size;
		pageEnd = pageEnd > count ? count : pageEnd;
		result.setSuccess(playbookList.subList(pageBegin, pageEnd), (long) count);
		return result;
	}
	
	@Override
	public ResultDTO deletePlaybooks(List<String> playbookNames, String token) {
		ResultDTO result = new ResultDTO();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < playbookNames.size(); i++) {
			File playbook = new File(playbookPath + File.separator + playbookNames.get(i));
			if (playbook.exists()) {
				boolean deleted = playbook.delete();
				if (!deleted) {
					sb.append(playbookNames.get(i) + " 删除失败！").append("\r\n");
				}
			} else {
				sb.append(playbookNames.get(i) + " 不存在！").append("\r\n");
			}
		}
		if (sb.length() > 0) {
			result.setError(sb.toString());
		} else {
			result.setSuccess();
		}
		return result;
	}

	@Override
	public DataResultDTO<AnsiblePlaybookDTO> getPlaybookContent(String name, String token) {
		DataResultDTO<AnsiblePlaybookDTO> result = new DataResultDTO<AnsiblePlaybookDTO>();
		File playbook = new File(playbookPath + File.separator + name);
		BufferedReader reader = null;
		StringBuffer contentSB = new StringBuffer();
		AnsiblePlaybookDTO playbookDTO = new AnsiblePlaybookDTO();
		try {
			
			if (playbook.exists()) {
	            reader = new BufferedReader(new FileReader(playbook));
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	            	contentSB.append(line).append("\r\n");
	            }
	            reader.close();
	            playbookDTO.setContent(contentSB.toString());
	            playbookDTO.setName(name);
			    
	            result.setSuccess(playbookDTO);
			} else {
				result.setError("脚本文件不存在！");
			}
		 } catch (IOException e) {
	            e.printStackTrace();
	            result.setError("读取脚本数据出错！");
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e1) {
	                	result.setError("读取脚本数据出错！");
	                }
	            }
	        }
		return result;
	}

	@Override
	public ResultDTO updatePlaybook(AnsiblePlaybookDTO playbookDTO, String token) {
		ResultDTO result = new ResultDTO();
		String playbootPath = playbookPath + File.separator + playbookDTO.getName();
		try {
			File playbook = new File(playbootPath);
			if (!playbook.exists()) {
				boolean created = playbook.createNewFile();
				if (!created) {
					result.setError("该脚本文件不存在，且创建失败！");
					return result;
				}
			}
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(playbootPath));
			bufferedWriter.write(playbookDTO.getContent()); 
			bufferedWriter.close();
			result.setSuccess("脚本内容更新成功！");
		}catch(Exception e) {
	    	e.printStackTrace();
	    	result.setError("脚本内容更新失败！");
	    }
		
		return result;
	}
	
	
}
 