package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.ReportDTO;
import com.massclouds.opencmp.dto.ReportResultsDTO;
import com.massclouds.opencmp.dto.ReportScheduleCreateDTO;
import com.massclouds.opencmp.dto.ReportSchedulesDTO;
import com.massclouds.opencmp.dto.ReportSchedulesReturnDTO;
import com.massclouds.opencmp.dto.ReportTemplateDTO;
import com.massclouds.opencmp.dto.query.ReportsQueryDTO;
import com.massclouds.opencmp.service.ReportsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.DateFormatUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.PDFUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("ReportsService")
public class ReportsServiceImpl  implements ReportsService{

	@Override
	public PageDataResultDTO<List<ReportTemplateDTO>> reportTemplatesListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<ReportTemplateDTO>> result = new PageDataResultDTO<>();
		String url = "/api/reports" + "?expand=resources" + "&attributes=";
		
		int page = query.getPage();
		int size = query.getPageSize();
		int offset = page == 0 ? 0 : page * size;
		
		url = url + "&filter[]=rpt_type=Custom";//过滤掉manageiq初始化的报表,自己创建的报表模板类型是Custom
		try {
			//根据 名称 模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			
			//根据指定的排序列进行排序，指定列包括：name、title、created_on
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			url = url + "&offset=" + offset + "&limit=" + size;
			ResponseEntity<MiqListReturnDTO<ReportTemplateDTO>> reportTemplatesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ReportTemplateDTO>>(){}, token);
			if (401 == reportTemplatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == reportTemplatesRes.getStatusCodeValue()) {
 		    	result.setSuccess(reportTemplatesRes.getBody().getResources(), (long) reportTemplatesRes.getBody().getSubquery_count());
 		    } else {
 		    	result.setError(500, "获取报表模板信息失败！");
 		    }
			
			return result;
			
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<List<ReportTemplateDTO>> reportTemplatesList(String token) {
		DataResultDTO<List<ReportTemplateDTO>> result = new DataResultDTO<>();
		String url = "/api/reports" + "?expand=resources" + "&attributes=";
		url = url + "&filter[]=rpt_type=Custom";//过滤掉manageiq初始化的报表,自己创建的报表模板类型是Custom
		url = url + "&sort_by=id&sort_order=desc";
		try {
			ResponseEntity<MiqListReturnDTO<ReportTemplateDTO>> reportTemplatesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ReportTemplateDTO>>(){}, token);
			if (401 == reportTemplatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == reportTemplatesRes.getStatusCodeValue()) {
				result.setSuccess(reportTemplatesRes.getBody().getResources());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	/**
	 * 对不同实例的操作方法，如run、delete
	 * @param url
	 * @param token
	 * @param action
	 * @return
	 */
	public ResultDTO instanceAction(HashMap<String, Object> params, String url, String token) {
		ResultDTO result = new ResultDTO();
        try {
        	ResponseEntity<String> reportRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(reportRes.getBody());
			
        	if (401 == reportRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == reportRes.getStatusCodeValue()) {
 		    	result.setSuccess();
        		result.setMessage("操作成功。");
        	} else {
				result.setError(jsonNode.get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	
	}
	@Override
	public ResultDTO deleteReportTemplate(String templateId, String token) {
		String url = "/api/reports/" + templateId;
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
		return instanceAction(params, url, token);
	}
	
	@Override
	public ResultDTO createReportFromTemplate(String templateId, String token) {
		String url = "/api/reports/" + templateId;
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "run");
		return instanceAction(params, url, token);
	}

	@Override
	public PageDataResultDTO<List<ReportDTO>> reportsListOfPage(ReportsQueryDTO query, String token) {
		PageDataResultDTO<List<ReportDTO>> result = new PageDataResultDTO<>();
		String url = "/api/results" + "?expand=resources" + "&attributes=miq_task&filter[]=miq_report_id!=null";
		
		int page = query.getPage();
		int size = query.getPageSize();
		int offset = page == 0 ? 0 : page * size;
		
		try {
			//根据 报表模板id进行查询
			if (!StringUtils.isEmpty(query.getTemplateId())) {
				url = url + "&filter[]=" + "miq_report_id=" + query.getTemplateId();
			}
			//根据 报表名称进行查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + query.getName();
			}
			//根据 报表生成时间进行查询
			if (!StringUtils.isEmpty(query.getCreateDate())) {
				url = url + "&filter[]=" + "created_on>" + query.getCreateDate();
			}
			url = url + "&offset=" + offset + "&limit=" + size;
			//根据指定的排序列进行排序，指定列包括：name、created_on
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			ResponseEntity<MiqListReturnDTO<ReportDTO>> reportsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ReportDTO>>(){}, token);
			if (401 == reportsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }else if (200 == reportsRes.getStatusCodeValue() ) {
 		    	result.setSuccess(reportsRes.getBody().getResources(), (long) reportsRes.getBody().getSubquery_count());
        	} else {
				result.setError("获取信息失败！");
        	}
			return result;
			
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<ReportResultsDTO> reportResult(String reportId, String resultId, String token) {
		DataResultDTO<ReportResultsDTO> result = new DataResultDTO<>();
		String url = "/api/reports/" + reportId + "/results/" + resultId ;
		try {
			ResponseEntity<ReportResultsDTO> reportTemplatesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<ReportResultsDTO>(){}, token);
			if (401 == reportTemplatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == reportTemplatesRes.getStatusCodeValue()) {
				result.setSuccess(reportTemplatesRes.getBody());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public void downloadReport(String reportId, String resultId, HttpServletResponse response, String token) {
		String fileName = "report_" + DateFormatUtils.getCurrentDate() + ".pdf";
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/pdf");
			//打开浏览器窗口预览文件
//			response.setHeader("Content-Disposition","filename=" + new String(fileName.getBytes(), "iso8859-1"));
			//直接下载
			response.setHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes(), "iso8859-1"));

			DataResultDTO<ReportResultsDTO> reportResult = reportResult(reportId, resultId, token);
			if (reportResult.getResult() != null) {
				PDFUtils pdfUtils = new PDFUtils();
				pdfUtils.createPDF(reportResult.getResult(), response);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public ResultDTO deleteReport(String id, String token) {
		String url = "/api/results/" + id;
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
		return instanceAction(params, url, token);
	}

	@Override
	public ResultDTO createSchedules(ReportScheduleCreateDTO reportScheduleCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		HashMap<String, Object> params = new HashMap<>();
		params.put("action", "schedule");
		
        HashMap<String, Object> paramsResource = new HashMap<>();
        paramsResource.put("name", reportScheduleCreateDTO.getName());
        paramsResource.put("description", reportScheduleCreateDTO.getDescription());
        paramsResource.put("resource_type", "MiqReport");    
        paramsResource.put("start_date", reportScheduleCreateDTO.getStartDate());    
        paramsResource.put("time_zone", "UTC");    
        paramsResource.put("enabled", Boolean.TRUE);
        Map<String, String> intervalMap = new HashMap<String, String>();
        intervalMap.put("unit", reportScheduleCreateDTO.getIntervalUnit());
        if (!StringUtils.isEmpty(reportScheduleCreateDTO.getIntervalValue())) {
        	intervalMap.put("value", reportScheduleCreateDTO.getIntervalValue());
        }
        paramsResource.put("interval", intervalMap);
        
        params.put("resource", paramsResource);
        
        String url = "/api/reports/" + reportScheduleCreateDTO.getReportId();
        try {
        	
        	ResponseEntity<String> scheduleRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == scheduleRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == scheduleRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("调度任务添加成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(scheduleRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public PageDataResultDTO<List<ReportSchedulesReturnDTO>> schedulesListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<ReportSchedulesReturnDTO>> result = new PageDataResultDTO<>();
		Map<String, String> reportTemplateMap = new HashMap<String, String>();
		List<ReportSchedulesDTO> allSchedules = new ArrayList<ReportSchedulesDTO>();
		List<ReportSchedulesDTO> searchedSchedules = new ArrayList<ReportSchedulesDTO>();
		List<ReportSchedulesDTO> subSchedules = new ArrayList<ReportSchedulesDTO>();
		List<ReportSchedulesReturnDTO> returnSchedules = new ArrayList<ReportSchedulesReturnDTO>();
		int count = 0;
		try {
			//1、获取所有的报表模板
			List<ReportTemplateDTO> reportTemplateList = reportTemplatesList(token).getResult();
			if (reportTemplateList != null) {
				for (ReportTemplateDTO reportTemplateDTO: reportTemplateList) {
					//把报表模板处理成map
					reportTemplateMap.put(reportTemplateDTO.getId(), reportTemplateDTO.getName());
					//2、获取每个报表模板的调度任务
					String url = "/api/reports/" +  reportTemplateDTO.getId() + "/schedules?expand=resources&attributes=";
					url = url + "&sort_by=created_on&sort_order=desc";
					ResponseEntity<MiqListReturnDTO<ReportSchedulesDTO>> schedulesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ReportSchedulesDTO>>(){}, token);
					if (401 == schedulesRes.getStatusCodeValue()) {
		 				result.setError(401, ConstantsUtils.tokenMessage);
		 				return result;
		 		    }
					//3、整合所有报表模板的调度任务
					if (200 == schedulesRes.getStatusCodeValue() && schedulesRes.getBody().getResources() != null) {
						allSchedules.addAll(schedulesRes.getBody().getResources());
		 		    }
				}
			}
			
			//4、根据分页获取当前页的返回list
			if (allSchedules != null && allSchedules.size() > 0) {
				
				//4.1 根据查询条件进行查询，名称、描述
				if (!StringUtils.isEmpty(query.getName()) && !StringUtils.isEmpty(query.getDescription())) { //名称、描述组合查询
					searchedSchedules = allSchedules.stream().filter(x -> (x.getName().contains(query.getName()) == true && x.getDescription().contains(query.getDescription()) == true)).collect(Collectors.toList());
				} else if (!StringUtils.isEmpty(query.getName()) && StringUtils.isEmpty(query.getDescription())) {//名称查询
					searchedSchedules = allSchedules.stream().filter(x -> (x.getName().contains(query.getName()) == true)).collect(Collectors.toList());
				} else if (StringUtils.isEmpty(query.getName()) && !StringUtils.isEmpty(query.getDescription())) {//描述查询
					searchedSchedules = allSchedules.stream().filter(x -> (x.getDescription().contains(query.getDescription()) == true)).collect(Collectors.toList());
				} else {//不查询
					searchedSchedules = allSchedules;
				}
				
				int page = query.getPage();
				int size = query.getPageSize();
				count = searchedSchedules.size();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				subSchedules = searchedSchedules.subList(pageBegin, pageEnd);
			}
			//5、处理当前页的返回list,整理出任务周期信息
			if (subSchedules != null && subSchedules.size() > 0) {
				for (ReportSchedulesDTO item : subSchedules) {
					ReportSchedulesReturnDTO reportSchedulesReturnDTO = new ReportSchedulesReturnDTO();
					reportSchedulesReturnDTO.setHref(item.getHref());
					reportSchedulesReturnDTO.setId(item.getId());
					reportSchedulesReturnDTO.setName(item.getName());
					reportSchedulesReturnDTO.setDescription(item.getDescription());
					reportSchedulesReturnDTO.setEnabled(item.isEnabled());
					reportSchedulesReturnDTO.setCreated_on(DateFormatUtils.utcToBeijing(item.getCreated_on()));
					reportSchedulesReturnDTO.setUpdated_at(DateFormatUtils.utcToBeijing(item.getUpdated_at()));
					reportSchedulesReturnDTO.setLast_run_on(item.getLast_run_on() == null ? null : DateFormatUtils.utcToBeijing(item.getLast_run_on()));
					reportSchedulesReturnDTO.setFilter(item.getFilter());
					reportSchedulesReturnDTO.setRun_at(item.getRun_at());
					
					Map<Object, Object> expO = (Map<Object, Object>) item.getFilter().get("exp");
					String reportTemplateId = ((Map<Object, Object>) expO.get("=")).get("value").toString();
					reportSchedulesReturnDTO.setReportTemplateId(reportTemplateId);
					reportSchedulesReturnDTO.setReportTemplateName(reportTemplateMap.get(reportTemplateId));
					
					String startTime = DateFormatUtils.utcToBeijing(item.getRun_at().get("start_time").toString());
					reportSchedulesReturnDTO.setStartTime(startTime);
					Map<Object, Object> intervalO = (Map<Object, Object>) item.getRun_at().get("interval");
					String unit = intervalO.get("unit").toString();
					String value = intervalO.get("value") == null ? null : intervalO.get("value").toString();
					
					String message = null;
					switch (unit) {
					 	case "once": message = "从 " + startTime + " 开始，只运行一次。"; break;
					 	case "hourly": message = "从 " + startTime + " 开始，每 " + value + " 个小时运行一次。"; break;
					 	case "daily": message = "从 " + startTime + " 开始，每 " + value + " 天运行一次。"; break;
					 	case "weekly": message = "从 " + startTime + " 开始，每 " + value + " 个周运行一次。"; break;
					 	case "monthly": message = "从 " + startTime + " 开始，每 " + value + " 个月运行一次。"; break;
					 	default: message = null; break;
					}
					reportSchedulesReturnDTO.setSchedulecycle(message);
					returnSchedules.add(reportSchedulesReturnDTO);
				}
			}
			result.setSuccess(returnSchedules, (long)count);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			result.setError();
        	return result; 
		}
	}

	@Override
	public ResultDTO deleteSchedules(String id, String token) {
		String url = "/api/schedules/" + id;
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
		return instanceAction(params,url, token);
	}

	@Override
	public ResultDTO editSchedules(String id, QueryDTO query, String token) {
		String url = "/api/schedules/" + id;
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        if (!StringUtils.isEmpty(query.getName())) {
        	params.put("name", query.getName());
        }
        if (!StringUtils.isEmpty(query.getDescription())) {
        	params.put("description", query.getDescription());
        }
		return instanceAction(params, url, token);
	}

	@Override
	public ResultDTO schedulesAction(QueryDTO query, String token) {
		String url = "/api/schedules/" + query.getId();
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        if ("start".equals(query.getCriteria())) {
        	params.put("enabled", "true");
        }
        if ("stop".equals(query.getCriteria())) {
        	params.put("enabled", "false");
        }
        params.put("action", "edit");
        
		return instanceAction(params, url, token);
	}
	
}
