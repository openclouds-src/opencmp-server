package com.massclouds.opencmp.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.AnsibleRepositoryDTO;
import com.massclouds.opencmp.service.RepositoryManageService;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("RepositoryManageService")
public class RepositoryManageServiceImpl  implements RepositoryManageService{

	@Value("${ansible.repository.path}")
    private String repositoryPath;
	
	@Override
	public PageDataResultDTO<List<AnsibleRepositoryDTO>> repositoryList(QueryDTO query, String token) {
		PageDataResultDTO<List<AnsibleRepositoryDTO>> result = new PageDataResultDTO<>();
		List<AnsibleRepositoryDTO> repositoryList = new ArrayList<AnsibleRepositoryDTO>();
		
		File[] files = new File(repositoryPath).listFiles();
		for (int i = 0; i < files.length; i++) {
			AnsibleRepositoryDTO repository = new AnsibleRepositoryDTO();
			repository.setName(files[i].getName());
			repository.setSize(StringUtils.formetFileSize(files[i].length()));
			
			repositoryList.add(repository);
		}
		
		int page = query.getPage();
		int size = query.getPageSize();
		int count = repositoryList.size();
		int pageBegin = page == 0 ? 0 : page * size;
		int pageEnd = pageBegin + size;
		pageEnd = pageEnd > count ? count : pageEnd;
		result.setSuccess(repositoryList.subList(pageBegin, pageEnd), (long) count);
		return result;
	}
	

	@Override
	public ResultDTO deleteRepository(List<String> names, String token) {
		ResultDTO result = new ResultDTO();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < names.size(); i++) {
			File playbook = new File(repositoryPath + File.separator + names.get(i));
			if (playbook.exists()) {
				boolean deleted = playbook.delete();
				if (!deleted) {
					sb.append(names.get(i) + " 删除失败！").append("\r\n");
				}
			} else {
				sb.append(names.get(i) + " 不存在！").append("\r\n");
			}
		}
		if (sb.length() > 0) {
			result.setError(sb.toString());
		} else {
			result.setSuccess();
		}
		return result;
	}

}
 