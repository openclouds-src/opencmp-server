package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.TagDTO;
import com.massclouds.opencmp.dto.TenantResourceStatisticsDTO;
import com.massclouds.opencmp.dto.TenantsCreateDTO;
import com.massclouds.opencmp.dto.TenantsDTO;
import com.massclouds.opencmp.dto.TenantsTreeDTO;
import com.massclouds.opencmp.dto.UserGroupDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.dto.global.ParamsTagDTO;
import com.massclouds.opencmp.service.TenantsService;
import com.massclouds.opencmp.service.UserGroupsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("TenantsService")
public class TenantsServiceImpl  implements TenantsService{

    @Autowired
	public UserGroupsService userGroupsService;
    
	@Override
	public PageDataResultDTO<List<TenantsDTO>> tenantsListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<TenantsDTO>> result = new PageDataResultDTO<>();
		String attributes = "miq_groups,tags";
		String url = "/api/tenants?expand=resources" + "&attributes=" + attributes;;
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			//根据描述模糊查询
			if (!StringUtils.isEmpty(query.getDescription())) {
				url = url + "&filter[]=" + "description=" + "'*" + query.getDescription() + "*'";
			}
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			//根据指定的排序列进行排序，指定列包括：name、description
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			
			ResponseEntity<MiqListReturnDTO<TenantsDTO>> tenantsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<TenantsDTO>>(){}, token);
			if (401 == tenantsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == tenantsRes.getStatusCodeValue()) {
 		    	//带查询的请求
 				if (url.contains("filter") && tenantsRes.getBody().getSubquery_count() != null) {
 					count = tenantsRes.getBody().getSubquery_count();
 				} else {//不带查询的请求
 					count = tenantsRes.getBody().getCount();
 				}
 				
 				List<TenantsDTO> tenantsList = tenantsRes.getBody().getResources();
 				
 				//用户组的类型group_type包括三种：system、user、tenant。租户下的组过滤掉类型是tenant的
 				for(TenantsDTO tenant : tenantsList) {
 					if (tenant.getMiq_groups() != null) {
 						List<UserGroupDTO> groupsList = new ArrayList<UserGroupDTO>();
 						for (UserGroupDTO group : tenant.getMiq_groups()) {
 							if (!group.getGroup_type().equals("tenant")) {
 								groupsList.add(group);
 							}
 						}
 						tenant.setMiq_groups(groupsList);
 					}
 				}
 				result.setSuccess(tenantsRes.getBody().getResources(), (long) count);
        	} else {
        		result.setError(500, "租户信息获取失败");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "租户信息获取失败");
        	return result;
		}
	}
	
	@Override
	public DataResultDTO<List<TenantsDTO>> tenantsList(String token) {
		DataResultDTO<List<TenantsDTO>> result = new DataResultDTO<>();
		String attributes = "miq_groups";
		String url = "/api/tenants?expand=resources" + "&attributes=" + attributes;
		url = url + "&sort_by=id&sort_order=desc";
		try {
			
			ResponseEntity<MiqListReturnDTO<TenantsDTO>> tenantsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<TenantsDTO>>(){}, token);
			if (401 == tenantsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == tenantsRes.getStatusCodeValue()) {
				result.setSuccess(tenantsRes.getBody().getResources());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		} 
	}
	
	@Override
	public DataResultDTO<List<TenantsTreeDTO>> tenantsListTree(String token) {

		DataResultDTO<List<TenantsTreeDTO>> result = new DataResultDTO<>();
		List<TenantsTreeDTO> listTree = new ArrayList<TenantsTreeDTO>();
		try {
			
			//1、获取所有租户
			String tenantUrl = "/api/tenants?expand=resources";
			ResponseEntity<MiqListReturnDTO<TenantsTreeDTO>> tenantsRes = ManageIQHttpUtils.httpGetRequest(tenantUrl, new ParameterizedTypeReference<MiqListReturnDTO<TenantsTreeDTO>>(){}, token);
            List<TenantsTreeDTO> tenantsList = tenantsRes.getBody().getResources();
            
			//2、获取所有用户组
			String groupsUrl = "/api/groups?expand=resources" + "&attributes=miq_user_role,tenant";
			List<UserGroupDTO> groupsList = userGroupsService.getGroupsList(groupsUrl, token);
			
			//3、把组信息填充到其相应租户下
			for (TenantsTreeDTO tenant : tenantsList) {
				for (UserGroupDTO group : groupsList) {
					if (group.getTenant().getId().equals(tenant.getId())) {
						List<UserGroupDTO> tenantGroupList = tenant.getMiq_groups() == null ? new ArrayList<UserGroupDTO>() : tenant.getMiq_groups();
						tenantGroupList.add(group);
						tenant.setMiq_groups(tenantGroupList);
					}
				}
			}
			
			//4、把租户信息处理成树
			TenantsTreeDTO topAncestry = null;
			for (TenantsTreeDTO tenant : tenantsList) {
				//获取父级租户My Company
				if (StringUtils.isEmpty(tenant.getAncestry()) && "1".equals(tenant.getId()) && "My Company".equals(tenant.getName().trim())) {
					topAncestry = tenant;
				}
				if (!StringUtils.isEmpty(tenant.getAncestry())) { //父级
					listChildTree(tenantsList, tenant);
					if(!tenant.getAncestry().contains("/")) {
						listTree.add(tenant);
					}
				}
        	}
			
			if (topAncestry != null) { //超级管理员可以获取到最顶级父级My Company
				topAncestry.setChildren(listTree);
				
				List<TenantsTreeDTO> allListTree = new ArrayList<TenantsTreeDTO>();
				allListTree.add(topAncestry);
				
				result.setSuccess(allListTree);
			} else { //非超级管理员获取不到My Company
				result.setSuccess(listTree);
			}
			
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		} 
	}
	
	private void listChildTree(List<TenantsTreeDTO> allList, TenantsTreeDTO tenantsGroupsTreeDTO) {
		List<TenantsTreeDTO> childrenList = new ArrayList<TenantsTreeDTO>();
		
		//遍历子节点
		for (TenantsTreeDTO tenant : allList) {
			if (!StringUtils.isEmpty(tenant.getAncestry())) {
				String parentId = tenant.getAncestry().contains("/") ? tenant.ancestry.substring(tenant.ancestry.lastIndexOf("/") + 1) : tenant.getAncestry();
				if (tenantsGroupsTreeDTO.getId().equals(parentId)) {
					// 迭代循环执行
					listChildTree(allList, tenant);			
					childrenList.add(tenant);
				}
			}
		}
		tenantsGroupsTreeDTO.setChildren(childrenList);
	}

	@Override
	public ResultDTO createOrUpdateTenant(TenantsCreateDTO tenantsCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		ResultDTO setTagsRes = null;
		StringBuffer messageBuf = new StringBuffer();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        
        //id不为空则为编辑，id为空则为创建
        if (!StringUtils.isEmpty(tenantsCreateDTO.getId())) {
        	params.put("action", "edit");
        	params.put("id", tenantsCreateDTO.getId());
        }
        params.put("name", tenantsCreateDTO.getName());
        params.put("description", tenantsCreateDTO.getDescription());
        params.put("parent", tenantsCreateDTO.getAncestry() == null ? new ParamIdDTO("1") : new ParamIdDTO(tenantsCreateDTO.getAncestry()));
        
        String url = "/api/tenants";
        try {
        	ResponseEntity<String> groupRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(groupRes.getBody());
			
        	if (401 == groupRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == groupRes.getStatusCodeValue()) {
 		    	String tenantId = jsonNode.get("results").get(0).get("id").asText();
 		    	messageBuf.append("租户添加/编辑成功。");
 		    	//给租户添加标签
 		    	if (!StringUtils.isEmpty(tenantsCreateDTO.getCategoryName()) && !StringUtils.isEmpty(tenantsCreateDTO.getTagName()) ) {
 		    		setTagsRes = assignTenantsTags(tenantId, tenantsCreateDTO.getCategoryName(), tenantsCreateDTO.getTagName(), token);
 		    	} else {//删除已分配的标签
 		    		setTagsRes = unassignTenantsTags(tenantId, token);
 		    	}
 		    	if (setTagsRes != null && 200 != setTagsRes.getCode()) {
        			messageBuf.append("\n").append("该租户的标签设置失败！");
        		}
	        		
        		result.setSuccess();
        		result.setMessage(messageBuf.toString());
        		
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
	@Override
	public ResultDTO createTenant(TenantsCreateDTO tenantsCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", tenantsCreateDTO.getName());
        params.put("description", tenantsCreateDTO.getDescription());
        params.put("parent", tenantsCreateDTO.getAncestry() == null ? new ParamIdDTO("1") : new ParamIdDTO(tenantsCreateDTO.getAncestry()));
        
        String url = "/api/tenants";
        try {
        	ResponseEntity<String> groupRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(groupRes.getBody());
			
        	if (401 == groupRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				result.setId(null);
 		    } else if (200 == groupRes.getStatusCodeValue()) {
 		    	String tenantId = jsonNode.get("results").get(0).get("id").asText();
 		    	result.setId(tenantId);
 		    	result.setSuccess();
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
				result.setId(null);
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	/**
	 * 设置租户的标签
	 * @param id
	 * @param categoryName
	 * @param tagName
	 * @return
	 */
	public ResultDTO assignTenantsTags(String id, String categoryName, String tagName, String token) {

		ResultDTO result = new ResultDTO();
		List<ParamsTagDTO> resourceParam = new ArrayList<ParamsTagDTO>();
		resourceParam.add(new ParamsTagDTO(ConstantsUtils.tagPrefix + categoryName, ConstantsUtils.tagPrefix + tagName));
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "assign");
        params.put("resources", resourceParam);
        
        String url = "/api/tenants/" + id + "/tags";
        try {
        	ResponseEntity<String> setTagRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(setTagRes.getBody());
			
			if (401 == setTagRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == setTagRes.getStatusCodeValue()) {
        		boolean resultBoolean = jsonNode.get("results").get(0).get("success").asBoolean();
        		if (resultBoolean == Boolean.TRUE) {
        			result.setSuccess();
            		result.setMessage("租户设置标签成功！");
        		} else {
        			result.setError(500, "租户设置标签失败！");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
			return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}
	
	/**
	 * 删除租户的标签
	 * @param id
	 * @return
	 */
	public ResultDTO unassignTenantsTags(String id, String token) {

		ResultDTO result = new ResultDTO();
		StringBuffer errorSB = new StringBuffer();
		try {
			String getTagUrl  = "/api/tenants/" + id + "/tags?expand=resources";
			ResponseEntity<MiqListReturnDTO<TagDTO>> tagsRes = ManageIQHttpUtils.httpGetRequest(getTagUrl, new ParameterizedTypeReference<MiqListReturnDTO<TagDTO>>(){}, token);
			
			if (tagsRes.getStatusCodeValue() == 200 && tagsRes.getBody().getResources() != null) {
				//设置访问参数
		        HashMap<String, Object> params = new HashMap<>();
		        params.put("action", "unassign");
				for (TagDTO tag : tagsRes.getBody().getResources()) {
					String unassignTagUrl = "/api/tenants/" + id + "/tags/" + tag.getId();
					ResponseEntity<String> unsetTagRes = ManageIQHttpUtils.httpRequest(unassignTagUrl, "POST", params, new ParameterizedTypeReference<String>(){}, token);
					ObjectMapper objectMapper = new ObjectMapper();
					JsonNode jsonNode = objectMapper.readTree(unsetTagRes.getBody());
					if (200 == unsetTagRes.getStatusCodeValue()) {
		        		boolean resultBoolean = jsonNode.get("success").asBoolean();
		        		if (resultBoolean != Boolean.TRUE) {
		            		errorSB.append("租户修改标签失败！");
		        		} 
		        	} else {
		        		errorSB.append(jsonNode.get("error").get("message").toString());
		        	}
				}
			}
			if (errorSB.length() > 0) {
				result.setError(500, errorSB.toString());
			} else {
				result.setSuccess();
				result.setMessage("租户修改标签成功！");
			}
			return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteTenant(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/tenants/" + id;
        try {
        	
        	ResponseEntity<String> templatesRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == templatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == templatesRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("租户删除成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(templatesRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}


	@Override
	public ResultDTO deleteTenants(List<String> ids, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/tenants";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除租户成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataResultDTO<TenantResourceStatisticsDTO> resourceStatisticsTenant(String id, String token) {
		DataResultDTO<TenantResourceStatisticsDTO> result = new DataResultDTO<>();
		TenantResourceStatisticsDTO data = new TenantResourceStatisticsDTO();
		
		String attributes = "id,name,combined_quotas";
		String url = "/api/tenants/" + id + "?&attributes=" + attributes;;
		try {
			ResponseEntity<TenantsDTO> tenantsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<TenantsDTO>(){}, token);
			if (401 == tenantsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == tenantsRes.getStatusCodeValue()) {
				TenantsDTO tenant = tenantsRes.getBody();
				Map<Object, Object> cpuMap = (Map<Object, Object>) tenant.getCombined_quotas().get("cpu_allocated");
				double cpuValue = cpuMap.get("value") == null ? 0.0 : (double)cpuMap.get("value");
				double cpuUsed = cpuMap.get("used") == null ? 0.0 : Double.parseDouble(cpuMap.get("used").toString());
				double cpuAvailable = cpuMap.get("available") == null ? 0.0 : (double)cpuMap.get("available");
				data.setCpuTotal(cpuValue);
				data.setCpuUsed(cpuUsed);
				data.setCpuAvailable(cpuAvailable);
				
				Map<Object, Object> memMap = (Map<Object, Object>) tenant.getCombined_quotas().get("mem_allocated");
				double memValue = memMap.get("value") == null ? 0.0 : (double)memMap.get("value");
				double memUsed = memMap.get("used") == null ? 0.0 : Double.parseDouble(memMap.get("used").toString());
				double memAvailable = memMap.get("available") == null ? 0.0 : (double)memMap.get("available");
				double memoryTotal = memValue / 1024 / 1024 / 1024;//GB
				double memoryUsed = memUsed / 1024 / 1024 / 1024;
				double memoryAvailable = memAvailable / 1024 / 1024 /1024;
				data.setMemoryTotal(memoryTotal);
				data.setMemoryUsed(memoryUsed);
				data.setMemoryAvailable(memoryAvailable);
				
				Map<Object, Object> storageMap = (Map<Object, Object>) tenant.getCombined_quotas().get("storage_allocated");
				double sValue = storageMap.get("value") == null ? 0.0 : (double)storageMap.get("value");
				double sUsed = storageMap.get("used") == null ? 0.0 : Double.parseDouble(storageMap.get("used").toString());
				double sAvailable = storageMap.get("available") == null ? 0.0 : (double)storageMap.get("available");
				double storageTotal = sValue / 1024 / 1024 / 1024;//GB
				double storageUsed = sUsed / 1024 / 1024 / 1024;
				double storageAvailable = sAvailable / 1024 / 1024 / 1024;
				data.setStorageTotal(Double.parseDouble(String.format("%.1f", storageTotal)));
				data.setStorageUsed(Double.parseDouble(String.format("%.1f", storageUsed)));
				data.setStorageAvailable(Double.parseDouble(String.format("%.1f", storageAvailable)));
				
				Map<Object, Object> vmsMap = (Map<Object, Object>) tenant.getCombined_quotas().get("vms_allocated");
				double vmValue = vmsMap.get("value") == null ? 0.0 : (double)vmsMap.get("value");
				double vmUsed = vmsMap.get("used") == null ? 0.0 : Double.parseDouble(vmsMap.get("used").toString());
				double vmAvailable = vmsMap.get("available") == null ? 0.0 : (double)vmsMap.get("available");
				data.setVmTotal(vmValue);
				data.setVmUsed(vmUsed);
				data.setVmAvailable(vmAvailable);
				
				result.setSuccess(data);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		} 
	}

	@Override
	public DataResultDTO<TenantsDTO> isExistByName(String name, String token) {
		DataResultDTO<TenantsDTO> result = new DataResultDTO<TenantsDTO>();
		String attributes = "id,name";
		String url = "/api/tenants?expand=resources" + "&attributes=" + attributes;;
		try {
			url = url + "&filter[]=" + "name=" + name;
			ResponseEntity<MiqListReturnDTO<TenantsDTO>> tenantsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<TenantsDTO>>(){}, token);
			if (401 == tenantsRes.getStatusCodeValue()) {
				result.setSuccess(new TenantsDTO());
 		    } else if (200 == tenantsRes.getStatusCodeValue() && tenantsRes.getBody().getResources() != null && tenantsRes.getBody().getResources().size() > 0) {
 		    	result.setSuccess(tenantsRes.getBody().getResources().get(0));
 		    } else {
 		    	result.setSuccess(new TenantsDTO());
        	}
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setSuccess(new TenantsDTO());
			return result;
		}
	}

}
