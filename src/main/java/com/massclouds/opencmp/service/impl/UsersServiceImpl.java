package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.UserCreateDTO;
import com.massclouds.opencmp.dto.UserDTO;
import com.massclouds.opencmp.dto.UserDetailsDTO;
import com.massclouds.opencmp.dto.UserGroupDTO;
import com.massclouds.opencmp.dto.UserRoleDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.dto.query.UsersQueryDTO;
import com.massclouds.opencmp.service.UserGroupsService;
import com.massclouds.opencmp.service.UsersService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("UsersService")
public class UsersServiceImpl  implements UsersService{

	@Autowired
	private UserGroupsService userGroupsService;
	
	@Override
	public PageDataResultDTO<List<UserDTO>> usersList(UsersQueryDTO query, String token) {
		PageDataResultDTO<List<UserDTO>> result = new PageDataResultDTO<>();
		String attributes = "miq_groups";
		String url = "/api/users" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=";
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		List<UserDTO> resultUsers = new ArrayList<UserDTO>();
		
		try {
			//tanantId等于1，是指租户My Company，则查询所有用户
			if (StringUtils.isEmpty(query.getTenantId()) || "1".equals(query.getTenantId())) {
				//根据名称模糊查询
				if (!StringUtils.isEmpty(query.getName())) {
					url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
				}
				//根据登录名称模糊查询
				if (!StringUtils.isEmpty(query.getLoginName())) {
					url = url + "&filter[]=" + "userid=" + "'*" + query.getLoginName() + "*'";
				}
				//根据用户组id查询
				if (!StringUtils.isEmpty(query.getGroupId())) {
					url = url + "&filter[]=" + "current_group_id=" + query.getGroupId();
				}
				int offset = page == 0 ? 0 : page * size;
				url = url + "&offset=" + offset + "&limit=" + size;
				//根据指定的排序列进行排序，指定列包括：name、userid、created_on
				if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
					url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
				} else {
					url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
				}
				
				ResponseEntity<MiqListReturnDTO<UserDTO>> usersRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserDTO>>(){}, token);
				if (401 == usersRes.getStatusCodeValue()) {
	 				result.setError(401, ConstantsUtils.tokenMessage);
	 				return result;
	 		    }
				count = usersRes.getBody().getSubquery_count();
				resultUsers = usersRes.getBody().getResources();
			} else { //根据租户id查询租户下的所有用户
				List<UserDTO> allUsers = userGroupsService.getUsersByTenantId(query.getTenantId(), token);
				if (allUsers != null && allUsers.size() > 0) {
					//根据名称模糊查询
					if (!StringUtils.isEmpty(query.getName())) {
						allUsers = allUsers.stream().filter(x -> x.getName().contains(query.getName()) == true).collect(Collectors.toList());
					}
					//根据登录名称模糊查询
					if (!StringUtils.isEmpty(query.getLoginName())) {
						allUsers = allUsers.stream().filter(x -> x.getUserid().contains(query.getLoginName()) == true).collect(Collectors.toList());
					}
					
					count = allUsers.size();
					int pageBegin = page == 0 ? 0 : page * size;
					int pageEnd = pageBegin + size;
					pageEnd = pageEnd > count ? count : pageEnd;
					resultUsers = allUsers.subList(pageBegin, pageEnd);
				}
			}
			//获取用户的角色，需先获取用户组及其角色的信息
			Map<String, String> groupIdRoleNameMap = new HashMap<String, String>();
			DataResultDTO<List<UserGroupDTO>> groupsList = userGroupsService.groupsList(token);
			if (groupsList.getResult() != null) {
				for(UserGroupDTO group : groupsList.getResult()) {
					groupIdRoleNameMap.put(group.getId(), group.getMiq_user_role().getName());
				}
			}
			for(UserDTO user : resultUsers) {
				user.setRoleName(groupIdRoleNameMap.get(user.getCurrent_group_id()));
			}
			
			result.setSuccess(resultUsers, (long) count);
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<UserDetailsDTO> usersDetailsByLoginName(String loginName, String token) {
		
		DataResultDTO<UserDetailsDTO> result = new DataResultDTO<>();
		UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
		String attributes = "miq_groups";
		String url = "/api/users" + "?expand=resources" + "&attributes=" + attributes + "&offset=0";
		url = url + "&filter[]=" + "userid=" + loginName;

		try {
			ResponseEntity<MiqListReturnDTO<UserDetailsDTO>> usersRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserDetailsDTO>>(){}, token);
			if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			 //获取用户的角色
			if (200 == usersRes.getStatusCodeValue() && usersRes.getBody().getResources() != null && usersRes.getBody().getResources().size() > 0) {
				userDetailsDTO = usersRes.getBody().getResources().get(0);
				DataResultDTO<UserGroupDTO> userGroupDTO = userGroupsService.getById(usersRes.getBody().getResources().get(0).getCurrent_group_id(), token);
				if (userGroupDTO.getResult() != null && userGroupDTO.getResult().getMiq_user_role() != null) {
					UserRoleDTO userRoleDTO = new UserRoleDTO();
					userRoleDTO.setId(userGroupDTO.getResult().getMiq_user_role().getId());
					userRoleDTO.setName(userGroupDTO.getResult().getMiq_user_role().getName());
					userDetailsDTO.setRole(userRoleDTO);
				}
				result.setSuccess(userDetailsDTO);
			} else {
				result.setError("获取信息失败！");
        	}	
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public ResultDTO createUser(UserCreateDTO userCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("userid", userCreateDTO.getLoginName());
        params.put("password", userCreateDTO.getPassword());
        params.put("name", userCreateDTO.getName());
        params.put("group", new ParamIdDTO(userCreateDTO.getGroupId()));
        
        String url = "/api/users";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("用户添加成功。");
        		String tenantId = jsonNode.get("results").get(0).get("id").asText();
 		    	result.setId(tenantId);
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}
	
	@Override
	public ResultDTO createMultipleUser(List<UserCreateDTO> userCreateDTOList, String token) {
		ResultDTO result = new ResultDTO();
		
		List<Map<String, Object>> paramsLsit = new ArrayList<Map<String, Object>>();
		for (UserCreateDTO user : userCreateDTOList) {
			//设置访问参数
	        HashMap<String, Object> p = new HashMap<>();
	        p.put("userid", user.getLoginName());
	        p.put("password", user.getPassword());
	        p.put("name", user.getName());
	        p.put("group", new ParamIdDTO(user.getGroupId()));
	        paramsLsit.add(p);
		}
		 HashMap<String, Object> params = new HashMap<>();
		 params.put("action", "create");
		 params.put("resources", paramsLsit);
        
        String url = "/api/users";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("用户添加成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public ResultDTO updateUser(UserCreateDTO userCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        params.put("userid", userCreateDTO.getLoginName());
        params.put("password", userCreateDTO.getPassword());
        params.put("name", userCreateDTO.getName());
        params.put("group", new ParamIdDTO(userCreateDTO.getGroupId()));
        
        String url = "/api/users/" + userCreateDTO.getId();
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("用户编辑成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO updateUserPassword(UserCreateDTO userCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        params.put("password", userCreateDTO.getPassword());
        
        String url = "/api/users/" + userCreateDTO.getId();
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("修改用户密码成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteUser(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/users/" + id;
        try {
        	ResponseEntity<String> deleteRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == deleteRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == deleteRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("用户删除成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(deleteRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteUsers(List<String> userIds, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : userIds) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/users";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除用户成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public List<UserDTO> usersAllList(String token) {
		String attributes = "id,name";
		String url = "/api/users" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&sort_by=id&sort_order=desc";
		
		try {
			ResponseEntity<MiqListReturnDTO<UserDTO>> usersRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserDTO>>(){}, token);
			if (401 == usersRes.getStatusCodeValue()) {
 				return null;
 		    }
			return usersRes.getBody().getResources();
		}catch(Exception e) {
			e.printStackTrace();
        	return null;
		}
	}

}
