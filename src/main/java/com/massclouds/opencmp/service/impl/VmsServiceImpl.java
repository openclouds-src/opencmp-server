package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.InstanceActionDTO;
import com.massclouds.opencmp.dto.InstanceResourceOfCloudPlatformDTO;
import com.massclouds.opencmp.dto.TagDTO;
import com.massclouds.opencmp.dto.VmDTO;
import com.massclouds.opencmp.dto.VmDetailsDTO;
import com.massclouds.opencmp.dto.VmDiskDTO;
import com.massclouds.opencmp.dto.VmHostOnRunDTO;
import com.massclouds.opencmp.dto.VmHtml5ConsoleDTO;
import com.massclouds.opencmp.dto.VmNoVNCDTO;
import com.massclouds.opencmp.dto.VmSetConfigDTO;
import com.massclouds.opencmp.dto.VmSpiceDTO;
import com.massclouds.opencmp.dto.global.ParamsTagDTO;
import com.massclouds.opencmp.dto.query.VmsQueryDTO;
import com.massclouds.opencmp.service.VmsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("VmsService")
public class VmsServiceImpl  implements VmsService{

	@Override
	public PageDataResultDTO<List<VmDetailsDTO>> vmsList(VmsQueryDTO query, String token) {
		PageDataResultDTO<List<VmDetailsDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage();
		int size = query.getPageSize();
		
		String attributes = "href,id,name,vendor,description,created_on,updated_on,guid,service_type,tools_status,power_state," + 
							"raw_power_state,connection_state,ext_management_system,ext_management_system.hostname," + 
							"ext_management_system.ipaddress,ext_management_system.zone_id,host_name,ems_cluster_name,ram_size,num_hard_disks,num_cpu," + 
							"cpu_total_cores,cpu_cores_per_socket,used_storage,total_disks_size,operating_system,ipaddresses," + 
							"lans,storage,ems_cluster,archived,orphaned,evm_owner_id,evm_owner_name,tags";
		String url = "/api/vms/" + "?expand=resources" + "&attributes=" + attributes;
		
		url = url + "&filter[]=archived=false&filter[]=orphaned=false";//过滤掉归档的archived、孤立的orphaned 虚机
		if ("server".equals(query.getCriteria().trim().toLowerCase())) {//过滤 云主机 的虚机
			url = url + "&filter[]=service_type=server";
		} else if ("desktop".equals(query.getCriteria().trim().toLowerCase())) {//过滤 云桌面 的虚机
			url = url + "&filter[]=service_type=desktop";
		}
		
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			//根据集群名称模糊查询
			if (!StringUtils.isEmpty(query.getClusterName())) {
				url = url + "&filter[]=" + "ems_cluster_name=" + "'*" + query.getClusterName() + "*'";
			}
			//根据供应商id查询
			if (!StringUtils.isEmpty(query.getProviderId())) {
				url = url + "&filter[]=" + "ext_management_system.id=" + query.getProviderId();
			}
			//根据供应商类型查询
			if (!StringUtils.isEmpty(query.getProviderType())) {
				url = url + "&filter[]=" + "type=" + query.getProviderType().trim() + "::Vm";
			}
			//根据标签id查询
			if (!StringUtils.isEmpty(query.getTagId())) {
				url = url + "&filter[]=" + "tags.id=" + query.getTagId();
			}
			//根据租户id查询
			if (!StringUtils.isEmpty(query.getTenantId())) {
				url = url + "&filter[]=" + "tenant_id=" + query.getTenantId();
			}
			
			int offset = page == 0 ? 0 : page * size;
			
			url = url + "&offset=" + offset + "&limit=" + size;
			
			//根据指定的排序列进行排序，指定列包括：name、ram_size、raw_power_state、host_name
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			
			ResponseEntity<MiqListReturnDTO<VmDetailsDTO>> vmsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<VmDetailsDTO>>(){}, token);
			if (401 == vmsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == vmsRes.getStatusCodeValue()) {
				List<VmDetailsDTO> vmsLsit = vmsRes.getBody().getResources();
				if (vmsLsit != null) {
					vmsLsit.forEach((vm) -> {
						vm.setRam_size(vm.getRam_size() / 1024);  //单位GB
							
					    long totalDisksSize = Long.parseLong(vm.getTotal_disks_size());
					    vm.setTotal_disks_size((totalDisksSize / 1024 /1024 /1024) + "" ); //单位GB
					    
					    if (vm.getTags() != null && vm.getTags().size() > 0) {
					    	for(TagDTO tag : vm.getTags()) {
					    		if (tag.getName().contains(ConstantsUtils.businessTagPrefix)) {
					    			String tagName = tag.getName();
					    			vm.setAssigned_tag_name(tagName.substring(tagName.lastIndexOf("/") + 1 + ConstantsUtils.businessTagPrefix.length())); //处理得到标签名称，去掉前缀的
					    		}
					    	}
					    }
							
						});
				}
				
				result.setSuccess(vmsLsit,  vmsRes.getBody().getSubquery_count() == null ? 0 : (long) vmsRes.getBody().getSubquery_count());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public ResultDTO vmAction(InstanceActionDTO instanceActionDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		Map<String, String> actionMap = new HashMap<String, String>();
		actionMap.put("start", "start");//开机
		actionMap.put("stop", "stop");//断电
		actionMap.put("suspend", "suspend");//暂停
		actionMap.put("reboot", "reboot_guest");//重启
		actionMap.put("shutdown", "shutdown_guest");//关机
		actionMap.put("reset", "reset");//重置
		actionMap.put("refresh", "refresh");//刷新
		
		String url = "/api/vms/" + instanceActionDTO.getId();
		
		try {
			//设置访问参数
	        HashMap<String, Object> params = new HashMap<>();
	        params.put("action", actionMap.get(instanceActionDTO.getAction()));
			
			ResponseEntity<String> vmRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
			if (401 == vmRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(vmRes.getBody());
			
			String message = "";
			switch(instanceActionDTO.getAction()) {
			case "start": message = "虚拟机开机"; break;
			case "stop": message = "虚拟机断电"; break;
			case "suspend": message = "虚拟机暂停"; break;
			case "reboot": message = "虚拟机重启"; break;
			case "shutdown": message = "虚拟机关机"; break;
			case "reset": message = "虚拟机重置"; break;
			case "refresh": message = "虚拟机刷新"; break;
			default: message = "";
			}
			
        	if (200 == vmRes.getStatusCodeValue()) {
        		if (jsonNode.get("success").asBoolean() == true) {
        			result.setSuccess();
            		result.setMessage(message + "成功！请等待其操作完成。");
        		} else {
        			result.setError(jsonNode.get("message").toString());
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError("虚拟机操作失败！");
			return result;
		}
		
	}
	
	@Override
	public DataResultDTO<VmNoVNCDTO> getVncInfo(String id, String token) {
		DataResultDTO<VmNoVNCDTO> result = new DataResultDTO<>();
		VmNoVNCDTO vncDTO = new VmNoVNCDTO();
		
		try {
			//1、获取虚拟的名称、运行主机地址
			String attributes = "href,id,name,host_name,provider_address,ipaddresses";
			String infoUrl = "/api/vms/" + id + "?expand=resources" + "&attributes=" + attributes;
			
			ResponseEntity<VmNoVNCDTO> infoRes = ManageIQHttpUtils.httpGetRequest(infoUrl, new ParameterizedTypeReference<VmNoVNCDTO>(){}, token);
			if (401 == infoRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
 		    } else if (200 == infoRes.getStatusCodeValue()) {
 		    	vncDTO.setName(infoRes.getBody().getName());
				vncDTO.setProvider_address(infoRes.getBody().getProvider_address());
 		    } 
			
			//2、获取虚机的VNC信息
			HashMap<String, Object> params = new HashMap<>();
			params.put("action", "request_console");
			params.put("protocol", "vnc");
			
			String vncUrl = "/api/vms/" + id;
			ResponseEntity<Map<String, Object>> vncRes = ManageIQHttpUtils.httpRequest(vncUrl, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == vncRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == vncRes.getStatusCodeValue() && "true".equals(vncRes.getBody().get("success").toString()) && vncRes.getBody().get("result") != null) {
				Map<String, Object> valueMap = (Map<String, Object>) vncRes.getBody().get("result");
				vncDTO.setVnc_port(valueMap.get("vnc_port") == null ? null : valueMap.get("vnc_port").toString());
				vncDTO.setVnc_proxyticket(valueMap.get("vnc_proxyticket") == null ? null : valueMap.get("vnc_proxyticket").toString());
				vncDTO.setVnc_ticket(valueMap.get("vnc_ticket") == null ? null : valueMap.get("vnc_ticket").toString());
				
				vncDTO.setHref(vncRes.getBody().get("href").toString());
				vncDTO.setId(id);
				
				result.setSuccess(vncDTO);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "获取虚机VNC控制台信息失败！");
			return result;
		}
	}
	
	@Override
	public DataResultDTO<VmSpiceDTO> getSpiceInfo(String id, String token) {
		DataResultDTO<VmSpiceDTO> result = new DataResultDTO<>();
		VmSpiceDTO spiceDTO = new VmSpiceDTO();
		
		try {
			//1、获取虚拟的名称、运行主机地址
			String attributes = "href,id,name,host_name,provider_address,ipaddresses";
			String infoUrl = "/api/vms/" + id + "?expand=resources" + "&attributes=" + attributes;
			
			ResponseEntity<VmNoVNCDTO> infoRes = ManageIQHttpUtils.httpGetRequest(infoUrl, new ParameterizedTypeReference<VmNoVNCDTO>(){}, token);
			if (401 == infoRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
 		    } else if (200 == infoRes.getStatusCodeValue()) {
 		    	spiceDTO.setName(infoRes.getBody().getName());
				spiceDTO.setProvider_address(infoRes.getBody().getProvider_address());
 		    } 
			
			//2、获取虚机的VNC信息
			HashMap<String, Object> params = new HashMap<>();
			params.put("action", "request_console");
			params.put("protocol", "spice");
			
			String vncUrl = "/api/vms/" + id;
			ResponseEntity<Map<String, Object>> vncRes = ManageIQHttpUtils.httpRequest(vncUrl, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == vncRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == vncRes.getStatusCodeValue() && "true".equals(vncRes.getBody().get("success").toString()) && vncRes.getBody().get("result") != null) {
				Map<String, Object> valueMap = (Map<String, Object>) vncRes.getBody().get("result");
				spiceDTO.setCertificate(valueMap.get("certificate") == null ? null : valueMap.get("certificate").toString());
				spiceDTO.setTicket(valueMap.get("ticket") == null ? null : valueMap.get("ticket").toString());
				spiceDTO.setSpice_port(valueMap.get("spice_secure_port") == null ? null : valueMap.get("spice_secure_port").toString());
				spiceDTO.setSpice_secure_port(valueMap.get("spice_port") == null ? null : valueMap.get("spice_port").toString());
				
				spiceDTO.setHref(vncRes.getBody().get("href").toString());
				spiceDTO.setId(id);
				
				result.setSuccess(spiceDTO);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "获取虚机SPICE控制台信息失败！");
			return result;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataResultDTO<VmHtml5ConsoleDTO> getHtml5ControlInfo(String id, String token) {
		DataResultDTO<VmHtml5ConsoleDTO> result = new DataResultDTO<>();
		VmHtml5ConsoleDTO html5 = new VmHtml5ConsoleDTO();
		HashMap<String, Object> params = new HashMap<>();
		params.put("action", "request_console");
		params.put("protocol", "html5");
		
		String url = "/api/vms/" + id;
		try {
			
			ResponseEntity<Map<String, Object>> html5Res = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == html5Res.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == html5Res.getStatusCodeValue() && "true".equals(html5Res.getBody().get("success").toString())) {
				Map<String, Object> valueMap = (Map<String, Object>) html5Res.getBody().get("result");
				html5.setUrl(valueMap.get("url") == null ? null : valueMap.get("url").toString());
				html5.setProto(valueMap.get("proto") == null ? null : valueMap.get("proto").toString());
				html5.setSecret(valueMap.get("secret") == null ? null : valueMap.get("secret").toString());
				result.setSuccess(html5);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "获取虚机html5控制台信息失败！");
			return result;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public DataResultDTO<Map<String, Object>> getControlInfo(String id, String vendor, String token) {
		DataResultDTO<Map<String, Object>> result = new DataResultDTO<>();
		HashMap<String, Object> params = new HashMap<>();
		params.put("action", "request_console");
		if ("vmware".equals(vendor)) {
			params.put("protocol", "vmrc");
		} else {
			params.put("protocol", "html5");
		}
		
		String url = "/api/vms/" + id;
		try {
			
			ResponseEntity<Map<String, Object>> html5Res = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == html5Res.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == html5Res.getStatusCodeValue() && "true".equals(html5Res.getBody().get("success").toString())) {
				Map<String, Object> valueMap = (Map<String, Object>) html5Res.getBody().get("result");
				result.setSuccess(valueMap);
			} else {
				result.setError("获取信息失败！");
        	}
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "获取虚机console控制台信息失败！");
			return result;
		}
	}

	@Override
	public ResultDTO deleteVm(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/vms/" + id;
        try {
        	
        	ResponseEntity<String> vmsRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == vmsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == vmsRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("删除虚拟机成功！请等待其删除完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(vmsRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public List<VmDTO> vmsAllByType(String vmType, String token) {
		
		String  attributes = "id,name,operating_system";
		String url = "/api/vms/" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&filter[]=archived=false&filter[]=orphaned=false";//过滤掉归档的archived、孤立的orphaned 虚机
		
		if ("server".equals(vmType)) {//过滤 云主机 的虚机
			url = url + "&filter[]=service_type=server";
		} else if ("desktop".equals(vmType)) {//过滤 云桌面 的虚机
			url = url + "&filter[]=service_type=desktop";
		}
		
		try {
			ResponseEntity<MiqListReturnDTO<VmDTO>> vmsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<VmDTO>>(){}, token);
			if (401 == vmsRes.getStatusCodeValue()) {
 				return null;
 		    }
			List<VmDTO> vmsLsit = vmsRes.getBody().getResources();
			return vmsLsit;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<VmDTO> vmsOnLine(String token) {
		
		String  attributes = "id,name,power_state";
		String url = "/api/vms/" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&filter[]=power_state=on";
		
		try {
			ResponseEntity<MiqListReturnDTO<VmDTO>> vmsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<VmDTO>>(){}, token);
			if (401 == vmsRes.getStatusCodeValue()) {
 				return null;
 		    }
			List<VmDTO> vmsLsit = vmsRes.getBody().getResources();
			return vmsLsit;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public DataResultDTO<Map<String, List<VmHostOnRunDTO>>> listOnRun(String token) {
		DataResultDTO<Map<String, List<VmHostOnRunDTO>>> result = new DataResultDTO<Map<String, List<VmHostOnRunDTO>>>();
		Map<String, List<VmHostOnRunDTO>> vmsMap = new HashMap<String, List<VmHostOnRunDTO>>();
		
		String  attributes = "id,name,ipaddresses,type";
		String url = "/api/vms/?expand=resources&attributes=" + attributes ;
		url = url + "&filter[]=raw_power_state=up&filter[]=ipaddresses!=nil";//过滤出开机的、ip不为空的
		url = url + "&filter[]=archived=false&filter[]=orphaned=false";//过滤掉归档的archived、孤立的orphaned 虚机
		
		String serverUrl = url + "&filter[]=service_type=server";
		String desktopUrl = url + "&filter[]=service_type=desktop";
		
		try {
			ResponseEntity<MiqListReturnDTO<VmHostOnRunDTO>> serverVmsRes = ManageIQHttpUtils.httpGetRequest(serverUrl, 
											new ParameterizedTypeReference<MiqListReturnDTO<VmHostOnRunDTO>>(){}, 
											token);
			if (401 == serverVmsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == serverVmsRes.getStatusCodeValue()) {
				vmsMap.put("server", serverVmsRes.getBody().getResources());
 		    }
			
			ResponseEntity<MiqListReturnDTO<VmHostOnRunDTO>> desktopVmsRes = ManageIQHttpUtils.httpGetRequest(desktopUrl, 
					new ParameterizedTypeReference<MiqListReturnDTO<VmHostOnRunDTO>>(){}, 
					token);
			if (401 == desktopVmsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == desktopVmsRes.getStatusCodeValue()) {
				vmsMap.put("desktop", desktopVmsRes.getBody().getResources());
			}
			
			result.setSuccess(vmsMap);
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResultDTO setVmOwnership(String id, String groupName, String userName, String token) {
		ResultDTO result = new ResultDTO();
		
		HashMap<String, String> ownerParam = new HashMap<>();
		ownerParam.put("owner", userName);
		ownerParam.put("group", groupName);
		
		HashMap<String, Object> params = new HashMap<>();
		params.put("action", "set_ownership");
		params.put("resource", ownerParam);
		
		
		String url = "/api/vms/" + id;
		try {
			
			ResponseEntity<Map<String, Object>> ownerRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == ownerRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == ownerRes.getStatusCodeValue() && "true".equals(ownerRes.getBody().get("success").toString())) {
				result.setSuccess();
			} else {
				result.setError();
			}
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "虚机设置所有权失败！");
			return result;
		}
	}
	
	@Override
	public ResultDTO setVmTag(String vmId, String categoryName, String tagName, String token) {
		//如果标签类、标签都为空，则是删除已分配的标签
		if (StringUtils.isEmpty(categoryName) && StringUtils.isEmpty(tagName)) {
			ResultDTO unassignRes = unassignVmTag(vmId, token);
			return unassignRes;
		} else {
			//1、先删除已分配的业务标签
			ResultDTO unassignRes = unassignVmTag(vmId, token);
			if (200 == unassignRes.getCode()) {
				//2、设置标签
				return assignVmTag(vmId, categoryName, tagName, token);
			} else {
				return unassignRes;
			}
		}
	}

	/**
	 * 设置虚机的标签
	 * @param vmId
	 * @param categoryName
	 * @param tagName
	 * @return
	 */
	public ResultDTO assignVmTag(String vmId, String categoryName, String tagName, String token) {

		ResultDTO result = new ResultDTO();
		List<ParamsTagDTO> resourceParam = new ArrayList<ParamsTagDTO>();
		resourceParam.add(new ParamsTagDTO(ConstantsUtils.businessTagPrefix + categoryName, ConstantsUtils.businessTagPrefix + tagName));
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "assign");
        params.put("resources", resourceParam);
        
        String url = "/api/vms/" + vmId + "/tags";
        try {
        	ResponseEntity<String> setTagRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(setTagRes.getBody());
			
			if (401 == setTagRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == setTagRes.getStatusCodeValue()) {
        		boolean resultBoolean = jsonNode.get("results").get(0).get("success").asBoolean();
        		if (resultBoolean == Boolean.TRUE) {
        			result.setSuccess();
            		result.setMessage("虚机设置标签成功！");
        		} else {
        			result.setError(500, "虚机设置标签失败！");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}
	
	/**
	 * 删除虚机的标签
	 * @param id
	 * @return
	 */
	public ResultDTO unassignVmTag(String vmId, String token) {

		ResultDTO result = new ResultDTO();
		StringBuffer errorSB = new StringBuffer();
		try {
			String getTagUrl  = "/api/vms/" + vmId + "/tags?expand=resources";
			ResponseEntity<MiqListReturnDTO<TagDTO>> tagsRes = ManageIQHttpUtils.httpGetRequest(getTagUrl, new ParameterizedTypeReference<MiqListReturnDTO<TagDTO>>(){}, token);
			
			if (tagsRes.getStatusCodeValue() == 200 && tagsRes.getBody().getResources() != null) {
				//设置访问参数
		        HashMap<String, Object> params = new HashMap<>();
		        params.put("action", "unassign");
				for (TagDTO tag : tagsRes.getBody().getResources()) {
					if (tag.getName().contains(ConstantsUtils.businessTagPrefix)) {//只删除业务标签
						String unassignTagUrl = "/api/vms/" + vmId + "/tags/" + tag.getId();
						ResponseEntity<String> unsetTagRes = ManageIQHttpUtils.httpRequest(unassignTagUrl, "POST", params, new ParameterizedTypeReference<String>(){}, token);
						ObjectMapper objectMapper = new ObjectMapper();
						JsonNode jsonNode = objectMapper.readTree(unsetTagRes.getBody());
						if (200 == unsetTagRes.getStatusCodeValue()) {
			        		boolean resultBoolean = jsonNode.get("success").asBoolean();
			        		if (resultBoolean != Boolean.TRUE) {
			            		errorSB.append("虚机修改标签失败！");
			        		} 
			        	} else {
			        		errorSB.append(jsonNode.get("error").get("message").toString());
			        	}
					}
				}
			}
			if (errorSB.length() > 0) {
				result.setError(500, errorSB.toString());
			} else {
				result.setSuccess();
				result.setMessage("虚机修改标签成功！");
			}
			return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
	@Override
	public ResultDTO updateVm(String vmId, VmsQueryDTO query, String token) {

		ResultDTO result = new ResultDTO();
		//设置访问参数
		HashMap<String, Object> resourceParam = new HashMap<>();
		resourceParam.put("description", query.getDescription());
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        params.put("resource", resourceParam); //现在虚机只需要编辑描述
        
        String url = "/api/vms/" + vmId;
        try {
        	ResponseEntity<String> vmRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(vmRes.getBody());
			
			if (401 == vmRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == vmRes.getStatusCodeValue()) {
 		    	result.setSuccess();
        		result.setMessage("虚机编辑成功！");
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}

	@Override
	public DataResultDTO<VmDTO> getVmByEmsuid(String uidems, String token) {
		DataResultDTO<VmDTO> result = new DataResultDTO<VmDTO>();
		String  attributes = "id,name,uid_ems";
		String url = "/api/vms/" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&filter[]=uid_ems=" + uidems;
		try {
			ResponseEntity<MiqListReturnDTO<VmDTO>> vmsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<VmDTO>>(){}, token);
			if (401 == vmsRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == vmsRes.getStatusCodeValue()) {
 		    	List<VmDTO> vmsLsit = vmsRes.getBody().getResources();
 		    	if (vmsLsit != null && vmsLsit.size() > 0) {
 		    		result.setSuccess(vmsLsit.get(0));
 		    	} 
 		    } else {
 		    	result.setError();
 		    }
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	
	}

	@Override
	public DataResultDTO<List<VmDiskDTO>> getVmDisks(String vmId, String token) {
		DataResultDTO<List<VmDiskDTO>> result = new DataResultDTO<>();
		
		String url = "/api/vms/" + vmId + "/disks" + "?expand=resources";
		
		try {
			ResponseEntity<MiqListReturnDTO<VmDiskDTO>> disksRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<VmDiskDTO>>(){}, token);
			if (401 == disksRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == disksRes.getStatusCodeValue()) {
				List<VmDiskDTO> diskLsit = disksRes.getBody().getResources();
				if (diskLsit != null) {
					diskLsit.forEach((disk) -> {
					    long totalDisksSize = Long.parseLong(disk.getSize());
					    disk.setSize((totalDisksSize / 1024 /1024 /1024) + "" ); //单位GB
							
						});
				}
				
				result.setSuccess(diskLsit);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	public HashMap<String, Object> getVmDiskParam(String vmId, String actionType, Map<String, Object> valueMap) {
		String[] vmIds = {vmId};
		Object[] actions = {valueMap};
		
		HashMap<String, Object> options = new HashMap<>();
		options.put("request_type", "vm_reconfigure");
		options.put("src_ids", vmIds);
		options.put(actionType, actions);
		
		HashMap<String, Object> params = new HashMap<>();
		params.put("action", "create");
		params.put("options", options);
		return params;
	}

	@Override
	public ResultDTO addVmDisk(String vmId, VmSetConfigDTO vmSetConfigDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		HashMap<String, Object> diskAddParam = new HashMap<>();
		diskAddParam.put("new_controller_type", "VirtualLsiLogicController");
		diskAddParam.put("type", "thin");
		diskAddParam.put("persistent", true);
		diskAddParam.put("thin_provisioned", true);
		diskAddParam.put("dependent", true);
		diskAddParam.put("disk_size_in_mb", (vmSetConfigDTO.getDiskSize() * 1024) + "");
		diskAddParam.put("bootable", vmSetConfigDTO.isBootable());
		HashMap<String, Object> params = getVmDiskParam(vmId, "disk_add", diskAddParam);
		
		String url = "/api/requests";
		try {
			
			ResponseEntity<Map<String, Object>> diskRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == diskRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == diskRes.getStatusCodeValue()) {
				result.setSuccess();
			} else {
				result.setError();
			}
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "虚机添加磁盘失败！");
			return result;
		}
		
	}

	@Override
	public ResultDTO removeVmDisk(String vmId, VmSetConfigDTO vmSetConfigDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		HashMap<String, Object> diskAddParam = new HashMap<>();
		diskAddParam.put("disk_name", vmSetConfigDTO.getDiskFileName());
		diskAddParam.put("delete_backing", true);
		HashMap<String, Object> params = getVmDiskParam(vmId, "disk_remove", diskAddParam);
		
		String url = "/api/requests";
		try {
			
			ResponseEntity<Map<String, Object>> diskRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == diskRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == diskRes.getStatusCodeValue()) {
				result.setSuccess();
			} else {
				result.setError();
			}
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "删除虚机磁盘失败！");
			return result;
		}
	}
	
	@Override
	public ResultDTO resizeVmDisk(String vmId, VmSetConfigDTO vmSetConfigDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		HashMap<String, Object> diskResizeParam = new HashMap<>();
		diskResizeParam.put("disk_name", vmSetConfigDTO.getDiskFileName());
		diskResizeParam.put("disk_size_in_mb", vmSetConfigDTO.getDiskSize());
		HashMap<String, Object> params = getVmDiskParam(vmId, "disk_resize", diskResizeParam);
		
		String url = "/api/requests";
		try {
			
			ResponseEntity<Map<String, Object>> diskRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == diskRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == diskRes.getStatusCodeValue()) {
				result.setSuccess();
			} else {
				result.setError();
			}
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "修改虚机磁盘大小失败！");
			return result;
		}
	}

	@Override
	public ResultDTO updateVmMemoryCPU(String vmId, VmSetConfigDTO vmSetConfigDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		String[] vmIds = {vmId};
		HashMap<String, Object> options = new HashMap<>();
		options.put("request_type", "vm_reconfigure");
		options.put("src_ids", vmIds);
		options.put("vm_memory", vmSetConfigDTO.getMemory() * 1024);
		options.put("cores_per_socket", vmSetConfigDTO.getCores());
		options.put("number_of_sockets", vmSetConfigDTO.getSockets());
		options.put("number_of_cpus", vmSetConfigDTO.getSockets() * vmSetConfigDTO.getCores());
		
		HashMap<String, Object> params = new HashMap<>();
		params.put("action", "create");
		params.put("options", options);
		
		String url = "/api/requests";
		try {
			
			ResponseEntity<Map<String, Object>> resetRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<Map<String, Object>>(){}, token);
			if (401 == resetRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == resetRes.getStatusCodeValue()) {
				result.setSuccess();
			} else {
				result.setError();
			}
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "虚机内存、CPU配置失败！");
			return result;
		}
	}
	
	@Override
	public InstanceResourceOfCloudPlatformDTO getAllInstancesCpusMemoryOfCloudPlatform(String providerId, String token){
		InstanceResourceOfCloudPlatformDTO instanceResource = new InstanceResourceOfCloudPlatformDTO();
		
		double totalRam = 0;
		int totalCpus = 0; 
		double totalDisksSize = 0;
		
		VmsQueryDTO query = new VmsQueryDTO();
		query.setPage(0);
		query.setPageSize(9999);
		query.setProviderId(providerId);
		query.setCriteria("server");
		PageDataResultDTO<List<VmDetailsDTO>> vmsPageList = vmsList(query, token);
		if (vmsPageList.getCode() == 200 && vmsPageList.getResult() != null && vmsPageList.getResult().getItems() != null) {
			for (VmDetailsDTO vm : vmsPageList.getResult().getItems()) {
				totalRam = totalRam + (vm.getRam_size() * 1024); //转换成MB
				totalCpus = totalCpus + vm.getNum_cpu();
				
				double totalDisksKB = (Double.parseDouble(vm.getTotal_disks_size()) * 1024 * 1024 * 1024);//转换成KB
				totalDisksSize = totalDisksSize + totalDisksKB; 
			}
			
		}
		instanceResource.setTotalCpus(totalCpus);
		instanceResource.setTotalMemory(totalRam);
		instanceResource.setTotalDisk(totalDisksSize);
		
		return  instanceResource;
	}
	
	
}
