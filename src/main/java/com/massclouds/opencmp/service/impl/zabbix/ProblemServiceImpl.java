package com.massclouds.opencmp.service.impl.zabbix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.zabbix.EventAcknowledgeDTO;
import com.massclouds.opencmp.dto.zabbix.ProblemSearchDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixProblemDTO;
import com.massclouds.opencmp.service.zabbix.ProblemService;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.ZabbixHttpUtils;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;
import com.massclouds.opencmp.utils.resultdto.ZabbixDateReturnDTO;
import com.massclouds.opencmp.utils.resultdto.ZabbixListReturnDTO;

@Service("ZabbixProblemService")
public class ProblemServiceImpl  implements ProblemService{
	
	@Override
	public PageDataResultDTO<List<ZabbixProblemDTO>> problemListOfPage(ProblemSearchDTO query) {
		PageDataResultDTO<List<ZabbixProblemDTO>> result = new PageDataResultDTO<List<ZabbixProblemDTO>>();
		List<ZabbixProblemDTO> problemList = new ArrayList<ZabbixProblemDTO>();
		
		String[] output = {"eventid", "objectid", "severity", "clock", "ns", "name", "opdata", "acknowledged", "r_clock", "suppressed" };
		String[] hosts = {"id", "host"};
		Integer[] severities = {1,2,3,4,5};
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("output", output);
		parms.put("selectHosts", hosts);
		
		parms.put("sortfield", "eventid");
		parms.put("sortorder", "DESC");
		//按主机id查询
		if (!StringUtils.isEmpty(query.getHostId())) {
			parms.put("hostids", query.getHostId());
		}
		//按告警级别查询
        if (!StringUtils.isEmpty(query.getSeverities())) {
        	parms.put("severities", Integer.parseInt(query.getSeverities()));
		} else {
			parms.put("severities", severities);
		}
        //按确认状态查询
		if (!StringUtils.isEmpty(query.getAcknowledged())) {
			parms.put("acknowledged", Integer.parseInt(query.getAcknowledged()));
		}
		
		String apiMethod = "event.get";
		try {
			ResponseEntity<ZabbixListReturnDTO<ZabbixProblemDTO>> problemRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, 
					 new ParameterizedTypeReference<ZabbixListReturnDTO<ZabbixProblemDTO>>(){});
	   	  
	   	    if (problemRes.getStatusCodeValue() == 200) {
	   		    problemList = problemRes.getBody().getResult();
		   		int count = problemList.size();
		   	    int page = query.getPage(), size = query.getPageSize();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				result.setSuccess(problemList.subList(pageBegin, pageEnd), (long) count);
	   	    } else {
				result.setError("获取信息失败！");
        	}
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public ResultDTO updateAcknowledge(EventAcknowledgeDTO eventAcknowledgeDTO) {
		ResultDTO result = new ResultDTO();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("eventids", eventAcknowledgeDTO.getEventId());
		params.put("action", eventAcknowledgeDTO.getAction());
		params.put("message", eventAcknowledgeDTO.getMessage());
		
		String apiMethod = "event.acknowledge";
		try {
			ResponseEntity<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>> acknowledgeRes = ZabbixHttpUtils.httpRequest(apiMethod, params, 
					 new ParameterizedTypeReference<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>>(){});
	   	  
	   	    if (acknowledgeRes.getStatusCodeValue() == 200 && acknowledgeRes.getBody().getResult() != null) {
	   	    	Map<String, ArrayList<Object>> hostId = (Map<String, ArrayList<Object>>) acknowledgeRes.getBody().getResult();
	   	    	if (hostId != null && hostId.get("eventids") != null && hostId.get("eventids").size() > 0) {
	   	    		result.setId(hostId.get("eventids").get(0).toString());
	   	    	}
	   	    	result.setSuccess("事件确认成功");
	   	    } else {
	   	    	Map<String, Object> error = acknowledgeRes.getBody().getError();
	   	    	result.setError(error.get("data").toString());
	   	    }
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
		
	}
	
}
 