package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.common.redis.RedisUtil;
import com.massclouds.opencmp.dto.ContainerManagerDetailsDTO;
import com.massclouds.opencmp.dto.InstanceResourceOfCloudPlatformDTO;
import com.massclouds.opencmp.dto.ProviderAddCredentialsDTO;
import com.massclouds.opencmp.dto.ProviderAddCredentialsHistoryDBDTO;
import com.massclouds.opencmp.dto.ProviderAddDTO;
import com.massclouds.opencmp.dto.ProviderDTO;
import com.massclouds.opencmp.dto.ProviderDetailsDTO;
import com.massclouds.opencmp.dto.ProviderRedisDTO;
import com.massclouds.opencmp.dto.TenantsCreateDTO;
import com.massclouds.opencmp.dto.TenantsDTO;
import com.massclouds.opencmp.dto.UserCreateDTO;
import com.massclouds.opencmp.dto.UserGroupCreateDTO;
import com.massclouds.opencmp.dto.UserRoleDTO;
import com.massclouds.opencmp.dto.VmDTO;
import com.massclouds.opencmp.dto.global.ParamHrefDTO;
import com.massclouds.opencmp.dto.global.ParamIdHrefDTO;
import com.massclouds.opencmp.dto.vmp.UserDTO;
import com.massclouds.opencmp.dto.vmp.UserGroupDTO;
import com.massclouds.opencmp.dto.vmp.UserGroupResultDTO;
import com.massclouds.opencmp.dto.vmp.UserResultDTO;
import com.massclouds.opencmp.service.ProvidersService;
import com.massclouds.opencmp.service.TenantsService;
import com.massclouds.opencmp.service.UserGroupsService;
import com.massclouds.opencmp.service.UserRolesService;
import com.massclouds.opencmp.service.UsersService;
import com.massclouds.opencmp.service.VmsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.VmpHttpUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层代码
 * 
 * @date: 2022年3月2日 上午10:13:24
 * @author hou_jjing
 */
@Service("ProvidersService")
public class ProvidersServiceImpl implements ProvidersService{
	
	private static Logger logger = LoggerFactory.getLogger(ProvidersServiceImpl.class);
	
	@Autowired 
	TenantsService tenantsService;
	
	@Autowired
	private UserGroupsService userGroupsService;
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private VmsService vmsService;
	
    @Autowired
    private RedisUtil redisUtil;
	  
	@Override
	public DataResultDTO<Map<String, Object>> providerTypeAll(String token) {
		DataResultDTO<Map<String, Object>> result = new DataResultDTO<>();
		Map<String,Object> typeAllType = new HashMap<>();
		typeAllType.put("InfraManager", ConstantsUtils.providerTypeInfraManagerMap);
		typeAllType.put("ContainerManager", ConstantsUtils.providerTypeContainerManagerMap);
		typeAllType.put("CloudManager", ConstantsUtils.providerTypeCloudManagerMap);
		typeAllType.put("PhysicalInfraManager", ConstantsUtils.providerTypePhysicalInfraManagerMap);
		
		result.setSuccess(typeAllType);
		return result;
	}

	@Override
	public PageDataResultDTO<List<ProviderDTO>> providersList(QueryDTO query, String token) {
		PageDataResultDTO<List<ProviderDTO>> result = new PageDataResultDTO<>();
		
		String  attributes = "id,href,name,type,guid,zone_id,total_hosts,total_vms,created_on,hostname,ipaddress,zone," +
				             "total_vcpus,total_memory,total_miq_templates,total_storages_size,authentication_status";
		
		String url = "/api/providers/" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&sort_by=id&sort_order=desc";
		int page = query.getPage();
		int size = query.getPageSize();
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			//根据指定的排序列进行排序，指定列包括：name、authentication_status
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			
			ResponseEntity<MiqListReturnDTO<ProviderDTO>> providerRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ProviderDTO>>(){}, token);
			if (401 == providerRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			List<ProviderDTO> allResources = providerRes.getBody().getResources();
			allResources = setInfraManagerTypeName(allResources);
			if (allResources != null) {
				List<ProviderDTO> resources =  new ArrayList<ProviderDTO>();
				
				if (!StringUtils.isEmpty(query.getCriteria()) && "InfraManager".equals(query.getCriteria().trim())) {  //虚拟化平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeInfraManager).contains(x.getType()) == true).collect(Collectors.toList());
				} else if (!StringUtils.isEmpty(query.getCriteria()) && "CloudManager".equals(query.getCriteria().trim())) {  //云平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeCloudManager).contains(x.getType()) == true).collect(Collectors.toList());
				    //获取每个云平台的CPU、内存、存储总数，即各个云平台中的实例的CPU、内存、存储的累加
					if(resources != null && resources.size() > 0) {
						for (ProviderDTO containerP : resources) {
							InstanceResourceOfCloudPlatformDTO instanceRes = vmsService.getAllInstancesCpusMemoryOfCloudPlatform(containerP.getId(), token);
							containerP.setTotal_vcpus(instanceRes.getTotalCpus() + "");
							containerP.setTotal_memory(instanceRes.getTotalMemory() + ""); //MB
							containerP.setTotal_storages_size(instanceRes.getTotalDisk()+ "");//KB
						}
					}
				} else  if (!StringUtils.isEmpty(query.getCriteria()) && "ContainerManager".equals(query.getCriteria().trim())) {  //容器平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeContainerManager).contains(x.getType()) == true).collect(Collectors.toList());
					//获取容器平台的详情统计
					if(resources != null && resources.size() > 0) {
						for (ProviderDTO containerP : resources) {
							DataResultDTO<ContainerManagerDetailsDTO> containerDetail = getContainerManagerDetails(containerP.getId(), token);
							if (200 == containerDetail.getCode() && containerDetail.getResult() != null) {
								containerP.setContainerNodeCount(containerDetail.getResult().getContainer_nodes().size());
								containerP.setContainerProjectCount(containerDetail.getResult().getContainer_projects().size());
								containerP.setContainerPodsCount(containerDetail.getResult().getContainer_groups().size());
								containerP.setContainerCount(containerDetail.getResult().getContainers().size());
								containerP.setContainerServiceCount(containerDetail.getResult().getContainer_services().size());
								containerP.setContainerTemplateCount(containerDetail.getResult().getContainer_templates().size());
								containerP.setContainerImagesCount(containerDetail.getResult().getContainer_images().size());
							}
						}
					}
				} else if (!StringUtils.isEmpty(query.getCriteria()) && "PhysicalInfraManager".equals(query.getCriteria().trim())) {  //物理平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypePhysicalInfraManager).contains(x.getType()) == true).collect(Collectors.toList());
				} else {  //全部平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerType).contains(x.getType()) == true).collect(Collectors.toList());
				}
				
				resources.forEach((item) -> {
					//处理内存，转换成GB
					double memory = Double.parseDouble(item.getTotal_memory());
					memory = memory / 1024;   //内存单位是GB
					item.setTotal_memory(String.format("%.1f", memory)); //保留一位小数
					
					//处理存储总大小，转换成GB
					double storages = Double.parseDouble(item.getTotal_storages_size());
					storages = storages / 1024 / 1024 / 1024;
					item.setTotal_storages_size(String.format("%.1f", storages));
					
				});
				int count = resources.size();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				result.setSuccess(resources.subList(pageBegin, pageEnd), (long) count);
				return result;
			} else {
				result.setError(500, "获取供应商信息失败！");
				return result;
			}
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public DataResultDTO<ContainerManagerDetailsDTO> getContainerManagerDetails(String id, String token) {
		DataResultDTO<ContainerManagerDetailsDTO> result = new DataResultDTO<>();
		
		String  attributes = "id,container_nodes,container_projects,container_routes,container_templates,containers,container_groups,container_services,container_images";
		
		String url = "/api/providers/" + id + "?expand=resources" + "&attributes=" + attributes;
		try {
			ResponseEntity<ContainerManagerDetailsDTO> containerRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<ContainerManagerDetailsDTO>(){}, token);
			
			if (401 == containerRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == containerRes.getStatusCodeValue()) {
				result.setSuccess(containerRes.getBody());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	
	}

	@Override
	public DataResultDTO<Map<String, List<ProviderDTO>>> listAll(String token) {
		DataResultDTO<Map<String, List<ProviderDTO>>> result = new DataResultDTO<>();
		Map<String, List<ProviderDTO>> listAllMap = new HashMap<String, List<ProviderDTO>>();
		
		String  attributes = "id,href,name,type,guid,zone_id,total_hosts,total_vms,created_on,hostname,ipaddress";
		String url = "/api/providers/" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&sort_by=id&sort_order=desc";
		try {
		
			ResponseEntity<MiqListReturnDTO<ProviderDTO>> providerRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ProviderDTO>>(){}, token);
			if (200 == providerRes.getStatusCodeValue()) {
				List<ProviderDTO> allResources = providerRes.getBody().getResources();
				allResources = setInfraManagerTypeName(allResources);
				if (allResources != null) {
					List<ProviderDTO> resources =  new ArrayList<ProviderDTO>();
					//虚拟化平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeInfraManager).contains(x.getType()) == true).collect(Collectors.toList());
					listAllMap.put("InfraManager", resources);
					//云平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeCloudManager).contains(x.getType()) == true).collect(Collectors.toList());
					listAllMap.put("CloudManager", resources);
					//容器平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeContainerManager).contains(x.getType()) == true).collect(Collectors.toList());
					listAllMap.put("ContainerManager", resources);
					//物理平台
					resources = allResources.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypePhysicalInfraManager).contains(x.getType()) == true).collect(Collectors.toList());
					listAllMap.put("PhysicalInfraManager", resources);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setError(500, "获取平台失败！");
			return result;
		}
		result.setSuccess(listAllMap);
		return result;
	}
	
	@Override
	public List<ProviderRedisDTO> getInfraManagerFromRedis() throws Exception{
		ObjectMapper objectMapper = new ObjectMapper();
		List<ProviderRedisDTO> redisList = null;
	
		if (redisUtil.get("RedhatInfraManager") == null) {
			redisList = new ArrayList<ProviderRedisDTO>();
		} else {
			// json字符串转对象、集合
			redisList =  objectMapper.readValue(redisUtil.get("RedhatInfraManager").toString(), 
					                         new TypeReference<List<ProviderRedisDTO>>(){} );
		}
		return redisList;
	}
	
	//从redis中取出虚拟化平台，匹配虚拟化平台中的平台类型名称
	public List<ProviderDTO> setInfraManagerTypeName(List<ProviderDTO> resources) throws Exception {
		List<ProviderRedisDTO> redisList = getInfraManagerFromRedis();
	
		if (redisList != null && redisList.size() > 0 && resources != null && resources.size() > 0) {
			for(ProviderDTO providerDTO : resources) {
				if ("ManageIQ::Providers::Redhat::InfraManager".equals(providerDTO.getType())) {
					List<ProviderRedisDTO> providerRedisDTO = redisList.stream().filter(item -> item.getId().equals(providerDTO.getId()) == true).collect(Collectors.toList());;
					if (providerRedisDTO != null && providerRedisDTO.size() > 0) {
						providerDTO.setTypeName(providerRedisDTO.get(0) == null ? null : providerRedisDTO.get(0).getTypeName());
						providerDTO.setIsVMP(providerRedisDTO.get(0) == null ? Boolean.FALSE : providerRedisDTO.get(0).getIsVMP());
					}
				}
			}
		}
		return resources;
	}
	
	public HashMap<String, Object> setProviderParams(ProviderAddDTO providerAddDTO, String flag) {
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        String type = providerAddDTO.getType().trim();
        
        //公共参数
        if("add".equals(flag)) {
        	params.put("type", type);
        }
        params.put("name", providerAddDTO.getName());
        params.put("zone", new ParamIdHrefDTO(providerAddDTO.getZoneId(), providerAddDTO.getZoneHref()));
        
        //添加ovirt的参数
        if ("ManageIQ::Providers::Redhat::InfraManager".equals(type)) {
        	params.put("hostname", providerAddDTO.getHostname());
            params.put("port", providerAddDTO.getPort());
            params.put("ipaddress", providerAddDTO.getIpaddress());
            params.put("security_protocol", "kerberos");
            
            String certificate = providerAddDTO.getCertificateAuthority().replaceAll("\n", "\r\n");
            params.put("certificate_authority", certificate);
            
            //认证ovirt api的用户名、密码
            ProviderAddCredentialsDTO providerAddCredentialsDTO = new ProviderAddCredentialsDTO(providerAddDTO.getUserName(), providerAddDTO.getUserPassword());
            //认证ovirt 历史数据库的信息
            ProviderAddCredentialsHistoryDBDTO providerAddCredentialsHistoryDBDTO = new ProviderAddCredentialsHistoryDBDTO("ovirt_engine_history", providerAddDTO.getHistoryDBPassword(), "metrics");
            List<Object> crdentialsList = new ArrayList<Object>();
            crdentialsList.add(providerAddCredentialsDTO);
            crdentialsList.add(providerAddCredentialsHistoryDBDTO);
            
            params.put("credentials", crdentialsList);
        }
        //添加VMware的参数
        if ("ManageIQ::Providers::Vmware::InfraManager".equals(type)) {
        	params.put("hostname", providerAddDTO.getHostname());
        	params.put("host_default_vnc_port_start", providerAddDTO.getVncPortStart());
        	params.put("host_default_vnc_port_end", providerAddDTO.getVncPortEnd());
        	
        	HashMap<String,String> credenDefault = new HashMap<String,String>();
        	credenDefault.put("userid", providerAddDTO.getUserName());
        	credenDefault.put("password", providerAddDTO.getUserPassword());
        	credenDefault.put("auth_type", "default");
        	
        	HashMap<String,String> credenConsole = new HashMap<String,String>();
        	credenConsole.put("userid", providerAddDTO.getConsoleUserName());
        	credenConsole.put("password", providerAddDTO.getConsoleUserPassword());
        	credenConsole.put("auth_type", "console");
        	
        	List<Object> crdentialsList = new ArrayList<Object>();
        	crdentialsList.add(credenDefault);
        	crdentialsList.add(credenConsole);
        	
        	params.put("credentials", crdentialsList);
        }
        //添加OpenStack的参数
        if ("ManageIQ::Providers::Openstack::CloudManager".equals(type)) {
        	 params.put("hostname", providerAddDTO.getHostname());
        	 params.put("port", providerAddDTO.getPort());
        	 params.put("api_version", "v3");
        	 params.put("security_protocol", "non-ssl");
        	 params.put("provider_region", providerAddDTO.getProviderRegion());
        	 params.put("keystone_v3_domain_id", providerAddDTO.getUidEms());
        	 
        	 HashMap<String,String> credential = new HashMap<String,String>();
         	 credential.put("userid", providerAddDTO.getUserName());
         	 credential.put("password", providerAddDTO.getUserPassword());
         	
         	 List<Object> crdentialsList = new ArrayList<Object>();
         	 crdentialsList.add(credential);
         	
         	params.put("credentials", crdentialsList);
         	
         	 if("add".equals(flag)) {
         		HashMap<String,String> connRole = new HashMap<String,String>();
             	connRole.put("role", "ceilometer");
             	HashMap<String,Object> endpoint = new HashMap<String,Object>();
             	endpoint.put("endpoint", connRole);
             	List<Object> connConfig = new ArrayList<Object>();
             	connConfig.add(endpoint);
             	params.put("connection_configurations", connConfig);
         	 }
        }
        //添加Amazon的参数
        if ("ManageIQ::Providers::Amazon::CloudManager".equals(type)) {
        	 params.put("provider_region", providerAddDTO.getProviderRegion());
        	 
        	 HashMap<String,String> credential = new HashMap<String,String>();
         	 credential.put("userid", providerAddDTO.getUserName());
         	 credential.put("password", providerAddDTO.getUserPassword());
         	 params.put("credentials", credential);
        }
        //添加K8S的参数
        if ("ManageIQ::Providers::Kubernetes::ContainerManager".equals(type)) {
        	 params.put("hostname", providerAddDTO.getHostname());
        	 params.put("port", providerAddDTO.getPort());
        	 params.put("security_protocol", "ssl-without-validation");
        	 
        	 HashMap<String,String> credential = new HashMap<String,String>();
         	 credential.put("auth_key", providerAddDTO.getAuthKey());
         	 credential.put("auth_type", "bearer");
         	 params.put("credentials", credential);
        }
        
        return params;
	}
	
	@Override
	public ResultDTO addProvider(ProviderAddDTO providerAddDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		HashMap<String, Object> params = setProviderParams(providerAddDTO, "add");
		params.put("action", "create");
        String url = "/api/providers";
        try {
        	ResponseEntity<String> providerRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	logger.info(providerRes.getStatusCodeValue() + "");
        	logger.info(providerRes.getBody());
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(providerRes.getBody());
			
        	if (401 == providerRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == providerRes.getStatusCodeValue()) {
				//虚拟化平台，其中oVirt、Red Hat Virtualization、MCOS cServer的type值相同，把他们实际对应的类型放入redis，回显时使用
				//oVirt = 原始ovirt、   Red Hat Virtualization = Red Hat Virtualization、   MCOS cServer = VMP
				if ("ManageIQ::Providers::Redhat::InfraManager".equals(providerAddDTO.getType())) {
					String providerId = jsonNode.get("results").get(0).get("id").asText();
					
					List<ProviderRedisDTO> redisList = getInfraManagerFromRedis();
					ProviderRedisDTO providerRedisDTO = new ProviderRedisDTO();
					providerRedisDTO.setId(providerId);
					providerRedisDTO.setName(providerAddDTO.getName());
					providerRedisDTO.setIpaddress(providerAddDTO.getIpaddress());
					providerRedisDTO.setHostname(providerAddDTO.getHostname());
					providerRedisDTO.setPort(providerAddDTO.getPort());
					providerRedisDTO.setUserName(providerAddDTO.getUserName());
					providerRedisDTO.setHistoryDBPassword(providerAddDTO.getHistoryDBPassword());
					providerRedisDTO.setUserPassword(providerAddDTO.getUserPassword());
					providerRedisDTO.setSyncUserName(providerAddDTO.getSyncUserName());
					providerRedisDTO.setSyncPassword(providerAddDTO.getSyncPassword());
					providerRedisDTO.setType(providerAddDTO.getType());
					providerRedisDTO.setTypeName(providerAddDTO.getTypeName());
					providerRedisDTO.setIsVMP(providerAddDTO.isVMP());
					
					redisList.add(providerRedisDTO);
					
					String jsonString = objectMapper.writeValueAsString(redisList);
					redisUtil.set("RedhatInfraManager", jsonString);
				}
        		result.setSuccess();
        		result.setMessage("供应商添加成功！请等待其添加完成。");
        	} else {
        		
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}

	
	@Override
	public ResultDTO updateProvider(String id, ProviderAddDTO providerAddDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object> resourceParams = setProviderParams(providerAddDTO, "update");
        params.put("action", "edit");
        params.put("resource", resourceParams);
        String url = "/api/providers/" + id;
        try {
        	
        	ResponseEntity<String> updateRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	logger.info(updateRes.getStatusCodeValue() + "");
        	logger.info(updateRes.getBody());
        	
        	if (401 == updateRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == updateRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("供应商编辑成功！");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(updateRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	
	}
	
	@Override
	public ResultDTO recheckProvider(String id, String token) {
		ResultDTO result = new ResultDTO();
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "recheck");
        
        String url = "/api/providers/" + id;
        try {
        	ResponseEntity<String> recheckRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(recheckRes.getBody());
        	
			if (401 == recheckRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == recheckRes.getStatusCodeValue() ) {
        		if (jsonNode.get("success").asBoolean() == Boolean.TRUE) {
        			result.setSuccess();
            		result.setMessage("重新检测供应商认证状态成功！");
        		} else {
        			result.setError(500, jsonNode.get("message").asText());
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}

	@Override
	public ResultDTO refreshProvider(String id, String token) {
		ResultDTO result = new ResultDTO();
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "refresh");
        
        String url = "/api/providers/" + id;
        try {
        	ResponseEntity<String> refreshRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	logger.info(refreshRes.getStatusCodeValue() + "");
        	logger.info(refreshRes.getBody());
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(refreshRes.getBody());
        	
			if (401 == refreshRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == refreshRes.getStatusCodeValue() ) {
        		if (jsonNode.get("success").asBoolean() == Boolean.TRUE) {
        			result.setSuccess();
            		result.setMessage("供应商刷新成功！");
        		} else {
        			result.setError(500, jsonNode.get("message").asText());
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}
	
	@Override
	public ResultDTO refreshAll(String token) {

		ResultDTO result = new ResultDTO();
		List<ParamHrefDTO> paramsResources = new ArrayList<ParamHrefDTO>();
		try {
			PageDataResultDTO<List<ProviderDTO>> providerList = providersList(new QueryDTO(0, 999, ""), token);
			List<ProviderDTO> allProvider = providerList.getResult().getItems();
			if (allProvider != null) {
				allProvider.forEach((item) -> {
					paramsResources.add(new ParamHrefDTO(item.getHref()));
				});
				
				//设置访问参数
		        HashMap<String, Object> params = new HashMap<>();
		        params.put("action", "refresh");
		        params.put("resources", paramsResources);
		        
		        String url = "/api/providers";
	        
	        	ResponseEntity<String> refreshRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
	        	
	        	ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(refreshRes.getBody());
	        	
				if (401 == refreshRes.getStatusCodeValue()) {
					result.setError(401, ConstantsUtils.tokenMessage);
					return result;
				} else if (200 == refreshRes.getStatusCodeValue() ) {
					result.setSuccess();
	        	} else {
					result.setError(jsonNode.get("error").get("message").toString());
	        	}
			} else {
				result.setSuccess();
			}
			
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}

	@Override
	public DataResultDTO<ProviderDetailsDTO> getProviderDetails(String id, String token) {
		DataResultDTO<ProviderDetailsDTO> result = new DataResultDTO<>();
		
		String  attributes = "id,name,type,guid,zone_id,total_hosts,total_vms,created_on,hostname,ipaddress,zone,hosts,storages,authentication_status" + 
				",port,authentications,endpoints,provider_region,host_default_vnc_port_start,host_default_vnc_port_end,provider_region,keystone_v3_domain";  //,lans，去掉lans，因为云平台、容器平台没有lans字段
		
		String url = "/api/providers/" + id + "?expand=resources" + "&attributes=" + attributes;
		try {
			ResponseEntity<ProviderDetailsDTO> providerRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<ProviderDetailsDTO>(){}, token);
			
			if (401 == providerRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == providerRes.getStatusCodeValue()) {
				ProviderDetailsDTO providerDetail = providerRes.getBody();
				List<ProviderRedisDTO> redisList = getInfraManagerFromRedis();
				if (redisList != null) {
					for (ProviderRedisDTO pro : redisList) {
						if(pro.getId().equals(providerDetail.getId())) {
							providerDetail.setTypeName(pro.getTypeName());
							providerDetail.setIsVMP(pro.getIsVMP());
						}
					}
				}
				
				result.setSuccess(providerDetail);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	
	}

	@Override
	public ResultDTO deleteProvider(String id, String token) {
		 ResultDTO result = new ResultDTO();
		 
		 String url = "/api/providers/" + id;
	        try {
	        	ResponseEntity<String> providerRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
	        	logger.info(providerRes.getStatusCodeValue() + "");
	        	logger.info(providerRes.getBody());
	        	if (401 == providerRes.getStatusCodeValue()) {
					result.setError(401, ConstantsUtils.tokenMessage);
					return result;
				} else if (204 == providerRes.getStatusCodeValue()) {
	        		result.setSuccess();
	        		result.setMessage("删除供应商成功！请等待其删除完成。");
	        	} else {
	        		ObjectMapper objectMapper = new ObjectMapper();
					JsonNode jsonNode = objectMapper.readTree(providerRes.getBody());
					result.setError(jsonNode.get("error").get("message").toString());
	        	}
	        	
	        	return result;
	        }catch(Exception e){
	        	e.printStackTrace();
	        	result.setError();
	        	return result;
	        }
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResultDTO syncMCOScServerUserGroup(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		try {
			//1、获取MCOS cServer的用户组、用户信息
			Map<String, Object> userGroupMap = getVMPUserGroup(id);
			if (userGroupMap == null) {
				result.setError(500, "获取平台用户组/用户失败！");
				return result;
			}
			//2、获取ManageIQ中自服务用户角色的id
			List<UserRoleDTO> roleRes = userRolesService.userGroupRolesList(token).getResult();
			roleRes = roleRes.stream().filter(role -> "EvmRole-user_limited_self_service".equals(role.getName()) == true).collect(Collectors.toList());
			String roleId = roleRes.get(0).getId();
			
			//3、获取供应商的名称，供应商名称作为父级租户，用户组同步过来变成租户，全都在这个父级租户下
			String parentTenantId = null;
			DataResultDTO<ProviderDetailsDTO> providerDTO = getProviderDetails(id, token);
			if (200 == providerDTO.getCode() && providerDTO.getResult() != null) {
				//2、用供应商名称创建父级租户
				parentTenantId = createParentTeanant(providerDTO.getResult(), token);
			} else {
				result.setError(500, "创建父级供应商失败！");
				return result;
			}
			
			//4、创建租户，VMP的用户组对应ManageIQ的租户；在manageIQ中同步过来的每一层级的租户都要创建一个相应的组，角色是自服务用户
			Map<String, String> vmpGroupIdTenantId = new HashMap<String, String>();//保存VMP的组id与ManageIQ中tenant id的对应关系
			Map<String, String> tenantIdIqGroupId = new HashMap<String, String>();//保存ManageIQ中tenant id 与 ManageIQ中group id的对应关系
			List<UserGroupDTO> groupsList = (List<UserGroupDTO>) userGroupMap.get("groups");
			if (groupsList != null && groupsList.size() > 0) {
				for (UserGroupDTO group : groupsList) {
					createTeanant(group, id, parentTenantId, token, vmpGroupIdTenantId, tenantIdIqGroupId, roleId);
				}
			}
			//5、创建用户，VMP里的用户对应manageIQ里的用户
			Map<String, String> vmpUserIdIqUserId = new HashMap<String, String>();//保存VMP中用户id 与 manageIQ中用户id 的对应关系
			List<UserDTO> usersList = (List<UserDTO>) userGroupMap.get("users");
			if (usersList != null && usersList.size() > 0) {
				for (UserDTO userDTO : usersList) {
					//用户组id，该用户在VMP中用户组id 对应的 manageIQ中的用户组id
					String groupId = tenantIdIqGroupId.get(vmpGroupIdTenantId.get(userDTO.getUsergroups_id()));
					//VMP中用户密码是MD5加密，无法解密，先设置一个默认密码
					String password = "123qwe";
					String name = StringUtils.isEmpty(userDTO.getName()) ? userDTO.getUsername() : userDTO.getName();
					UserCreateDTO userCreateDTO = new UserCreateDTO(name, userDTO.getUsername(), password, groupId);
					ResultDTO userRes = usersService.createUser(userCreateDTO, token);
					if (200 == userRes.getCode()) {
						vmpUserIdIqUserId.put(userDTO.getId(), userRes.getId());
						
						//6、把用户在VMP中已分配的虚机  在manageIQ里创建相应关系
						if (userDTO.getVms() != null && userDTO.getVms().size() > 0) {
							for (String guid : userDTO.getVms()) {
								//根据VMP中VM的guid，在manageIQ里查询相应的虚机
								DataResultDTO<VmDTO> vmRes = vmsService.getVmByEmsuid(guid, token);
								if (200 == vmRes.getCode() && vmRes.getResult() != null) {
									VmDTO vmDTO = vmRes.getResult();
									//给虚机设置相应的所有权
									vmsService.setVmOwnership(vmDTO.getId(), userDTO.getUser_group_name(), userDTO.getUsername(), token);
								}
							}
						}
						
					}
				}
			}
			
			result.setSuccess("");
			return result;
		} catch (Exception e) { 
			e.printStackTrace();
			result.setError(500, "");
			return result;
		}
	}
	
	public String createParentTeanant(ProviderDetailsDTO provider, String token) {
		String description = "已接管平台： " + provider.getName() + "("  + provider.getIpaddress() + ")";
		String tenantParentId = null;
		//1、创建租户
		   //1.1 判断租户是否已存在
		DataResultDTO<TenantsDTO> isExist = tenantsService.isExistByName(provider.getName(), token);
		
		if (isExist.getResult() == null || isExist.getResult().getName() == null) {//不存在
			TenantsCreateDTO tenantsCreateDTO = new TenantsCreateDTO(provider.getName(), description, null);
			ResultDTO tenantRes = tenantsService.createTenant(tenantsCreateDTO, token);
			if (200 == tenantRes.getCode()) {
				tenantParentId = tenantRes.getId();
			}
		} else { //存在
			tenantParentId = isExist.getResult().getId();
		}
		return tenantParentId;
	}
	
	public ResultDTO createTeanant(UserGroupDTO group, String providerId, String parentId, String token, 
			Map<String, String> vmpGroupIdTenantId, Map<String, String> tenantIdIqGroupId, String roleId) {
		ResultDTO result = new ResultDTO();
		String description = "MCOS cServer id = " + providerId + ", 组id="  + group.getId();
		String tenantParentId = null;
		//1、创建租户，VMP中组名是“未分配组”的不同步；
		   //1.1 判断租户是否已存在
		if("2".equals(group.getId()) && "未分配组".equals(group.getTitle())) {
			result.setSuccess();
			return result;
		}
		DataResultDTO<TenantsDTO> isExist = tenantsService.isExistByName(group.getTitle(), token);
		
		if (isExist.getResult() == null || isExist.getResult().getName() == null) {//不存在
			TenantsCreateDTO tenantsCreateDTO = new TenantsCreateDTO(group.getTitle(), description, parentId);
			ResultDTO tenantRes = tenantsService.createTenant(tenantsCreateDTO, token);
			if (200 == tenantRes.getCode()) {
				tenantParentId = tenantRes.getId();
				vmpGroupIdTenantId.put(group.getId(), tenantParentId);//记录VMP中用户组 和 manageIQ中租户的 对应关系
				
				//2、创建和租户同名的用户组。VMP中没有子组的用户组同步到CMP中的租户后才在该租户下创建同名的用户组；
				if (group.getChildren() == null || group.getChildren().size() == 0) {
					UserGroupCreateDTO userGroupCreateDTO = new UserGroupCreateDTO(group.getTitle(), roleId, tenantParentId);
					ResultDTO groupRes = userGroupsService.createUserGroup(userGroupCreateDTO, token);
					if (200 == groupRes.getCode()) {
						String iqGroupId = groupRes.getId();
						tenantIdIqGroupId.put(tenantParentId, iqGroupId);//记录manageIQ中租户 和 manageIQ中用户组id的 对应关系
					}
				}
				
			}
		} else { //存在
			tenantParentId = isExist.getResult().getId();
			vmpGroupIdTenantId.put(group.getId(), tenantParentId);//记录VMP中用户组 和 manageIQ中租户的 对应关系
		}
		
		//3、迭代创建子租户
		if (group.getChildren() != null && group.getChildren().size() > 0) {
			for (UserGroupDTO child : group.getChildren()) {
				createTeanant(child, providerId, tenantParentId, token, vmpGroupIdTenantId, tenantIdIqGroupId, roleId);
			}
		}
		
		result.setSuccess();
		return result;
	}
	
	public Map<String, Object> getVMPUserGroup(String vmpId) throws Exception {
		//1、获取MCOS cServer的ip、用户名、密码
		String vmpIp = "";
		String syncUserName = "";
		String syncPassword = "";
		List<ProviderRedisDTO> redisList = getInfraManagerFromRedis();
		if (redisList != null && redisList.size() > 0 ) {
			List<ProviderRedisDTO> vmpLsit = redisList.stream().filter(item -> vmpId.equals(item.getId()) == true).collect(Collectors.toList());
			if (vmpLsit != null && vmpLsit.size() > 0) {
				vmpIp = vmpLsit.get(0).getIpaddress();
				syncUserName = vmpLsit.get(0).getSyncUserName();
				syncPassword = vmpLsit.get(0).getSyncPassword();
			} else {
				return null;
			}
		} else {
			return null;
		}
		//2、获取token
		String token = null;
        HashMap<String, Object> params = new HashMap<>();
        params.put("username", syncUserName);
        params.put("password", syncPassword);
        params.put("grant_type", "password");
        params.put("scope", "ovirt-app-api");
        String url = "https://" + vmpIp + "/sso/oauth/token";
        ResponseEntity<String> tokenRes = VmpHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, "");
        ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(tokenRes.getBody());
		if (200 == tokenRes.getStatusCodeValue()) {
			token = jsonNode.get("access_token").asText();
		}
		
		//3、获取用户组
		List<UserGroupDTO> groupsList = null;
		String groupUrl = "https://" + vmpIp + "/api/usersgroups";
		ResponseEntity<UserGroupResultDTO> groupRes = VmpHttpUtils.httpRequest(groupUrl, "GET", new HashMap<>(), 
				new ParameterizedTypeReference<UserGroupResultDTO>(){}, token);
		
		if (200 == groupRes.getStatusCodeValue()) {
			groupsList = groupRes.getBody().getChildren();
		}
//		if (groupsList != null && groupsList.size() > 0) {
//			groupsList = groupsList.stream().filter(n -> (!"未分配组".equals(n.getTitle())) == true).collect(Collectors.toList());
//		}
		//4、获取用户
		List<UserDTO> usersList = null;
		String userUrl = "https://" + vmpIp + "/api/users/allusers";
		HashMap<String, Object> userParams = new HashMap<>();
		userParams.put("page", 1);
		userParams.put("pagerSize", 99999);
		userParams.put("groupIds", "1");
		userParams.put("searchStr", "");
		userParams.put("sortColumn", new HashMap<>());
		ResponseEntity<UserResultDTO> userRes = VmpHttpUtils.httpRequest(userUrl, "POST", userParams, 
				new ParameterizedTypeReference<UserResultDTO>(){}, token);
		
		if (200 == userRes.getStatusCodeValue()) {
			usersList = userRes.getBody().getUsers();
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("groups", groupsList);
		result.put("users", usersList);
		
		return result;
	}

	@Override
	public Map<String, String> getProviderZoneMapper(String token) {
		Map<String, String> providerZoneMapper = new HashMap<String, String>();
		PageDataResultDTO<List<ProviderDTO>> pageResult = providersList(new QueryDTO(0, 999, null), token);
		if ( 401 == pageResult.getCode()) {
			return providerZoneMapper;
		}
		List<ProviderDTO> allProvider = pageResult.getResult() == null ? null : pageResult.getResult().getItems();
		if (allProvider != null && allProvider.size() > 0) {
			for (ProviderDTO provider : allProvider) {
				providerZoneMapper.put(provider.getId(), provider.getZone().getName());
			}
		}
		return providerZoneMapper;
	}
	
}
