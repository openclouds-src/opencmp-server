package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.CategoryDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.service.CategoriesService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("CategoriesService")
public class CategoriesServiceImpl  implements CategoriesService{

	@Override
	public PageDataResultDTO<List<CategoryDTO>> categoriesListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<CategoryDTO>> result = new PageDataResultDTO<>();
		
		String url = "/api/categories" + "?expand=resources";
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		List<CategoryDTO> resultResoures = null;
		try {
			//只获取带自定义前缀的
			url = url + "&filter[]=" + "name=" + "'*" + ConstantsUtils.tagPrefix.replace("_", "") + "*'";
			
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			
			//根据描述模糊查询
			if (!StringUtils.isEmpty(query.getDescription())) {
				url = url + "&filter[]=" + "description=" + "'*" + query.getDescription() + "*'";
			}
			
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			//根据指定的排序列进行排序，指定列包括：description
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			ResponseEntity<MiqListReturnDTO<CategoryDTO>> categoriesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<CategoryDTO>>(){}, token);
			
			if (401 == categoriesRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == categoriesRes.getStatusCodeValue()) {
				resultResoures = categoriesRes.getBody().getResources();
				count = categoriesRes.getBody().getSubquery_count();
				
				//处理一下category的名称，过滤掉添加的前缀
				if (resultResoures != null) {
					for (CategoryDTO category : resultResoures) {
						category.setFullName(category.getName());
						if (category.getName().startsWith(ConstantsUtils.tagPrefix)) {
							category.setName(category.getName().replace(ConstantsUtils.tagPrefix, ""));
							category.setBusinessTag(false);
						}
						if (category.getName().startsWith(ConstantsUtils.businessTagPrefix)) {
							category.setName(category.getName().replace(ConstantsUtils.businessTagPrefix, ""));
							category.setBusinessTag(true);
						}
					}
				}
				result.setSuccess(resultResoures, (long) count);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<List<CategoryDTO>> categoriesList(String token) {
		DataResultDTO<List<CategoryDTO>> result = new DataResultDTO<>();
		List<CategoryDTO> resultResoures = null;
		String url = "/api/categories" + "?expand=resources";
		
		try {
			//只获取带自定义前缀的
			url = url + "&filter[]=" + "name=" + "'*" + ConstantsUtils.tagPrefix.replace("_", "") + "*'";
			url = url + "&sort_by=id&sort_order=desc";
			ResponseEntity<MiqListReturnDTO<CategoryDTO>> categoriesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<CategoryDTO>>(){}, token);
			
			if (401 == categoriesRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == categoriesRes.getStatusCodeValue()) {
				resultResoures = categoriesRes.getBody().getResources();
				
				//处理一下category的名称，过滤掉添加的前缀
				if (resultResoures != null) {
					for (CategoryDTO category : resultResoures) {
						category.setFullName(category.getName());
						if (category.getName().startsWith(ConstantsUtils.tagPrefix)) {
							category.setName(category.getName().replace(ConstantsUtils.tagPrefix, ""));
							category.setBusinessTag(false);
						}
						if (category.getName().startsWith(ConstantsUtils.businessTagPrefix)) {
							category.setName(category.getName().replace(ConstantsUtils.businessTagPrefix, ""));
							category.setBusinessTag(true);
						}
					}
				}
				result.setSuccess(resultResoures);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}


	@Override
	public ResultDTO createOrUpdateCategories(CategoryDTO categoryDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        
       //id不为空则为编辑，id为空则为创建
        if (!StringUtils.isEmpty(categoryDTO.getId())) {
        	params.put("action", "edit");
        	params.put("id", categoryDTO.getId());
        }
        //是业务分类的标签类
        if (categoryDTO.getBusinessTag() != null && categoryDTO.getBusinessTag()) {
        	params.put("name", ConstantsUtils.businessTagPrefix + categoryDTO.getName());
        } else {//普通的标签类
        	params.put("name", ConstantsUtils.tagPrefix + categoryDTO.getName());
        }
        
        params.put("description", categoryDTO.getDescription());
        
        String url = "/api/categories";
        try {
        	
        	ResponseEntity<String> categoryRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == categoryRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == categoryRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("标签类添加成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(categoryRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public ResultDTO deleteCategory(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/categories/" + id;
        try {
        	ResponseEntity<String> deleteRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == deleteRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (204 == deleteRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("标签类删除成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(deleteRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteCategorys(List<String> ids, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/categories";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除标签类成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

}
