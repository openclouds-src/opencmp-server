package com.massclouds.opencmp.service.impl.ansible;

import java.util.HashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.ansible.CredentialDTO;
import com.massclouds.opencmp.service.ansible.CredentialService;
import com.massclouds.opencmp.utils.AnsibleHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.AnsibleListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("CredentialService")
public class CredentialServiceImpl  implements CredentialService{
	
	@Override
	public PageDataResultDTO<List<CredentialDTO>> credentialListOfPage(QueryDTO query) {
		PageDataResultDTO<List<CredentialDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage() + 1;
		int size = query.getPageSize();
		
		String url = "credentials" + "?page=" + page + "&page_size=" + size + "&order_by=-created";
		if (!StringUtils.isEmpty(query.getCriteria())) {
			url = url + "&search=" + query.getCriteria();
		}
		try {
			ResponseEntity<AnsibleListReturnDTO<CredentialDTO>> credentialRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<CredentialDTO>>(){});
			if (200 == credentialRes.getStatusCodeValue()) {
				result.setSuccess(credentialRes.getBody().getResults(), (long)credentialRes.getBody().getCount());
			} else {
				result.setError(500, "获取凭证失败！");
			}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResultDTO createUpdateCredential(CredentialDTO credentialDTO) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", credentialDTO.getName());
        params.put("description", credentialDTO.getDescription());
        params.put("organization", 1);   //使用默认域 
        params.put("credential_type", 1);  //该值为1，表示默认值Machine
        
        HashMap<String, String> inputMap = new HashMap<String, String>();
        inputMap.put("username", credentialDTO.getUsername());
        inputMap.put("password", credentialDTO.getPassword());
        params.put("inputs", inputMap);  //用户名、密码认证
        
        String url = "credentials/"; //url中最后的/一定要有
        String method = "POST";
        //编辑清单
        if (!StringUtils.isEmpty(credentialDTO.getId())) {
        	url = url + credentialDTO.getId() + "/";
        	method = "PUT";
        }
        try {
        	ResponseEntity<String> credentialRes = AnsibleHttpUtils.httpRequest(url, method, 
        			params, 
					new ParameterizedTypeReference<String>(){});
        	 int status = credentialRes.getStatusCodeValue();
        	 if (201 == status || 200 == status) {
        		result.setSuccess();
        		result.setMessage("凭证添加/编辑成功。");
        	} else {
				result.setError(credentialRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public ResultDTO deleteCredential(String id) {
		ResultDTO result = new ResultDTO();
		
        String url = "credentials/" + id + "/"; //url中最后的/一定要有
        try {
        	ResponseEntity<String> projectRes = AnsibleHttpUtils.httpRequest(url, "DELETE", 
        			new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
        	int status = projectRes.getStatusCodeValue();
       	     if (204 == status || 200 == status) {
        		result.setSuccess();
        		result.setMessage("凭证删除成功。");
        	} else {
				result.setError(projectRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
}
 