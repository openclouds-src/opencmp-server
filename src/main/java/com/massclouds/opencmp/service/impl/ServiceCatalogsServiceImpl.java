package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.ServiceCatalogsDTO;
import com.massclouds.opencmp.dto.ServiceCatalogsListDTO;
import com.massclouds.opencmp.dto.ServiceDialogsDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.service.ServiceCatalogsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("ServiceCatalogsService")
public class ServiceCatalogsServiceImpl  implements ServiceCatalogsService{
		
	@Override
	public PageDataResultDTO<List<ServiceCatalogsDTO>> serviceCatalogsListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<ServiceCatalogsDTO>> result = new PageDataResultDTO<>();
		String  attributes = "service_templates";
		String url = "/api/service_catalogs/" + "?expand=resources" + "&attributes=" + attributes;
		
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		
		try {
			
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			//根据描述模糊查询
			if (!StringUtils.isEmpty(query.getDescription())) {
				url = url + "&filter[]=" + "description=" + "'*" + query.getDescription() + "*'";
			}
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			//根据指定的排序列进行排序，指定列包括：name
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			ResponseEntity<MiqListReturnDTO<ServiceCatalogsDTO>> sCatalogsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceCatalogsDTO>>(){}, token);
			
			if (401 == sCatalogsRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == sCatalogsRes.getStatusCodeValue()) {
				//带查询的请求
				if (url.contains("filter") && sCatalogsRes.getBody().getSubquery_count() != null) {
					count = sCatalogsRes.getBody().getSubquery_count();
				} else {//不带查询的请求
					count = sCatalogsRes.getBody().getCount();
				}
				result.setSuccess(sCatalogsRes.getBody().getResources(), (long) count);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public DataResultDTO<List<ServiceCatalogsListDTO>> serviceCatalogsList(String token) {
		DataResultDTO<List<ServiceCatalogsListDTO>> result = new DataResultDTO<>();
		String url = "/api/service_catalogs" + "?expand=resources";
		url = url + "&sort_by=id&sort_order=desc";
		try {
			ResponseEntity<MiqListReturnDTO<ServiceCatalogsListDTO>> sCatalogsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceCatalogsListDTO>>(){}, token);
			
			if (401 == sCatalogsRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == sCatalogsRes.getStatusCodeValue()) {
				result.setSuccess(sCatalogsRes.getBody().getResources());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	
	}

	@Override
	public ResultDTO createServiceCatalog(ServiceCatalogsDTO serviceCatalogsDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", serviceCatalogsDTO.getName());
        params.put("description", serviceCatalogsDTO.getDescription());
        
        String url = "/api/service_catalogs";
        try {
        	
        	ResponseEntity<String> sCatalogRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == sCatalogRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == sCatalogRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("服务目录添加成功！请等待其添加完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(sCatalogRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}
	
	@SuppressWarnings("unused")
	private static class UpdateServiceCatalog{
		private String name;
		private String description;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public UpdateServiceCatalog(String name, String description) {
			super();
			this.name = name;
			this.description = description;
		}
	}
	
	@Override
	public ResultDTO updateServiceCatalog(ServiceCatalogsDTO serviceCatalogsDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        params.put("resource", new UpdateServiceCatalog(serviceCatalogsDTO.getName(), serviceCatalogsDTO.getDescription()));
        
        String url = "/api/service_catalogs/" + serviceCatalogsDTO.getId();
        try {
        	ResponseEntity<String> sCatalogRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == sCatalogRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == sCatalogRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("服务目录编辑成功！");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(sCatalogRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public ResultDTO deleteServiceCatalog(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/service_catalogs/" + id;
        try {
        	ResponseEntity<String> templatesRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
            if (401 == templatesRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (204 == templatesRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("删除服务目录成功！请等待其删除完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(templatesRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
            return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	

	@Override
	public ResultDTO deleteServiceCatalogs(List<String> ids, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/service_catalogs";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除服务目录成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public DataResultDTO<List<ServiceDialogsDTO>> serviceDialogsList(String token) {
		DataResultDTO<List<ServiceDialogsDTO>> result = new DataResultDTO<>();
		String url = "/api/service_dialogs/" + "?expand=resources" + "&sort_by=id&sort_order=desc";
		
		try {
			
			ResponseEntity<MiqListReturnDTO<ServiceDialogsDTO>> sDialogsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceDialogsDTO>>(){}, token);
			if (401 == sDialogsRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
		    }
			if (200 == sDialogsRes.getStatusCodeValue()) {
				result.setSuccess(sDialogsRes.getBody().getResources());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

}
