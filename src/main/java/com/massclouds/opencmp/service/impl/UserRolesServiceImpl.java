package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.UserGroupDTO;
import com.massclouds.opencmp.dto.UserRoleDTO;
import com.massclouds.opencmp.service.UserRolesService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

@Service("UserRolesService")
public class UserRolesServiceImpl  implements UserRolesService{

	@Override
	public PageDataResultDTO<List<UserRoleDTO>> userGroupRolesListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<UserRoleDTO>> result = new PageDataResultDTO<>();
		String url = "/api/roles?expand=resources,features&attributes=miq_groups" ;
		int page = query.getPage();
		int size = query.getPageSize();
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			//根据指定的排序列进行排序，指定列包括：name
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			ResponseEntity<MiqListReturnDTO<UserRoleDTO>> rolesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserRoleDTO>>(){}, token);
			if (401 == rolesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == rolesRes.getStatusCodeValue()) {
 		    	List<UserRoleDTO> rolesResources = rolesRes.getBody().getResources();
 				List<UserRoleDTO> resources = rolesResources.stream().filter(x -> Arrays.asList(ConstantsUtils.userRoles).contains(x.getName()) == true).collect(Collectors.toList());
 				
 				int count = resources.size();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				List<UserRoleDTO> resultList = resources.subList(pageBegin, pageEnd);
				
				//用户组的类型group_type包括三种：system、user、tenant。角色下的组过滤掉类型是tenant的
 				for(UserRoleDTO role : resultList) {
 					if (role.getMiq_groups() != null) {
 						List<UserGroupDTO> groupsList = new ArrayList<UserGroupDTO>();
 						for (UserGroupDTO group : role.getMiq_groups()) {
 							if (!group.getGroup_type().equals("tenant")) {
 								groupsList.add(group);
 							}
 						}
 						role.setMiq_groups(groupsList);
 					}
 				}
				
				result.setSuccess(resultList, (long) count);
 		    } else {
        		result.setError(500, "角色信息获取失败");
        	}
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<List<UserRoleDTO>> userGroupRolesList(String token) {
		DataResultDTO<List<UserRoleDTO>> result = new DataResultDTO<>();
		String url = "/api/roles?expand=resources&attributes=id,name" ;
		url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
		try {
			ResponseEntity<MiqListReturnDTO<UserRoleDTO>> rolesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserRoleDTO>>(){}, token);
			if (401 == rolesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == rolesRes.getStatusCodeValue()) {
				List<UserRoleDTO> rolesResources = rolesRes.getBody().getResources();
				List<UserRoleDTO> resources = rolesResources.stream().filter(x -> Arrays.asList(ConstantsUtils.userRoles).contains(x.getName()) == true).collect(Collectors.toList());
			
				result.setSuccess(resources);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
}
