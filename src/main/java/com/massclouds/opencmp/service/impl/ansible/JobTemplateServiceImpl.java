package com.massclouds.opencmp.service.impl.ansible;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.ansible.CredentialDTO;
import com.massclouds.opencmp.dto.ansible.InventoryDTO;
import com.massclouds.opencmp.dto.ansible.JobTemplateDTO;
import com.massclouds.opencmp.dto.ansible.ProjectDTO;
import com.massclouds.opencmp.service.ansible.CredentialService;
import com.massclouds.opencmp.service.ansible.InventoryService;
import com.massclouds.opencmp.service.ansible.JobTemplateService;
import com.massclouds.opencmp.service.ansible.ProjectService;
import com.massclouds.opencmp.utils.AnsibleHttpUtils;
import com.massclouds.opencmp.utils.DateFormatUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.AnsibleListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("JobTemplateService")
public class JobTemplateServiceImpl  implements JobTemplateService{
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private CredentialService credentialService;
	
	@SuppressWarnings("serial")
	@Override
	public PageDataResultDTO<List<JobTemplateDTO>> jobTemplateListOfPage(QueryDTO query) {
		PageDataResultDTO<List<JobTemplateDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage() + 1;
		int size = query.getPageSize();
		
		String url = "job_templates" + "?page=" + page + "&page_size=" + size + "&order_by=-created";
		if (!StringUtils.isEmpty(query.getCriteria())) {
			url = url + "&search=" + query.getCriteria();
		}
		try {
			ResponseEntity<AnsibleListReturnDTO<JobTemplateDTO>> templateRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<JobTemplateDTO>>(){});
			if (200 == templateRes.getStatusCodeValue()) {
				//获取job的清单的名称
				Map<String, String> inventoryMap = new HashMap<String, String>(){};
				DataResultDTO<List<InventoryDTO>> inventoryRes = inventoryService.inventoryListAll();
				if (inventoryRes.getResult() != null) {
					inventoryRes.getResult().forEach(item -> {
						inventoryMap.put(item.getId(), item.getName());
					});
				}
				
				List<JobTemplateDTO> templateList = templateRes.getBody().getResults();
				if (templateList != null) {
					templateList.forEach(n -> {
						n.setInventoryName(inventoryMap.get(n.getInventory() + ""));
					});
				}
				
				result.setSuccess(templateList, (long)templateRes.getBody().getCount());
			} else {
				result.setError(500, "获取任务模板失败！");
			}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResultDTO createUpdateJobTemplate(JobTemplateDTO jobTemplateDTO) {
		ResultDTO result = new ResultDTO();
		/*************1、创建/编辑任务模板*************/
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", jobTemplateDTO.getName());
        params.put("description", jobTemplateDTO.getDescription());
        params.put("job_type", jobTemplateDTO.getJob_type());  //默认值run、还有check
        params.put("inventory", jobTemplateDTO.getInventory());  //清单id
        params.put("playbook", jobTemplateDTO.getPlaybook());  //脚本文件
        
        //1.1 获取初始化的项目的id
        ProjectDTO defaultProject =  projectService.getDefaultProject();
		if (defaultProject != null && defaultProject.getId() != null) {
			params.put("project", defaultProject.getId());  //任务模板id
		} else {
			result.setError("没有初始化Project！");
        	return result;
		}
        //1.2 创建/编辑清单
        String url = "job_templates/"; //url中最后的/一定要有
        String method = "POST";
        //如果id不为空，则为编辑清单
        if (!StringUtils.isEmpty(jobTemplateDTO.getId())) {
        	url = url + jobTemplateDTO.getId() + "/";
        	method = "PUT";
        }
        try {
        	ResponseEntity<String> templateRes = AnsibleHttpUtils.httpRequest(url, method, 
        			params, 
					new ParameterizedTypeReference<String>(){});
        	 int status = templateRes.getStatusCodeValue();
        	 if (201 == status || 200 == status) {
        		 /*************2、为任务模板设置凭证*************/
        		ObjectMapper objectMapper = new ObjectMapper();
  				JsonNode jsonNode = objectMapper.readTree(templateRes.getBody());
        		if (!StringUtils.isEmpty(jobTemplateDTO.getUserName()) && jsonNode.get("id") != null) {
        			String templateId = jsonNode.get("id").asText();
        			//编辑模板凭证，如果模板已存在凭证，则删除原有了
        			JsonNode credentialNode = jsonNode.get("summary_fields").get("credentials");
        			if (credentialNode.size() > 0 ) {
        				String credentialId = credentialNode.get(0).get("id").asText();
        				ResultDTO credentialDelRes = credentialService.deleteCredential(credentialId);
        			}
 					//给模板分配凭证
        			ResultDTO credentialRes = associateJobTemplateCredential(templateId, jobTemplateDTO.getUserName(), jobTemplateDTO.getPassword());
 		        	 if (200 == credentialRes.getCode()) {
 		        		result.setSuccess();
 		        		result.setMessage("任务模板添加/编辑成功。");
 		        	 } else {
 		        		result.setError(200, "任务模板创建成功，为其配置凭证失败！"); 
 		        	 }
        		} else {
        			result.setSuccess();
	        		result.setMessage("任务模板添加/编辑成功。");
        		}
        	} else {
				result.setError(templateRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO launchJobTemplate(String id) {
		ResultDTO result = new ResultDTO();
		
		String url = "job_templates/" + id + "/launch/";
		try {
			ResponseEntity<String> launchRes = AnsibleHttpUtils.httpRequest(url, "POST", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
	     	 if (201 ==launchRes.getStatusCodeValue()) {
	     		result.setSuccess();
	     		result.setMessage("任务模板执行成功。");
	     	 } else {
	     		result.setError(500, launchRes.getBody()); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
		
	}

	@Override
	public ResultDTO associateJobTemplateCredential(String jobTemplateid, String userName, String password) {
		ResultDTO result = new ResultDTO();
		
		String url = "job_templates/" + jobTemplateid + "/credentials/";
		HashMap<String, Object> credentialParm = new HashMap<String, Object>();
		credentialParm.put("name", DateFormatUtils.getCurrentDate());//随机生成一个名字
		credentialParm.put("credential_type", 1);  //该值为1，表示默认值Machine
        HashMap<String, String> inputMap = new HashMap<String, String>();
        inputMap.put("username", userName);
        inputMap.put("password", password);
        credentialParm.put("inputs", inputMap);  //用户名、密码认证
		
		try {
			ResponseEntity<String> credentialRes = AnsibleHttpUtils.httpRequest(url, "POST", 
					credentialParm, 
					new ParameterizedTypeReference<String>(){});
	     	 if (201 ==credentialRes.getStatusCodeValue()) {
	     		result.setSuccess();
	     		result.setMessage("任务模板的凭证设置成功。");
	     	 } else {
	     		result.setError(500, "任务模板的凭证设置失败。"); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
		
	}

	@Override
	public ResultDTO disassociateJobTemplateCredential(String jobTemplateid, String credentialId) {
		ResultDTO result = new ResultDTO();
		
		String url = "job_templates/" + jobTemplateid + "/credentials/";
		HashMap<String, Object> credentialParm = new HashMap<String, Object>();
		credentialParm.put("id", credentialId);
		credentialParm.put("disassociate", Boolean.TRUE);
		try {
			ResponseEntity<String> credentialRes = AnsibleHttpUtils.httpRequest(url, "POST", 
					credentialParm, 
					new ParameterizedTypeReference<String>(){});
	     	 if (204 ==credentialRes.getStatusCodeValue()) {
	     		result.setSuccess();
	     		result.setMessage("任务模板的凭证解除成功。");
	     	 } else {
	     		result.setError(500, "任务模板的凭证解除失败。"); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
		
	}

	@Override
	public ResultDTO deleteJobTemplate(String id) {
		ResultDTO result = new ResultDTO();
		
		//获取模板的凭证
		List<CredentialDTO> templateCred = null;
		DataResultDTO<List<CredentialDTO>> credentialDTO = getJobTemplateCredential(id);
		if (200 == credentialDTO.getCode() && credentialDTO.getResult() != null) {
			templateCred = credentialDTO.getResult();
		}
		
        String url = "job_templates/" + id + "/"; //url中最后的/一定要有
        try {
        	ResponseEntity<String> templateRes = AnsibleHttpUtils.httpRequest(url, "DELETE", 
        			new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
        	int status = templateRes.getStatusCodeValue();
       	     if (204 == status || 200 == status) {
       	    	 //模板删除成功后，删除模板的凭证
       	    	 if (templateCred != null) {
       	    		 for (CredentialDTO cred : templateCred) {
       	    			credentialService.deleteCredential(cred.getId());
       	    		 }
       	    	 }
       	    	 
        		result.setSuccess();
        		result.setMessage("任务模板删除成功。");
        	} else {
				result.setError(templateRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public DataResultDTO<List<CredentialDTO>> getJobTemplateCredential(String jobTemplateid) {
		DataResultDTO<List<CredentialDTO>> result = new DataResultDTO<>();
		
		String url = "job_templates/" + jobTemplateid + "/credentials/";
		try {
			ResponseEntity<AnsibleListReturnDTO<CredentialDTO>> credentialRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<CredentialDTO>>(){});
			if (200 == credentialRes.getStatusCodeValue() && credentialRes.getBody().getResults() != null) {
				result.setSuccess(credentialRes.getBody().getResults());
			} else {
				result.setError(500, "获取凭证失败！");
			}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
 