package com.massclouds.opencmp.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.ZoneDTO;
import com.massclouds.opencmp.service.ZonesService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("ZonesService")
public class ZonesServiceImpl  implements ZonesService{

	@Override
	public PageDataResultDTO<List<ZoneDTO>> zonesListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<ZoneDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage();
		int size = query.getPageSize();
		
		String  attributes = "";
		String url = "/api/zones/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=description!=Maintenance Zone";
		
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			//根据描述模糊查询
			if (!StringUtils.isEmpty(query.getDescription())) {
				url = url + "&filter[]=" + "description=" + "'*" + query.getDescription() + "*'";
			}
			
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			
			//根据指定的排序列进行排序，指定列包括：name
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			
			ResponseEntity<MiqListReturnDTO<ZoneDTO>> zoneRes = getZonesList(url, token);
			if (401 == zoneRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == zoneRes.getStatusCodeValue()) {
				result.setSuccess(zoneRes.getBody().getResources(), (long) zoneRes.getBody().getSubquery_count());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public DataResultDTO<List<ZoneDTO>> zonesList(String token) {
		DataResultDTO<List<ZoneDTO>> result = new DataResultDTO<>();
		String  attributes = "";
		String url = "/api/zones/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=description!=Maintenance Zone";
		url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
		try {
			
			ResponseEntity<MiqListReturnDTO<ZoneDTO>> zoneRes = getZonesList(url, token);
			if (401 == zoneRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == zoneRes.getStatusCodeValue()) {
				result.setSuccess(zoneRes.getBody().getResources());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	public ResponseEntity<MiqListReturnDTO<ZoneDTO>> getZonesList(String url, String token) throws Exception {
		return ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ZoneDTO>>(){}, token);
	}
	
	

	@Override
	public ResultDTO createZone(ZoneDTO zoneDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", zoneDTO.getName());
        params.put("description", zoneDTO.getDescription());
        
        String url = "/api/zones";
        try {
        	ResponseEntity<String> zoneRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	if (401 == zoneRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == zoneRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("区添加成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(zoneRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO updateZone(ZoneDTO zoneDTO, String token) {

		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        params.put("id", zoneDTO.getId());
        params.put("name", zoneDTO.getName());
        params.put("description", zoneDTO.getDescription());
        
        String url = "/api/zones";
        try {
        	ResponseEntity<String> zoneRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == zoneRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == zoneRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("区编辑成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(zoneRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteZone(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/zones/" + id;
        try {
        	ResponseEntity<String> zoneRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == zoneRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == zoneRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("删除区域成功！请等待其删除完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(zoneRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
}
 