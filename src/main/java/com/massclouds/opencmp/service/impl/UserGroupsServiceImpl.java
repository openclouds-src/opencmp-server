package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.common.utils.JsonUtils;
import com.massclouds.opencmp.dto.UserDTO;
import com.massclouds.opencmp.dto.UserGroupCreateDTO;
import com.massclouds.opencmp.dto.UserGroupDTO;
import com.massclouds.opencmp.dto.global.ParamFilterDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.service.UserGroupsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.PinYinUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("UserGroupsService")
public class UserGroupsServiceImpl  implements UserGroupsService{

	private static Logger logger = LoggerFactory.getLogger(UserGroupsServiceImpl.class);
	@Override
	public PageDataResultDTO<List<UserGroupDTO>> groupsListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<UserGroupDTO>> result = new PageDataResultDTO<>();
		String attributes = "users,miq_user_role,tenant,entitlement.filters";
		String url = "/api/groups?expand=resources" + "&attributes=" + attributes;;
		
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "description=" + "'*" + query.getName() + "*'";
			}
			//根据指定的排序列进行排序，指定列包括：description、alias_name、created_on
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			
			List<UserGroupDTO> groupsList = getGroupsList(url, token);
			if (groupsList != null) {
				
				int page = query.getPage();
				int size = query.getPageSize();
				int count = groupsList.size();
				
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				result.setSuccess(groupsList.subList(pageBegin, pageEnd), (long) count);
			} else {
				result.setSuccess(null, (long) 0);
			}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public DataResultDTO<List<UserGroupDTO>> groupsList(String token) {
		DataResultDTO<List<UserGroupDTO>> result = new DataResultDTO<>();
		String attributes = "href,id,description,miq_user_role,alias_name";
		String url = "/api/groups?expand=resources" + "&attributes=" + attributes;;
		url = url + "&sort_by=id&sort_order=desc";
		try {
			List<UserGroupDTO> groupRes = getGroupsList(url, token);
			result.setSuccess(groupRes);
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public List<UserGroupDTO> getGroupsList(String url, String token) {
		try {
			ResponseEntity<MiqListReturnDTO<UserGroupDTO>> groupsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserGroupDTO>>(){}, token);
			List<UserGroupDTO> groupsList = groupsRes.getBody().getResources();
			if (groupsList != null) {
				//根据支持的三种角色，过滤My Company下的组，即，租户My Company下的组只返回支持的那三种角色的组
				List<UserGroupDTO> groupsListRes = groupsList.stream().filter(x -> Arrays.asList(ConstantsUtils.userRoles).contains(x.getMiq_user_role().getName()) == true).collect(Collectors.toList());
				for(UserGroupDTO group : groupsListRes ) {
					if (StringUtils.isEmpty(group.getAlias_name())) {
						group.setAlias_name(group.getDescription());
					}
				}
				return groupsListRes;
			} else {
				return null;
			}
		}catch(Exception e) {
			e.printStackTrace();
        	return null;
		}
	}

	@Override
	public DataResultDTO<UserGroupDTO> getById(String id, String token) {
		DataResultDTO<UserGroupDTO> result = new DataResultDTO<>();
		String attributes = "miq_user_role,tenant,users,entitlement,alias_name";
		String url = "/api/groups/" + id + "?expand=resources" + "&attributes=" + attributes;
		try {
			ResponseEntity<UserGroupDTO> groupsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<UserGroupDTO>(){}, token);
			
			if (401 == groupsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == groupsRes.getStatusCodeValue()) {
				result.setSuccess(groupsRes.getBody());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError(500, "获取信息失败！");
			return result;
		}
	}

	@Override
	public ResultDTO createOrUpdateUserGroup(UserGroupCreateDTO userGroupCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		String url = "/api/groups";
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        
        //id不为空则为编辑，id为空则为创建
        if (!StringUtils.isEmpty(userGroupCreateDTO.getId())) {
        	params.put("action", "edit");
        	url = url + "/" + userGroupCreateDTO.getId();
        } else {
        	params.put("action", "create");
        }
        params.put("description", PinYinUtils.getPinyin(userGroupCreateDTO.getName()));//description不能有中文，所以把中文转换成拼音
        params.put("alias_name", userGroupCreateDTO.getName());//alias_name字段存放用户组的中文名
        
        Map<String, Integer> roleParm = new HashMap<String, Integer>();
        roleParm.put("id", Integer.parseInt(userGroupCreateDTO.getRoleId()));
        params.put("role", roleParm);
        
        Map<String, Integer> tenantParm = new HashMap<String, Integer>();
        tenantParm.put("id", Integer.parseInt(userGroupCreateDTO.getTenantId()));
        params.put("tenant", tenantParm);
        
        //过滤标签的设置
        List<Object> tagAllFilter = new ArrayList<Object>();
        
        if (!StringUtils.isEmpty(userGroupCreateDTO.getTagName())) { //设置过滤标签
         	List<String> tagFilter = new ArrayList<String>();
             tagFilter.add(userGroupCreateDTO.getTagName());
             tagAllFilter.add(tagFilter);
             params.put("filters", new ParamFilterDTO(tagAllFilter));//添加标签时，标签格式："filters" : {"managed" : [ "/managed/opencmp/tag_qichuangyanfa" ] }
         } else {//删除过滤标签，如果是删除，就传空对象{}
         	params.put("filters", new HashMap<String, Object>());
         }
        
        String paramJson = JsonUtils.obj2json(params);
        logger.info("-------" + paramJson);
        
        try {
        	ResponseEntity<String> groupRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == groupRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == groupRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("用户组添加/编辑成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(groupRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
	@Override
	public ResultDTO createUserGroup(UserGroupCreateDTO userGroupCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		String url = "/api/groups";
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("description", PinYinUtils.getPinyin(userGroupCreateDTO.getName()));//description不能有中文，所以把中文转换成拼音
        params.put("alias_name", userGroupCreateDTO.getName());//alias_name字段存放用户组的中文名
        params.put("role", new ParamIdDTO(userGroupCreateDTO.getRoleId()));
        params.put("tenant", new ParamIdDTO(userGroupCreateDTO.getTenantId()));
        try {
        	ResponseEntity<String> groupRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(groupRes.getBody());
        	if (401 == groupRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 		    } else if (200 == groupRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("用户组添加/编辑成功。");
        		String tenantId = jsonNode.get("results").get(0).get("id").asText();
 		    	result.setId(tenantId);
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteUserGroup(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/groups/" + id;
        try {
        	ResponseEntity<String> templatesRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == templatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == templatesRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("删除用户组成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(templatesRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}


	@Override
	public ResultDTO deleteUserGroups(List<String> ids, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/groups";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除用户组成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public List<UserDTO> getUsersByTenantId(String tenantId, String token) {
		List<UserDTO> resultUsers = new ArrayList<UserDTO>();
		
		String attributes = "href,id,description,miq_user_role,users,tenant_id,alias_name";
		String url = "/api/groups?expand=resources" + "&attributes=" + attributes;
		url = url + "&filter[]=tenant_id=" + tenantId;
		url = url + "&sort_by=id&sort_order=desc";
		try {
			ResponseEntity<MiqListReturnDTO<UserGroupDTO>> groupsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<UserGroupDTO>>(){}, token);
			if (200 == groupsRes.getStatusCodeValue() && groupsRes.getBody().getResources() != null) {
				List<UserGroupDTO> groupsList = groupsRes.getBody().getResources();
				for(int i = 0; i < groupsList.size(); i++) {
					UserGroupDTO group = groupsList.get(i);
					if (group.getUsers() != null && group.getUsers().size() > 0) {
						for (UserDTO user : group.getUsers()) {
							List<UserGroupDTO> miqGroups = new ArrayList<UserGroupDTO>();
							UserGroupDTO userGroup = new UserGroupDTO();
							userGroup.setId(group.getId());
							userGroup.setHref(group.getHref());
							userGroup.setDescription(group.getDescription());
							userGroup.setTenant_id(group.getTenant_id());
							miqGroups.add(userGroup);
							user.setMiq_groups(miqGroups);
							resultUsers.add(user);
						}
					}
				}
 		    }
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		return resultUsers;
	}

}
