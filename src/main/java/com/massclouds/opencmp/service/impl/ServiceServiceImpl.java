package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.ServiceCountDTO;
import com.massclouds.opencmp.dto.ServiceCreateDTO;
import com.massclouds.opencmp.dto.ServiceDTO;
import com.massclouds.opencmp.dto.ServiceRequestApprovalDTO;
import com.massclouds.opencmp.dto.ServiceRequestCountDTO;
import com.massclouds.opencmp.dto.ServiceRequestDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.dto.query.ServerQueryDTO;
import com.massclouds.opencmp.dto.query.ServerRequestQueryDTO;
import com.massclouds.opencmp.service.ServiceService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("ServiceService")
public class ServiceServiceImpl  implements ServiceService{
	private static Logger logger = LoggerFactory.getLogger(ServiceServiceImpl.class);
	@Override
	public PageDataResultDTO<List<ServiceDTO>> servicesList(ServerQueryDTO query, String token) {
		PageDataResultDTO<List<ServiceDTO>> result = new PageDataResultDTO<>();
		String  attributes = "service_template.config_info";
		String url = "/api/services/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=";
		
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			//根据描述模糊查询
			if (!StringUtils.isEmpty(query.getDescription())) {
				url = url + "&filter[]=" + "description=" + "'*" + query.getDescription() + "*'";
			}
			//根据请求时间查询
			if (!StringUtils.isEmpty(query.getCreatedDate())) {
				url = url + "&filter[]=" + "created_at=" + query.getCreatedDate();
			}
			
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			//根据指定的排序列进行排序，指定列包括：name
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			ResponseEntity<MiqListReturnDTO<ServiceDTO>> servicesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceDTO>>(){}, token);
			if (401 == servicesRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
		    }
			if (200 == servicesRes.getStatusCodeValue()) {
				//带查询的请求
				if (url.contains("filter") && servicesRes.getBody().getSubquery_count() != null) {
					count = servicesRes.getBody().getSubquery_count();
				} else {//不带查询的请求
					count = servicesRes.getBody().getCount();
				}
				result.setSuccess(servicesRes.getBody().getResources(), (long) count);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public ResultDTO createService(ServiceCreateDTO serviceCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		serviceCreateDTO.setMemory_limit(Integer.parseInt(serviceCreateDTO.getVm_memory()) * 4);//允许的内存最大值
		serviceCreateDTO.setMemory_reserve(Integer.parseInt(serviceCreateDTO.getVm_memory()));//保证物理内存
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "order");
        params.put("resource", serviceCreateDTO);
        
        String url = "/api/service_catalogs/" + serviceCreateDTO.getService_catalogs_id() + "/service_templates";
        try {
        	
        	ResponseEntity<String> serviceRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == serviceRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
		    } else if (200 == serviceRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("服务申请成功！请等待其创建完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(serviceRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	
	@Override
	public ResultDTO deleteService(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/services/" + id;
        try {
        	
        	ResponseEntity<String> templatesRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == templatesRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
		    } else if (204 == templatesRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("删除服务成功！请等待其删除完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(templatesRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	

	@Override
	public ResultDTO deleteServices(List<String> ids, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/services";
        try {
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除服务成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public ResultDTO retireService(String id, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "request_retire");
        
        String url = "/api/services/" + id;
        try {
        	
        	ResponseEntity<String> retireRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == retireRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
		    } else if (200 == retireRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("服务归还成功！请等待其归还完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(retireRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public DataResultDTO<ServiceCountDTO> servicesCount(String token) {
		DataResultDTO<ServiceCountDTO> result = new DataResultDTO<>();
		
		String  attributes = "id,name,retired,retirement_state,lifecycle_state";
		String url = "/api/services/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=";
		
		try {
			ResponseEntity<MiqListReturnDTO<ServiceDTO>> servicesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceDTO>>(){}, token);
			if (401 == servicesRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
		    }
			if (200 == servicesRes.getStatusCodeValue()) {
				int allCount = 0;
				int retiredCount = 0;
				int activeCount = 0;
				
				List<ServiceDTO> allServices = servicesRes.getBody().getResources();
				if (allServices != null) {
					allCount = allServices.size();
					retiredCount = allServices.stream().filter(s -> s.isRetired() == Boolean.TRUE).collect(Collectors.toList()).size();
					activeCount = allServices.stream().filter(s -> s.isRetired() == Boolean.FALSE).collect(Collectors.toList()).size();
				}
				ServiceCountDTO serviceCountDTO = new ServiceCountDTO();
				serviceCountDTO.setAllCount(allCount);
				serviceCountDTO.setActiveCount(activeCount);
				serviceCountDTO.setRetiredCount(retiredCount);
				serviceCountDTO.setWaitRetire(0);
				
				result.setSuccess(serviceCountDTO);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public PageDataResultDTO<List<ServiceRequestDTO>> serviceRequestList(ServerRequestQueryDTO query, String token) {
		PageDataResultDTO<List<ServiceRequestDTO>> result = new PageDataResultDTO<>();
		String  attributes = "reason,v_approved_by";
		String url = "/api/requests/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=";
		
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		
		try {
			//根据请求类型查询
			if (!StringUtils.isEmpty(query.getRequestType())) {
				url = url + "&filter[]=" + "request_type=" + query.getRequestType();
			} else {
				url = url + "&filter[]=request_type=clone_to_service&filter[]=or request_type=vm_reconfigure";
			}
			
			//根据状态查询
			if (!StringUtils.isEmpty(query.getApprovalState())) {
				url = url + "&filter[]=" + "approval_state=" +  query.getApprovalState();
			}
			//根据请求者名称查询
			if (!StringUtils.isEmpty(query.getRequesterName())) {
				url = url + "&filter[]=" + "requester_name="  + "'*" + query.getRequesterName() + "*'";
			}
			//根据请求时间查询
			if (!StringUtils.isEmpty(query.getCreatedDate())) {
				url = url + "&filter[]=" + "created_on=" + query.getCreatedDate();
			}
			//根据审批原因模糊查询
			if (!StringUtils.isEmpty(query.getReason())) {
				url = url + "&filter[]=" + "reason=" + "'*" + query.getReason() + "*'";
			}
			
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			url = url + "&sort_by=created_on&sort_order=desc";  //按创建时间倒序排序
			
			ResponseEntity<MiqListReturnDTO<ServiceRequestDTO>> sRequestRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceRequestDTO>>(){}, token);
            if (401 == sRequestRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
		    }
            if (200 == sRequestRes.getStatusCodeValue()) {
            	//带查询的请求
    			if (url.contains("filter") && sRequestRes.getBody().getSubquery_count() != null) {
    				count = sRequestRes.getBody().getSubquery_count();
    			} else {//不带查询的请求
    				count = sRequestRes.getBody().getCount();
    			}
    			result.setSuccess(sRequestRes.getBody().getResources(), (long) count);
            } else {
				result.setError("获取信息失败！");
        	}
          
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	

	@Override
	public ResultDTO serviceRequestApproval(ServiceRequestApprovalDTO serviceRequestApprovalDTO, String token) {

		ResultDTO result = new ResultDTO();
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", serviceRequestApprovalDTO.getAction());  //审批通过：approve、拒绝： deny
        params.put("reason", serviceRequestApprovalDTO.getReason());
        
        String url = "/api/requests/" + serviceRequestApprovalDTO.getId();
        try {
        	ResponseEntity<String> requestRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	if (401 == requestRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == requestRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("服务审批成功！");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(requestRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	 return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public DataResultDTO<ServiceRequestCountDTO> serviceRequestCount(String token) {

		DataResultDTO<ServiceRequestCountDTO> result = new DataResultDTO<>();
		ServiceRequestCountDTO serviceRequestCountDTO = new ServiceRequestCountDTO();
		String  attributes = "reason,v_approved_by";
		String url = "/api/requests/" + "?expand=resources" + "&attributes=" + attributes ;
		
	    int pendingApproval;
	    int approved;
	    int denied;
		
		try {
			url = url + "&sort_by=created_on&sort_order=desc";  //按创建时间倒序排序
			
			ResponseEntity<MiqListReturnDTO<ServiceRequestDTO>> sRequestRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceRequestDTO>>(){}, token);
            if (200 == sRequestRes.getStatusCodeValue() && sRequestRes.getBody().getResources() != null) {
				List<ServiceRequestDTO> allRequest = sRequestRes.getBody().getResources();

				pendingApproval = allRequest.stream().filter(s -> s.getApproval_state().equals("pending_approval")).collect(Collectors.toList()).size(); //待批准的
				approved = allRequest.stream().filter(s -> s.getApproval_state().equals("approved")).collect(Collectors.toList()).size();//已审批通过的
				denied = allRequest.stream().filter(s -> s.getApproval_state().equals("denied")).collect(Collectors.toList()).size();//拒绝的

				serviceRequestCountDTO.setAllCount(allRequest.size());
				serviceRequestCountDTO.setPendingApproval(pendingApproval);
				serviceRequestCountDTO.setApproved(approved);
				serviceRequestCountDTO.setDenied(denied);
				
				result.setSuccess(serviceRequestCountDTO);
		    }  else {
				result.setError("获取信息失败！");
        	}
			       
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

}
