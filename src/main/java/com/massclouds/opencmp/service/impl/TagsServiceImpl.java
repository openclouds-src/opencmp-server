package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.TagCreateDTO;
import com.massclouds.opencmp.dto.TagDTO;
import com.massclouds.opencmp.dto.global.ParamHrefDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.dto.query.TagsQueryDTO;
import com.massclouds.opencmp.service.TagsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("TagsService")
public class TagsServiceImpl  implements TagsService{

	@Override
	public PageDataResultDTO<List<TagDTO>> tagsListOfPage(TagsQueryDTO query, String token) {
		PageDataResultDTO<List<TagDTO>> result = new PageDataResultDTO<>();
		
		String attributes = "category.name,category.id,classification";
		String url = "/api/tags" + "?expand=resources" + "&attributes=" + attributes;
		int page = query.getPage();
		int size = query.getPageSize();
		
		//只获取带自定义前缀的
		url = url + "&filter[]=" + "name=" + "'*" + ConstantsUtils.tagPrefix.replace("_", "") + "*'";
		//创建标签类时会同步创建出一个和标签类同名的标签，过滤掉这些和标签类同名的标签
		url = url + "&filter[]=classification.parent_id!=nil";
		
		//根据 名称 模糊查询
		if (!StringUtils.isEmpty(query.getName())) {
			url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
		}
		//根据描述模糊查询
		if (!StringUtils.isEmpty(query.getDescription())) {
			url = url + "&filter[]=" + "classification.description=" + "'*" + query.getDescription() + "*'";
		}
		
		//根据 标签类名称 模糊查询
		if (!StringUtils.isEmpty(query.getCategoryName())) {
			url = url + "&filter[]=" + "name=" + "'/managed/" + query.getCategoryName() + "/*'";
		}
		
		int offset = page == 0 ? 0 : page * size;
		url = url + "&offset=" + offset + "&limit=" + size;
		//根据指定的排序列进行排序，指定列包括：name
		if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
			url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
		} else {
			url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
		}
		
		result = getTagsList(url, token);
		return result;
	}

	public PageDataResultDTO<List<TagDTO>> getTagsList(String url, String token) {
		PageDataResultDTO<List<TagDTO>> result = new PageDataResultDTO<>();
		List<TagDTO> resultResoures = new ArrayList<TagDTO>();
		int count = 0;
		try {
			ResponseEntity<MiqListReturnDTO<TagDTO>> tagsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<TagDTO>>(){}, token);
			if (401 == tagsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			
			if (200 == tagsRes.getStatusCodeValue()) {
				count = tagsRes.getBody().getSubquery_count();
				resultResoures = tagsRes.getBody().getResources();
				
				//处理一下tag的名称，1、简化名称，只显示输入的名称；2、过滤掉添加的前缀
				if (resultResoures != null) {
					for (TagDTO tag : resultResoures) {
						if (tag.getName().contains("/") && tag.getName().split("/").length >= 3) {
							tag.setFullName(tag.getName());
							String simName = tag.getName().substring(tag.getName().lastIndexOf("/") + 1);
							
							if (simName.startsWith(ConstantsUtils.tagPrefix)) {
								tag.setName(simName.replace(ConstantsUtils.tagPrefix, ""));
								tag.setBusinessTag(false);
							}
							if (simName.startsWith(ConstantsUtils.businessTagPrefix)) {
								tag.setName(simName.replace(ConstantsUtils.businessTagPrefix, ""));
								tag.setBusinessTag(true);
							}
						}
						if(tag.getCategory() != null && tag.getCategory().getName() != null) {
							String categoryName = tag.getCategory().getName();
							tag.getCategory().setFullName(categoryName);
							if (categoryName.startsWith(ConstantsUtils.tagPrefix)) {
								tag.getCategory().setName(categoryName.replace(ConstantsUtils.tagPrefix, ""));
								tag.getCategory().setBusinessTag(false);
							}
							if (categoryName.startsWith(ConstantsUtils.businessTagPrefix)) {
								tag.getCategory().setName(categoryName.replace(ConstantsUtils.businessTagPrefix, ""));
								tag.getCategory().setBusinessTag(true);
							}
						}
					}
				}
				result.setSuccess(resultResoures, (long) count);
 		    } else {
				result.setError("获取信息失败！");
        	}
			
			return result;
			
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public DataResultDTO<List<TagDTO>> tagsList(String token) {
		DataResultDTO<List<TagDTO>> result = new DataResultDTO<>();
		
		String attributes = "category.name,category.id";
		String url = "/api/tags" + "?expand=resources" + "&attributes=" + attributes;
		
		//只获取带自定义前缀的
		url = url + "&filter[]=" + "name=" + "'*" + ConstantsUtils.tagPrefix.replace("_", "") + "*'";
		//创建标签类时会同步创建出一个和标签类同名的标签，过滤掉这些和标签类同名的标签
		url = url + "&filter[]=classification.parent_id!=nil";
		url = url + "&sort_by=id&sort_order=desc";
		
		PageDataResultDTO<List<TagDTO>> pageResult = getTagsList(url, token);
		if (401 == pageResult.getCode()) {
			result.setError(401, ConstantsUtils.tokenMessage);
			return result;
	    }
		if (200 == pageResult.getCode()) {
			result.setSuccess(pageResult.getResult().getItems());
		} else {
			result.setError("获取信息失败！");
    	}
		return result;
	}


	@Override
	public ResultDTO createUpdateTag(TagCreateDTO tagCreateDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
       //id不为空则为编辑，id为空则为创建
        if (!StringUtils.isEmpty(tagCreateDTO.getId())) {
        	params.put("action", "edit");
        	params.put("id", tagCreateDTO.getId());
        } else {
        	params.put("category", new ParamHrefDTO(tagCreateDTO.getCategoryHref()));   
        }
        //是业务分类的标签类
        if (tagCreateDTO.getBusinessTag() != null && tagCreateDTO.getBusinessTag()) {
        	params.put("name", ConstantsUtils.businessTagPrefix + tagCreateDTO.getName());
        } else {//普通的标签类
        	params.put("name", ConstantsUtils.tagPrefix + tagCreateDTO.getName());
        }
        params.put("description", tagCreateDTO.getDescription());
        
        String url = "/api/tags";
        try {
        	
        	ResponseEntity<String> tagRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == tagRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == tagRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("标签添加成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(tagRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public ResultDTO deleteTag(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/tags/" + id;
        try {
        	ResponseEntity<String> deleteRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == deleteRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == deleteRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("标签删除成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(deleteRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteTags(List<String> ids, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/tags";
        try {
        	
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除标签成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

}
