package com.massclouds.opencmp.service.impl.ansible;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.ansible.ProjectDTO;
import com.massclouds.opencmp.service.ansible.ProjectService;
import com.massclouds.opencmp.utils.AnsibleHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.AnsibleListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("ProjectService")
public class ProjectServiceImpl  implements ProjectService{
	
	@Override
	public PageDataResultDTO<List<ProjectDTO>> projectListOfPage(QueryDTO query) {
		PageDataResultDTO<List<ProjectDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage() + 1;
		int size = query.getPageSize();
		
		String url = "projects" + "?page=" + page + "&page_size=" + size + "&order_by=-created";
		if (!StringUtils.isEmpty(query.getCriteria())) {
			url = url + "&search=" + query.getCriteria();
		}
		try {
			ResponseEntity<AnsibleListReturnDTO<ProjectDTO>> projectRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<ProjectDTO>>(){});
			if (200 == projectRes.getStatusCodeValue()) {
				result.setSuccess(projectRes.getBody().getResults(), (long)projectRes.getBody().getCount());
			} else {
				result.setError(500, "获取项目失败！");
			}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ProjectDTO getDefaultProject() {
		ProjectDTO result = null;
		
		String url = "projects" + "?order_by=-created&search=default" ;
		try {
			ResponseEntity<AnsibleListReturnDTO<ProjectDTO>> projectRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<ProjectDTO>>(){});
			 if (200 == projectRes.getStatusCodeValue() && projectRes.getBody().getResults() != null) {
		        	List<ProjectDTO> defauleProject = projectRes.getBody().getResults().stream().filter(item -> "default".equals(item.getName().toLowerCase()) == true).collect(Collectors.toList());
		        	if (defauleProject != null && defauleProject.size() > 0) {
		        		result = defauleProject.get(0); 
		        	} 
		        }
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public DataResultDTO<List<String>> getPlaybooksOfDefault() {
		DataResultDTO<List<String>> result = new DataResultDTO<List<String>>();
		List<String> playbooks = new ArrayList<String>();
		try {
			ProjectDTO defaultProject =  getDefaultProject();
			if (defaultProject != null && defaultProject.getId() != null) {
				String url = "projects/" + defaultProject.getId() + "/playbooks/";
				ResponseEntity<List<String>> playbookRes = AnsibleHttpUtils.httpRequest(url, "GET", 
						new HashMap<String, Object>(), 
						new ParameterizedTypeReference<List<String>>(){});
				if (200 == playbookRes.getStatusCodeValue()) {
					playbooks = playbookRes.getBody();
				}
				
			}
			result.setSuccess(playbooks);
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResultDTO createUpdateProject(ProjectDTO projectDTO) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", projectDTO.getName());
        params.put("description", projectDTO.getDescription());
        params.put("organization", 1);   //使用默认域 
        params.put("scm_type", "");  //该值为空字符串，表示默认值Manual
        params.put("local_path", projectDTO.getLocal_path());  //脚本目录
        
        String url = "projects/"; //url中最后的/一定要有
        String method = "POST";
        //编辑清单
        if (!StringUtils.isEmpty(projectDTO.getId())) {
        	url = url + projectDTO.getId() + "/";
        	method = "PUT";
        }
        try {
        	ResponseEntity<String> projectRes = AnsibleHttpUtils.httpRequest(url, method, 
        			params, 
					new ParameterizedTypeReference<String>(){});
        	 int status = projectRes.getStatusCodeValue();
        	 if (201 == status || 200 == status) {
        		result.setSuccess();
        		result.setMessage("项目添加/编辑成功。");
        	} else {
				result.setError(projectRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

	@Override
	public ResultDTO deleteProject(String id) {
		ResultDTO result = new ResultDTO();
		
        String url = "projects/" + id + "/"; //url中最后的/一定要有
        try {
        	ResponseEntity<String> projectRes = AnsibleHttpUtils.httpRequest(url, "DELETE", 
        			new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
        	int status = projectRes.getStatusCodeValue();
       	     if (204 == status || 200 == status) {
        		result.setSuccess();
        		result.setMessage("项目删除成功。");
        	} else {
				result.setError(projectRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
}
 