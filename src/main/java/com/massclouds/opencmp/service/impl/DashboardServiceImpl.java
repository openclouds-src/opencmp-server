package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.common.redis.RedisUtil;
import com.massclouds.opencmp.dto.ClusterDTO;
import com.massclouds.opencmp.dto.DashboardEntityCountDTO;
import com.massclouds.opencmp.dto.DashboardEntityCountForSelfDTO;
import com.massclouds.opencmp.dto.DashboardPlatformCountDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.DashboardStatisticsOnlineDTO;
import com.massclouds.opencmp.dto.DashboardTerminalStatisticsByTypeDTO;
import com.massclouds.opencmp.dto.HostDTO;
import com.massclouds.opencmp.dto.InstanceResourceOfCloudPlatformDTO;
import com.massclouds.opencmp.dto.ProviderDTO;
import com.massclouds.opencmp.dto.ProviderRedisDTO;
import com.massclouds.opencmp.dto.ServiceCatalogsListDTO;
import com.massclouds.opencmp.dto.ServiceTemplatesDTO;
import com.massclouds.opencmp.dto.TenantsDTO;
import com.massclouds.opencmp.dto.UserDTO;
import com.massclouds.opencmp.dto.VMPTerminalsDTO;
import com.massclouds.opencmp.dto.VmDTO;
import com.massclouds.opencmp.dto.VmDetailsDTO;
import com.massclouds.opencmp.dto.Vms6SaveToRedisDTO;
import com.massclouds.opencmp.dto.ZoneDTO;
import com.massclouds.opencmp.dto.query.ServiceTemplateQueryDTO;
import com.massclouds.opencmp.dto.query.VmsQueryDTO;
import com.massclouds.opencmp.service.ClustersService;
import com.massclouds.opencmp.service.DashboardService;
import com.massclouds.opencmp.service.HostsService;
import com.massclouds.opencmp.service.ProvidersService;
import com.massclouds.opencmp.service.ServiceCatalogsService;
import com.massclouds.opencmp.service.ServiceTemplatesService;
import com.massclouds.opencmp.service.TemplatesService;
import com.massclouds.opencmp.service.TenantsService;
import com.massclouds.opencmp.service.UsersService;
import com.massclouds.opencmp.service.VmsService;
import com.massclouds.opencmp.service.ZonesService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.VmpHttpUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

@Service("DashboardService")
public class DashboardServiceImpl  implements DashboardService{
	@Autowired
	private ProvidersService providersService;
	
	@Autowired
	private ClustersService clustersService;
	
	@Autowired
	private ZonesService zonesService;
	
	@Autowired
	private HostsService hostsService;
	
	@Autowired
	private VmsService vmsService;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private TenantsService tenantsService;
	
	@Autowired
	private TemplatesService templatesService;
	
	@Autowired
	private ServiceCatalogsService serviceCatalogsService;
	
	@Autowired
	private ServiceTemplatesService serviceTemplatesService;
	
	@Autowired
    private RedisUtil redisUtil;
	
	@Override
	public DataResultDTO<DashboardPlatformCountDTO> platformCount(String token) { 
		DataResultDTO<DashboardPlatformCountDTO> result = new DataResultDTO<DashboardPlatformCountDTO>();
		DashboardPlatformCountDTO dashboardPlatformCountDTO = new DashboardPlatformCountDTO();
		int vmpCount = 0;//虚拟化平台数量
		int cloudCount = 0;//云平台数量
		int containerCount = 0;//容器平台数量
		int physicalCount = 0;//物理平台数量
		List<ProviderDTO> privateCloudsList = null; //私有云平台
		List<ProviderDTO> publicCloudsList = null;//公有云平台
		
		
		PageDataResultDTO<List<ProviderDTO>> pageResult = providersService.providersList(new QueryDTO(0, 999, null), token);
		if ( 401 == pageResult.getCode()) {
			result.setError(401, ConstantsUtils.tokenMessage);
			return result;
		}
		List<ProviderDTO> allProvider = pageResult.getResult() == null ? null : pageResult.getResult().getItems();
		if (allProvider != null && allProvider.size() > 0) {
			privateCloudsList = allProvider.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeInfraManager).contains(x.getType()) == true).collect(Collectors.toList());
			publicCloudsList = allProvider.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeCloudManager).contains(x.getType()) == true).collect(Collectors.toList());
			containerCount = allProvider.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypeContainerManager).contains(x.getType()) == true).collect(Collectors.toList()).size();
			physicalCount = allProvider.stream().filter(x -> Arrays.asList(ConstantsUtils.providerTypePhysicalInfraManager).contains(x.getType()) == true).collect(Collectors.toList()).size();
		}
		//1、统计不同类型的平台的总数
		vmpCount = privateCloudsList == null ? 0 : privateCloudsList.size();
		cloudCount = publicCloudsList == null ? 0 : publicCloudsList.size();
		
		
		dashboardPlatformCountDTO.setVmpCount(vmpCount);
		dashboardPlatformCountDTO.setCloudCount(cloudCount);
		dashboardPlatformCountDTO.setContainerCount(containerCount);
		dashboardPlatformCountDTO.setPhysicalCount(physicalCount);
		
		//2、统计各个平台的不同对象的总数
		Map<String, Integer> vmsCountPerPlatform = new HashMap<String, Integer>();//统计各个平台的虚机总数
		Map<String, Integer> vcpuCountPerPlatform = new HashMap<String, Integer>();//统计各个平台的vcpu总数
		Map<String, Double> memoryCountPerPlatform = new HashMap<String, Double>();//统计各个平台的内存总数
		Map<String, Double> storageCountPerPlatform = new HashMap<String, Double>();//统计各个平台的存储总数
		if (allProvider != null) {
			for (ProviderDTO item : allProvider) {
				vmsCountPerPlatform.put(item.getName(), item.getTotal_vms());
				vcpuCountPerPlatform.put(item.getName(), Integer.parseInt(item.getTotal_vcpus()));
				memoryCountPerPlatform.put(item.getName(), Double.parseDouble(item.getTotal_memory())); //云平台的该值是0，在第三步处理
				storageCountPerPlatform.put(item.getName(), Double.parseDouble(item.getTotal_storages_size()));//云平台的该值是0，在第三步处理
			}
		}
		dashboardPlatformCountDTO.setVmsCountPerPlatform(vmsCountPerPlatform);
		dashboardPlatformCountDTO.setVcpuCountPerPlatform(vcpuCountPerPlatform);
		dashboardPlatformCountDTO.setMemoryCountPerPlatform(memoryCountPerPlatform);
		dashboardPlatformCountDTO.setStorageCountPerPlatform(storageCountPerPlatform);
		
		//3、根据公有云、私有云分别统计数据
		int vmsCountOfPrivateClouds = 0;//统计公有云平台虚机总数
		int vmsCountOfPublicClouds = 0;//统计私有云虚机总数
		int vcpuCountOfPrivateClouds = 0;//统计公有云vcpu总数
		int vcpuCountOfPublicClouds = 0;//统计私有云vcpu总数
		double memoryCountOfPrivateClouds = 0;//统计公有云内存总数
		double memoryCountOfPublicClouds = 0;//统计私有云内存总数
		double storageCountOfPrivateClouds = 0;//统计公有云存储总数
		double storageCountOfPublicClouds = 0;//统计私有云存储总数
		if (privateCloudsList != null) {
			for (ProviderDTO item : privateCloudsList) {
				vmsCountOfPrivateClouds = vmsCountOfPrivateClouds + item.getTotal_vms();
				vcpuCountOfPrivateClouds = vcpuCountOfPrivateClouds + Integer.parseInt(item.getTotal_vcpus());
				memoryCountOfPrivateClouds = memoryCountOfPrivateClouds + Double.parseDouble(item.getTotal_memory());
				storageCountOfPrivateClouds = storageCountOfPrivateClouds + Double.parseDouble(item.getTotal_storages_size());
			}
		}
		if (publicCloudsList != null) {
			for (ProviderDTO item : publicCloudsList) {
				InstanceResourceOfCloudPlatformDTO instanceRes = vmsService.getAllInstancesCpusMemoryOfCloudPlatform(item.getId(), token);
				
				vcpuCountPerPlatform.put(item.getName(), instanceRes.getTotalCpus()); //保存每个公有云平台的CPU数量
				vcpuCountOfPublicClouds = vcpuCountOfPublicClouds + instanceRes.getTotalCpus();//累加所有公有云平台的cpu数量
				
				//处理内存，转换成GB
				double memoryMB = instanceRes.getTotalMemory();//MB
				double memoryGB = memoryMB / 1024;   //内存单位是GB
				memoryCountPerPlatform.put(item.getName(), Double.parseDouble(String.format("%.1f", memoryGB)));//保存每个公有云平台的内存数
				memoryCountOfPublicClouds = memoryCountOfPublicClouds + memoryMB;//累加所有公有云平台的内存总数，单位是MB
				
				//处理存储总大小，转换成GB
				double storagesKB = instanceRes.getTotalDisk();
				double storagesGB = storagesKB / 1024 / 1024 / 1024;
				storageCountPerPlatform.put(item.getName(), Double.parseDouble(String.format("%.1f", storagesGB)));//保存每个公有云平台的磁盘大小
				storageCountOfPublicClouds = storageCountOfPublicClouds + storagesKB;//累加所有公有云平台的磁盘大小总数
				
				vmsCountOfPublicClouds = vmsCountOfPublicClouds + item.getTotal_vms();
			}
		}
		dashboardPlatformCountDTO.setVmsCountOfPrivateClouds(vmsCountOfPrivateClouds);
		dashboardPlatformCountDTO.setVmsCountOfPublicClouds(vmsCountOfPublicClouds);
		dashboardPlatformCountDTO.setVcpuCountOfPrivateClouds(vcpuCountOfPrivateClouds);
		dashboardPlatformCountDTO.setVcpuCountOfPublicClouds(vcpuCountOfPublicClouds);
		dashboardPlatformCountDTO.setMemoryCountOfPrivateClouds(memoryCountOfPrivateClouds);
		dashboardPlatformCountDTO.setMemoryCountOfPublicClouds(Double.parseDouble(String.format("%.1f", (memoryCountOfPublicClouds / 1024)))); //转成GB
		dashboardPlatformCountDTO.setStorageCountOfPrivateClouds(storageCountOfPrivateClouds);
		dashboardPlatformCountDTO.setStorageCountOfPublicClouds(Double.parseDouble(String.format("%.1f", (storageCountOfPublicClouds / 1024 / 1024 / 1024))));//转成GB
		
		result.setSuccess(dashboardPlatformCountDTO);
		
		return result;
	}

	@Override
	public DataResultDTO<DashboardEntityCountDTO> entityCount(String token) {
		DataResultDTO<DashboardEntityCountDTO> result = new DataResultDTO<DashboardEntityCountDTO>();
		int zoneCount = 0; //域数量
		int clusterCount = 0;//集群数量
		int hostCount = 0;//物理主机数量
		int vmServerCount = 0;//云主机数量
		int vmDesktopCount = 0;//云桌面数量
		int tenantCount = 0;//租户数量
		int userCount = 0;//用户数量
		int terminalCount = 0;//终端数量
		int templateVmCount = 0;//虚机模板数量
		int serviceCatalogCount = 0;//服务目录数量
		
		int containerCount = 0;//容器数量
		int containerImageCount = 0;//容器镜像数量
		
		DataResultDTO<List<ZoneDTO>> zoneRes = zonesService.zonesList(token);
		if ( 401 == zoneRes.getCode()) {
			result.setError(401, ConstantsUtils.tokenMessage);
			return result;
		}
		zoneCount = zoneRes.getResult() == null ? 0 : zoneRes.getResult().size();
		
		List<ClusterDTO> clustersList = clustersService.clustersList(token).getResult();
		clusterCount = clustersList == null ? 0 : clustersList.size();
		
		PageDataResultDTO<List<HostDTO>> hostsRes = hostsService.hostsList(new QueryDTO(0, 9999, ""), token);
		hostCount = hostsRes.getResult().getItems() == null ? 0 : hostsRes.getResult().getItems().size();
		
		List<VmDTO> vmServerList = vmsService.vmsAllByType("server", token);
		vmServerCount = vmServerList == null ? 0 : vmServerList.size();
		
		List<VmDTO> vmDesktopList = vmsService.vmsAllByType("desktop", token);
		vmDesktopCount = vmDesktopList == null ? 0 : vmDesktopList.size();
		
		List<UserDTO> userList = usersService.usersAllList(token);
		userCount = userList == null ? 0 : userList.size();
		
		List<TenantsDTO> tenantList = tenantsService.tenantsList(token).getResult();
		tenantCount = tenantList == null ? 0 : tenantList.size();
		
		List<VmDetailsDTO> templateList = templatesService.templatesList(token).getResult();
		templateVmCount = templateList == null ? 0 : templateList.size();
		
		List<ServiceCatalogsListDTO> serviceCatalogList = serviceCatalogsService.serviceCatalogsList(token).getResult();
		serviceCatalogCount = serviceCatalogList == null ? 0 : serviceCatalogList.size();
		
		PageDataResultDTO<List<ProviderDTO>> containerResult = providersService.providersList(new QueryDTO(0, 999, "ContainerManager"), token); //后去所有容器平台
		if (200 == containerResult.getCode() && containerResult.getResult().getItems() != null) {
			for (ProviderDTO container : containerResult.getResult().getItems()) {
				containerCount = containerCount + container.getContainerCount();
				containerImageCount = containerImageCount + container.getContainerImagesCount();
			}
		}
		
		DashboardEntityCountDTO dashboardEntityCountDTO = new DashboardEntityCountDTO();
		dashboardEntityCountDTO.setZoneCount(zoneCount);
		dashboardEntityCountDTO.setHostCount(hostCount);
		dashboardEntityCountDTO.setClusterCount(clusterCount);
		dashboardEntityCountDTO.setVmDesktopCount(vmDesktopCount);
		dashboardEntityCountDTO.setVmServerCount(vmServerCount);
		dashboardEntityCountDTO.setVmCount(vmServerCount + vmDesktopCount);
		dashboardEntityCountDTO.setUserCount(userCount);
		dashboardEntityCountDTO.setTenantCount(tenantCount);
		dashboardEntityCountDTO.setContainerCount(containerCount);
		dashboardEntityCountDTO.setTerminalCount(terminalCount);
		dashboardEntityCountDTO.setServiceCatalogCount(serviceCatalogCount);
		dashboardEntityCountDTO.setTemplateAllCount(templateVmCount + containerImageCount);
		dashboardEntityCountDTO.setTemplateContainterCount(containerImageCount);
		dashboardEntityCountDTO.setTemplateVmCount(templateVmCount);
		
		
		result.setSuccess(dashboardEntityCountDTO);
		
		return result;
	}
	
	@Override
	public DataResultDTO<DashboardEntityCountForSelfDTO> entityCountForSelf(String token) {
		int vmServerCount = 0;//云主机数量
		int vmDesktopCount = 0;//云桌面数量
		
		List<VmDTO> vmServerList = vmsService.vmsAllByType("server", token);
		vmServerCount = vmServerList == null ? 0 : vmServerList.size();
		
		List<VmDTO> vmDesktopList = vmsService.vmsAllByType("desktop", token);
		vmDesktopCount = vmDesktopList == null ? 0 : vmDesktopList.size();
		
		List<ServiceTemplatesDTO> serviceTemplatesList = null;
		PageDataResultDTO<List<ServiceTemplatesDTO>> sTemplatePage = serviceTemplatesService.serviceTemplatesList(new ServiceTemplateQueryDTO(0, 9999, ""), token);
		if (200 == sTemplatePage.getCode() && sTemplatePage.getResult().getItems() != null) {
			serviceTemplatesList = sTemplatePage.getResult().getItems();
		} else {
			serviceTemplatesList = new ArrayList<ServiceTemplatesDTO>();
		}
		
		DashboardEntityCountForSelfDTO data = new DashboardEntityCountForSelfDTO();
		data.setVmDesktopCount(vmDesktopCount);
		data.setVmServerCount(vmServerCount);
		data.setVmCount(vmDesktopCount + vmServerCount);
		data.setServiceTemplatesCount(serviceTemplatesList.size());
		
		DataResultDTO<DashboardEntityCountForSelfDTO> result = new DataResultDTO<DashboardEntityCountForSelfDTO>();
		result.setSuccess(data);
		return result;
	}

	@Override
	public DataResultDTO<DashboardStatisticsByTypeDTO> statisticsByType(String token) {
		DataResultDTO<DashboardStatisticsByTypeDTO> result = new DataResultDTO<DashboardStatisticsByTypeDTO>();
		Map<String, Integer> vmCountPerOS = new HashMap<String, Integer>();
		//1、根据虚机操作系统类型统计虚机的数量
		//1.1、 获取所有虚机，因为虚机数量会很大，所以分类获取
		List<VmDTO> vmServerList = vmsService.vmsAllByType("server", token);
		List<VmDTO> vmDesktopList = vmsService.vmsAllByType("desktop", token);
		//过滤掉操作系统为空的虚机
		vmServerList = vmServerList == null ? new ArrayList<VmDTO>() : vmServerList;
		vmDesktopList = vmDesktopList == null ? new ArrayList<VmDTO>() : vmDesktopList;
		//1.2、根据操作系统类型去重，获取所有虚机操作系统类型
		List<VmDTO> vmServerListTemp = vmServerList.stream().filter(item -> (item.getOperating_system() != null && item.getOperating_system().getProduct_name() != null)).collect(Collectors.toList());
		List<VmDTO> vmDesktopListTemp = vmDesktopList.stream().filter(item -> (item.getOperating_system() != null && item.getOperating_system().getProduct_name() != null)).collect(Collectors.toList());
		List<VmDTO> vmServerOSType = vmServerListTemp.stream().collect(
		  Collectors.collectingAndThen(
              Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(item -> item.getOperating_system().getProduct_name()))),ArrayList::new
          )
		);
		List<VmDTO> vmDesktopOSType = vmDesktopListTemp.stream().collect(
		  Collectors.collectingAndThen(
              Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(item -> item.getOperating_system().getProduct_name()))),ArrayList::new
          )
		);
		
		vmServerOSType.forEach(item -> {
			vmCountPerOS.put(item.getOperating_system().getProduct_name(), 0);
		});
		vmDesktopOSType.forEach(item -> {
			vmCountPerOS.put(item.getOperating_system().getProduct_name(), 0);
		});
		
		//1.3、获取不同操作系统类型的虚机总数
		for(String osType : vmCountPerOS.keySet()) {
			int n = vmServerListTemp.stream().filter(x -> x.getOperating_system().getProduct_name().equals(osType) == true).collect(Collectors.toList()).size();
			int m = vmDesktopListTemp.stream().filter(x -> x.getOperating_system().getProduct_name().equals(osType) == true).collect(Collectors.toList()).size();
			vmCountPerOS.put(osType, vmCountPerOS.get(osType) + n + m);
		}
		
		//2、服务器架构统计
		Map<String, Integer> cpuArch = new HashMap<String, Integer>();
		//2.1、获取所有集群
		DataResultDTO<List<ClusterDTO>> clusterRes= clustersService.clustersList(token);
		if ( 401 == clusterRes.getCode()) {
			result.setError(401, ConstantsUtils.tokenMessage);
			return result;
		}
		List<ClusterDTO> clustersList = clusterRes.getResult();
		
		if (clustersList != null) {
			//2.2、过滤集群，获取集群CPU架构的类型,CPU架构为null，就用cpu类型
			for (ClusterDTO cluster : clustersList) {
				if (cluster.getCpu_arch() != null || cluster.getCpu_type() != null) {
					cpuArch.put(cluster.getCpu_arch() == null ? cluster.getCpu_type() : cluster.getCpu_arch(), 0);
				}
			}
			//2.3、计算不同CPU架构的集群的数量
			for(String arch : cpuArch.keySet()) {
				int n = clustersList.stream().filter(x -> (x.getCpu_arch() != null && x.getCpu_arch().equals(arch) == true)).collect(Collectors.toList()).size();
				int m = clustersList.stream().filter(x -> (x.getCpu_arch() == null && x.getCpu_type() != null && x.getCpu_type().equals(arch) == true)).collect(Collectors.toList()).size();
				cpuArch.put(arch, cpuArch.get(arch) + n + m);
			}
		}
		
		DashboardStatisticsByTypeDTO dashboardStatisticsByTypeDTO = new DashboardStatisticsByTypeDTO();
		dashboardStatisticsByTypeDTO.setVmCountPerOS(vmCountPerOS);
		dashboardStatisticsByTypeDTO.setCpuArch(cpuArch);
		result.setSuccess(dashboardStatisticsByTypeDTO);
		return result;
	}

	@Override
	public DataResultDTO<DashboardStatisticsOnlineDTO> statisticsOnline(String token) {
		DataResultDTO<DashboardStatisticsOnlineDTO> result = new DataResultDTO<DashboardStatisticsOnlineDTO>();
		List<VmDTO> vmServerList = vmsService.vmsOnLine(token);
		
		DashboardStatisticsOnlineDTO data= new DashboardStatisticsOnlineDTO();
		data.setVmCountOnline(vmServerList == null ? 0 : vmServerList.size());
		data.setUserCountOnline(0);
		
		result.setSuccess(data);
		return result;
	}
	
	@Override
	public DataResultDTO<List<Vms6SaveToRedisDTO>> list6Vms(String vmType, String token) {
		DataResultDTO<List<Vms6SaveToRedisDTO>> result = new DataResultDTO<List<Vms6SaveToRedisDTO>>();
		List<Vms6SaveToRedisDTO> vms6ToRedis = new ArrayList<Vms6SaveToRedisDTO>();
		
		VmsQueryDTO query = new VmsQueryDTO();
		query.setPage(0);
		query.setPageSize(6);
		query.setCriteria(vmType);
		PageDataResultDTO<List<VmDetailsDTO>> vmsDetails = vmsService.vmsList(query, token);
		if (200 == vmsDetails.getCode() && vmsDetails.getResult().getItems() != null) {
			for (VmDetailsDTO vm : vmsDetails.getResult().getItems()) {
				Vms6SaveToRedisDTO vm6 = new Vms6SaveToRedisDTO();
				vm6.setId(vm.getId());
				vm6.setName(vm.getName());
				vm6.setDescription(vm.getDescription());
				vm6.setCreated_on(vm.getCreated_on());
				vm6.setOperating_system(vm.getOperating_system());
				vms6ToRedis.add(vm6);
			}
		}
		result.setSuccess(vms6ToRedis);
		return result;
	}
	
	@Override
	public DataResultDTO<DashboardTerminalStatisticsByTypeDTO> statisticsTerminalsByType(String token) {
		DataResultDTO<DashboardTerminalStatisticsByTypeDTO> result = new DataResultDTO<DashboardTerminalStatisticsByTypeDTO>();
		
		Map<String, Integer> terminalTypeMap = new HashMap<String, Integer>();
		
		//1、获取已接管的VMP平台中的所有终端信息
		List<VMPTerminalsDTO> allTerminalsList = getVMPTerminalsInfo(token);
		
		if (allTerminalsList != null && allTerminalsList.size() > 0) {
			//2、过滤出终端的不同类型
			List<VMPTerminalsDTO> terminalType = allTerminalsList.stream().collect(
			  Collectors.collectingAndThen(
	              Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(item -> item.getTerminal_type()))),ArrayList::new
	          )
			);
		   
			terminalType.forEach(item -> {
				terminalTypeMap.put(item.getTerminal_type(), 0);
			});
					
			//3、获取不同操作系统类型的虚机总数
			for(String tType : terminalTypeMap.keySet()) {
				int n = allTerminalsList.stream().filter(x -> x.getTerminal_type().equals(tType) == true).collect(Collectors.toList()).size();
				terminalTypeMap.put(tType, terminalTypeMap.get(tType) + n);
			}
		}
		
		
		DashboardTerminalStatisticsByTypeDTO terminalStatisticsDTO = new DashboardTerminalStatisticsByTypeDTO();
		terminalStatisticsDTO.setTerminalCountPerType(terminalTypeMap);
		terminalStatisticsDTO.setTotalTerminal(allTerminalsList == null ? 0 : allTerminalsList.size());
		
		result.setSuccess(terminalStatisticsDTO);
		return result;
	}

	public List<VMPTerminalsDTO> getVMPTerminalsInfo(String token) {
		List<VMPTerminalsDTO> allTerminalsOfVMPList = new ArrayList<VMPTerminalsDTO>();
		
        try {
        	//1、获取已接管的所有MCOS cServer的ip、用户名、密码
    		List<ProviderRedisDTO> redisList = providersService.getInfraManagerFromRedis();
    		if (redisList != null && redisList.size() > 0 ) {
    			List<ProviderRedisDTO> allVmpList = redisList.stream().filter(item -> item.getIsVMP() == true).collect(Collectors.toList());
    			if (allVmpList != null && allVmpList.size() > 0) {
    				//1.1 去重，radis里保存的信息容易有重复的
    				List<ProviderRedisDTO> vmpList = allVmpList.stream().collect(
					  Collectors.collectingAndThen(
			              Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(item -> item.getIpaddress()))),ArrayList::new
			          )
					);
    				//1.2 过滤VMP平台，过滤出状态是Valid的，即还可连接上的
    				List<ProviderRedisDTO> vmpValidList = new ArrayList<ProviderRedisDTO>();
    				PageDataResultDTO<List<ProviderDTO>> pageProviderResult = providersService.providersList(new QueryDTO(0, 999, "InfraManager"), token);
    				if (pageProviderResult.getCode() == 200 && pageProviderResult.getResult() != null && pageProviderResult.getResult().getItems() != null) {
    					for(ProviderDTO provider : pageProviderResult.getResult().getItems()) {
    						for (ProviderRedisDTO providerRedis : vmpList) {
    							if (providerRedis.getId().equals(provider.getId()) && "Valid".equals(provider.getAuthentication_status())) {
    								vmpValidList.add(providerRedis);
    							}
    						}
    					}
    				}
    				
    				//2、遍历连接VMP环境，获取终端信息
    				for(ProviderRedisDTO vmp : vmpValidList) {
    					String vmpIp = vmp.getIpaddress().trim();
    					String syncUserName = vmp.getSyncUserName();
    					String syncPassword = vmp.getSyncPassword();
    					
    					//2.1、获取token
    			        HashMap<String, Object> params = new HashMap<>();
    			        params.put("username", syncUserName);
    			        params.put("password", syncPassword);
    			        params.put("grant_type", "password");
    			        params.put("scope", "ovirt-app-api");
    			        String url = "https://" + vmpIp + "/sso/oauth/token";
    			        ResponseEntity<String> tokenRes = VmpHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, "");
    			        ObjectMapper objectMapper = new ObjectMapper();
    					JsonNode jsonNode = objectMapper.readTree(tokenRes.getBody());
    					if (200 == tokenRes.getStatusCodeValue()) {
    						String vmpToken = jsonNode.get("access_token").asText();
    						
    						//2.3、获取终端信息
    						List<VMPTerminalsDTO> terminalList = null;
    						String terminalUrl = "https://" + vmpIp + "/api/terminals";
    						ResponseEntity<List<VMPTerminalsDTO>> terminalRes = VmpHttpUtils.httpRequest(terminalUrl, "GET", new HashMap<>(), 
    								new ParameterizedTypeReference<List<VMPTerminalsDTO>>(){}, vmpToken);
    						
    						if (200 == terminalRes.getStatusCodeValue()) {
    							//2.4、累加所有终端信息
    							terminalList = terminalRes.getBody();
    							if (terminalList != null) {
    								allTerminalsOfVMPList.addAll(terminalList);
    							}
    						}
    					} 
    				}
    			} else {
    				return null;
    			}
    		} else {
    			return null;
    		}
        }catch (Exception e) {
        	e.printStackTrace();
        	return null;
        }
		
		return allTerminalsOfVMPList;
	}

	@Override
	public DataResultDTO<DashboardPlatformCountDTO> platformCountFromRedis(String token) {
		DataResultDTO<DashboardPlatformCountDTO> result = new DataResultDTO<DashboardPlatformCountDTO>();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			DashboardPlatformCountDTO platformCount =  objectMapper.readValue(redisUtil.get("platformCount").toString(), DashboardPlatformCountDTO.class);
			result.setSuccess(platformCount);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public DataResultDTO<DashboardEntityCountDTO> entityCountFromRedis(String token) {
		DataResultDTO<DashboardEntityCountDTO> result = new DataResultDTO<DashboardEntityCountDTO>();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			DashboardEntityCountDTO entityCount =  objectMapper.readValue(redisUtil.get("entityCount").toString(), DashboardEntityCountDTO.class);
			result.setSuccess(entityCount);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public DataResultDTO<DashboardStatisticsByTypeDTO> statisticsByTypeFromRedis(String token) {
		DataResultDTO<DashboardStatisticsByTypeDTO> result = new DataResultDTO<DashboardStatisticsByTypeDTO>();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			DashboardStatisticsByTypeDTO statisticsByType =  objectMapper.readValue(redisUtil.get("statisticsByType").toString(), DashboardStatisticsByTypeDTO.class);
			result.setSuccess(statisticsByType);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public DataResultDTO<DashboardStatisticsOnlineDTO> statisticsOnlineFromRedis(String token) {
		DataResultDTO<DashboardStatisticsOnlineDTO> result = new DataResultDTO<DashboardStatisticsOnlineDTO>();
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			DashboardStatisticsOnlineDTO statisticsOnLine =  objectMapper.readValue(redisUtil.get("statisticsOnline").toString(), DashboardStatisticsOnlineDTO.class);
			result.setSuccess(statisticsOnLine);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public DataResultDTO<List<Vms6SaveToRedisDTO>> list6VmsFromRedis(String vmType, String token) {
		DataResultDTO<List<Vms6SaveToRedisDTO>> result = new DataResultDTO<>();
		String redisKey = "";
		if ("server".equals(vmType)) {
			redisKey = "vms6OfServer";
		} else {
			redisKey = "vms6OfDesktop";
		}
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			List<Vms6SaveToRedisDTO> vms6List = objectMapper.readValue(redisUtil.get(redisKey).toString(), new TypeReference<List<Vms6SaveToRedisDTO>>(){} );
			result.setSuccess(vms6List);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}
	
	@Override
	public DataResultDTO<DashboardTerminalStatisticsByTypeDTO> statisticsTerminalsByTypeFromRedis(String token) {
		DataResultDTO<DashboardTerminalStatisticsByTypeDTO> result = new DataResultDTO<DashboardTerminalStatisticsByTypeDTO>();
		DashboardTerminalStatisticsByTypeDTO terminalType = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			if(redisUtil.get("terminalStatisticsByType") != null) {
				terminalType =  objectMapper.readValue(redisUtil.get("terminalStatisticsByType").toString(), DashboardTerminalStatisticsByTypeDTO.class);
			}
			result.setSuccess(terminalType);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}
	
}
