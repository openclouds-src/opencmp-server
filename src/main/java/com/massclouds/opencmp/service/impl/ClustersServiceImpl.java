package com.massclouds.opencmp.service.impl;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.ClusterDTO;
import com.massclouds.opencmp.dto.ClusterDetailDTO;
import com.massclouds.opencmp.service.ClustersService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;

@Service("ClustersService")
public class ClustersServiceImpl  implements ClustersService{

	@Override
	public PageDataResultDTO<List<ClusterDTO>> clustersListOfPage(int page, int size, String token) {
		PageDataResultDTO<List<ClusterDTO>> result = new PageDataResultDTO<>();
   //	String  attributes = "id,name,ems_id,created_on,updated_on,uid_ems,total_hosts,total_vms,total_miq_templates,ext_management_system.name";
	    String  attributes = "ext_management_system";
		String url = "/api/clusters/" + "?expand=resources" + "&attributes=" + attributes;
		try {
			
			ResponseEntity<MiqListReturnDTO<ClusterDTO>> clusterRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ClusterDTO>>(){}, token);
			
			if (401 == clusterRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == clusterRes.getStatusCodeValue()) {
				List<ClusterDTO> allResources = clusterRes.getBody().getResources();
				
				int count = allResources.size();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				result.setSuccess(allResources.subList(pageBegin, pageEnd), (long) count);
				
			} else {
				result.setError("获取信息失败！");
        	}
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<List<ClusterDTO>> clustersList(String token) {
		DataResultDTO<List<ClusterDTO>> result = new DataResultDTO<>();
		String  attributes = "ext_management_system";
		
		String url = "/api/clusters/" + "?expand=resources" + "&attributes=" + attributes;
		try {
			
			ResponseEntity<MiqListReturnDTO<ClusterDTO>> clusterRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ClusterDTO>>(){}, token);
			if (401 == clusterRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == clusterRes.getStatusCodeValue()) {
				result.setSuccess(clusterRes.getBody().getResources());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public DataResultDTO<ClusterDetailDTO> getDetailById(String id, String token) {
		DataResultDTO<ClusterDetailDTO> result = new DataResultDTO<>();
		
		String  attributes = "ext_management_system,lans,hosts,storages";
		String url = "/api/clusters/"  + id + "?expand=resources" + "&attributes=" + attributes;
		try {
			
			ResponseEntity<ClusterDetailDTO> clusterRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<ClusterDetailDTO>(){}, token);
			if (401 == clusterRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == clusterRes.getStatusCodeValue()) {
				result.setSuccess(clusterRes.getBody());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
}
