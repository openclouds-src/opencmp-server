package com.massclouds.opencmp.service.impl;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.common.redis.RedisUtil;
import com.massclouds.opencmp.dto.LoginUserDTO;
import com.massclouds.opencmp.dto.ManageIQTokenDTO;
import com.massclouds.opencmp.service.LoginService;
import com.massclouds.opencmp.utils.DateFormatUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;

@Service("LoginService")
public class LoginServiceImpl  implements LoginService{
	
	private static Logger logger = LoggerFactory.getLogger(LoginService.class);
	
	  @Autowired
	  private RedisUtil redisUtil;

	@Override
	public DataResultDTO<LoginUserDTO> login(LoginUserDTO loginUserDTO) {
		DataResultDTO<LoginUserDTO> result = new DataResultDTO<LoginUserDTO>();
		
    	String manageIQUrl = "";
    	
    	try {
//    		 List<String> lines = Files.readAllLines(Paths.get("D:\\OpenCMP\\OpenCMP_config.conf"), StandardCharsets.UTF_8);
        	 List<String> lines = Files.readAllLines(Paths.get("/etc/opencmp/opencmp.conf"), StandardCharsets.UTF_8);
        	  for(String line : lines){
        		  if (!StringUtils.isEmpty(line) && line.split("=").length == 2) {
        			  redisUtil.set(line.split("=")[0].trim(), line.split("=")[1].trim());
        		  }
        		  
        	   if (line.startsWith("manageIQ.ip")) {
        		   manageIQUrl = line.replace("manageIQ.ip=", "").trim();
        	   }
        	  }
        	  
        	  String loginUrl = manageIQUrl + "/api/auth?requester_type=ui";
        	  
        	  HashMap<String, Object> parms = new HashMap<String, Object>();
        	  parms.put("manageIQ.userName", loginUserDTO.getName());
        	  parms.put("manageIQ.password", loginUserDTO.getPassword());
        	  
        	  ResponseEntity<ManageIQTokenDTO> tokenRes = ManageIQHttpUtils.httpRequest(loginUrl, "GET", parms, new ParameterizedTypeReference<ManageIQTokenDTO>(){},"");
        	  
        	  if (tokenRes.getStatusCodeValue() == 200) {
            	  loginUserDTO.setAuth_token(tokenRes.getBody().getAuth_token());
            	  loginUserDTO.setExpires_on(DateFormatUtils.utcToBeijing(tokenRes.getBody().getExpires_on()));
            	  
            	  result.setSuccess(loginUserDTO);
        	  } else {
        		  result.setError("登录失败，用户/密码不正确！");
        	  }
        	  return result;
    	} catch(Exception e) {
    		e.printStackTrace();
    		result.setError("登录失败，用户/密码不正确！");
    		return result;
    	}
    	
    	
    }

}
