package com.massclouds.opencmp.service.impl.zabbix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.zabbix.ZabbixHostCreateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixTemplateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixTriggerDTO;
import com.massclouds.opencmp.service.zabbix.TemplateService;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.TriggerZHContantsUtils;
import com.massclouds.opencmp.utils.ZabbixHttpUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;
import com.massclouds.opencmp.utils.resultdto.ZabbixDateReturnDTO;
import com.massclouds.opencmp.utils.resultdto.ZabbixListReturnDTO;

@Service("ZabbixTemplateService")
public class TemplateServiceImpl  implements TemplateService{
	
	@Override
	public PageDataResultDTO<List<ZabbixTemplateDTO>> templatesListOfPage(QueryDTO query) {
		PageDataResultDTO<List<ZabbixTemplateDTO>> result = new PageDataResultDTO<List<ZabbixTemplateDTO>>();
		List<ZabbixTemplateDTO> hostList = new ArrayList<ZabbixTemplateDTO>();
		
		String[] output = {"name", "templateid","host","description", "status"};
		String[] selectHosts = {"hostid", "host", "name", "status", "description"};
		String[] selectTriggers = {"triggerid", "status","priority", "expression", "description", "templateid","value","opdata"};
		
		Map<String, Object> tagParms = new HashMap<String, Object>();
		tagParms.put("tag", "cManager");
		tagParms.put("value", 1);
		tagParms.put("operator", "0");
		List<Map<String, Object>> tagsList = new ArrayList<Map<String, Object>>();
		tagsList.add(tagParms);
		
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("output", output);
		parms.put("selectHosts", selectHosts);
		parms.put("selectTriggers", selectTriggers);
		parms.put("tags", tagsList);
		parms.put("sortfield", "name");
		parms.put("sortorder", "ASC");
		
		//根据名字模糊查询
		if (!StringUtils.isEmpty(query.getCriteria())) {
			Map<String, Object> nameParms = new HashMap<String, Object>();
			nameParms.put("name", query.getCriteria().trim());
			parms.put("search", nameParms);
		}
		String apiMethod = "template.get";
		try {
			ResponseEntity<ZabbixListReturnDTO<ZabbixTemplateDTO>> templateRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, 
					 new ParameterizedTypeReference<ZabbixListReturnDTO<ZabbixTemplateDTO>>(){});
	   	  
	   	    if (templateRes.getStatusCodeValue() == 200) {
	   		    hostList = templateRes.getBody().getResult();
	   		    int count = hostList.size();
		   	    int page = query.getPage(), size = query.getPageSize();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				result.setSuccess(hostList.subList(pageBegin, pageEnd), (long) count);
	   	    } else {
				result.setError("获取信息失败！");
        	}
	   	   
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public DataResultDTO<List<ZabbixTemplateDTO>> templatesList() {
		DataResultDTO<List<ZabbixTemplateDTO>> result = new DataResultDTO<List<ZabbixTemplateDTO>>();
		List<ZabbixTemplateDTO> hostList = new ArrayList<ZabbixTemplateDTO>();
		
		String[] output = {"name", "templateid","host","description", "status"};
		
		Map<String, Object> tagParms = new HashMap<String, Object>();
		tagParms.put("tag", "opencmp");
		tagParms.put("value", 1);
		tagParms.put("operator", "0");
		List<Map<String, Object>> tagsList = new ArrayList<Map<String, Object>>();
		tagsList.add(tagParms);
		
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("output", output);
		parms.put("tags", tagsList);
		parms.put("sortfield", "name");
		parms.put("sortorder", "ASC");
		
		String apiMethod = "template.get";
		try {
			ResponseEntity<ZabbixListReturnDTO<ZabbixTemplateDTO>> templateRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, 
					 new ParameterizedTypeReference<ZabbixListReturnDTO<ZabbixTemplateDTO>>(){});
	   	  
	   	    if (templateRes.getStatusCodeValue() == 200) {
	   		   hostList = templateRes.getBody().getResult();
	   		   result.setSuccess(hostList);
	   	    } else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public PageDataResultDTO<List<ZabbixTriggerDTO>> templateTriggerListOfPage(QueryDTO query) {
		PageDataResultDTO<List<ZabbixTriggerDTO>> result = new PageDataResultDTO<List<ZabbixTriggerDTO>>();
		List<ZabbixTriggerDTO> triggerList = new ArrayList<ZabbixTriggerDTO>();
		
		String[] output = {"triggerid", "status", "expression", "description", "templateid","value","priority","comments","opdata","event_name"};
		
		List<String> templateIpsList = new ArrayList<String>();
		templateIpsList.add(query.getId());
		
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("output", output);
		parms.put("templateids", templateIpsList);
		parms.put("sortfield", "triggerid");
		parms.put("sortorder", "DESC");
		
		String apiMethod = "trigger.get";
		try {
			ResponseEntity<ZabbixListReturnDTO<ZabbixTriggerDTO>> triggerRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, 
					 new ParameterizedTypeReference<ZabbixListReturnDTO<ZabbixTriggerDTO>>(){});
	   	  
	   	    if (triggerRes.getStatusCodeValue() == 200) {
	   		    triggerList = triggerRes.getBody().getResult();

		   	    int count = triggerList.size();
		   	    int page = query.getPage(), size = query.getPageSize();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				List<ZabbixTriggerDTO> triggerListRes = triggerList.subList(pageBegin, pageEnd);
				//获取每个触发器的中文翻译
				if (triggerListRes != null) {
					for (ZabbixTriggerDTO trigger : triggerListRes) {
						trigger.setEvent_name_zh(TriggerZHContantsUtils.trigger_zh.get(trigger.getTriggerid()));
					}
				}
				
				result.setSuccess(triggerListRes, (long) count);
	   	    } else {
				result.setError("获取信息失败！");
        	}
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public ResultDTO updateStatusTrigger(ZabbixHostCreateDTO zabbixHostCreateDTO) {
		ResultDTO result = new ResultDTO();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("triggerid", zabbixHostCreateDTO.getId());
		if (zabbixHostCreateDTO.getEnabled()) {
			params.put("status", 0);//enabled
		} else {
			params.put("status", 1);//disabled
		}
		
		String apiMethod = "trigger.update";
		try {
			ResponseEntity<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>> triggerRes = ZabbixHttpUtils.httpRequest(apiMethod, params, 
					 new ParameterizedTypeReference<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>>(){});
	   	  
	   	    if (triggerRes.getStatusCodeValue() == 200 && triggerRes.getBody().getResult() != null) {
	   	    	Map<String, ArrayList<Object>> hostId = (Map<String, ArrayList<Object>>) triggerRes.getBody().getResult();
	   	    	if (hostId != null && hostId.get("triggerids") != null && hostId.get("triggerids").size() > 0) {
	   	    		result.setId(hostId.get("triggerids").get(0).toString());
	   	    	}
	   	    	result.setSuccess("触发器状态更新成功");
	   	    } else {
	   	    	Map<String, Object> error = triggerRes.getBody().getError();
	   	    	result.setError(error.get("data").toString());
	   	    }
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
		
	}
	
}
 