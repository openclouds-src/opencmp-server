package com.massclouds.opencmp.service.impl.zabbix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.zabbix.ZabbixHostCreateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixHostDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixHostGroupDTO;
import com.massclouds.opencmp.service.zabbix.HostService;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.ZabbixHttpUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;
import com.massclouds.opencmp.utils.resultdto.ZabbixDateReturnDTO;
import com.massclouds.opencmp.utils.resultdto.ZabbixListReturnDTO;

@Service("ZabbixHostService")
public class HostServiceImpl  implements HostService{

	@Override
	public PageDataResultDTO<List<ZabbixHostDTO>> hostsListOfPage(QueryDTO query) {
		PageDataResultDTO<List<ZabbixHostDTO>> result = new PageDataResultDTO<List<ZabbixHostDTO>>();
		List<ZabbixHostDTO> hostList = new ArrayList<ZabbixHostDTO>();
		
		Map<String, Object> parms = new HashMap<String, Object>();
		String[] output = {"hostid", "host", "name", "status", "description", "proxy_hostid"};
		String[] selectParentTemplates = {"templateid", "name"};
		String[] selectInterfaces = {"interfaceid", "ip", "port","available"};
		parms.put("output", output);
		parms.put("selectParentTemplates", selectParentTemplates);
		parms.put("selectInterfaces", selectInterfaces);
		parms.put("sortfield", "hostid");
		parms.put("sortorder", "DESC");
		
		//根据名字模糊查询
		if (!StringUtils.isEmpty(query.getCriteria())) {
			Map<String, Object> nameParms = new HashMap<String, Object>();
			nameParms.put("name", query.getCriteria().trim());
			parms.put("search", nameParms);
		}
		String apiMethod = "host.get";
		try {
			ResponseEntity<ZabbixListReturnDTO<ZabbixHostDTO>> hostRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, 
					 new ParameterizedTypeReference<ZabbixListReturnDTO<ZabbixHostDTO>>(){});
	   	  
	   	    if (hostRes.getStatusCodeValue() == 200) {
	   		    hostList = hostRes.getBody().getResult();
		   		int count = hostList.size();
		   	    int page = query.getPage(), size = query.getPageSize();
				int pageBegin = page == 0 ? 0 : page * size;
				int pageEnd = pageBegin + size;
				pageEnd = pageEnd > count ? count : pageEnd;
				
				result.setSuccess(hostList.subList(pageBegin, pageEnd), (long) count);
	   	    } else {
				result.setError("获取信息失败！");
        	}
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}

	@Override
	public ResultDTO createHost(ZabbixHostCreateDTO zabbixHostCreateDTO) {
		ResultDTO result = new ResultDTO();
		String defaultHostGroupId = null;
		DataResultDTO<ZabbixHostGroupDTO> defaultGroupRes = getDefaultHostGroup();
		if (defaultGroupRes.getResult() != null && defaultGroupRes.getResult().getGroupid() != null) {
			defaultHostGroupId = defaultGroupRes.getResult().getGroupid();
		} else {
			result.setError("未配置默认主机组(opencmp)！");
			return result;
		}
		
		Map<String, Object> interfacesParams = new HashMap<String, Object>();
		interfacesParams.put("ip", zabbixHostCreateDTO.getIp());
		interfacesParams.put("port", "10050");
		interfacesParams.put("type", 1);
		interfacesParams.put("main", 1);
		interfacesParams.put("useip", 1);
		interfacesParams.put("dns", "");
		List<Map<String, Object>> interfacesList = new ArrayList<Map<String, Object>>();
		interfacesList.add(interfacesParams);
		
		Map<String, Object> templateParams = new HashMap<String, Object>();
		templateParams.put("templateid", zabbixHostCreateDTO.getTemplateId());
		List<Map<String, Object>> templateList = new ArrayList<Map<String, Object>>();
		templateList.add(templateParams);
		
		Map<String, Object> groupParams = new HashMap<String, Object>();
		groupParams.put("groupid", defaultHostGroupId);
		List<Map<String, Object>> groupList = new ArrayList<Map<String, Object>>();
		groupList.add(groupParams);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("host", zabbixHostCreateDTO.getHost());
		params.put("interfaces", interfacesList);
		params.put("templates", templateList);
		params.put("groups", groupList);
		
		String apiMethod = "host.create";
		try {
			ResponseEntity<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>> hostRes = ZabbixHttpUtils.httpRequest(apiMethod, params, 
					 new ParameterizedTypeReference<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>>(){});
	   	  
	   	    if (hostRes.getStatusCodeValue() == 200 && hostRes.getBody().getResult() != null) {
	   	    	Map<String, ArrayList<Object>> hostId = (Map<String, ArrayList<Object>>) hostRes.getBody().getResult();
	   	    	if (hostId != null && hostId.get("hostids") != null && hostId.get("hostids").size() > 0) {
	   	    		result.setId(hostId.get("hostids").get(0).toString());
	   	    	}
	   	    	result.setSuccess("主机添加成功");
	   	    } else {
	   	    	Map<String, Object> error = hostRes.getBody().getError();
	   	    	result.setError(error.get("data").toString());
	   	    }
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
		
	}

	
	@Override
	public ResultDTO updateStatusHost(ZabbixHostCreateDTO zabbixHostCreateDTO) {
		ResultDTO result = new ResultDTO();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("hostid", zabbixHostCreateDTO.getId());
		if (zabbixHostCreateDTO.getEnabled()) {
			params.put("status", 0);//enabled
		} else {
			params.put("status", 1);//disabled
		}
		
		String apiMethod = "host.update";
		try {
			ResponseEntity<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>> hostRes = ZabbixHttpUtils.httpRequest(apiMethod, params, 
					 new ParameterizedTypeReference<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>>(){});
	   	  
	   	    if (hostRes.getStatusCodeValue() == 200 && hostRes.getBody().getResult() != null) {
	   	    	Map<String, ArrayList<Object>> hostId = (Map<String, ArrayList<Object>>) hostRes.getBody().getResult();
	   	    	if (hostId != null && hostId.get("hostids") != null && hostId.get("hostids").size() > 0) {
	   	    		result.setId(hostId.get("hostids").get(0).toString());
	   	    	}
	   	    	result.setSuccess("主机状态更新成功");
	   	    } else {
	   	    	Map<String, Object> error = hostRes.getBody().getError();
	   	    	result.setError(error.get("data").toString());
	   	    }
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
		
	}

	@Override
	public ResultDTO deleteHost(List<String> ids) {
		ResultDTO result = new ResultDTO();
		String apiMethod = "host.delete";
		try {
			ResponseEntity<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>> hostRes = ZabbixHttpUtils.httpRequest(apiMethod, ids, 
					 new ParameterizedTypeReference<ZabbixDateReturnDTO<Map<String, ArrayList<Object>>>>(){});
	   	  
	   	    if (hostRes.getStatusCodeValue() == 200) {
	   	    	result.setSuccess();
	   	    } else {
	   	    	result.setError();
	   	    }
	   	    
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
		
	}

	@Override
	public DataResultDTO<ZabbixHostGroupDTO> getDefaultHostGroup() {
		DataResultDTO<ZabbixHostGroupDTO> result = new DataResultDTO<ZabbixHostGroupDTO>();
		List<ZabbixHostGroupDTO> hostList = null;
		ZabbixHostGroupDTO defaultGroup = new ZabbixHostGroupDTO();
		
		Map<String, Object> searchParms = new HashMap<String, Object>();
		searchParms.put("name", "opencmp");
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("search", searchParms);
		
		String apiMethod = "hostgroup.get";
		try {
			ResponseEntity<ZabbixListReturnDTO<ZabbixHostGroupDTO>> hostGroupRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, 
					 new ParameterizedTypeReference<ZabbixListReturnDTO<ZabbixHostGroupDTO>>(){});
	   	  
	   	    if (hostGroupRes.getStatusCodeValue() == 200) {
	   		   hostList = hostGroupRes.getBody().getResult();
	   		   if (hostList != null && hostList.size() > 0) {
	   			defaultGroup = hostList.get(0);
	   		   }
	   	    }
			result.setSuccess(defaultGroup);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			result.setError();
			return result;
		}
	}
	
}
 