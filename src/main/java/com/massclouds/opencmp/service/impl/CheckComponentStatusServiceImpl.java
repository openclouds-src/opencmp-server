package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.common.redis.RedisUtil;
import com.massclouds.opencmp.dto.ComponentStatusDTO;
import com.massclouds.opencmp.dto.ZoneDTO;
import com.massclouds.opencmp.service.CheckComponentStatusService;
import com.massclouds.opencmp.utils.AnsibleHttpUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.ZabbixHttpUtils;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

@Service("CheckComponentStatusService")
public class CheckComponentStatusServiceImpl  implements CheckComponentStatusService{

	@Autowired
	private RedisUtil redisUtil;
	 
	@Override
	public PageDataResultDTO<List<ComponentStatusDTO>> componentList(QueryDTO query, String token) {
		PageDataResultDTO<List<ComponentStatusDTO>> result = new PageDataResultDTO<>();
		List<ComponentStatusDTO> allComponent = new ArrayList<ComponentStatusDTO>();
		
		//1、检测manageIQ的服务状态
		ComponentStatusDTO manageIQ = new ComponentStatusDTO();
		manageIQ.setComponentName("manager");
		manageIQ.setComponentAddress(redisUtil.get("manageIQ.ip").toString());
		manageIQ.setDescription("管理组件");
		try {
			
			String url = "/api/zones/" + "?expand=resources" + "&attributes=" + "&filter[]=description!=Maintenance Zone";
			ResponseEntity<MiqListReturnDTO<ZoneDTO>> zoneRes = ManageIQHttpUtils.httpGetRequest(url, 
					new ParameterizedTypeReference<MiqListReturnDTO<ZoneDTO>>(){}, redisUtil.get("manageIQ.token").toString());
			if (200 == zoneRes.getStatusCodeValue()) {
				manageIQ.setStatus("OK");
			} else {
				manageIQ.setStatus("ERROR");
			}
		}catch(Exception e) {
			e.printStackTrace();
			manageIQ.setStatus("ERROR");
		}
		allComponent.add(manageIQ);
		
		//2、检测ansible的服务状态
		ComponentStatusDTO ansible = new ComponentStatusDTO();
		ansible.setComponentName("automation");
		ansible.setComponentAddress(redisUtil.get("ansible.ip").toString());
		ansible.setDescription("自动化组件");
		try {
			ResponseEntity<String> ansibleeRes =AnsibleHttpUtils.httpRequest("check", "GET", new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
			if (200 == ansibleeRes.getStatusCodeValue()) {
				ansible.setStatus("OK");
			} else {
				ansible.setStatus("ERROR");
			}
		}catch(Exception e) {
			e.printStackTrace();
			ansible.setStatus("ERROR");
		}
		allComponent.add(ansible);
		
		//2、检测zabbix的服务状态
		ComponentStatusDTO zabbix = new ComponentStatusDTO();
		zabbix.setComponentName("monitor");
		zabbix.setDescription("监控组件");
		
    	String loginUrl = redisUtil.get("zabbix.ip").toString();
   	    String userName = redisUtil.get("zabbix.userName").toString();
   	    String password = redisUtil.get("zabbix.password").toString();
    	  
    	HashMap<String, Object> parms = new HashMap<String, Object>();
    	parms.put("user", userName);
    	parms.put("password", password);
    	String apiMethod = "user.login";
    	zabbix.setComponentAddress(loginUrl);
    	try {
    		ResponseEntity<Map<String, Object>> tokenRes = ZabbixHttpUtils.httpRequest(apiMethod, parms, new ParameterizedTypeReference<Map<String, Object>>(){});
        	  
        	if (200 == tokenRes.getStatusCodeValue()) {
        		zabbix.setStatus("OK");
			} else {
				zabbix.setStatus("ERROR");
			}
    	}catch(Exception e) {
  			e.printStackTrace();
  			zabbix.setStatus("ERROR");
  		}
    	allComponent.add(zabbix);
    	
		int page = query.getPage();
		int size = query.getPageSize();
		int count = allComponent.size();
		int pageBegin = page == 0 ? 0 : page * size;
		int pageEnd = pageBegin + size;
		pageEnd = pageEnd > count ? count : pageEnd;
		
		result.setSuccess(allComponent.subList(pageBegin, pageEnd), (long) count);
		return result;
	}

}
