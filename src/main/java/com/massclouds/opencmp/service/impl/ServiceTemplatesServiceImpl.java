package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.common.utils.JsonUtils;
import com.massclouds.opencmp.dto.PictureAddDTO;
import com.massclouds.opencmp.dto.ServiceDialogsDTO;
import com.massclouds.opencmp.dto.ServiceTemplateCreateConfigInfoDTO;
import com.massclouds.opencmp.dto.ServiceTemplateCreateProvisionDTO;
import com.massclouds.opencmp.dto.ServiceTemplateCreateReceiveDTO;
import com.massclouds.opencmp.dto.ServiceTemplatesDTO;
import com.massclouds.opencmp.dto.ServiceTemplatesDetailsDTO;
import com.massclouds.opencmp.dto.TagDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.dto.global.ParamsTagDTO;
import com.massclouds.opencmp.dto.query.ServiceTemplateQueryDTO;
import com.massclouds.opencmp.service.PicturesService;
import com.massclouds.opencmp.service.ServiceCatalogsService;
import com.massclouds.opencmp.service.ServiceTemplatesService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("ServiceTemplatesService")
public class ServiceTemplatesServiceImpl  implements ServiceTemplatesService{

	private static Logger logger = LoggerFactory.getLogger(ServiceTemplatesServiceImpl.class);

	@Autowired
	public PicturesService picturesService;
	
	@Autowired
	public ServiceCatalogsService serviceCatalogsService;
	
	@Override
	public PageDataResultDTO<List<ServiceTemplatesDTO>> serviceTemplatesList(ServiceTemplateQueryDTO query, String token) {
		    PageDataResultDTO<List<ServiceTemplatesDTO>> result = new PageDataResultDTO<>();
		    String  attributes = "picture,picture.image_href,zone";
			String url = "/api/service_templates" + "?expand=resources" + "&attributes=" + attributes;
			int page = query.getPage();
			int size = query.getPageSize();
			int count = 0;
			
			try {
				//添加一个空的查询，为了保证接口返回数据的数量字段subquery_count有正确的值
				url = url + "&filter[]=";
				//根据名称模糊查询
				if (!StringUtils.isEmpty(query.getName())) {
					url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
				}
				//根据描述模糊查询
				if (!StringUtils.isEmpty(query.getDescription())) {
					url = url + "&filter[]=" + "description=" + "'*" + query.getDescription() + "*'";
				}
				//根据目录id查询
				if (!StringUtils.isEmpty(query.getCatalogId())) {
					url = url + "&filter[]=" + "service_template_catalog_id=" + query.getCatalogId();
				}
				
				int offset = page == 0 ? 0 : page * size;
				url = url + "&offset=" + offset + "&limit=" + size;
				//根据指定的排序列进行排序，指定列包括：name
				if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
					url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
				} else {
					url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
				}
				ResponseEntity<MiqListReturnDTO<ServiceTemplatesDTO>> sTemplatesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<ServiceTemplatesDTO>>(){}, token);
				if (401 == sTemplatesRes.getStatusCodeValue()) {
	 				result.setError(401, ConstantsUtils.tokenMessage);
	 				return result;
	 		    }
				if (200 == sTemplatesRes.getStatusCodeValue()) {
					//带查询的请求
					if (url.contains("filter") && sTemplatesRes.getBody().getSubquery_count() != null) {
						count = sTemplatesRes.getBody().getSubquery_count();
					} else {//不带查询的请求
						count = sTemplatesRes.getBody().getCount();
					}
					
					result.setSuccess(sTemplatesRes.getBody().getResources(), (long) count);
				} else {
					result.setError("获取信息失败！");
	        	}
				
				return result;
			}catch(Exception e) {
				e.printStackTrace();
				result.setError();
	        	return result;
			}
	}
	
	@Override
	public DataResultDTO<ServiceTemplatesDetailsDTO> getServiceTemplatesDetails(String id, String token) {
		DataResultDTO<ServiceTemplatesDetailsDTO> result = new DataResultDTO<>();
		String  attributes = "config_info,tags,picture,picture.image_href,tenant,zone";
		String url = "/api/service_templates/" + id + "?expand=resources" + "&attributes=" + attributes;
		try {
			
			ResponseEntity<ServiceTemplatesDetailsDTO> sTemplatesRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<ServiceTemplatesDetailsDTO>(){}, token);
			if (401 == sTemplatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == sTemplatesRes.getStatusCodeValue()) {
				result.setSuccess(sTemplatesRes.getBody());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public ResultDTO createServiceTemplate(ServiceTemplateCreateReceiveDTO serviceTemplateCreateReceiveDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		List<Object> trueList = new ArrayList<Object>();
		trueList.add(Boolean.TRUE);
		trueList.add(1);
		 
		List<Object> falseList = new ArrayList<Object>();
		falseList.add(Boolean.FALSE);
		falseList.add(0);
		
		//1、创建目录项
		//1.1 获取对话框id，默认获取自己创建的对话框General Massclouds Service
		String dialogId = null;
		DataResultDTO<List<ServiceDialogsDTO>> dialogRes = serviceCatalogsService.serviceDialogsList(token);
		if (200 == dialogRes.getCode() && dialogRes.getResult() != null && dialogRes.getResult().size() > 0) {
			dialogId = dialogRes.getResult().get(0).getId();
		}
		
		//1.2、设置访问参数，基本信息
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", serviceTemplateCreateReceiveDTO.getName());
        params.put("prov_type", serviceTemplateCreateReceiveDTO.getProvType());
        params.put("description", serviceTemplateCreateReceiveDTO.getDescription());
        params.put("service_template_catalog_id", serviceTemplateCreateReceiveDTO.getCatalogId());
        params.put("service_type", "atomic");
        params.put("display", Boolean.TRUE);
        
        //1.3、设置访问参数，请求信息
        ServiceTemplateCreateConfigInfoDTO serviceTemplateCreateConfigInfoDTO = new ServiceTemplateCreateConfigInfoDTO();
        
        //1.3.1 catalog信息
        serviceTemplateCreateConfigInfoDTO.setMiq_request_dialog_name("miq_provision_redhat_dialogs_template");
        serviceTemplateCreateConfigInfoDTO.setService_template_request(Boolean.TRUE); //该值必须为true，表示创建目录项后不会自动审批，否则会自动审批且创建虚拟机
        serviceTemplateCreateConfigInfoDTO.setInitial_pass(Boolean.TRUE);
        
        ServiceTemplateCreateProvisionDTO serviceTemplateCreateProvisionDTO = new ServiceTemplateCreateProvisionDTO();
        serviceTemplateCreateProvisionDTO.setFqname("/Service/Provisioning/StateMachines/ServiceProvision_Template/CatalogItemInitialization");
        serviceTemplateCreateProvisionDTO.setDialog_id(dialogId);//对话框类型，默认是General Massclouds Service
        serviceTemplateCreateConfigInfoDTO.setProvision(serviceTemplateCreateProvisionDTO);
        
        serviceTemplateCreateConfigInfoDTO.setZone_id(serviceTemplateCreateReceiveDTO.getZoneId());//时区
        
        List<Object> providerSetInfo = new ArrayList<Object>();
        providerSetInfo.add(serviceTemplateCreateReceiveDTO.getProviderId());
        providerSetInfo.add(serviceTemplateCreateReceiveDTO.getProviderName());
        serviceTemplateCreateConfigInfoDTO.setSrc_ems_id(providerSetInfo); //供应商
        
        List<Object> templateSetInfo = new ArrayList<Object>();
        templateSetInfo.add(serviceTemplateCreateReceiveDTO.getVmTemplateId());
        templateSetInfo.add(serviceTemplateCreateReceiveDTO.getVmTemplateName());
        serviceTemplateCreateConfigInfoDTO.setSrc_vm_id(templateSetInfo);//虚拟机模板
        
        List<Object> numberSetInfo = new ArrayList<Object>();
        numberSetInfo.add(serviceTemplateCreateReceiveDTO.getNumberOfVms());
        numberSetInfo.add(serviceTemplateCreateReceiveDTO.getNumberOfVms() + "");
        serviceTemplateCreateConfigInfoDTO.setNumber_of_vms(numberSetInfo);//虚拟机数量
        
        serviceTemplateCreateConfigInfoDTO.setVm_name(serviceTemplateCreateReceiveDTO.getName());//申请服务时虚拟机的名称,默认和服务名同名
        
        serviceTemplateCreateConfigInfoDTO.setLinked_clone(trueList);//连接克隆   默认：true
        
        List<Object> nativeSetInfo = new ArrayList<Object>();
        nativeSetInfo.add("native_clone");
        nativeSetInfo.add("Native Clone");
        serviceTemplateCreateConfigInfoDTO.setProvision_type(nativeSetInfo);
        
        //1.3.2 environment信息
        //如果自动配置为true，则不用设置集群、主机、存储域的信息，否则需要配置
        if (serviceTemplateCreateReceiveDTO.isPlacementAuto()) {
        	serviceTemplateCreateConfigInfoDTO.setPlacement_auto(trueList);
             
        } else {
        	serviceTemplateCreateConfigInfoDTO.setPlacement_auto(falseList);
            
       	    List<Object> clusterSetInfo = new ArrayList<Object>();
            clusterSetInfo.add(serviceTemplateCreateReceiveDTO.getClusterId());
            clusterSetInfo.add(serviceTemplateCreateReceiveDTO.getClusterName());
            serviceTemplateCreateConfigInfoDTO.setPlacement_cluster_name(clusterSetInfo);//集群
            
            List<Object> hostSetInfo = new ArrayList<Object>();
            hostSetInfo.add(serviceTemplateCreateReceiveDTO.getHostId());
            hostSetInfo.add(serviceTemplateCreateReceiveDTO.getHostName());  
            serviceTemplateCreateConfigInfoDTO.setPlacement_host_name(hostSetInfo);//主机
            
            List<Object> storageSetInfo = new ArrayList<Object>();
            storageSetInfo.add(serviceTemplateCreateReceiveDTO.getStorageId());
            storageSetInfo.add(serviceTemplateCreateReceiveDTO.getStorageName());  
            serviceTemplateCreateConfigInfoDTO.setPlacement_ds_name(storageSetInfo);//存储域
        }
        
        //1.3.3 hardware信息
        List<Object> socketSetInfo = new ArrayList<Object>();
        socketSetInfo.add(serviceTemplateCreateReceiveDTO.getNumberOfSockets());
        socketSetInfo.add(serviceTemplateCreateReceiveDTO.getNumberOfSockets() + "");
        serviceTemplateCreateConfigInfoDTO.setNumber_of_sockets(socketSetInfo);//颗数
        
        List<Object> coresSetInfo = new ArrayList<Object>();
        coresSetInfo.add(serviceTemplateCreateReceiveDTO.getCoresPerSocket());
        coresSetInfo.add(serviceTemplateCreateReceiveDTO.getCoresPerSocket() + "");
        serviceTemplateCreateConfigInfoDTO.setCores_per_socket(coresSetInfo);//核数
        
        List<Object> memorySetInfo = new ArrayList<Object>();
        memorySetInfo.add(serviceTemplateCreateReceiveDTO.getVmMemory());
        memorySetInfo.add(serviceTemplateCreateReceiveDTO.getVmMemory() + "");
        serviceTemplateCreateConfigInfoDTO.setVm_memory(memorySetInfo);//内存
        
        List<Object> diskForm = new ArrayList<Object>();
        diskForm.add("thin");
        diskForm.add("Thin");
        serviceTemplateCreateConfigInfoDTO.setDisk_format(diskForm); //磁盘类型，默认精简
        
        serviceTemplateCreateConfigInfoDTO.setMemory_limit(serviceTemplateCreateReceiveDTO.getVmMemory() * 4);//允许的内存最大值
        serviceTemplateCreateConfigInfoDTO.setMemory_reserve(serviceTemplateCreateReceiveDTO.getVmMemory());//保证物理内存
        
        //1.3.4 network信息
        List<Object> vlanSetInfo = new ArrayList<Object>();
        vlanSetInfo.add(serviceTemplateCreateReceiveDTO.getVmVlanId());
        vlanSetInfo.add(serviceTemplateCreateReceiveDTO.getVmVlanName());
        serviceTemplateCreateConfigInfoDTO.setVlan(vlanSetInfo);//虚拟机模板的网络id
        
        //1.3.5 其他信息
        serviceTemplateCreateConfigInfoDTO.setVm_auto_start(trueList); //虚拟机创建成功后自动开机，默认为true
        
        List<Object> scheduleSetInfo = new ArrayList<Object>();
        scheduleSetInfo.add("immediately");
        scheduleSetInfo.add("Immediately on Approval");
        serviceTemplateCreateConfigInfoDTO.setSchedule_type(scheduleSetInfo);
        
        serviceTemplateCreateConfigInfoDTO.setOwner_email("opencmp@massclouds.com");//
        
        params.put("config_info", serviceTemplateCreateConfigInfoDTO);
        
        String url = "/api/service_templates";
        
        //id不为空，则为编辑目录项
        if (!StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getId())) {
        	params.put("action", "edit");
        	url = url + "/" + serviceTemplateCreateReceiveDTO.getId();
        }
        
       String paramsJson = JsonUtils.obj2json(params);
       logger.info(paramsJson);
        try {
        	
        	ResponseEntity<String> sTemplateRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(sTemplateRes.getBody());
			
			if (401 == sTemplateRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == sTemplateRes.getStatusCodeValue()) {
        		ResultDTO setOwnerRes = null;
        		ResultDTO setTagsRes = null;
        		ResultDTO setPictureRes = null;
        		StringBuffer messageBuf = new StringBuffer();
        		String sTemplateId = jsonNode.get("results") == null ? serviceTemplateCreateReceiveDTO.getId() : jsonNode.get("results").get(0).get("id").asText();
        		if (!StringUtils.isEmpty(sTemplateId)) {
        			//2、给目录项设置所有权，即设置用户、用户组
        			   //用户、用户组可只设置一个，也可同时设置，不选择用户或用户组时，其值为空字符串
//        			if (!StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getOwnerId()) || !StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getGroupId()) ) {
        				String ownerId = StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getOwnerId()) ? "" : serviceTemplateCreateReceiveDTO.getOwnerId();
            			String groupId = StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getGroupId()) ? "" : serviceTemplateCreateReceiveDTO.getGroupId();
            			setOwnerRes = setServiceTemplateOwner(sTemplateId, ownerId, groupId, token);
//        			}
        			//3、给目录项设置标签
        			if (!StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getCategoryName()) && !StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getTagName()) ) {
        				setTagsRes = assignServiceTemplateTags(sTemplateId, serviceTemplateCreateReceiveDTO.getCategoryName(), serviceTemplateCreateReceiveDTO.getTagName(), token);
        			} else {//删除标签
        				setTagsRes = unassignServiceTemplateTags(sTemplateId, token);
        			}
        			//4、给目录项设置logo
        			if(!StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getLogoBase64()) || !StringUtils.isEmpty(serviceTemplateCreateReceiveDTO.getLogoExtension())) {
        				PictureAddDTO pictureAddDTO = new PictureAddDTO();
        				pictureAddDTO.setResourceId(sTemplateId);
        				pictureAddDTO.setResourceType("ServiceTemplate");
        				pictureAddDTO.setExtension(serviceTemplateCreateReceiveDTO.getLogoExtension());
        				pictureAddDTO.setContent(serviceTemplateCreateReceiveDTO.getLogoBase64());
        				
        				setPictureRes = picturesService.addPictrue(pictureAddDTO, token);
        			}
        		}
        		messageBuf.append("服务目录项创建/编辑成功！");
        		if (setOwnerRes != null && 200 != setOwnerRes.getCode()) {
        			messageBuf.append("\n").append("该目录项的权限设置失败！");
        		}
        		if (setTagsRes != null && 200 != setTagsRes.getCode()) {
        			messageBuf.append("\n").append("该目录项的标签设置失败！");
        		}
        		if (setPictureRes != null && 200 != setPictureRes.getCode()) {
        			messageBuf.append("\n").append("该目录项的logo设置失败！");
        		}
        		result.setSuccess();
        		result.setMessage(messageBuf.toString());
        	} else {
				result.setError(500, jsonNode.get("error").get("message").toString());
        	}
			return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
	@SuppressWarnings("unused")
	private static class OwnerResourceParam {
		private String evm_owner_id;
		private String miq_group_id;
		public String getEvm_owner_id() {
			return evm_owner_id;
		}
		public void setEvm_owner_id(String evm_owner_id) {
			this.evm_owner_id = evm_owner_id;
		}
		public String getMiq_group_id() {
			return miq_group_id;
		}
		public void setMiq_group_id(String miq_group_id) {
			this.miq_group_id = miq_group_id;
		}
	    public OwnerResourceParam(String evm_owner_id, String miq_group_id) {
			super();
			this.evm_owner_id = evm_owner_id;
			this.miq_group_id = miq_group_id;
		}
	}
	
	/**
	 * 设置目录项的权限，即设置所属组、所属用户
	 * @param id
	 * @param ownerId
	 * @param groupId
	 * @return
	 */
	public ResultDTO setServiceTemplateOwner(String id, String ownerId, String groupId, String token) {

		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        params.put("resource", new OwnerResourceParam(ownerId, groupId ));
        
        String url = "/api/service_templates/" + id;
        try {
        	ResponseEntity<String> setOwnerRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == setOwnerRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == setOwnerRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("服务目录设置权限成功！");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(setOwnerRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
	/**
	 * 设置目录项的标签
	 * @param id
	 * @param categoryName
	 * @param tagName
	 * @return
	 */
	public ResultDTO assignServiceTemplateTags(String id, String categoryName, String tagName, String token) {

		ResultDTO result = new ResultDTO();
		List<ParamsTagDTO> resourceParam = new ArrayList<ParamsTagDTO>();
		resourceParam.add(new ParamsTagDTO(ConstantsUtils.tagPrefix + categoryName, ConstantsUtils.tagPrefix + tagName));
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "assign");
        params.put("resources", resourceParam);
        
        String url = "/api/service_templates/" + id + "/tags";
        try {
        	ResponseEntity<String> setTagRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(setTagRes.getBody());
			
			if (401 == setTagRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == setTagRes.getStatusCodeValue()) {
        		boolean resultBoolean = jsonNode.get("results").get(0).get("success").asBoolean();
        		if (resultBoolean == Boolean.TRUE) {
        			result.setSuccess();
            		result.setMessage("服务目录设置标签成功！");
        		} else {
        			result.setError(500, "服务目录设置标签失败！");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}
	
	/**
	 * 删除目录项的标签
	 * @param id
	 * @return
	 */
	public ResultDTO unassignServiceTemplateTags(String id, String token) {

		ResultDTO result = new ResultDTO();
		StringBuffer errorSB = new StringBuffer();
		try {
			String getTagUrl  = "/api/service_templates/" + id + "/tags?expand=resources";
			ResponseEntity<MiqListReturnDTO<TagDTO>> tagsRes = ManageIQHttpUtils.httpGetRequest(getTagUrl, new ParameterizedTypeReference<MiqListReturnDTO<TagDTO>>(){}, token);
			
			if (tagsRes.getStatusCodeValue() == 200 && tagsRes.getBody().getResources() != null) {
				//设置访问参数
		        HashMap<String, Object> params = new HashMap<>();
		        params.put("action", "unassign");
				for (TagDTO tag : tagsRes.getBody().getResources()) {
					String unassignTagUrl = "/api/service_templates/" + id + "/tags/" + tag.getId();
					ResponseEntity<String> unsetTagRes = ManageIQHttpUtils.httpRequest(unassignTagUrl, "POST", params, new ParameterizedTypeReference<String>(){}, token);
					ObjectMapper objectMapper = new ObjectMapper();
					JsonNode jsonNode = objectMapper.readTree(unsetTagRes.getBody());
					if (200 == unsetTagRes.getStatusCodeValue()) {
		        		boolean resultBoolean = jsonNode.get("success").asBoolean();
		        		if (resultBoolean != Boolean.TRUE) {
		            		errorSB.append("目录项修改标签失败！");
		        		} 
		        	} else {
		        		errorSB.append(jsonNode.get("error").get("message").toString());
		        	}
				}
			}
			if (errorSB.length() > 0) {
				result.setError(500, errorSB.toString());
			} else {
				result.setSuccess();
				result.setMessage("目录项修改标签成功！");
			}
			return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
	@Override
	public ResultDTO updateServiceTemplate(ServiceTemplateCreateReceiveDTO serviceTemplateCreateReceiveDTO, String token) {
		return createServiceTemplate(serviceTemplateCreateReceiveDTO, token);
	}

	@Override
	public ResultDTO deleteServiceTemplate(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/service_templates/" + id;
        try {
        	
        	ResponseEntity<String> deleteRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == deleteRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == deleteRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("删除服务目录项成功！请等待其删除完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(deleteRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		return result;
	}
	
	
	@Override
	public ResultDTO deleteServiceTemplates(List<String> ids, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/service_templates";
        try {
        	
        	ResponseEntity<String> usersRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(usersRes.getBody());
			
        	if (401 == usersRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == usersRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("批量删除服务目录项成功。");
        		}
        	} else {
        		
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	//@Override
	public ResultDTO setOwner(String token) {
		ResultDTO result = new ResultDTO();
		
		//{"action":"edit", "evm_owner_id":4, "miq_group_id":21}
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "edit");
        params.put("evm_owner_id", 4);
        params.put("miq_group_id", 21);
        
        String url = "/api/service_templates/"  ;
        try {
        	
        	ResponseEntity<String> setRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == setRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == setRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("服务目录项创建成功！请等待其添加完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(setRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

}
