package com.massclouds.opencmp.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.VmDetailsDTO;
import com.massclouds.opencmp.service.TemplatesService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("TemplatesService")
public class TemplatesServiceImpl  implements TemplatesService{

	@Override
	public PageDataResultDTO<List<VmDetailsDTO>> templatesListOfPage(QueryDTO query, String token) {
		PageDataResultDTO<List<VmDetailsDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage();
		int size = query.getPageSize();
		
		String attributes = "href,id,name,vendor,description,created_on,updated_on,guid,service_type,tools_status,power_state,connection_state" +
		                    ",ext_management_system,ems_cluster,storage" +
		                    ",href,id,name,vendor,ext_management_system,host_name,ems_cluster_name,ram_size,num_hard_disks," +
				            "num_cpu,cpu_total_cores,cpu_cores_per_socket,used_storage,total_disks_size,operating_system";
		String url = "/api/templates/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=ems_id!=null";
		
		try {
			
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			//根据指定的排序列进行排序，指定列包括：name
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			ResponseEntity<MiqListReturnDTO<VmDetailsDTO>> templatesRes = getTemplateList(url, token);
			if (401 == templatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == templatesRes.getStatusCodeValue()) {
				List<VmDetailsDTO> vmsLsit = templatesRes.getBody().getResources();
				if (vmsLsit != null) {
					vmsLsit.forEach((vm) -> {
						vm.setRam_size(vm.getRam_size() / 1024);  //单位GB
							
					    long totalDisksSize = Long.parseLong(vm.getTotal_disks_size());
					    vm.setTotal_disks_size((totalDisksSize / 1024 /1024 /1024) + "" ); //单位GB
							
						});
				}
				
				result.setSuccess(vmsLsit, (long) templatesRes.getBody().getSubquery_count());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	@Override
	public DataResultDTO<List<VmDetailsDTO>> templatesList(String token) {
		DataResultDTO<List<VmDetailsDTO>> result = new DataResultDTO<>();
		String attributes = "href,id,name,vendor,description,created_on,updated_on,guid,service_type,tools_status,power_state,connection_state" +
                ",ext_management_system,ems_cluster,storage" +
                ",href,id,name,vendor,ext_management_system,host_name,ems_cluster_name,ram_size,num_hard_disks," +
	            "num_cpu,cpu_total_cores,cpu_cores_per_socket,used_storage,total_disks_size,operating_system";
        String url = "/api/templates/" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=ems_id!=null";
        url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
		try {
			ResponseEntity<MiqListReturnDTO<VmDetailsDTO>> templatesRes = getTemplateList(url, token);
			if (401 == templatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == templatesRes.getStatusCodeValue()) {
				List<VmDetailsDTO> vmsLsit = templatesRes.getBody().getResources();
				if (vmsLsit != null) {
					vmsLsit.forEach((vm) -> {
						vm.setRam_size(vm.getRam_size() / 1024);  //单位GB
							
					    long totalDisksSize = Long.parseLong(vm.getTotal_disks_size());
					    vm.setTotal_disks_size((totalDisksSize / 1024 /1024 /1024) + "" ); //单位GB
							
						});
				}
				
				result.setSuccess(vmsLsit);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
	public ResponseEntity<MiqListReturnDTO<VmDetailsDTO>> getTemplateList(String url, String token) throws Exception {
		return ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<VmDetailsDTO>>(){}, token);
	}

	@Override
	public ResultDTO deleteTemplate(String id, String token) {
		ResultDTO result = new ResultDTO();
		 
		String url = "/api/templates/" + id;
        try {
        	ResponseEntity<String> templatesRes = ManageIQHttpUtils.httpRequest(url, "DELETE", new HashMap<>(), new ParameterizedTypeReference<String>(){}, token);
        	if (401 == templatesRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (204 == templatesRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("删除模板成功！请等待其删除完成。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(templatesRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}
	
}
