package com.massclouds.opencmp.service.impl.ansible;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.ansible.AnsibleJobDTO;
import com.massclouds.opencmp.dto.ansible.InventoryDTO;
import com.massclouds.opencmp.service.ansible.AnsibleJobService;
import com.massclouds.opencmp.service.ansible.InventoryService;
import com.massclouds.opencmp.utils.AnsibleHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.AnsibleListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("AnsibleJobService")
public class AnsibleJobServiceImpl  implements AnsibleJobService{
	
	@Autowired
	private InventoryService inventoryService;
	
	@SuppressWarnings("serial")
	@Override
	public PageDataResultDTO<List<AnsibleJobDTO>> jobListOfPage(QueryDTO query) {
		PageDataResultDTO<List<AnsibleJobDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage() + 1;
		int size = query.getPageSize();
		
		String url = "jobs" + "?page=" + page + "&page_size=" + size + "&order_by=-created";
		if (!StringUtils.isEmpty(query.getCriteria())) {
			url = url + "&search=" + query.getCriteria();
		}
		try {
			ResponseEntity<AnsibleListReturnDTO<AnsibleJobDTO>> jobRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<AnsibleJobDTO>>(){});
			if (200 == jobRes.getStatusCodeValue()) {
				//获取job的清单的名称
				Map<String, String> inventoryMap = new HashMap<String, String>(){};
				DataResultDTO<List<InventoryDTO>> inventoryRes = inventoryService.inventoryListAll();
				if (inventoryRes.getResult() != null) {
					inventoryRes.getResult().forEach(item -> {
						inventoryMap.put(item.getId(), item.getName());
					});
				}
				
				List<AnsibleJobDTO> jobList = jobRes.getBody().getResults();
				if (jobList != null) {
					jobList.forEach(item -> {
						item.setInventoryName(inventoryMap.get(item.getInventory()));
					});
				}
				result.setSuccess(jobList, (long)jobRes.getBody().getCount());
			} else {
				result.setError(500, "获取任务失败！");
			}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResultDTO cancelJob(String id) {
		ResultDTO result = new ResultDTO();
		
		String url = "jobs/" + id + "/cancel/";
		try {
			ResponseEntity<String> cancelRes = AnsibleHttpUtils.httpRequest(url, "POST", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
	     	 if (201 ==cancelRes.getStatusCodeValue()) {
	     		result.setSuccess();
	     		result.setMessage("任务取消成功。");
	     	 } else {
	     		result.setError(500, cancelRes.getBody()); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
		
	}

	@Override
	public ResultDTO relaunchJob(String id) {
		ResultDTO result = new ResultDTO();
		
		String url = "jobs/" + id + "/relaunch/";
		try {
			ResponseEntity<String> cancelRes = AnsibleHttpUtils.httpRequest(url, "POST", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
	     	 if (201 ==cancelRes.getStatusCodeValue()) {
	     		result.setSuccess();
	     		result.setMessage("任务再次执行成功。");
	     	 } else {
	     		result.setError(500, cancelRes.getBody()); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
		
	}

	@Override
	public ResultDTO deleteJob(String id) {
		ResultDTO result = new ResultDTO();
		
        String url = "jobs/" + id + "/"; //url中最后的/一定要有
        try {
        	ResponseEntity<String> jobRes = AnsibleHttpUtils.httpRequest(url, "DELETE", 
        			new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
        	int status = jobRes.getStatusCodeValue();
       	     if (204 == status || 200 == status) {
        		result.setSuccess();
        		result.setMessage("任务删除成功。");
        	} else {
				result.setError(jobRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}
	
}
 