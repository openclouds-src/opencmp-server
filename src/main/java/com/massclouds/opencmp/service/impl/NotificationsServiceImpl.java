package com.massclouds.opencmp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.StringSubstitutor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.NotificationCountDTO;
import com.massclouds.opencmp.dto.NotificationDTO;
import com.massclouds.opencmp.dto.NotificationOriDTO;
import com.massclouds.opencmp.dto.global.ParamIdDTO;
import com.massclouds.opencmp.dto.query.NotificationsQueryDTO;
import com.massclouds.opencmp.service.NotificationsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.DateFormatUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("NotificationsService")
public class NotificationsServiceImpl  implements NotificationsService{
	@Override
	public PageDataResultDTO<List<NotificationDTO>> notificationsListOfPage(NotificationsQueryDTO query, String token) {

		PageDataResultDTO<List<NotificationDTO>> result = new PageDataResultDTO<>();
		List<NotificationDTO> notificationsList = new ArrayList<NotificationDTO>();
		
		String attributes = "details,user.name,user.userid";
		String url = "/api/notifications" + "?expand=resources" + "&attributes=" + attributes + "&filter[]=";;
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		
		try {
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			url = url + "&sort_by=id&sort_order=desc";		
			
			//根据已读状态查询
			if (!StringUtils.isEmpty(query.getIsRead())) {
				url = url + "&filter[]=" + "seen=" + Boolean.parseBoolean(query.getIsRead().toLowerCase());
			}
            
			ResponseEntity<String> notificationRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<String>(){}, token);
			if (401 == notificationRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    }
			if (200 == notificationRes.getStatusCodeValue()) {
				
				ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(notificationRes.getBody());
				if (jsonNode.get("subquery_count") != null) {
					count = jsonNode.get("subquery_count").asInt();
				} else {
					count = jsonNode.get("count").asInt();
				}
				
				if (jsonNode.get("resources") != null) {
					JsonNode resourcesNode = jsonNode.get("resources");
	        		for(int i = 0; i < resourcesNode.size(); i++) {
	        			NotificationDTO notificationDTO = new NotificationDTO();
	        			notificationDTO.setHref(resourcesNode.get(i).get("href").asText());
	        			notificationDTO.setId(resourcesNode.get(i).get("id").asText());
	        			notificationDTO.setNotificationId(resourcesNode.get(i).get("notification_id").asText());
	        			notificationDTO.setSeen(resourcesNode.get(i).get("seen").asBoolean());
	        			notificationDTO.setLevel(resourcesNode.get(i).get("details").get("level").asText());
	        			notificationDTO.setCreatedAt(DateFormatUtils.utcToBeijing(resourcesNode.get(i).get("details").get("created_at").asText()));
	        			notificationDTO.setUserId(resourcesNode.get(i).get("user_id").asText());
	        			notificationDTO.setUserName(resourcesNode.get(i).get("user").get("name").asText());
	        			
	        			/*解析通知的内容，这是接口返回值中通知详情部分的信息，message不为null ，就取message的值，message为null，就取text的值
	        			 * "details": {
	                     *     "level": "success",
	                     *     "created_at": "2022-05-24T08:56:46Z",
	                     *     "text": "Service %{subject} has been provisioned.",
	                     *     "message": null,
	                     *     "bindings": {
	                     *         "subject": {
	                     *               "text": "8kiu8ik"
	                     *          },
	                     *         "initiator": {
	                     *      	        "text": "Administrator"
	                     *          }
	                     *     },
		                 *    "seen": false
		                 *  },
		        		 */
	        			//message不为空，则去mesage的值，否则取text的值
	        			if (resourcesNode.get(i).get("details").get("message") != null 
	        				&& resourcesNode.get(i).get("details").get("message").asText() != null
	        				&& !"null".equals(resourcesNode.get(i).get("details").get("message").asText())) {
	        				notificationDTO.setDetails(resourcesNode.get(i).get("details").get("message").asText());
	        			} else {
	        				//获取通知内容中变量的key、value
	            			Map<String, String> valuesMap = new HashMap<>();
	            			Iterator<String> keys = resourcesNode.get(i).get("details").get("bindings").fieldNames();
	            			while(keys.hasNext()) {
	            				String key = keys.next();
	            				valuesMap.put(key, resourcesNode.get(i).get("details").get("bindings").get(key).get("text").asText());
	            			}
	            			//通知内容原始值，带匹配符的
	            			String mesageOri = resourcesNode.get(i).get("details").get("text").asText();
	            			//替换通知内容中的匹配符
	            			StringSubstitutor substitutor = new StringSubstitutor(valuesMap, "%{", "}", '%');
	            			String resolvedMessage = substitutor.replace(mesageOri);
	            			notificationDTO.setDetails(resolvedMessage);
	        			}
	        			notificationsList.add(notificationDTO);
	        		}
					
				}
				result.setSuccess(notificationsList, (long) count);
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
			
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<NotificationCountDTO> notificationsCount(String token) {

		DataResultDTO<NotificationCountDTO> result = new DataResultDTO<>();
		NotificationCountDTO notificationCountDTO = new NotificationCountDTO();
		
		String attributes = "details,user.name,user.userid";
		String url = "/api/notifications" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&sort_by=id&sort_order=desc";	
	    long successCount;
	    long errorCount;
	    long warnCount;
		
		try {
			ResponseEntity<MiqListReturnDTO<NotificationOriDTO>> notificationsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<NotificationOriDTO>>(){}, token);
            if (200 == notificationsRes.getStatusCodeValue() && notificationsRes.getBody().getResources() != null) {
				List<NotificationOriDTO> allNotification = notificationsRes.getBody().getResources();

				successCount = allNotification.stream().filter(s -> (s.getDetails().getLevel().equals("success") || s.getDetails().getLevel().equals("info"))).count(); //成功的
				errorCount = allNotification.stream().filter(s -> s.getDetails().getLevel().equals("error")).count();//失败的
				warnCount = allNotification.stream().filter(s -> s.getDetails().getLevel().equals("warning")).count();//警告的

				notificationCountDTO.setCount(allNotification.size());
				notificationCountDTO.setSuccessCount((int) successCount);
				notificationCountDTO.setErrorCount((int) errorCount);
				notificationCountDTO.setWarnCount((int) warnCount);
				
				result.setSuccess(notificationCountDTO);
		    } else {
				result.setError("获取信息失败！");
        	} 
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public ResultDTO readNotifications(List<String> ids, String token) {
		
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "mark_as_seen");
        params.put("resources", sources);
        
        String url = "/api/notifications";
        try {
        	
        	ResponseEntity<String> notificationsRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(notificationsRes.getBody());
			
        	if (401 == notificationsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == notificationsRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("通知已读成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}

	@Override
	public ResultDTO deleteNotifications(List<String> ids, String token) {

		ResultDTO result = new ResultDTO();
		
		//设置访问参数
		List<ParamIdDTO> sources = new ArrayList<ParamIdDTO>();
		for(String id : ids) {
			ParamIdDTO idParm = new ParamIdDTO(id);
			sources.add(idParm);
		}
        HashMap<String, Object> params = new HashMap<>();
        params.put("action", "delete");
        params.put("resources", sources);
        
        String url = "/api/notifications";
        try {
        	
        	ResponseEntity<String> notificationsRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	ObjectMapper objectMapper = new ObjectMapper();
			JsonNode jsonNode = objectMapper.readTree(notificationsRes.getBody());
			
        	if (401 == notificationsRes.getStatusCodeValue()) {
 				result.setError(401, ConstantsUtils.tokenMessage);
 				return result;
 		    } else if (200 == notificationsRes.getStatusCodeValue()) {
        		StringBuffer error_bf = new StringBuffer();
 		    	JsonNode resultNode = jsonNode.get("results");
        		for(int i = 0; i < resultNode.size(); i++) {
        			JsonNode node = resultNode.get(i);
					if (Boolean.FALSE == node.get("success").asBoolean()) {
						error_bf.append(node.get("message").asText());
						error_bf.append("\r\n");
					}
        		}
        		if (error_bf.length() != 0) {
        			result.setError(500, error_bf.toString());
        		} else {
        			result.setSuccess();
            		result.setMessage("删除通知成功。");
        		}
        	} else {
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
	}


}
