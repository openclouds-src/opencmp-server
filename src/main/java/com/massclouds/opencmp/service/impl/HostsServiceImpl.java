package com.massclouds.opencmp.service.impl;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.massclouds.opencmp.dto.HostDTO;
import com.massclouds.opencmp.dto.VmHostOnRunDTO;
import com.massclouds.opencmp.service.HostsService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.MiqListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

@Service("HostsService")
public class HostsServiceImpl  implements HostsService{
	
//	@Autowired
//	private ProvidersService providersService;

	@Override
	public PageDataResultDTO<List<HostDTO>> hostsList(QueryDTO query, String token) {
		PageDataResultDTO<List<HostDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage();
		int size = query.getPageSize();
		int count = 0;
		
	    String  attributes = "ems_cluster,v_total_vms,v_on_total_vms,v_total_miq_templates,num_cpu,total_vcpus,ram_size,"+ 
	    		             "ext_management_system.id,ext_management_system.name,ext_management_system.zone_id,ext_management_system.type";
		
		String url = "/api/hosts/" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&filter[]=archived=false";//过滤掉归档的
		try {
			//根据名称模糊查询
			if (!StringUtils.isEmpty(query.getName())) {
				url = url + "&filter[]=" + "name=" + "'*" + query.getName() + "*'";
			}
			int offset = page == 0 ? 0 : page * size;
			url = url + "&offset=" + offset + "&limit=" + size;
			//根据指定的排序列进行排序，指定列包括：name
			if (!StringUtils.isEmpty(query.getSortBy()) && !StringUtils.isEmpty(query.getSortOrder())) {
				url = url + "&sort_by=" + query.getSortBy() + "&sort_order=" + query.getSortOrder();
			} else {
				url = url + "&sort_by=id&sort_order=desc"; //默认按id倒序排序
			}
			ResponseEntity<MiqListReturnDTO<HostDTO>> hostsRes = ManageIQHttpUtils.httpGetRequest(url, new ParameterizedTypeReference<MiqListReturnDTO<HostDTO>>(){}, token);
			
			if (401 == hostsRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			
			if (200 == hostsRes.getStatusCodeValue()) {
				count = hostsRes.getBody().getSubquery_count();
				List<HostDTO> hostsList = hostsRes.getBody().getResources();
//				区域字段的获取太慢了，先屏蔽
//				Map<String, String> zoneMap = providersService.getProviderZoneMapper(token);
//				for (HostDTO host : hostsList) {
//					host.setZoneName(zoneMap.get(host.getEms_id()));
//				}
				
				result.setSuccess(hostsList, (long) count);
			}  else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}

	@Override
	public DataResultDTO<List<VmHostOnRunDTO>> listOnRun(String token) {
		DataResultDTO<List<VmHostOnRunDTO>> result = new DataResultDTO<>();
		
	    String  attributes = "id,name,ipaddress,type";
		String url = "/api/hosts/" + "?expand=resources" + "&attributes=" + attributes;
		url = url + "&filter[]=archived=false&filter[]=power_state=on&filter[]=ipaddress!=nil";//过滤掉归档的、过滤出开机的、ip不为空的
		try {
			
			ResponseEntity<MiqListReturnDTO<VmHostOnRunDTO>> hostsRes = ManageIQHttpUtils.httpGetRequest(url, 
					new ParameterizedTypeReference<MiqListReturnDTO<VmHostOnRunDTO>>(){}, token);
			
			if (401 == hostsRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			}
			if (200 == hostsRes.getStatusCodeValue()) {
				result.setSuccess(hostsRes.getBody().getResources());
			} else {
				result.setError("获取信息失败！");
        	}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			result.setError();
        	return result;
		}
	}
	
}
