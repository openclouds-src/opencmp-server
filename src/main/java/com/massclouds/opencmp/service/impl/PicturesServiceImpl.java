package com.massclouds.opencmp.service.impl;

import java.util.HashMap;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.PictureAddDTO;
import com.massclouds.opencmp.service.PicturesService;
import com.massclouds.opencmp.utils.ConstantsUtils;
import com.massclouds.opencmp.utils.ManageIQHttpUtils;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("PicturesService")
public class PicturesServiceImpl  implements PicturesService{

	
	@Override
	public ResultDTO addPictrue(PictureAddDTO pictureAddDTO, String token) {
		ResultDTO result = new ResultDTO();
		
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("resource_id", pictureAddDTO.getResourceId());
        params.put("resource_type", pictureAddDTO.getResourceType());
        params.put("extension", pictureAddDTO.getExtension());
        params.put("content", pictureAddDTO.getContent());
        
        String url = "/api/pictures";
        try {
        	ResponseEntity<String> categoryRes = ManageIQHttpUtils.httpRequest(url, "POST", params, new ParameterizedTypeReference<String>(){}, token);
        	
        	if (401 == categoryRes.getStatusCodeValue()) {
				result.setError(401, ConstantsUtils.tokenMessage);
				return result;
			} else if (200 == categoryRes.getStatusCodeValue()) {
        		result.setSuccess();
        		result.setMessage("logo配置成功。");
        	} else {
        		ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(categoryRes.getBody());
				result.setError(jsonNode.get("error").get("message").toString());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}

}
