package com.massclouds.opencmp.service.impl.ansible;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.massclouds.opencmp.dto.ansible.HostDTO;
import com.massclouds.opencmp.dto.ansible.InventoryDTO;
import com.massclouds.opencmp.service.ansible.InventoryService;
import com.massclouds.opencmp.utils.AnsibleHttpUtils;
import com.massclouds.opencmp.utils.StringUtils;
import com.massclouds.opencmp.utils.resultdto.AnsibleListReturnDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

@Service("InventoryService")
public class InventoryServiceImpl  implements InventoryService{
	private static Logger logger = LoggerFactory.getLogger(InventoryServiceImpl.class);
	@Override
	public PageDataResultDTO<List<InventoryDTO>> inventoryListOfPage(QueryDTO query) {
		PageDataResultDTO<List<InventoryDTO>> result = new PageDataResultDTO<>();
		int page = query.getPage() + 1;
		int size = query.getPageSize();
		
		String url = "inventories" + "?page=" + page + "&page_size=" + size + "&order_by=-created";
		if (!StringUtils.isEmpty(query.getCriteria())) {
			url = url + "&search=" + query.getCriteria();
		}
		try {
			ResponseEntity<AnsibleListReturnDTO<InventoryDTO>> inventoryRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<InventoryDTO>>(){});
			if (200 == inventoryRes.getStatusCodeValue()) {
				List<InventoryDTO> inventoryList = inventoryRes.getBody().getResults();
				if (inventoryList != null) {
					//获取清单已分配的主机
					for (InventoryDTO inventory : inventoryList) {
						DataResultDTO<List<HostDTO>> hostsRes =  inventoryListHosts(inventory.getId());
						if (200 == hostsRes.getCode()) {
							inventory.setHosts(hostsRes.getResult());
						}
					}
				}
				result.setSuccess(inventoryRes.getBody().getResults(), (long)inventoryRes.getBody().getCount());
			} else {
				result.setError(500, "获取清单数据失败！");
			}
			
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public DataResultDTO<List<InventoryDTO>> inventoryListAll() {
		DataResultDTO<List<InventoryDTO>> result = new DataResultDTO<>();
		
		String url = "inventories" + "?order_by=-created";
		try {
			ResponseEntity<AnsibleListReturnDTO<InventoryDTO>> inventoryRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<InventoryDTO>>(){});
			if (200 == inventoryRes.getStatusCodeValue()) {
				result.setSuccess(inventoryRes.getBody().getResults());
			} else {
				result.setError(500, "获取清单数据失败！");
			}
			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * 创建清单，需要做的动作有：创建清单、给清单添加可操作的主机或虚机的ip
	 */
	@Override
	public ResultDTO createUpdateInventory(InventoryDTO inventoryDTO) {
		ResultDTO result = new ResultDTO();
		/*************1、创建/编辑清单*************/
		//设置访问参数
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", inventoryDTO.getName());
        params.put("description", inventoryDTO.getDescription());
        params.put("organization", 1);   //使用默认域 
        params.put("kind", "");  //该值为空字符串，表示创建普通的inventory。kind的值包括""、smart。
        params.put("variables", inventoryDTO.getVariables() == null ? "" : inventoryDTO.getVariables());//参数，yaml格式的字符串
        
        String url = "inventories/"; //url中最后的/一定要有
        String method = "POST";
        //编辑清单
        if (!StringUtils.isEmpty(inventoryDTO.getId())) {
        	url = url + inventoryDTO.getId() + "/";
        	method = "PUT";
        }
        try {
        	ResponseEntity<String> inventoryRes = AnsibleHttpUtils.httpRequest(url, method, 
        			params, 
					new ParameterizedTypeReference<String>(){});
        	
			JsonNode jsonNode = new ObjectMapper().readTree(inventoryRes.getBody());
        	 int status = inventoryRes.getStatusCodeValue();
        	 if (201 == status || 200 == status) {
        		 /*************2、为清单设置主机/虚机*************/
        		if (jsonNode.get("id") != null && inventoryDTO.getHosts() != null && inventoryDTO.getHosts().size() > 0) {
        			String inventoryId = jsonNode.get("id").asText();
        			ResultDTO setHostRes = updateInventoryHosts(inventoryId, inventoryDTO.getHosts());
        			result.setSuccess("清单添加/编辑成功。" + setHostRes.getMessage());
        		} else {
        			result.setSuccess("清单添加/编辑成功。");
        		}
        	} else {
				result.setError(inventoryRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}
	
	public ResultDTO updateInventoryHosts(String inventoryId, List<HostDTO> hostsIp) {
		ResultDTO result = new ResultDTO();
		List<HostDTO> existedHosts = new ArrayList<HostDTO>();//已分配的主机或虚机
		List<HostDTO> associateHosts = new ArrayList<HostDTO>();//需要分配的主机或虚机
		List<HostDTO> disassociateHosts = new ArrayList<HostDTO>();//需要分离的主机或虚机
		
		DataResultDTO<List<HostDTO>> existedRes =  inventoryListHosts(inventoryId);
		if (200 == existedRes.getCode()) {
			existedHosts = existedRes.getResult();
		}
		//过滤出哪些是需要分离的
		if (existedHosts != null) {
			for (HostDTO host : existedHosts) {
				List<HostDTO> isRemove = hostsIp.stream().filter(n -> n.getName().equals(host.getName()) == true).collect(Collectors.toList());
				if (isRemove == null || isRemove.size() == 0) {
					disassociateHosts.add(host);
				}
			}
		}
		//过滤出哪些是需要分配的
		if (hostsIp != null) {
			for (HostDTO host : hostsIp) {
				List<HostDTO> isExisted = existedHosts.stream().filter(item -> item.getName().equals(host.getName()) == true).collect(Collectors.toList());
				if (isExisted == null || isExisted.size() == 0) {
					associateHosts.add(host);
				}
			}
		}
		
		//分离主机或虚机
		StringBuffer removeErrBuffer = new StringBuffer();
		if (disassociateHosts != null && disassociateHosts.size() > 0) {
			for (HostDTO host : disassociateHosts) {
				ResultDTO res = disassociateInventoryHosts(inventoryId, host.getId());
				if (200 != res.getCode()) {
					removeErrBuffer.append(host.getName()).append(",");
				}
			}
		}
				
		//分配主机或虚机
		StringBuffer setErrBuffer = new StringBuffer();
		if (associateHosts != null && associateHosts.size() > 0) {
			for (HostDTO host : associateHosts) {
				ResultDTO res =  associateInventoryHosts(inventoryId, host.getName(), null);
				if (200 != res.getCode()) {
					setErrBuffer.append(host.getName()).append(",");
				}
			}
		}
		String message = "";
		if (removeErrBuffer.length() > 0) {
			message = message + "为其分离主机/虚机【" + removeErrBuffer.toString() + "】失败！";
		}
		if (setErrBuffer.length() > 0) {
			message = message + "为其分配主机/虚机【" + setErrBuffer.toString() + "】失败！";
		}
		result.setSuccess(message);
		return result;
	}

	@Override
	public DataResultDTO<List<HostDTO>> inventoryListHosts(String inventoryId) {
		DataResultDTO<List<HostDTO>> result = new DataResultDTO<List<HostDTO>>();
		
		String url = "inventories/" + inventoryId + "/hosts/";
		try {
			ResponseEntity<AnsibleListReturnDTO<HostDTO>> hostRes = AnsibleHttpUtils.httpRequest(url, "GET", 
					new HashMap<String, Object>(), 
					new ParameterizedTypeReference<AnsibleListReturnDTO<HostDTO>>(){});
	     	 if (200 ==hostRes.getStatusCodeValue()) {
	     		result.setSuccess(hostRes.getBody().getResults());
	     	 } else {
	     		result.setError(500, null); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
	}

	@Override
	public ResultDTO associateInventoryHosts(String inventoryId, String hostIp, String hostDescription) {
		ResultDTO result = new ResultDTO();
		
		String url = "inventories/" + inventoryId + "/hosts/";
		HashMap<String, Object> hostParm = new HashMap<String, Object>();
		hostParm.put("name", hostIp);
//		hostParm.put("description", hostDescription == null ? "" : hostDescription);
		try {
			ResponseEntity<String> credentialRes = AnsibleHttpUtils.httpRequest(url, "POST", 
					hostParm, 
					new ParameterizedTypeReference<String>(){});
	     	 if (201 ==credentialRes.getStatusCodeValue()) {
	     		result.setSuccess();
	     		result.setMessage("清单的主机设置成功。");
	     	 } else {
	     		result.setError(500, "清单的主机设置失败。"); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
	}

	@Override
	public ResultDTO disassociateInventoryHosts(String inventoryId, String hostId) {
		ResultDTO result = new ResultDTO();
		
		String url = "inventories/" + inventoryId + "/hosts/";
		HashMap<String, Object> hostParm = new HashMap<String, Object>();
		hostParm.put("id", Integer.parseInt(hostId));
		hostParm.put("disassociate", Boolean.TRUE);//解除任务模板的凭证
		try {
			ResponseEntity<String> credentialRes = AnsibleHttpUtils.httpRequest(url, "POST", 
					hostParm, 
					new ParameterizedTypeReference<String>(){});
	     	 if (204 ==credentialRes.getStatusCodeValue()) {
	     		result.setSuccess("清单的主机解除成功。");
	     	 } else {
	     		result.setError(500, "清单的主机解除失败。"); 
	     	 }
	     	 return result;
		}catch(Exception e) {
			e.printStackTrace();
        	result.setError();
        	return result;
		}
	}

	@Override
	public ResultDTO deleteInventory(String id) {
		ResultDTO result = new ResultDTO();
		
        String url = "inventories/" + id + "/"; //url中最后的/一定要有
        try {
        	ResponseEntity<String> inventoryRes = AnsibleHttpUtils.httpRequest(url, "DELETE", 
        			new HashMap<String, Object>(), 
					new ParameterizedTypeReference<String>(){});
        	int status = inventoryRes.getStatusCodeValue();
       	     if (202 == status || 200 == status) {
        		result.setSuccess();
        		result.setMessage("清单删除成功。");
        	} else {
				result.setError(inventoryRes.getBody());
        	}
        	return result;
        }catch(Exception e){
        	e.printStackTrace();
        	result.setError();
        	return result;
        }
		
	}
	
}
 