/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import com.massclouds.opencmp.dto.LoginUserDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月24日 上午10:13:24
 */
public interface LoginService {
	
	DataResultDTO<LoginUserDTO> login(LoginUserDTO loginUserDTO);
	
}