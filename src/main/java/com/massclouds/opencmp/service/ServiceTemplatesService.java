/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.ServiceTemplateCreateReceiveDTO;
import com.massclouds.opencmp.dto.ServiceTemplatesDTO;
import com.massclouds.opencmp.dto.ServiceTemplatesDetailsDTO;
import com.massclouds.opencmp.dto.query.ServiceTemplateQueryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月1日 上午10:13:24
 * @author hou_jjing
 */
public interface ServiceTemplatesService {
	
	PageDataResultDTO<List<ServiceTemplatesDTO>> serviceTemplatesList(ServiceTemplateQueryDTO query, String token);
	
	DataResultDTO<ServiceTemplatesDetailsDTO> getServiceTemplatesDetails(String id, String token);
	
	ResultDTO createServiceTemplate(ServiceTemplateCreateReceiveDTO serviceTemplateCreateReceiveDTO, String token);
	
	ResultDTO updateServiceTemplate(ServiceTemplateCreateReceiveDTO serviceTemplateCreateReceiveDTO, String token);
	
	ResultDTO deleteServiceTemplate(String id, String token);
	
	ResultDTO deleteServiceTemplates(List<String> ids, String token);
}