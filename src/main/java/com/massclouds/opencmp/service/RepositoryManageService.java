/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.AnsibleRepositoryDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 软件仓库管理的业务层接口
 * 
 * @date: 2022年8月25日 上午10:13:24
 * @author hou_jjing
 */
public interface RepositoryManageService {
	
	PageDataResultDTO<List<AnsibleRepositoryDTO>> repositoryList(QueryDTO query, String token);
	
	ResultDTO deleteRepository(List<String> names, String token);
	
}