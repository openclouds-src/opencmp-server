/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;
import java.util.Map;

import com.massclouds.opencmp.dto.ContainerManagerDetailsDTO;
import com.massclouds.opencmp.dto.ProviderAddDTO;
import com.massclouds.opencmp.dto.ProviderDTO;
import com.massclouds.opencmp.dto.ProviderDetailsDTO;
import com.massclouds.opencmp.dto.ProviderRedisDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年3月1日 上午10:13:24
 * @author hou_jjing
 */
public interface ProvidersService {
	
	DataResultDTO<Map<String, Object>> providerTypeAll(String token);
	
	PageDataResultDTO<List<ProviderDTO>> providersList(QueryDTO query, String token);
	
	DataResultDTO<Map<String, List<ProviderDTO>>> listAll(String token);
	
	DataResultDTO<ProviderDetailsDTO> getProviderDetails(String id, String token);
	
	DataResultDTO<ContainerManagerDetailsDTO> getContainerManagerDetails(String id, String token);
	
	ResultDTO addProvider(ProviderAddDTO providerAddDTO, String token);
	
	ResultDTO updateProvider(String id, ProviderAddDTO providerAddDTO, String token);
	
	ResultDTO recheckProvider(String id, String token);
	
	ResultDTO refreshProvider(String id, String token);
	
	ResultDTO refreshAll(String token);
	
	ResultDTO deleteProvider(String id, String token);
	
	ResultDTO syncMCOScServerUserGroup(String id, String token);
	
	Map<String, String> getProviderZoneMapper(String token);
	
	List<ProviderRedisDTO> getInfraManagerFromRedis() throws Exception;
	
}