/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import com.massclouds.opencmp.dto.PictureAddDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月20日 上午10:13:24
 */
public interface PicturesService {
	
	ResultDTO addPictrue(PictureAddDTO pictureAddDTO, String token);
	
}