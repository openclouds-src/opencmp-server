/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.ServiceCatalogsDTO;
import com.massclouds.opencmp.dto.ServiceCatalogsListDTO;
import com.massclouds.opencmp.dto.ServiceDialogsDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月1日 上午10:13:24
 * @author hou_jjing
 */
public interface ServiceCatalogsService {
	
	PageDataResultDTO<List<ServiceCatalogsDTO>> serviceCatalogsListOfPage(QueryDTO query, String token);
	
	DataResultDTO<List<ServiceCatalogsListDTO>> serviceCatalogsList(String token);
	
	ResultDTO createServiceCatalog(ServiceCatalogsDTO serviceCatalogsDTO, String token);
	
	ResultDTO updateServiceCatalog(ServiceCatalogsDTO serviceCatalogsDTO, String token);
	
	ResultDTO deleteServiceCatalog(String id, String token);
	
	ResultDTO deleteServiceCatalogs(List<String> ids, String token);
	
	
	
	DataResultDTO<List<ServiceDialogsDTO>> serviceDialogsList(String token);
	
}