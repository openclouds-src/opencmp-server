/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.ClusterDTO;
import com.massclouds.opencmp.dto.ClusterDetailDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年3月16日 上午10:13:24
 * @author hou_jjing
 */
public interface ClustersService {
	
	PageDataResultDTO<List<ClusterDTO>> clustersListOfPage(int page, int size, String token);
	
	DataResultDTO<List<ClusterDTO>> clustersList(String token);
	
	DataResultDTO<ClusterDetailDTO> getDetailById(String id, String token);
	
}