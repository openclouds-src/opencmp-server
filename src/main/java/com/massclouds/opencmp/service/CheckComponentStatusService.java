/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.ComponentStatusDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年6月14日 上午10:13:24
 * @author hou_jjing
 */
public interface CheckComponentStatusService {
	
	PageDataResultDTO<List<ComponentStatusDTO>> componentList(QueryDTO query, String token);
	
}