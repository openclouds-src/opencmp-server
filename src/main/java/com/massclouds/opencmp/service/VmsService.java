/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;
import java.util.Map;

import com.massclouds.opencmp.dto.InstanceActionDTO;
import com.massclouds.opencmp.dto.InstanceResourceOfCloudPlatformDTO;
import com.massclouds.opencmp.dto.VmDTO;
import com.massclouds.opencmp.dto.VmDetailsDTO;
import com.massclouds.opencmp.dto.VmDiskDTO;
import com.massclouds.opencmp.dto.VmHostOnRunDTO;
import com.massclouds.opencmp.dto.VmHtml5ConsoleDTO;
import com.massclouds.opencmp.dto.VmNoVNCDTO;
import com.massclouds.opencmp.dto.VmSetConfigDTO;
import com.massclouds.opencmp.dto.VmSpiceDTO;
import com.massclouds.opencmp.dto.query.VmsQueryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年3月16日 上午10:13:24
 * @author hou_jjing
 */
public interface VmsService {
	
	PageDataResultDTO<List<VmDetailsDTO>> vmsList(VmsQueryDTO query, String token);
	
	ResultDTO vmAction(InstanceActionDTO instanceActionDTO, String token);
	
	DataResultDTO<VmNoVNCDTO> getVncInfo( String id, String token);
	
	DataResultDTO<VmSpiceDTO> getSpiceInfo( String id, String token);
	
	DataResultDTO<VmHtml5ConsoleDTO> getHtml5ControlInfo(String id, String token);
	
	DataResultDTO<Map<String, Object>> getControlInfo(String id,String vendor, String token);
	
	ResultDTO deleteVm(String id, String token);
	
	List<VmDTO> vmsAllByType(String vmType, String token);
	
	List<VmDTO> vmsOnLine( String token);
	
	DataResultDTO<VmDTO> getVmByEmsuid(String uidems, String token);
	
	ResultDTO setVmOwnership(String id, String groupName, String userName, String token);
	
	ResultDTO setVmTag(String vmId, String categoryName, String tagName, String token);
	
	ResultDTO updateVm(String vmId, VmsQueryDTO query, String token);
	
	DataResultDTO<Map<String, List<VmHostOnRunDTO>>> listOnRun(String token);
	
	
	DataResultDTO<List<VmDiskDTO>> getVmDisks(String vmId, String token);
	
	ResultDTO addVmDisk(String vmId, VmSetConfigDTO vmSetConfigDTO, String token);
	
	ResultDTO removeVmDisk(String vmId, VmSetConfigDTO vmSetConfigDTO, String token);
	
	ResultDTO resizeVmDisk(String vmId, VmSetConfigDTO vmSetConfigDTO, String token);
	
	ResultDTO updateVmMemoryCPU(String vmId, VmSetConfigDTO vmSetConfigDTO, String token);
	
	InstanceResourceOfCloudPlatformDTO getAllInstancesCpusMemoryOfCloudPlatform(String providerId, String token);
	
}