package com.massclouds.opencmp.service.ansible;

import java.util.List;

import com.massclouds.opencmp.dto.ansible.ProjectDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: ansible 项目 的业务层接口
 * 
 * @date: 2022年6月16日 上午10:13:24
 * @author hou_jjing
 */
public interface ProjectService {
	
	PageDataResultDTO<List<ProjectDTO>> projectListOfPage(QueryDTO query);
	
	ProjectDTO getDefaultProject();
	
	DataResultDTO<List<String>> getPlaybooksOfDefault();
	
	ResultDTO createUpdateProject(ProjectDTO projectDTO);
	
	ResultDTO deleteProject(String id);
}
