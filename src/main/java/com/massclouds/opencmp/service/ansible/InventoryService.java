package com.massclouds.opencmp.service.ansible;

import java.util.List;

import com.massclouds.opencmp.dto.ansible.HostDTO;
import com.massclouds.opencmp.dto.ansible.InventoryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: ansible 清单 的业务层接口
 * 
 * @date: 2022年6月15日 上午10:13:24
 * @author hou_jjing
 */
public interface InventoryService {
	PageDataResultDTO<List<InventoryDTO>> inventoryListOfPage(QueryDTO query);
	
	DataResultDTO<List<InventoryDTO>> inventoryListAll();
	
	ResultDTO createUpdateInventory(InventoryDTO inventoryDTO);
	
	ResultDTO deleteInventory(String id);
	
	ResultDTO associateInventoryHosts(String inventoryId, String hostIp, String hostDescription);
	
	ResultDTO disassociateInventoryHosts(String inventoryId, String hostId);
	
	DataResultDTO<List<HostDTO>> inventoryListHosts(String inventoryId);
}
