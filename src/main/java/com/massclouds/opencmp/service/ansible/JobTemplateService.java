package com.massclouds.opencmp.service.ansible;

import java.util.List;

import com.massclouds.opencmp.dto.ansible.CredentialDTO;
import com.massclouds.opencmp.dto.ansible.JobTemplateDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: ansible 任务模板 的业务层接口
 * 
 * @date: 2022年6月17日 上午10:13:24
 * @author hou_jjing
 */
public interface JobTemplateService {
	
	PageDataResultDTO<List<JobTemplateDTO>> jobTemplateListOfPage(QueryDTO query);
	
	ResultDTO createUpdateJobTemplate(JobTemplateDTO jobTemplateDTO);
	
	ResultDTO deleteJobTemplate(String id);
	
	ResultDTO launchJobTemplate(String id);
	
	ResultDTO associateJobTemplateCredential(String jobTemplateid, String userName, String password);
	
	ResultDTO disassociateJobTemplateCredential(String jobTemplateid, String credentialId);
	
	DataResultDTO<List<CredentialDTO>> getJobTemplateCredential(String jobTemplateid);
}
