package com.massclouds.opencmp.service.ansible;

import java.util.List;

import com.massclouds.opencmp.dto.ansible.CredentialDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: ansible 凭证 的业务层接口
 * 
 * @date: 2022年6月16日 上午10:13:24
 * @author hou_jjing
 */
public interface CredentialService {
	
	PageDataResultDTO<List<CredentialDTO>> credentialListOfPage(QueryDTO query);
	
	ResultDTO createUpdateCredential(CredentialDTO credentialDTO);
	
	ResultDTO deleteCredential(String id);
}
