package com.massclouds.opencmp.service.ansible;

import java.util.List;

import com.massclouds.opencmp.dto.ansible.AnsibleJobDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: ansible 任务 的业务层接口
 * 
 * @date: 2022年6月20日 上午10:13:24
 * @author hou_jjing
 */
public interface AnsibleJobService {
	
	PageDataResultDTO<List<AnsibleJobDTO>> jobListOfPage(QueryDTO query);
	
	ResultDTO cancelJob(String id);
	
	ResultDTO relaunchJob(String id);
	
	ResultDTO deleteJob(String id);
}
