/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.UserDTO;
import com.massclouds.opencmp.dto.UserGroupCreateDTO;
import com.massclouds.opencmp.dto.UserGroupDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月6日 上午10:13:24
 * @author hou_jjing
 */
public interface UserGroupsService {
	
	PageDataResultDTO<List<UserGroupDTO>> groupsListOfPage(QueryDTO query, String token);
	
	DataResultDTO<List<UserGroupDTO>> groupsList(String token);
	
	List<UserGroupDTO> getGroupsList(String url, String token);
	
	DataResultDTO<UserGroupDTO> getById(String id, String token);
	
	ResultDTO createOrUpdateUserGroup(UserGroupCreateDTO userGroupCreateDTO, String token);
	
	ResultDTO createUserGroup(UserGroupCreateDTO userGroupCreateDTO, String token);
	
	ResultDTO deleteUserGroup(String id, String token);
	
	ResultDTO deleteUserGroups(List<String> userIds, String token);
	
	List<UserDTO> getUsersByTenantId(String tenantId, String token);
	
}