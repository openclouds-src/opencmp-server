/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.TagCreateDTO;
import com.massclouds.opencmp.dto.TagDTO;
import com.massclouds.opencmp.dto.query.TagsQueryDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月8日 上午10:13:24
 * @author hou_jjing
 */
public interface TagsService {
	
	PageDataResultDTO<List<TagDTO>> tagsListOfPage(TagsQueryDTO query, String token);
	
	DataResultDTO<List<TagDTO>> tagsList(String token);
	
	ResultDTO createUpdateTag(TagCreateDTO tagCreateDTO, String token);
	
	ResultDTO deleteTag(String id, String token);
	
	ResultDTO deleteTags(List<String> ids, String token);
	
}