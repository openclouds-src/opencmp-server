package com.massclouds.opencmp.service.zabbix;

import java.util.List;

import com.massclouds.opencmp.dto.zabbix.ZabbixHostCreateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixTemplateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixTriggerDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: zabbix 模板 的业务层接口
 * 
 * @date: 2022年6月24日 上午10:13:24
 * @author 
 */
public interface TemplateService {
	
	PageDataResultDTO<List<ZabbixTemplateDTO>> templatesListOfPage(QueryDTO query);
	
	DataResultDTO<List<ZabbixTemplateDTO>> templatesList();
	
	PageDataResultDTO<List<ZabbixTriggerDTO>> templateTriggerListOfPage(QueryDTO query);
	
	ResultDTO updateStatusTrigger(ZabbixHostCreateDTO zabbixHostCreateDTO);
	
}
