package com.massclouds.opencmp.service.zabbix;

import java.util.List;

import com.massclouds.opencmp.dto.zabbix.ZabbixHostCreateDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixHostDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixHostGroupDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: zabbix 主机 的业务层接口
 * 
 * @date: 2022年6月23日 上午10:13:24
 * @author 
 */
public interface HostService {
	
	PageDataResultDTO<List<ZabbixHostDTO>> hostsListOfPage(QueryDTO query);
	
	ResultDTO createHost(ZabbixHostCreateDTO zabbixHostCreateDTO);
	
	ResultDTO updateStatusHost(ZabbixHostCreateDTO zabbixHostCreateDTO);
	
	ResultDTO deleteHost(List<String> ids);
	
	DataResultDTO<ZabbixHostGroupDTO> getDefaultHostGroup();
	
}
