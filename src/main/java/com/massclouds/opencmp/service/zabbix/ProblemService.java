package com.massclouds.opencmp.service.zabbix;

import java.util.List;

import com.massclouds.opencmp.dto.zabbix.EventAcknowledgeDTO;
import com.massclouds.opencmp.dto.zabbix.ProblemSearchDTO;
import com.massclouds.opencmp.dto.zabbix.ZabbixProblemDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: zabbix 告警事件 的业务层接口
 * 
 * @date: 2022年6月24日 上午10:13:24
 * @author 
 */
public interface ProblemService {
	
	PageDataResultDTO<List<ZabbixProblemDTO>> problemListOfPage(ProblemSearchDTO query);
	
	ResultDTO updateAcknowledge(EventAcknowledgeDTO eventAcknowledgeDTO);
	
}
