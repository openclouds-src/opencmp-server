/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.ZoneDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;
import com.massclouds.opencmp.utils.resultdto.ResultDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年4月18日 上午10:13:24
 * @author hou_jjing
 */
public interface ZonesService {
	
	PageDataResultDTO<List<ZoneDTO>> zonesListOfPage(QueryDTO query, String token);
	
	DataResultDTO<List<ZoneDTO>> zonesList(String token);
	
	ResultDTO createZone(ZoneDTO zoneDTO, String token);
	
	ResultDTO updateZone(ZoneDTO zoneDTO, String token);
	
	ResultDTO deleteZone(String id, String token);
	
	
	
}