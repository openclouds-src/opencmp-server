/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.service;

import java.util.List;

import com.massclouds.opencmp.dto.HostDTO;
import com.massclouds.opencmp.dto.VmHostOnRunDTO;
import com.massclouds.opencmp.utils.resultdto.DataResultDTO;
import com.massclouds.opencmp.utils.resultdto.PageDataResultDTO;
import com.massclouds.opencmp.utils.resultdto.QueryDTO;

/**
 * @Description: 开源云平台的业务层接口
 * 
 * @date: 2022年3月16日 上午10:13:24
 * @author hou_jjing
 */
public interface HostsService {
	
	PageDataResultDTO<List<HostDTO>> hostsList(QueryDTO query, String token);
	
	DataResultDTO<List<VmHostOnRunDTO>> listOnRun(String token);
	
}