/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.common.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.slf4j.Logger;

/**
 * @Description: 日志工具类
 *
 * @date: 2018年11月8日 下午5:20:32
 * @author li_ming 
 */
public class LogUtils {
	/**
	 * @Description: 记录INFO级别日志
	 *
	 * @param log 日志对象
	 * @param msg 日志内容
	 */
	public static void info(Logger log, String msg) {
		if (log.isInfoEnabled()) {
			log.info(msg);
		}
	}

	/**
	 * @Description: 记录DEBUG级别日志
	 *
	 * @param log 日志对象
	 * @param msg 日志内容
	 */
	public static void debug(Logger log, String msg) {
		if (log.isDebugEnabled()) {
			log.debug(msg);
		}
	}

	/**
	 * @Description: 记录WARNING级别日志
	 *
	 * @param log 日志对象
	 * @param msg 日志内容
	 */
	public static void warn(Logger log, String msg) {
		if (log.isWarnEnabled()) {
			log.warn(msg);
		}
	}

	/**
	 * @Description: 记录ERROR级别日志
	 *
	 * @param log 日志对象
	 * @param msg 日志内容
	 */
	public static void error(Logger log, String msg) {
		log.error(msg);
	}

	/**
	 * @Description: 记录ERROR级别日志
	 *
	 * @param log 日志对象
	 * @param t 异常对象
	 */
	public static void error(Logger log, Throwable t) {
		log.error(t.getMessage(), t);
	}

	/**
	 * @Description: 记录ERROR级别日志
	 *
	 * @param log 日志对象
	 * @param msg 日志内容
	 * @param t 异常对象
	 */
	public static void error(Logger log, String msg, Throwable t) {
		log.error(msg, t);
	}
	/**
	 * @Description: 获取Throwable对象堆栈对应的字符串
	 *
	 * @param t
	 * @return 
	 */
	public static String getThrowableString( Throwable t) {
		Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        t.printStackTrace(printWriter);
       return t.getClass()+":"+t.getLocalizedMessage()+"\r\n"+result.toString();
	}

}
