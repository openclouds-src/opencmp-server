/**
 * Copyright © 2018 - 2118 massclouds. All Rights Reserved.
 */
package com.massclouds.opencmp.common.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @Description: Json字符串处理工具类
 *
 * @date: 2018年11月8日 下午4:37:25
 * @author li_ming 
 */
public class JsonUtils {
	/**
	 * @Description: 静态ObjectMapper对象
	 */
	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	/**
	 * @Description: 日志
	 */
	private static final Logger log = LoggerFactory.getLogger(JsonUtils.class);

	/**
	 * @Description: javaBean、列表数组转换为json字符串
	 *
	 * @param obj javaBean、列表数组
	 * @return json字符串
	 */
	public static String obj2json(Object obj) {
		try {
			return OBJECT_MAPPER.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			LogUtils.error(log, e);
			return "";
		}
	}

	/**
	 * @Description: json转JavaBean
	 *
	 * @param jsonString json字符串
	 * @param clazz 类对象
	 * @return javaBean
	 */
	public static <T> T json2pojo(String jsonString, Class<T> clazz) {
		OBJECT_MAPPER.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		try {
			return OBJECT_MAPPER.readValue(jsonString, clazz);
		} catch (Exception e) {
			LogUtils.error(log, e);
			return null;
		}
	}

	/**
	 * @Description: json转JavaBean
	 *
	 * @param jsonString json字符串
	 * @param typeReference 类型
	 * @return
	 */
	public static <T> T json2pojo(String jsonString, TypeReference<T> typeReference) {
		try {
			return OBJECT_MAPPER.readValue(jsonString, typeReference);
		} catch (Exception e) {
			LogUtils.error(log, e);
			return null;
		}
	}

	/**
	 * @Description: json字符串转换为map
	 *
	 * @param jsonString json字符串
	 * @return map
	 */
	@SuppressWarnings("unchecked")
	public static <T> Map<String, Object> json2map(String jsonString) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		try {
			return mapper.readValue(jsonString, Map.class);
		} catch (Exception e) {
			LogUtils.error(log, e);
			return null;
		}
	}

	/**
	 * @Description: json字符串转换为map
	 *
	 * @param jsonString json字符串
	 * @param clazz 类对象
	 * @return map
	 */
	public static <T> Map<String, T> json2map(String jsonString, Class<T> clazz) {
		try {
			Map<String, Map<String, Object>> map = (Map<String, Map<String, Object>>) OBJECT_MAPPER.readValue(jsonString, new TypeReference<Map<String, T>>() {});
			Map<String, T> result = new HashMap<String, T>();
			for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
				result.put(entry.getKey(), map2pojo(entry.getValue(), clazz));
			}
			return result;
		} catch (Exception e) {
			LogUtils.error(log, e);
			return null;
		}
	}
	/**
	 * @Description: map转JavaBean
	 *
	 * @param map map
	 * @param clazz 类对象
	 * @return JavaBean
	 */
	public static <T> T map2pojo(Map<String, Object> map, Class<T> clazz) {
		return OBJECT_MAPPER.convertValue(map, clazz);
	}
}
